package com.sunecity.lilium.view.converter;

import com.sunecity.lilium.core.model.BaseIdentity;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

public class IdConverter<T> implements Converter, Serializable {

    private static final long serialVersionUID = 6917088233427250250L;

    private Collection<T> entities;

    public IdConverter(Collection<T> entities) {
        setEntities(entities);
    }

    @SafeVarargs
    public IdConverter(Collection<T>... entities) {
        setEntities(entities);
    }

    public Collection<T> getEntities() {
        return entities;
    }

    public void setEntities(Collection<T> entities) {
        if (entities == null) {
            this.entities = new ArrayList<>();
        } else {
            this.entities = new ArrayList<>(entities);
        }
    }

    @SuppressWarnings("unchecked")
    public void setEntities(Collection<T>... entities) {
        this.entities = new ArrayList<>();
        if (entities != null && entities.length > 0) {
            Arrays.stream(entities).filter(Objects::nonNull).forEachOrdered((Collection<T> entity) -> this.entities.addAll(entity));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }
        BaseIdentity<?> t = (BaseIdentity<?>) value;
        if (t.getId() != null) {
            return String.valueOf(t.getId());
        } else {
            return t.toString();
        }
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            return null;
        }
        return entities.stream().filter(e -> {
            BaseIdentity<?> t = (BaseIdentity<?>) e;
            if (t.getId() != null) {
                return String.valueOf(t.getId()).equals(String.valueOf(value));
            } else {
                return t.toString().equals(String.valueOf(value));
            }
        }).findFirst().orElse(null);
    }

}
