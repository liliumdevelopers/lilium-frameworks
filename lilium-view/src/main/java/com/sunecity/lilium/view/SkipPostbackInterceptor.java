package com.sunecity.lilium.view;

import javax.annotation.Priority;
import javax.faces.context.FacesContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;

@Interceptor
@SkipPostback
@Priority(Interceptor.Priority.LIBRARY_BEFORE + 550)
public class SkipPostbackInterceptor implements Serializable {

    @AroundInvoke
    public Object aroundInvoke(InvocationContext ctx) throws Exception {
        Object o = null;
        if (!FacesContext.getCurrentInstance().isPostback()) {
            o = ctx.proceed();
        }
        return o;
    }

}