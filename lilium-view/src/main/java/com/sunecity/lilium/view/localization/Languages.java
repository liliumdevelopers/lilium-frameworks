package com.sunecity.lilium.view.localization;

import com.sunecity.lilium.core.localization.Locales;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Locale;

@Named
@SessionScoped
public class Languages implements Serializable {

    private static final long serialVersionUID = -2872091449269889458L;

    private Locale locale;

    public Locale getLocale() {
        locale = Locales.getLocale();
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public void persian() {
        save(Locales.PERSIAN);
    }

    public void arabic() {
        save(Locales.ARABIC);
    }

    public void kurdish() {
        save(Locales.KURDISH);
    }

    public void turkish() {
        save(Locales.TURKISH);
    }

    public void english() {
        save(Locales.ENGLISH);
    }

    public void french() {
        save(Locales.FRENCH);
    }

    public void italian() {
        save(Locales.ITALIAN);
    }

    public void german() {
        save(Locales.GERMAN);
    }

    private void save(Locale locale) {
        setLocale(locale);
        Locales.setLocale(locale);
    }

}
