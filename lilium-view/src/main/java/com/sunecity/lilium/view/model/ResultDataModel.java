package com.sunecity.lilium.view.model;

import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.data.criteria.ResultList;
import com.sunecity.lilium.data.criteria.SearchMetaData;

import java.util.List;

/**
 * <b>Title:</b> DataModel
 * Lazy Data Model
 * for fetching data from database lazily and show in data table.
 *
 * @author Shahram Goodarzi
 * @version 2.0.0
 */
public class ResultDataModel<T extends BaseIdentity<?>> extends AbstractDataModel<T> {

    private static final long serialVersionUID = 4605289840881617954L;

    private ResultList<T> resultList;
    private SearchMetaData<T> metaData;

    public ResultDataModel() {
    }

    public ResultDataModel(ResultList<T> resultList) {
        this.resultList = resultList;
    }

    @Override
    public List<T> loadEntities(int first, int max) throws Exception {
//        if (criteriaSelect != null) {
//            if (sorts != null) {
//                criteria.clearOrderBy();
//                criteria.orderBy(sorts.entrySet().stream().map(s -> new OrderSpecifier<>(s.getValue() ? Order.ASC : Order.DESC, (Path<? extends Comparable<?>>) s.getKey())).toArray(OrderSpecifier<?>[]::new));
//            }
//            criteria.setFirst(first);
//            criteria.setMax(max);
//            return criteriaSelect.select(criteria.getMetaData());
//        } else {
//            return new ArrayList<>();
//        }
        return resultList.search(metaData,first,max);
    }

    @Override
    public int loadRowCount() throws Exception {
//        if (criteriaSelect != null) {
//            return criteriaSelect.selectCount(criteria.getMetaData());
//        } else {
//            return 0;
//        }
        return (int) resultList.searchCount(metaData);
    }

    @Override
    public void search() throws Exception {
//        if (hasSearched) {
//            criteria.clearWhere();
//            if (predicate != null) {
//                criteria.where(predicate);
//            }
//        } else {
//            predicate = criteria.getMetadata().getWhere();
//        }
//        criteria.where(searchFields.stream().map(Field::toPredicate).toArray(Predicate[]::new));
//        hasSearched = true;
    }

    @Override
    public void reset() {
        super.reset();
//        if (hasSearched) {
//            criteria.clearWhere();
//            if (predicate != null) {
//                criteria.where(predicate);
//            }
//        } else {
//            predicate = criteria.getMetadata().getWhere();
//            criteria.clearWhere();
//            criteria.where(predicate);
//        }
    }

//    public void btnResetClick() {
//        searchField = null;
//        setSelectedField(null);
//        searchFields.clear();
//        if (hasSearched) {
//            criteria.clearWhere();
//            if (predicate != null) {
//                criteria.where(predicate);
//            }
//        }
//    }

}
