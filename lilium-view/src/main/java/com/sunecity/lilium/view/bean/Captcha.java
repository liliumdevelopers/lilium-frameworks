package com.sunecity.lilium.view.bean;

import com.google.code.kaptcha.Constants;
import com.sunecity.lilium.view.JsfServlet;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * Created by shahram on 5/8/15.
 */
@Named
@ViewScoped
public class Captcha implements Serializable {

    private String captcha;

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public boolean isCorrect() {
        String expectedCaptcha = (String) ((HttpSession) JsfServlet.getExternalContext().getSession(true)).getAttribute(Constants.KAPTCHA_SESSION_KEY);
        return captcha == null || !captcha.equalsIgnoreCase(expectedCaptcha);
    }

}
