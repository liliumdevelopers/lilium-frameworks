package com.sunecity.lilium.view.bean;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by shahram on 2/23/15.
 */
@Named
@ViewScoped
public class UrlDownload implements Serializable {

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
