package com.sunecity.lilium.view.abstractview;

import com.sunecity.lilium.data.model.Menus;
import com.sunecity.lilium.security.Authentication;
import com.sunecity.lilium.security.PermissionCheck;
import com.sunecity.lilium.security.authentication.User;
import com.sunecity.lilium.view.JsfServlet;
import com.sunecity.lilium.view.bean.MenuCreator;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.primefaces.model.menu.MenuModel;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <b>Title:</b> AbstractTemplate
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public abstract class AbstractTemplate extends AbstractView {

    private static final long serialVersionUID = 5569196341404742540L;

    @Inject
    private PermissionCheck permissionCheck;

    @Inject
    private MenuCreator menuCreator;

    @Inject
    private Authentication authentication;

    private User user;
    private String username;
    private MenuModel menu;
    private Map<String, String> hides;

    @PostConstruct
    public void initTemplate() {
        try {
            hides = new HashMap<>();
            Principal principal = JsfServlet.getRequest().getUserPrincipal();
            if (principal instanceof KeycloakPrincipal) {
                KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
                permissionCheck.initKeycloakPrincipal(keycloakPrincipal);
                authentication.init(keycloakPrincipal.getKeycloakSecurityContext().getRealm());
                user = authentication.getUser(keycloakPrincipal.getName());
            } else {
                username = String.valueOf(principal);
            }
        } catch (Exception e) {
            logger.error("Initialization failed!", e);
        }
    }

    public String title() {
        try {
            String page = JsfServlet.getRequest().getServletPath();
            String title = page.substring(page.lastIndexOf('/') + 1, page.length()).replaceAll("/", ".");
            title = title.substring(0, title.length() - 6) + ".title";
            return getValueBundle(title);
        } catch (Exception e) {
            return "";
        }
    }

    public String getUsername() {
        return username;
    }

    public User getUser() {
        return user;
    }

    public MenuModel getMenu() {
        if (menu == null) {
            try {
//                menu = menuCreator.megaMenu(getMenus(), m -> permissionCheck.hasPagePermission(m.getName()), m -> permissionCheck.isPageDisabled(m.getName()));
//                menu = menuCreator.megaMenu(getMenus(), m -> permissionCheck.hasClientRole(m.getLabel()), m -> false);
                menu = menuCreator.megaMenu(getMenus(), m -> true, m -> false);
            } catch (Exception e) {
                logger.error("Failed to load menus", e);
            }
        }
        return menu;
    }

    public void setMenu(MenuModel menu) {
        this.menu = menu;
    }

    public boolean isRendered(String item) {
        String page = JsfServlet.getCurrentUrl();
        return hides.entrySet().stream().filter(r -> r.getKey().equals(page.substring(0, page.lastIndexOf(".xhtml") > 0 ? page.lastIndexOf(".xhtml") : page.length())) && r.getValue().equals(item)).findAny().map(r -> false).orElse(true);
    }

    protected void hide(String page, String item) {
        hides.put(page, item);
    }

    public abstract List<Menus> getMenus() throws Exception;

}