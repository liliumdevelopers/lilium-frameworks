package com.sunecity.lilium.view.model;

import com.sunecity.lilium.core.model.file.ArrayFile;
import org.primefaces.model.DefaultStreamedContent;

import java.io.ByteArrayInputStream;

/**
 * Created by shahram on 11/19/16.
 */
public class ArrayStreamedContent extends DefaultStreamedContent {

    private final long id;
    private final String name;
    private final String type;
    private final long size;
    private final byte[] value;
    private final String description;

    public ArrayStreamedContent(ArrayFile arrayFile) {
        super(new ByteArrayInputStream(arrayFile.getValue()), arrayFile.getType(), arrayFile.getName());
        this.id = arrayFile.getId();
        this.name = arrayFile.getName();
        this.type = arrayFile.getType();
        this.size = arrayFile.getSize();
        this.value = arrayFile.getValue();
        this.description = arrayFile.getDescription();
    }

    public ArrayStreamedContent(long id, String name, String type, long size, byte[] value, String description) {
        super(new ByteArrayInputStream(value), type, name);
        this.id = id;
        this.name = name;
        this.type = type;
        this.size = size;
        this.value = value;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public long getSize() {
        return size;
    }

    public byte[] getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public boolean isImage() {
        return type.startsWith("image");
    }

    public boolean isPdf() {
        return type.equalsIgnoreCase("application/pdf") || type.equalsIgnoreCase("application/x-pdf");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArrayStreamedContent that = (ArrayStreamedContent) o;

        if (id != that.id) return false;
        if (size != that.size) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        return description != null ? description.equals(that.description) : that.description == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (int) (size ^ (size >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

}
