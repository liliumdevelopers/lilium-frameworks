package com.sunecity.lilium.view.converter;

import com.sunecity.lilium.core.util.Numbers;
import org.javamoney.moneta.Money;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.BigDecimalConverter;
import javax.faces.convert.FacesConverter;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.text.DecimalFormat;

@FacesConverter(forClass = MonetaryAmount.class)
public class MoneyConverter extends BigDecimalConverter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            return null;
        } else {
            return Money.of((BigDecimal) super.getAsObject(context, component, Numbers.getTextEnglish(value.replace(",", ""))), "IRR");
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        } else {
            MonetaryAmount money = (MonetaryAmount) value;
            DecimalFormat formatter = new DecimalFormat("###,###.######");
            return Numbers.getTextLocale(formatter.format(Money.from(money).getNumberStripped()));
        }
    }

}
