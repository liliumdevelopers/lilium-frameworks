package com.sunecity.lilium.view.abstractview;

import com.sunecity.lilium.core.exception.UniqueException;
import com.sunecity.lilium.core.localization.Bundles;

public abstract class AbstractSave extends AbstractView {

    private static final long serialVersionUID = -7248661793852069029L;

    public void btnSaveClick() {
        try {
            if (saveValidate()) {
                save();
            }
        } catch (UniqueException e) {
            if (message.hasMessage(e.getConstraint())) {
                message.addError(message.getValueBundle(e.getConstraint()));
            } else {
                message.addError(Bundles.getValueDefault("message.UniqueException"));
            }
        } catch (Exception e) {
            logger.error("Create failed!", e);
        }
    }

    protected abstract boolean saveValidate() throws Exception;

    protected abstract void save() throws Exception;

    public void btnResetClick() {
        try {
            reset();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public void reset() throws Exception {

    }

}
