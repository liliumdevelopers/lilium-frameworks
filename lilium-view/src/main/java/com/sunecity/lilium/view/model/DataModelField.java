package com.sunecity.lilium.view.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.sunecity.lilium.core.time.Date;
import com.sunecity.lilium.core.time.DateTime;
import com.sunecity.lilium.core.time.Time;
import com.sunecity.lilium.data.field.BooleanField;
import com.sunecity.lilium.data.field.CharacterField;
import com.sunecity.lilium.data.field.DateField;
import com.sunecity.lilium.data.field.DateTimeField;
import com.sunecity.lilium.data.field.EnumField;
import com.sunecity.lilium.data.field.Field;
import com.sunecity.lilium.data.field.MoneyField;
import com.sunecity.lilium.data.field.NumberField;
import com.sunecity.lilium.data.field.SimpleField;
import com.sunecity.lilium.data.field.StringField;
import com.sunecity.lilium.data.field.TimeField;
import com.sunecity.lilium.view.JsfServlet;
import org.javamoney.moneta.Money;

import java.io.Serializable;
import java.util.Stack;

/**
 * <b>Title:</b> DataModelField
 * Fields for data model used in data table.
 *
 * @author Shahram Goodarzi
 * @version 2.0.0
 */
public class DataModelField implements Serializable {

    private static final long serialVersionUID = 8947202428907142514L;

    private Field field;
    private boolean search;
    private boolean column;
    private boolean export;
    private boolean info;

    public DataModelField(Field field) {
        if (field instanceof SimpleField) {
            this.column = true;
            this.export = true;
            this.info = true;
        }
        this.field = field;
        this.search = true;
    }

    public DataModelField(Field field, boolean search, boolean column, boolean export, boolean info) {
        this.field = field;
        this.search = search;
        this.column = column;
        this.export = export;
        this.info = info;
    }

    public DataModelField(Path<?> path, Class<?> clas, boolean showColumns) {
        if (path instanceof BooleanPath) {
            this.field = new BooleanField(path, getValueBundle(path, clas), getBooleanValueBundle(path, clas, true), getBooleanValueBundle(path, clas, false), getBooleanValueBundle(path, clas, null));
        } else if (path instanceof NumberPath) {
            this.field = new NumberField(path, getValueBundle(path, clas));
        } else if (path instanceof EnumPath) {
            this.field = new EnumField(path, getValueBundle(path, clas), null);
        } else if (path.getType() == Date.class) {
            this.field = new DateField(path, getValueBundle(path, clas));
        } else if (path.getType() == DateTime.class) {
            this.field = new DateTimeField(path, getValueBundle(path, clas));
        } else if (path.getType() == Time.class) {
            this.field = new TimeField(path, getValueBundle(path, clas));
        } else if (path.getType() == Character.class) {
            this.field = new CharacterField(path, getValueBundle(path, clas));
        } else if (path.getType() == Money.class) {
            this.field = new MoneyField(path, getValueBundle(path, clas));
        } else {
            this.field = new StringField(path, getValueBundle(path, clas));
        }
        if (field instanceof SimpleField) {
            this.column = showColumns;
            this.export = true;
            this.info = true;
        }
        this.search = true;

    }

    private String getBooleanValueBundle(Path<?> path, Class<?> clas, Boolean bool) {
        String parent = String.valueOf(path.getMetadata().getParent().getMetadata().getElement());
        parent = parent.substring(0, 1).toUpperCase() + parent.substring(1);
        StringBuilder key = new StringBuilder().append(clas.getSimpleName()).append(".").append(path.getMetadata().getName()).append(".").append(bool);
        try {
            return JsfServlet.getValueBundle("bundle", key.toString());
        } catch (Exception e) {
            try {
                key = new StringBuilder().append(parent).append(".").append(path.getMetadata().getName()).append(".").append(bool);
                return JsfServlet.getValueBundle("model", key.toString());
            } catch (Exception ex) {
                if (bool != null) {
                    if (bool) {
                        return "true";
                    } else {
                        return "false";
                    }
                } else {
                    return "null";
                }
            }
        }
    }

    private String getValueBundle(Path<?> path, Class<?> clas) {
        String parent = String.valueOf(path.getMetadata().getParent().getMetadata().getElement());
        parent = parent.substring(0, 1).toUpperCase() + parent.substring(1);
        StringBuilder key = new StringBuilder().append(clas.getSimpleName().substring(0, 1).toLowerCase()).append(clas.getSimpleName().substring(1));
        Path<?> p = path;
        Stack<Path<?>> paths = new Stack<>();
        while ((p = p.getMetadata().getParent()) != null) {
            paths.add(p);
        }
        while (!paths.empty()) {
            key.append(".").append(paths.pop().getMetadata().getElement());
        }
        key.append(".").append(path.getMetadata().getElement());
        try {
            return JsfServlet.getValueBundle("bundle", key.toString());
        } catch (Exception e) {
            key = new StringBuilder().append(parent).append(".").append(path.getMetadata().getName());
            return JsfServlet.getValueBundle("model", key.toString());
        }
    }

    public Field getField() {
        return field;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public boolean isColumn() {
        return column;
    }

    public void setColumn(boolean column) {
        this.column = column;
    }

    public boolean isExport() {
        return export;
    }

    public void setExport(boolean export) {
        this.export = export;
    }

    public boolean isInfo() {
        return info;
    }

    public void setInfo(boolean info) {
        this.info = info;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataModelField that = (DataModelField) o;

        if (search != that.search) return false;
        if (column != that.column) return false;
        if (export != that.export) return false;
        if (info != that.info) return false;
        return !(field != null ? !field.equals(that.field) : that.field != null);

    }

    @Override
    public int hashCode() {
        int result = field != null ? field.hashCode() : 0;
        result = 31 * result + (search ? 1 : 0);
        result = 31 * result + (column ? 1 : 0);
        result = 31 * result + (export ? 1 : 0);
        result = 31 * result + (info ? 1 : 0);
        return result;
    }

}
