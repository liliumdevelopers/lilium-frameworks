package com.sunecity.lilium.view.model;

import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.data.criteria.Criteria;
import com.sunecity.lilium.data.criteria.CriteriaMetaData;
import com.sunecity.lilium.data.criteria.CriteriaSelect;
import com.sunecity.lilium.data.field.Field;
import com.sunecity.lilium.data.report.ReportType;
import com.sunecity.lilium.view.bean.ReportDesigner;
import com.sunecity.lilium.view.bean.UrlDownload;

import javax.enterprise.inject.spi.CDI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <b>Title:</b> DataModel
 * Lazy Data Model
 * for fetching data from database lazily and show in data table.
 *
 * @author Shahram Goodarzi
 * @version 2.0.0
 */
public class DataModel<T extends BaseIdentity<?>> extends AbstractDataModel<T> {

    private static final long serialVersionUID = 4605289840881617954L;

    private CriteriaSelect criteriaSelect;
    private Criteria<T> criteria;
    private Field searchField;
    private List<Field> searchFields;

    private ReportDesigner reportDesigner;

    private Predicate predicate;
    private boolean hasSearched;

    public DataModel() {
    }

    public DataModel(CriteriaSelect criteriaSelect, CriteriaMetaData metaData) {
        super(Arrays.stream(metaData.getPaths()).map(p -> new DataModelField(p, getRef(), true)).collect(Collectors.toList()));
        criteria = new Criteria<>(metaData);
        reportDesigner = new ReportDesigner(getExports());
        searchFields = new ArrayList<>();
        this.criteriaSelect = criteriaSelect;
    }

    public DataModel(CriteriaSelect criteriaSelect, CriteriaMetaData metaData, boolean showColumns) {
        super(Arrays.stream(metaData.getPaths()).map(p -> new DataModelField(p, getRef(), showColumns)).collect(Collectors.toList()));
        criteria = new Criteria<>(metaData);
        reportDesigner = new ReportDesigner(getExports());
        searchFields = new ArrayList<>();
        this.criteriaSelect = criteriaSelect;
    }

    private static Class<?> getRef() {
        StackWalker sw = StackWalker.getInstance(Set.of(StackWalker.Option.RETAIN_CLASS_REFERENCE));
        Optional<Class<?>> caller = sw.walk(frames -> frames.skip(10).findFirst().map(StackWalker.StackFrame::getDeclaringClass));
        return caller.orElse(sw.getCallerClass());
    }

    public Field getSearchField() {
        if (searchField == null) {
            if (getSelectedField() != null) {
                searchField = getSelectedField();
            }
        }
        return searchField;
    }

    public void setSearchField(Field searchField) {
        this.searchField = searchField;
    }

    public List<Field> getSearchFields() {
        return searchFields;
    }

    public void setSearchFields(List<Field> searchFields) {
        this.searchFields = searchFields;
    }

    public ReportDesigner getReportDesigner() {
        return reportDesigner;
    }

    @Override
    public List<T> loadEntities(int first, int max) throws Exception {
        if (criteriaSelect != null) {
            if (sorts != null) {
                criteria.clearOrderBy();
                criteria.orderBy(sorts.entrySet().stream().map(s -> new OrderSpecifier<>(s.getValue() ? Order.ASC : Order.DESC, (Path<? extends Comparable<?>>) s.getKey())).toArray(OrderSpecifier<?>[]::new));
            }
            criteria.setFirst(first);
            criteria.setMax(max);
            return criteriaSelect.select(criteria.getMetaData());
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public int loadRowCount() throws Exception {
        if (criteriaSelect != null) {
            return criteriaSelect.selectCount(criteria.getMetaData());
        } else {
            return 0;
        }
    }

    @Override
    public void search() throws Exception {
        if (hasSearched) {
            criteria.clearWhere();
            if (predicate != null) {
                criteria.where(predicate);
            }
        } else {
            predicate = criteria.getMetadata().getWhere();
        }
        criteria.where(searchFields.stream().map(Field::toPredicate).toArray(Predicate[]::new));
        hasSearched = true;
    }

    public void btnAddClick() {
        searchFields.add(searchField.clone());
        searchField.clear();
        searchField = null;
    }

    public void btnDeleteClick(Field searchField) {
        searchFields.remove(searchField);
    }

    public void clear() {
        searchField = null;
    }

    @Override
    public void reset() {
        super.reset();
//        if (hasSearched) {
//            criteria.clearWhere();
//            if (predicate != null) {
//                criteria.where(predicate);
//            }
//        } else {
//            predicate = criteria.getMetadata().getWhere();
//            criteria.clearWhere();
//            criteria.where(predicate);
//        }
    }

    public void btnResetClick() {
        searchField = null;
        setSelectedField(null);
        searchFields.clear();
        if (hasSearched) {
            criteria.clearWhere();
            if (predicate != null) {
                criteria.where(predicate);
            }
        }
    }

    public void btnExportClick(ReportType type) {
        try {
            UrlDownload urlDownload = CDI.current().select(UrlDownload.class).get();
            urlDownload.setPath(criteriaSelect.createReport(reportDesigner.generate(), type, criteria.getMetaData()));
        } catch (Exception e) {
            logger.error("", e);
        }
    }

}
