package com.sunecity.lilium.view.util;

import com.sunecity.lilium.core.model.Treeable;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import javax.enterprise.context.Dependent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

@Dependent
public class Trees implements Serializable {

    private boolean found;

    public <T extends Treeable<? extends Serializable>> TreeNode convert(List<T> values) {
        TreeNode root = new DefaultTreeNode("root");
        List<TreeNode> treeNodes = new ArrayList<>();
        values.forEach(b -> treeNodes.add(new DefaultTreeNode(b)));
        convertToTree(root, treeNodes);
        return root;
    }

    public <T extends Treeable<? extends Serializable>> TreeNode convert(List<T> values, Consumer<TreeNode> consumer) {
        TreeNode root = new DefaultTreeNode("root");
        List<TreeNode> treeNodes = new ArrayList<>();
        values.forEach(b -> treeNodes.add(new DefaultTreeNode(b)));
        convertToTree(root, treeNodes);
        treeNodes.forEach(consumer);
        return root;
    }

    public <T extends Treeable<? extends Serializable>> List<T> convert(TreeNode root) {
        List<T> values = new ArrayList<>();
        convertFromTree(root, values);
        return values;
    }

    private <T extends Treeable<? extends Serializable>> void convertFromTree(TreeNode node, List<T> values) {
        for (TreeNode treeNode : node.getChildren()) {
            values.add((T) treeNode.getData());
            convertFromTree(treeNode, values);
        }
    }

    private <T extends Treeable<? extends Serializable>> void convertToTree(TreeNode root, List<TreeNode> treeNodes) {
        for (TreeNode node : treeNodes) {
            Treeable<? extends Serializable> treeable = (Treeable<? extends Serializable>) node.getData();
            if (treeable.getParentId() != null) {
                treeNodes.stream().filter(t -> ((T) t.getData()).getId().equals(treeable.getParentId())).findAny().orElseThrow(RuntimeException::new).getChildren().add(node);
            } else {
                root.getChildren().add(node);
            }
        }
    }

    public <T> TreeNode search(TreeNode root, Predicate<T> predicate) {
        TreeNode treeNode = clone(root);
        searchTree(treeNode, predicate);
        if (found) {
            found = false;
            return treeNode;
        } else {
            return null;
        }
    }

    public TreeNode searchNodes(TreeNode root, Predicate<TreeNode> predicate) {
        TreeNode treeNode = clone(root);
        searchNodesTree(treeNode, predicate);
        if (found) {
            found = false;
            return treeNode;
        } else {
            return null;
        }
    }

    private <T> void searchTree(TreeNode root, Predicate<T> predicate) {
        for (TreeNode treeNode : root.getChildren()) {
            treeNode.setExpanded(false);
            if (predicate.test((T) treeNode.getData())) {
                found = true;
                TreeNode parent = treeNode;
                while ((parent = parent.getParent()) != null) {
                    parent.setExpanded(true);
                }
                treeNode.setType(treeNode.getType().concat("_SEARCH"));
            }
            searchTree(treeNode, predicate);
        }
    }

    private void searchNodesTree(TreeNode root, Predicate<TreeNode> predicate) {
        for (TreeNode treeNode : root.getChildren()) {
            treeNode.setExpanded(false);
            if (predicate.test(treeNode)) {
                found = true;
                TreeNode parent = treeNode;
                while ((parent = parent.getParent()) != null) {
                    parent.setExpanded(true);
                }
                treeNode.setType(treeNode.getType().concat("_SEARCH"));
            }
            searchNodesTree(treeNode, predicate);
        }
    }

    public void remove(TreeNode treeNode, boolean recursive) {
        TreeNode parent = treeNode.getParent();
        if (parent != null) {
            parent.getChildren().remove(treeNode);
            if (recursive && parent.getChildren().isEmpty()) {
                remove(parent, true);
            }
        }
    }

    public TreeNode clone(TreeNode source) {
        TreeNode target = new DefaultTreeNode(source.getData());
        copy(source, target);
        return target;
    }

    public void copy(TreeNode source, TreeNode target) {
        copy(source, target, false);
    }

    public void copy(TreeNode source, TreeNode target, boolean withParent) {
        if (withParent) {
            target = new DefaultTreeNode(source.getData(), target);
        }
        for (TreeNode node : source.getChildren()) {
            TreeNode treeNode = new DefaultTreeNode(node.getData(), target);
            copy(node, treeNode);
        }
    }

    public <S, H> void copy(TreeNode source, TreeNode target, Function<S, H> function) {
        copy(source, target, function, false);
    }

    public <S, H> void copy(TreeNode source, TreeNode target, Function<S, H> function, boolean withParent) {
        if (withParent) {
            target = new DefaultTreeNode(function.apply((S) source.getData()), target);
        }
        for (TreeNode node : source.getChildren()) {
            TreeNode treeNode = new DefaultTreeNode(function.apply((S) node.getData()), target);
            copy(node, treeNode, function);
        }
    }

    public <S, H> void copyNodes(TreeNode source, TreeNode target, BiFunction<S, TreeNode, H> function) {
        copyNodes(source, target, function, false);
    }

    public <S, H> void copyNodes(TreeNode source, TreeNode target, BiFunction<S, TreeNode, H> function, boolean withParent) {
        if (withParent) {
            target = new DefaultTreeNode(function.apply((S) source.getData(), target), target);
        }
        for (TreeNode node : source.getChildren()) {
            TreeNode treeNode = new DefaultTreeNode(function.apply((S) node.getData(), target), target);
            copyNodes(node, treeNode, function);
        }
    }

    public void move(TreeNode source, TreeNode target) {
        source.getParent().getChildren().remove(source);
        target.getChildren().add(source);
    }

}