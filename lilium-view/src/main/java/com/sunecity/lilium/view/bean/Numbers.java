package com.sunecity.lilium.view.bean;

import com.sunecity.lilium.core.localization.Locales;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Shahram Goodarzi
 */
@Named
@SessionScoped
public class Numbers implements Serializable {

    private static final long serialVersionUID = -2465575722417928183L;

    public String round(double number, int decimalLen) {
        return new BigDecimal(number).setScale(decimalLen, RoundingMode.CEILING).toString();
    }

    public String getText(String text) {
        return Locales.getLocale().getLanguage().equalsIgnoreCase("fa") ? com.sunecity.lilium.core.util.Numbers.getTextPersian(text) : com.sunecity.lilium.core.util.Numbers.getTextEnglish(text);
    }

    public String getSpilt(String str) {
        return getText(com.sunecity.lilium.core.util.Numbers.split(str));
    }

}
