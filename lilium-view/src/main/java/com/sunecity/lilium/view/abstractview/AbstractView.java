package com.sunecity.lilium.view.abstractview;

import com.sunecity.lilium.core.config.RestConfig;
import com.sunecity.lilium.core.localization.Message;
import com.sunecity.lilium.core.localization.MessageContext;
import com.sunecity.lilium.rest.client.ClientExceptionFilter;
import com.sunecity.lilium.rest.client.ServerExceptionFilter;
import com.sunecity.lilium.rest.ext.JsonContextResolver;
import com.sunecity.lilium.rest.ext.JsonParamConverterProvider;
import com.sunecity.lilium.view.JsfServlet;
import com.sunecity.lilium.view.bean.JsfMessage;
import org.aeonbits.owner.ConfigFactory;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.admin.client.resource.BearerAuthFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.Serializable;

public abstract class AbstractView implements Serializable {

    private static final long serialVersionUID = 9133972747540539863L;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Inject
    protected JsfMessage message;

    @Inject
    private MessageContext messageContext;

    private Message bundle;
    private ResteasyWebTarget target;

    @PostConstruct
    public void initView() {
        bundle = messageContext.messageSource(getClass().getPackage().getName().substring(0, getClass().getPackage().getName().lastIndexOf("view")) + "view.bundle").message();
        message.setClass(getClass());
        ResteasyClient client = new ResteasyClientBuilder().connectionPoolSize(10).
                register(JsonParamConverterProvider.class).register(JsonContextResolver.class)
                .register(ServerExceptionFilter.class).register(ClientExceptionFilter.class).build();
        target = client.target(ConfigFactory.create(RestConfig.class).url());
        Object o = JsfServlet.getRequest().getAttribute(KeycloakSecurityContext.class.getName());
        if (o != null) {
            target = target.register(new BearerAuthFilter(((KeycloakSecurityContext) o).getTokenString()));
        }
    }

    protected String getValueBundle(String key) {
        return bundle.template("{" + key + "}").toString();
    }

    protected <T> T proxyRest(Class<T> clas) {
        return target.proxy(clas);
    }

}
