package com.sunecity.lilium.view.converter;

import com.sunecity.lilium.data.report.AbstractColumn;
import com.sunecity.lilium.data.report.GroupColumn;
import com.sunecity.lilium.data.report.ReportColumn;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

public class ReportColumnConverter implements Converter {

    private List<AbstractColumn> columns;

    public ReportColumnConverter(List<AbstractColumn> columns) {
        this.columns = columns;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            return null;
        } else {
            if (columns != null) {
                for (AbstractColumn column : columns) {
                    if (column instanceof ReportColumn) {
                        if (((ReportColumn) column).getField().getDisplayName().equals(value)) {
                            return column;
                        }
                    } else if (column instanceof GroupColumn) {
                        if (((GroupColumn) column).getTitle().equals(value)) {
                            return column;
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        } else {
            AbstractColumn column = (AbstractColumn) value;
            if (column instanceof ReportColumn) {
                return ((ReportColumn) column).getField().getDisplayName();
            } else if (column instanceof GroupColumn) {
                return ((GroupColumn) column).getTitle();
            }
            return "";
        }
    }

}
