package com.sunecity.lilium.view.converter;

import com.sunecity.lilium.core.util.Numbers;
import com.sunecity.lilium.core.validation.Validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("stringConverter")
public class StringConverter implements Converter {

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }
        return Numbers.getTextLocale(String.valueOf(value));
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            return null;
        }
        String s = Numbers.getTextEnglish(value.replace('ي', 'ی').replace('ك', 'ک')).trim();
        return Validator.isEmail(s) ? s.toLowerCase() : s;
    }

}
