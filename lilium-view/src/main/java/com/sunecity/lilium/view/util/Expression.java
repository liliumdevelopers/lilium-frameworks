package com.sunecity.lilium.view.util;

import org.apache.commons.beanutils.MethodUtils;

import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.context.FacesContext;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Expression {

    public static MethodExpression getActionExpression(Class<?> clas, String methodName, Object... params) {
        String expression = "#{" + clas.getSimpleName().substring(0, 1).toLowerCase() + clas.getSimpleName().substring(1) + "." + methodName + "(";
        if (params.length != 0) {
            expression += String.join(", ", Arrays.stream(params).map(String::valueOf).toArray(String[]::new));
        }
        expression += ")}";
        Class<?>[] parameterTypes = new Class<?>[params.length];
        for (int i = 0; i < params.length; i++) {
            parameterTypes[i] = params[i].getClass();
        }
        Method method = MethodUtils.getMatchingAccessibleMethod(clas, methodName, parameterTypes);
        return ExpressionFactory.newInstance().createMethodExpression(FacesContext.getCurrentInstance().getELContext(), expression, method.getReturnType(), method.getParameterTypes());
    }

}
