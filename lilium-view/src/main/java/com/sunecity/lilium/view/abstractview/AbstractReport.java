package com.sunecity.lilium.view.abstractview;

import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.view.model.AbstractDataModel;

public abstract class AbstractReport<T extends BaseIdentity<?>> extends AbstractBase<T> {

    private static final long serialVersionUID = -3381310584139913019L;

    public void btnSearchClick() {
        try {
            if (searchValidate()) {
                search();
            }
            if (getDataModel() != null) {
                getDataModel().reset();
            }
        } catch (Exception e) {
            logger.error("Create failed!", e);
        }
    }

    protected abstract boolean searchValidate() throws Exception;

    protected abstract void search() throws Exception;

    @Override
    protected abstract AbstractDataModel<T> dataModel() throws Exception;

    public void btnResetClick() {
        try {
            reset();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public void reset() throws Exception {

    }

}
