package com.sunecity.lilium.view.facelet;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.FaceletContext;
import java.io.IOException;

/**
 * <b>Title:</b> Include
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
@FacesComponent(Include.COMPONENT_TYPE)
public class Include extends UIComponentBase {

    public static final String COMPONENT_TYPE = "com.sunecity.lilium.view.facelet.Include";
    public static final String COMPONENT_FAMILY = "com.sunecity.lilium";

    @Override
    public String getFamily() {
        return COMPONENT_FAMILY;
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeChildren(FacesContext context) throws IOException {
        getChildren().clear();
        ((FaceletContext) context.getAttributes().get(FaceletContext.FACELET_CONTEXT_KEY)).includeFacelet(this, getSrc());
        super.encodeChildren(context);
    }

    public String getSrc() {
        return (String) getStateHelper().eval("src");
    }

    public void setSrc(String src) {
        getStateHelper().put("src", src);
    }

}