package com.sunecity.lilium.view.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.sunecity.lilium.data.field.Field;

public class FieldConverter implements Converter {

	private List<Field> fields;

	public FieldConverter(List<Field> fields) {
		this.fields = fields;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.trim().length() == 0) {
			return null;
		} else {
			if (fields != null) {
				for (Field field : fields) {
					if (field.getDisplayName().equals(value)) {
						return field;
					}
				}
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value == null) {
			return "";
		} else {
			Field field = (Field) value;
			return field.getDisplayName();
		}
	}

}
