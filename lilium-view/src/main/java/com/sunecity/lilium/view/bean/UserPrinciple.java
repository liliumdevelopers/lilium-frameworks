package com.sunecity.lilium.view.bean;

import com.sunecity.lilium.security.Authentication;
import com.sunecity.lilium.security.PermissionCheck;
import com.sunecity.lilium.security.authentication.User;
import com.sunecity.lilium.view.JsfServlet;
import com.sunecity.lilium.view.abstractview.AbstractView;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

/**
 * Created by shahram on 5/1/17.
 */
@Named
@RequestScoped
//@Path("userPrinciple")
public class UserPrinciple extends AbstractView {

    @Inject
    private PermissionCheck permissionCheck;

    @Inject
    private Authentication authentication;

//    @Context
//    private SecurityContext securityContext;

    private User user;

    @PostConstruct
    public void init() {
        try {
            Principal principal = JsfServlet.getRequest().getUserPrincipal();
            if (principal instanceof KeycloakPrincipal) {
                KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
                permissionCheck.initKeycloakPrincipal(keycloakPrincipal);
                authentication.init(keycloakPrincipal.getKeycloakSecurityContext().getRealm());
                user = authentication.getUser(keycloakPrincipal.getName());
            }
        } catch (Exception e) {
            logger.error("Initialization failed!", e);
        }
    }

//    @GET
    public User getUser() {
        return user;
    }

    public String shg(HttpServletRequest httpServletRequest){
        System.out.println(httpServletRequest.getUserPrincipal());
        return "hello";
    }

}
