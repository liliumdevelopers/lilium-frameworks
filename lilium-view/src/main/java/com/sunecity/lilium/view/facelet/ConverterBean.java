package com.sunecity.lilium.view.facelet;

import com.sunecity.lilium.view.converter.IdConverter;
import org.primefaces.model.DualListModel;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.EnumConverter;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;

/**
 * <b>Title:</b> ConverterBean
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
@Named
@RequestScoped
public class ConverterBean implements Serializable {

    private static final long serialVersionUID = -7579580573581209440L;

    public <T> Converter converter(Collection<T> entities) {
        if (entities == null || entities.isEmpty()) {
            return new Converter() {
                @Override
                public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
                    return null;
                }

                @Override
                public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
                    return null;
                }
            };
        }
        T t = entities.iterator().next();
        if (t instanceof Enum<?>) {
            return new EnumConverter(t.getClass());
        }
        return new IdConverter<>(entities);
    }

    public <T> Converter converterDualList(DualListModel<T> entities) {
        if (entities != null) {
            if (entities.getSource().size() > 0) {
                T t = entities.getSource().get(0);
                if (t instanceof Enum<?>) {
                    return new EnumConverter(t.getClass());
                }
            }
            return new IdConverter<>(entities.getSource(), entities.getTarget());
        }
        return new Converter() {
            @Override
            public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
                return null;
            }

            @Override
            public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
                return null;
            }
        };
    }

}
