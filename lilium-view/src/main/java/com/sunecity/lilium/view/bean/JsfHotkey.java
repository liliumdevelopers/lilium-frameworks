package com.sunecity.lilium.view.bean;

import javax.faces.event.ActionListener;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shahram on 2/20/15.
 */
@Named
@ViewScoped
public class JsfHotkey implements Serializable {

    private List<Hotkey> hotkeys;

    public void addHotkey(String bind, ActionListener actionListener) {
        Hotkey hotkey = new Hotkey();
        hotkey.setBind(bind);
        hotkey.setActionListener(actionListener);
        getHotkeys().add(hotkey);
    }

    public void addHotkey(String bind, ActionListener actionListener, String update) {
        Hotkey hotkey = new Hotkey();
        hotkey.setBind(bind);
        hotkey.setActionListener(actionListener);
        hotkey.setHandler(update);
        getHotkeys().add(hotkey);
    }

    public List<Hotkey> getHotkeys() {
        if (hotkeys == null) {
            hotkeys = new ArrayList<>();
        }
        return hotkeys;
    }

    public void setHotkeys(List<Hotkey> hotkeys) {
        this.hotkeys = hotkeys;
    }

}
