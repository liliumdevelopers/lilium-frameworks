package com.sunecity.lilium.view.abstractview;

import com.sunecity.lilium.core.exception.ConcurrentException;
import com.sunecity.lilium.core.exception.ForeignKeyException;
import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.core.model.Treeable;
import com.sunecity.lilium.view.model.AbstractDataModel;
import com.sunecity.lilium.view.util.Trees;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractTreeCrud<T extends BaseIdentity<?>> extends AbstractCrud<T> {

    private static final long serialVersionUID = -4766038416997833057L;

    @Inject
    protected Trees trees;

    private TreeNode root;

    private TreeNode[] selectedTreeNodes;
    private TreeNode selectedTreeNode;
    private TreeNode lastSelectedTreeNode;

    private String searchValue;

    public AbstractTreeCrud() {
    }

    @Override
    public AbstractDataModel<?> dataModel() throws Exception {
        return null;
    }

    protected abstract TreeNode root();

    public TreeNode getRoot() {
        if (root == null) {
            root = root();
        }
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode[] getSelectedTreeNodes() {
        return selectedTreeNodes;
    }

    public void setSelectedTreeNodes(TreeNode[] selectedTreeNodes) {
        this.selectedTreeNodes = selectedTreeNodes;
    }

    public TreeNode getSelectedTreeNode() {
        return selectedTreeNode;
    }

    public void setSelectedTreeNode(TreeNode selectedTreeNode) {
        this.selectedTreeNode = selectedTreeNode;
    }

    public TreeNode getLastSelectedTreeNode() {
        return lastSelectedTreeNode;
    }

    public void setLastSelectedTreeNode(TreeNode lastSelectedTreeNode) {
        this.lastSelectedTreeNode = lastSelectedTreeNode;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public T getSelectedEntity() {
        if (selectedTreeNode != null) {
            return (T) selectedTreeNode.getData();
        }
        return super.getSelectedEntity();
    }

    @Override
    public void setSelectedEntity(T selectedEntity) {
        if (selectedTreeNode != null) {
            ((DefaultTreeNode) selectedTreeNode).setData(selectedEntity);
        }
        super.setSelectedEntity(selectedEntity);
    }

    public List<T> getSelectedEntities() {
        if (selectedTreeNodes != null) {
            return Arrays.stream(selectedTreeNodes).map(t -> (T) t.getData()).collect(Collectors.toList());
        }
        return super.getSelectedEntities();
    }

    @Override
    public void btnDeleteClick() {
        try {
            if (deleteValidate()) {
                TreeNode treeNode = getSelectedTreeNode();
                if (getDataModel() != null) {
                    getDataModel().reset();
                }
                if (treeNode != null) {
                    List<Serializable> ids = new ArrayList<>();
                    ids.add(((Treeable<T>) treeNode.getData()).getId());
                    trees.convert(treeNode).forEach(t -> ids.add(t.getId()));
                    delete(ids.toArray(new Serializable[ids.size()]));
                    treeNode.getParent().getChildren().remove(treeNode);
                }
            }
        } catch (ForeignKeyException e) {
            if (message.hasMessage(e.getConstraint())) {
                message.addError(message.getValueBundle(e.getConstraint()));
            } else {
                message.addError(Bundles.getValueDefault("message.ForeignKeyException"));
            }
        } catch (ConcurrentException e) {
            if (message.hasMessage("concurrentError")) {
                message.addError("concurrentError");
            } else {
                message.addError(Bundles.getValueDefault("message.ConcurrentException"));
            }
        } catch (Exception e) {
            logger.error("Delete failed!", e);
        }
    }

    public void btnSearchClick() {
        try {
            if (searchValue == null) {
                message.addWarn(Bundles.getValueDefault("message.empty"));
            } else {
                TreeNode root = trees.search(this.root, t -> {
                    try {
                        return search(searchValue).test((T) t);
                    } catch (Exception e) {
                        logger.error("Search failed!", e);
                        return false;
                    }
                });
                if (root != null) {
                    this.root = root;
                } else {
                    message.addInfo(Bundles.getValueDefault("message.notFound"));
                }
            }
        } catch (Exception e) {
            logger.error("Search failed!", e);
        }
    }

    public void btnInfoClick() {
        try {
            info();
        } catch (Exception e) {
            logger.error("Info failed!", e);
        }
    }

    public void info() throws Exception {

    }

    public void reset() throws Exception {
        root = null;
        searchValue = null;
    }

    public boolean hasFound(T t) {
        try {
            return searchValue != null && search(searchValue).test(t);
        } catch (Exception e) {
            logger.error("Finding failed!", e);
            return false;
        }
    }

    protected abstract Predicate<T> search(String value) throws Exception;

}