package com.sunecity.lilium.view.facelet;

import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.view.facelets.FaceletContext;
import javax.faces.view.facelets.TagAttribute;
import javax.faces.view.facelets.TagConfig;
import javax.faces.view.facelets.TagHandler;
import java.io.IOException;

/**
 * <b>Title:</b> ExpressionHandler
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public class ExpressionHandler extends TagHandler {

    private final TagAttribute name;
    private final TagAttribute value;

    public ExpressionHandler(TagConfig config) {
        super(config);
        this.name = this.getRequiredAttribute("name");
        this.value = this.getRequiredAttribute("value");
    }

    @Override
    public void apply(FaceletContext ctx, UIComponent parent) throws IOException {
        String nameStr = name.getValue(ctx);
        ValueExpression valueExpression = value.getValueExpression(ctx, Object.class);
        MethodExpression methodExpression = new LiliumMethodExpression(valueExpression);
        ctx.getVariableMapper().setVariable(nameStr, ctx.getExpressionFactory().createValueExpression(methodExpression, MethodExpression.class));
    }

}
