package com.sunecity.lilium.view.util;

public class ExceptionHandlerFactory extends javax.faces.context.ExceptionHandlerFactory {

	private final javax.faces.context.ExceptionHandlerFactory base;

	public ExceptionHandlerFactory(javax.faces.context.ExceptionHandlerFactory base) {
		this.base = base;
	}

	@Override
	public ExceptionHandler getExceptionHandler() {
		return new ExceptionHandler(base.getExceptionHandler());
	}

}
