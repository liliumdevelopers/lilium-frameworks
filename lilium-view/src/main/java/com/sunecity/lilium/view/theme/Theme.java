package com.sunecity.lilium.view.theme;

import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.core.util.EncrypterDecrypter;
import com.sunecity.lilium.view.Cookies;
import com.sunecity.lilium.view.JsfServlet;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
@Named
@SessionScoped
public class Theme implements Serializable {

    private static final long serialVersionUID = -7401440277030638179L;

    private LiliumTheme theme;

    public LiliumTheme getTheme() {
        if (theme == null) {
            try {
                String th = EncrypterDecrypter.decrypt(Cookies.get(EncrypterDecrypter.encrypt("theme." + JsfServlet.getRequest().getContextPath().substring(1), "sunecity")), "sunecity");
                if (th != null) {
                    getThemes().stream().filter(liliumTheme -> liliumTheme.getValue().equals(th)).forEach(liliumTheme -> theme = liliumTheme);
                }
                if (th == null || th.trim().length() == 0) {
                    theme = new LiliumTheme(Bundles.getValueDefault("aristo"), "aristo", "aristo.png");
                }
            } catch (Exception e) {
                theme = new LiliumTheme(Bundles.getValueDefault("aristo"), "aristo", "aristo.png");
            }
        }
        return theme;
    }

    public void setTheme(LiliumTheme theme) {
        this.theme = theme;
    }

    public List<LiliumTheme> getThemes() {
        List<LiliumTheme> themes = new ArrayList<>();
        themes.add(new LiliumTheme(Bundles.getValueDefault("flatui"), "flatui", "lilium.png"));
        themes.add(new LiliumTheme(Bundles.getValueDefault("spark"), "spark", "spark.png"));
        themes.add(new LiliumTheme(Bundles.getValueDefault("aristo"), "aristo", "aristo.png"));
        themes.add(new LiliumTheme(Bundles.getValueDefault("blitzer"), "blitzer", "blitzer.png"));
        themes.add(new LiliumTheme(Bundles.getValueDefault("bootstrap"), "bootstrap", "bootstrap.png"));
        themes.add(new LiliumTheme(Bundles.getValueDefault("home"), "home", "home.png"));
        themes.add(new LiliumTheme(Bundles.getValueDefault("hotsneaks"), "hot-sneaks", "hot-sneaks.png"));
        themes.add(new LiliumTheme(Bundles.getValueDefault("peppergrinder"), "pepper-grinder", "pepper-grinder.png"));


//		themes.add(new LiliumTheme(Bundles.getValueDefault("afterdark"), "afterdark", "afterdark.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("afternoon"), "afternoon", "afternoon.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("afterwork"), "afterwork", "afterwork.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("blacktie"), "black-tie", "black-tie.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("bluesky"), "bluesky", "bluesky.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("casablanca"), "casablanca", "casablanca.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("cruze"), "cruze", "cruze.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("cupertino"), "cupertino", "cupertino.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("darkhive"), "dark-hive", "dark-hive.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("dotluv"), "dot-luv", "dot-luv.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("eggplant"), "eggplant", "eggplant.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("excitebike"), "excite-bike", "excite-bike.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("flick"), "flick", "flick.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("glassx"), "glass-x", "glass-x.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("humanity"), "humanity", "humanity.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("lefrog"), "le-frog", "le-frog.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("midnight"), "midnight", "midnight.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("mintchoc"), "mint-choc", "mint-choc.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("overcast"), "overcast", "overcast.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("redmond"), "redmond", "redmond.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("rocket"), "rocket", "rocket.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("sam"), "sam", "sam.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("smoothness"), "smoothness", "smoothness.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("southstreet"), "south-street", "south-street.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("start"), "start", "start.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("sunny"), "sunny", "sunny.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("swankypurse"), "swanky-purse", "swanky-purse.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("trontastic"), "trontastic", "trontastic.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("uidarkness"), "ui-darkness", "ui-darkness.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("uilightness"), "ui-lightness", "ui-lightness.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("vader"), "vader", "vader.png"));
        return themes;
    }

    public void saveTheme() {
        try {
            Cookies.saveWithPath(EncrypterDecrypter.encrypt("theme." + JsfServlet.getRequest().getContextPath().substring(1), "sunecity"),
                    EncrypterDecrypter.encrypt(theme.getValue(), "sunecity"), Cookies.MAXIMUM, "/");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
