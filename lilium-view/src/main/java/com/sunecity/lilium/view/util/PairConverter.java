package com.sunecity.lilium.view.util;

import com.sunecity.lilium.core.util.Pair;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;

public class PairConverter implements Converter {

    @SuppressWarnings("rawtypes")
    private List<Pair> pairs;

    public PairConverter() {
    }

    @SuppressWarnings("rawtypes")
    public PairConverter(List<Pair> pairs) {
        this.pairs = pairs;
    }

    @SuppressWarnings("rawtypes")
    public List<Pair> getPairs() {
        return pairs;
    }

    @SuppressWarnings("rawtypes")
    public void setPairs(List<Pair> pairs) {
        this.pairs = pairs;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
                              String value) {
        if (value == null || value.trim().length() == 0) {
            return null;
        } else {
            for (@SuppressWarnings("rawtypes")
            Pair pair : getPairs()) {
                if (pair.getKey().equals(value)) {
                    return pair;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component,
                              Object value) {
        if (value == null) {
            return "";
        } else {
            @SuppressWarnings("rawtypes")
            Pair pair = (Pair) value;
            return pair.getKey() != null ? pair.getKey().toString() : "";
        }
    }

}
