package com.sunecity.lilium.view.converter;

import com.sunecity.lilium.core.util.Numbers;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = Float.class)
public class FloatConverter extends javax.faces.convert.FloatConverter {

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return Numbers.getTextLocale(super.getAsString(context, component, value));
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        value = Numbers.getTextEnglish(value);
        return super.getAsObject(context, component, value);
    }

}
