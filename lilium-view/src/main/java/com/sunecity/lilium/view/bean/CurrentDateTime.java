package com.sunecity.lilium.view.bean;

import com.sunecity.lilium.core.time.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ApplicationScoped
public class CurrentDateTime implements Serializable {

    public Date getDate() {
        return Date.now();
    }

}
