package com.sunecity.lilium.view.facelet;

import javax.el.ELContext;
import javax.el.ELException;
import javax.el.ELResolver;
import javax.el.MethodExpression;
import javax.el.MethodInfo;
import javax.el.MethodNotFoundException;
import javax.el.PropertyNotFoundException;
import javax.el.ValueExpression;

/**
 * <b>Title:</b> LiliumMethodExpression
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public class LiliumMethodExpression extends MethodExpression {

    private static final long serialVersionUID = 1102935348410795945L;

    private final ValueExpression valueExpression;

    public LiliumMethodExpression(ValueExpression valueExpression) {
        this.valueExpression = valueExpression;
    }

    @Override
    public Object invoke(ELContext context, Object[] params) {
        try {
            return valueExpression.getValue(new ValueToInvokeElContext(context, params));
        } catch (ELException e) {
            Throwable throwable = e.getCause();
            while (throwable != null) {
                if (throwable instanceof MethodNotFoundException) {
                    throw (MethodNotFoundException) throwable;
                }
                throwable = throwable.getCause();
            }
            throw e;
        }
    }

    @Override
    public MethodInfo getMethodInfo(ELContext context) {
        return new MethodInfo(null, valueExpression.getExpectedType(), null);
    }

    @Override
    public boolean isLiteralText() {
        return false;
    }

    @Override
    public int hashCode() {
        return valueExpression.hashCode();
    }

    @Override
    public String getExpressionString() {
        return valueExpression.getExpressionString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj == this || obj instanceof LiliumMethodExpression && ((LiliumMethodExpression) obj).getValueExpression().equals(valueExpression);
    }

    public ValueExpression getValueExpression() {
        return valueExpression;
    }

    static class ValueToInvokeElContext extends ELContextWrapper {

        private final Object[] callerProvidedParameters;

        public ValueToInvokeElContext(ELContext elContext, Object[] callerProvidedParameters) {
            super(elContext);
            this.callerProvidedParameters = callerProvidedParameters;
        }

        @Override
        public ELResolver getELResolver() {
            return new ValueToInvokeElResolver(super.getELResolver(), callerProvidedParameters);
        }
    }

    static class ValueToInvokeElResolver extends ELResolverWrapper {

        private static final Object[] EMPTY_PARAMETERS = new Object[0];
        private final Object[] callerProvidedParameters;

        public ValueToInvokeElResolver(ELResolver elResolver, Object[] callerProvidedParameters) {
            super(elResolver);
            this.callerProvidedParameters = callerProvidedParameters;
        }

        @Override
        public Object getValue(ELContext context, Object base, Object property) {
            if (base == null) {
                return super.getValue(context, base, property);
            }
            try {
                return super.getValue(context, base, property);
            } catch (PropertyNotFoundException pnfe) {
                try {
                    return super.invoke(context, base, property, null, callerProvidedParameters != null ? callerProvidedParameters : EMPTY_PARAMETERS);
                } catch (MethodNotFoundException e) {
                    throw new ELException(e.getMessage(), e);
                }
            }
        }
    }

}
