package com.sunecity.lilium.view.bean;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by shahram on 7/19/15.
 */
@Named
@SessionScoped
public class Growl implements Serializable {

    private long life = 4000;
    private boolean sticky = false;
    private boolean showDetail = true;

    public long getLife() {
        return life;
    }

    public void setLife(long life) {
        this.life = life;
    }

    public boolean isSticky() {
        return sticky;
    }

    public void setSticky(boolean sticky) {
        this.sticky = sticky;
    }

    public boolean isShowDetail() {
        return showDetail;
    }

    public void setShowDetail(boolean showDetail) {
        this.showDetail = showDetail;
    }

}
