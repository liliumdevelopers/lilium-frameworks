package com.sunecity.lilium.view.bean;

import org.primefaces.PrimeFaces;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

@Named
@SessionScoped
public class Notification implements Serializable {

    private static final long serialVersionUID = -3769986580208154755L;

    private boolean showTop;
    private boolean showBottom;
    private boolean showCenter;
    private String styleTop;
    private String styleBottom;

    public static void show(String title, String detail, String icon, String audio, Integer timeout) {
        int random = new Random().nextInt();
        if (random < 0) {
            random *= -1;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("if (!(\"Notification\" in window)) {\n");
        sb.append("  console.log(\"This browser does not support desktop notifications\");\n");
        sb.append("} else if (Notification.permission === \"granted\") {\n");
        sb.append("  notification").append(random).append("();\n");
        sb.append("} else if (Notification.permission !== \"denied\") {\n");
        sb.append("  Notification.requestPermission(function (permission) {\n");
        sb.append("    if (permission === \"granted\") {\n");
        sb.append("      notification").append(random).append("();\n");
        sb.append("    }\n");
        sb.append("  });\n");
        sb.append("}\n");
        sb.append("function notification").append(random).append("() {\n");
        sb.append("  var notification = new Notification('").append(title).append("', {\n");
        sb.append("    dir: 'rtl',\n");
        if (icon != null) {
            sb.append("    icon: '").append(icon).append("',\n");
        }
        if (timeout == null) {
            sb.append("    requireInteraction: true,\n");
        }
        sb.append("    body: '").append(detail).append("'\n");
        sb.append("  });\n");
        if (audio != null) {
            sb.append("  var audio = new Audio('").append(audio).append("');\n");
            sb.append("  audio.loop = false;\n");
            sb.append("  audio.play();\n");
        }
        if (timeout != null) {
            sb.append("  setTimeout(function () {\n");
            sb.append("    notification.close();\n");
            sb.append("  }, '").append(timeout).append("');\n");
        }
        sb.append("}");
        PrimeFaces.current().executeScript(sb.toString());
    }

    public boolean isShowTop() {
        return showTop;
    }

    public boolean isShowBottom() {
        return showBottom;
    }

    public boolean isShowCenter() {
        return showCenter;
    }

    public String getStyleTop() {
        return styleTop;
    }

    public void setStyleTop(String styleTop) {
        this.styleTop = styleTop;
    }

    public String getStyleBottom() {
        return styleBottom;
    }

    public void setStyleBottom(String styleBottom) {
        this.styleBottom = styleBottom;
    }

    public void showTop() {
        if (showTop) {
            PrimeFaces.current().executeScript("barTop.hide()");
            PrimeFaces.current().executeScript("barTop.show()");
            PrimeFaces.current().ajax().update("pnlNtfTop");
        } else {
            this.showTop = true;
            Collection<String> strings = new ArrayList<>();
            strings.add("pnlNtfTop");
            strings.add("jsNtfTop");
            PrimeFaces.current().ajax().update(strings);
        }
    }

    public void showBottom() {
        if (showBottom) {
            PrimeFaces.current().executeScript("barBottom.hide()");
            PrimeFaces.current().executeScript("barBottom.show()");
            PrimeFaces.current().ajax().update("pnlNtfBottom");
        } else {
            this.showBottom = true;
            Collection<String> strings = new ArrayList<>();
            strings.add("pnlNtfBottom");
            strings.add("jsNtfBottom");
            PrimeFaces.current().ajax().update(strings);
        }
    }

    public void showCenter() {
        if (showCenter) {
            PrimeFaces.current().executeScript("barCenter.show()");
            PrimeFaces.current().ajax().update("pnlNtfCenter");
        } else {
            this.showCenter = true;
            Collection<String> strings = new ArrayList<>();
            strings.add("pnlNtfCenter");
            strings.add("jsNtfCenter");
            PrimeFaces.current().ajax().update(strings);
        }
    }

    public void closeTop() {
        this.showTop = false;
    }

    public void closeBottom() {
        this.showBottom = false;
    }

    public void closeCenter() {
        this.showCenter = false;
    }

}
