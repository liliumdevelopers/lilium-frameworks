package com.sunecity.lilium.view.model;

/**
 * <b>Title:</b> LoadCount
 * Count of records to be loaded.
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public enum LoadCount {

    LOAD_PAGESIZE(0), LOAD_50(50), LOAD_100(100), LOAD_200(200), LOAD_500(500), LOAD_1000(1000);

    private final int count;

    LoadCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

}
