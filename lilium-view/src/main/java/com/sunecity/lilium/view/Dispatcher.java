package com.sunecity.lilium.view;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.Serializable;

/**
 * <b>Title:</b> Dispatcher
 *
 * @author Shahram Goodarzi
 * @version 3.0.0
 */
@Named
@RequestScoped
public class Dispatcher implements Serializable {

    public void redirect(String url) {
        try {
            JsfServlet.getExternalContext().redirect(
                    JsfServlet.getExternalContext().getRequestContextPath() + (url.startsWith("/") ? url : "/" + url));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void dispatch(String url) {
        try {
            JsfServlet.getRequest()
                    .getRequestDispatcher(url.startsWith("/") ? url : "/" + url).forward(JsfServlet.getRequest(), JsfServlet.getResponse());
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

}
