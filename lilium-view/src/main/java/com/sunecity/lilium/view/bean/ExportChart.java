package com.sunecity.lilium.view.bean;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.inject.Named;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.Base64;

@Named
@RequestScoped
public class ExportChart implements Serializable {

    private static final long serialVersionUID = -7847441779987087133L;

    private String chart;
    private byte[] bs;

    public String getChart() {
        return chart;
    }

    public void setChart(String chart) {
        this.chart = chart;
    }

    public void exportChart(ActionEvent event) {
        if (chart != null && chart.split(",").length > 1) {
            String encoded = chart.split(",")[1];
            byte[] decoded = Base64.getDecoder().decode(encoded);
            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                RenderedImage renderedImage = ImageIO
                        .read(new ByteArrayInputStream(decoded));
                ImageIO.write(renderedImage, "png", outputStream);
                this.bs = outputStream.toByteArray();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public StreamedContent getFile() {
        if (bs != null) {
            return new DefaultStreamedContent(new ByteArrayInputStream(bs),
                    "image/png", "chart.png");
        } else {
            return new DefaultStreamedContent();
        }
    }

}
