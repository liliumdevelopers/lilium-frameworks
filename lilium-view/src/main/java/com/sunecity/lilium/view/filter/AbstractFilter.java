package com.sunecity.lilium.view.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Shahram Goodarzi
 */
public abstract class AbstractFilter implements Filter {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected HttpServletRequest httpServletRequest;
    protected HttpServletResponse httpServletResponse;
    protected ServletContext servletContext;

    @Override
    public void init(FilterConfig config) throws ServletException {
        servletContext = config.getServletContext();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            this.httpServletRequest = (HttpServletRequest) request;
            this.httpServletResponse = (HttpServletResponse) response;
            filter(request, response, chain);
        } catch (Exception e) {
            logger.error("Error in filter.", e);
        }
    }

    @Override
    public void destroy() {
    }

    public abstract void filter(ServletRequest request, ServletResponse response, FilterChain chain) throws Exception;

}
