package com.sunecity.lilium.view.bean;

import com.sunecity.lilium.security.KeycloakConfig;
import com.sunecity.lilium.view.JsfServlet;
import com.sunecity.lilium.view.Sessions;
import org.aeonbits.owner.ConfigFactory;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.common.util.KeycloakUriBuilder;
import org.keycloak.constants.ServiceUrlConstants;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

@Named
@RequestScoped
public class Logout implements Serializable {

    private static final long serialVersionUID = -2483834946672831869L;

    public void logout() throws IOException {
        KeycloakConfig keycloakConfig = ConfigFactory.create(KeycloakConfig.class, System.getProperties(), System.getenv());
        String realm = ((KeycloakSecurityContext) JsfServlet.getRequest().getAttribute(KeycloakSecurityContext.class.getName())).getRealm();
        Sessions.clear();
        JsfServlet.getExternalContext().redirect(KeycloakUriBuilder.fromUri(keycloakConfig.url()).path(ServiceUrlConstants.TOKEN_SERVICE_LOGOUT_PATH).queryParam("redirect_uri", JsfServlet.getRequest().getRequestURL()).build(realm).toString());
    }

}
