package com.sunecity.lilium.view.bean;

import com.querydsl.core.types.Path;
import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.core.model.BaseNonentity;
import com.sunecity.lilium.data.field.BooleanField;
import com.sunecity.lilium.data.field.Field;
import org.apache.commons.beanutils.PropertyUtils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.money.MonetaryAmount;
import java.lang.reflect.InvocationTargetException;
import java.text.NumberFormat;
import java.util.Deque;
import java.util.LinkedList;

@Named
@RequestScoped
public class LazyDataTable {

    private Field field;

    public Object getValuePath(Object var, Field field) {
        if (field != null) {
            this.field = field;
            try {
                Deque<String> paths = new LinkedList<>();
                paths.add(String.valueOf(field.getPath().getMetadata().getElement()));
                Path<?> p = field.getPath();
                while ((p = p.getMetadata().getParent()) != null) {
                    paths.addFirst(String.valueOf(p.getMetadata().getElement()));
                }
                if (field.getPath().getRoot().getType().isAssignableFrom(var.getClass())) {
                    return getValue(var, paths);
                } else if (var instanceof BaseNonentity) {
                    return getValue(PropertyUtils.getProperty(var, field.getPath().getRoot().getMetadata().getName()), paths);
                }
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private Object getValue(Object var, Deque<String> paths) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        if (!paths.isEmpty()) {
            paths.pop();
        }
        if (var instanceof BaseIdentity) {
            Object value = PropertyUtils.getProperty(var, String.valueOf(paths.peek()));
            if (value != null) {
                return getValue(value, paths);
            } else {
                return null;
            }
        } else {
            if (var == null) {
                return null;
            } else if (var instanceof MonetaryAmount) {
                return com.sunecity.lilium.core.util.Numbers.getMoney((MonetaryAmount) var);
            } else {
                String s = var.toString();
                if (field instanceof BooleanField) {
                    if (s.equals("true")) {
                        return ((BooleanField) field).getTrueName();
                    } else if (s.equals("false")) {
                        return ((BooleanField) field).getFalseName();
                    }
                }
                if (var instanceof Number) {
                    s = NumberFormat.getNumberInstance().format(var);
                }
                return com.sunecity.lilium.core.util.Numbers.getTextLocale(s);
            }
        }
    }

}
