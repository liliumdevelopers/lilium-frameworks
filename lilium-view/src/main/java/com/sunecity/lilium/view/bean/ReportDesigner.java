package com.sunecity.lilium.view.bean;

import com.querydsl.core.types.Path;
import com.sunecity.lilium.data.field.Field;
import com.sunecity.lilium.data.report.AbstractColumn;
import com.sunecity.lilium.data.report.Border;
import com.sunecity.lilium.data.report.Color;
import com.sunecity.lilium.data.report.DynamicReport;
import com.sunecity.lilium.data.report.Logo;
import com.sunecity.lilium.data.report.Page;
import com.sunecity.lilium.data.report.ReportColumn;
import com.sunecity.lilium.data.report.Style;
import com.sunecity.lilium.data.report.enums.BorderType;
import com.sunecity.lilium.data.report.enums.HorizontalAlign;
import com.sunecity.lilium.data.report.enums.LineSpacing;
import com.sunecity.lilium.data.report.enums.LogoPosition;
import com.sunecity.lilium.data.report.enums.Paper;
import com.sunecity.lilium.data.report.enums.ReportDate;
import com.sunecity.lilium.data.report.enums.Rotation;
import com.sunecity.lilium.data.report.enums.VerticalAlign;
import com.sunecity.lilium.view.JsfServlet;
import com.sunecity.lilium.view.converter.ReportColumnConverter;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DualListModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ReportDesigner implements Serializable {

    private static final long serialVersionUID = 6673607661504458027L;

    private DynamicReport report;
    private Page page;
    private Paper selectedPaper;
    private Style style;
    private Border borderRight;
    private Border borderLeft;
    private Border borderTop;
    private Border borderBottom;
    private String fontColor;
    private String bgColor;
    private String borderTopColor;
    private String borderBottomColor;
    private String borderRightColor;
    private String borderLeftColor;
    private Logo logo;
    private DualListModel<AbstractColumn> dualListModel;
    private ReportColumnConverter reportColumnConverter;
    private List<AbstractColumn> columns;

    public ReportDesigner(List<Field> columns) {
        report = new DynamicReport();
        this.columns = columns.stream().map(ReportColumn::new).collect(Collectors.toList());
    }

    public Border getBorderRight() {
        if (borderRight == null) {
            borderRight = new Border();
        }
        return borderRight;
    }

    public void setBorderRight(Border borderRight) {
        this.borderRight = borderRight;
    }

    public Border getBorderLeft() {
        if (borderLeft == null) {
            borderLeft = new Border();
        }
        return borderLeft;
    }

    public void setBorderLeft(Border borderLeft) {
        this.borderLeft = borderLeft;
    }

    public Border getBorderTop() {
        if (borderTop == null) {
            borderTop = new Border();
        }
        return borderTop;
    }

    public void setBorderTop(Border borderTop) {
        this.borderTop = borderTop;
    }

    public Border getBorderBottom() {
        if (borderBottom == null) {
            borderBottom = new Border();
        }
        return borderBottom;
    }

    public void setBorderBottom(Border borderBottom) {
        this.borderBottom = borderBottom;
    }

    public String getBorderLeftColor() {
        return borderLeftColor;
    }

    public void setBorderLeftColor(String borderLeftColor) {
        this.borderLeftColor = borderLeftColor;
    }

    public String getBorderRightColor() {
        return borderRightColor;
    }

    public void setBorderRightColor(String borderRightColor) {
        this.borderRightColor = borderRightColor;
    }

    public String getBorderBottomColor() {
        return borderBottomColor;
    }

    public void setBorderBottomColor(String borderBottomColor) {
        this.borderBottomColor = borderBottomColor;
    }

    public String getBorderTopColor() {
        return borderTopColor;
    }

    public void setBorderTopColor(String borderTopColor) {
        this.borderTopColor = borderTopColor;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public Style getStyle() {
        if (style == null) {
            style = new Style();
        }
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public DynamicReport getReport() {
        return report;
    }

    public void setReport(DynamicReport report) {
        this.report = report;
    }

    public Page getPage() {
        if (page == null) {
            page = getSelectedPaper().getPage();
        }
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Paper getSelectedPaper() {
        if (selectedPaper == null) {
            selectedPaper = Paper.A4;
        }
        return selectedPaper;
    }

    public void setSelectedPaper(Paper selectedPaper) {
        this.selectedPaper = selectedPaper;
    }

    public Logo getLogo() {
        if (logo == null) {
            logo = new Logo();
        }
        return logo;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public DualListModel<AbstractColumn> getDualListModel() {
        if (dualListModel == null) {
            dualListModel = new DualListModel<>(new ArrayList<>(), new ArrayList<>(columns));
        }
        return dualListModel;
    }

    public void setDualListModel(DualListModel<AbstractColumn> dualListModel) {
        this.dualListModel = dualListModel;
    }

    public ReportColumnConverter getReportColumnConverter() {
        if (reportColumnConverter == null) {
            reportColumnConverter = new ReportColumnConverter(columns);
        }
        return reportColumnConverter;
    }

    public Paper[] getPapers() {
        return Paper.values();
    }

    public ReportDate[] getReportDates() {
        return ReportDate.values();
    }

    public LogoPosition[] getLogoPositions() {
        return LogoPosition.values();
    }

    public HorizontalAlign[] getHorizontalAligns() {
        return HorizontalAlign.values();
    }

    public VerticalAlign[] getVerticalAligns() {
        return VerticalAlign.values();
    }

    public Rotation[] getRotations() {
        return Rotation.values();
    }

    public LineSpacing[] getLineSpacings() {
        return LineSpacing.values();
    }

    public BorderType[] getBorderTypes() {
        return BorderType.values();
    }

    public void btnColumnHeaderStyle() {
        ReportColumn reportColumn = (ReportColumn) columns.stream().filter(c -> c instanceof ReportColumn && ((ReportColumn) c).getField().getName()
                .equals(JsfServlet.getExternalContext().getRequestParameterMap().get("columnHeaderStyle"))).findFirst().orElse(null);
        if (reportColumn.getHeaderStyle() == null) {
            reportColumn.setHeaderStyle(new Style());
        }
        style = reportColumn.getHeaderStyle();
    }

    public void btnColumnStyle() {
        ReportColumn reportColumn = (ReportColumn) columns.stream().filter(c -> c instanceof ReportColumn && ((ReportColumn) c).getField().getName()
                .equals(JsfServlet.getExternalContext().getRequestParameterMap().get("columnStyle"))).findFirst().orElse(null);
        if (reportColumn.getFieldStyle() == null) {
            reportColumn.setFieldStyle(new Style());
        }
        style = reportColumn.getFieldStyle();
    }

    public void btnStyleClick(String styleName) {
        switch (styleName) {
            case "title":
                if (report.getTitleStyle() == null) {
                    report.setTitleStyle(new Style());
                }
                style = report.getTitleStyle();
                break;
            case "subtitle":
                if (report.getSubtitleStyle() == null) {
                    report.setSubtitleStyle(new Style());
                }
                style = report.getSubtitleStyle();
                break;
            case "header":
                if (report.getHeaderStyle() == null) {
                    report.setHeaderStyle(new Style());
                }
                style = report.getHeaderStyle();
                break;
            case "footer":
                if (report.getFooterStyle() == null) {
                    report.setFooterStyle(new Style());
                }
                style = report.getFooterStyle();
                break;
            case "summary":
                if (report.getSummaryStyle() == null) {
                    report.setSummaryStyle(new Style());
                }
                style = report.getSummaryStyle();
                break;
            case "reportDate":
                if (report.getReportDateStyle() == null) {
                    report.setReportDateStyle(new Style());
                }
                style = report.getReportDateStyle();
                break;
            case "pageNumber":
                if (report.getPageNumberStyle() == null) {
                    report.setPageNumberStyle(new Style());
                }
                style = report.getPageNumberStyle();
                break;
            default:
                if (styleName.startsWith("header")) {
                    ReportColumn reportColumn = (ReportColumn) report.getColumns().stream().filter(c -> c instanceof ReportColumn && ((ReportColumn) c).getField().getName().equals(styleName.substring(6))).findFirst().orElse(null);
                    if (reportColumn.getHeaderStyle() == null) {
                        reportColumn.setHeaderStyle(new Style());
                    }
                    style = reportColumn.getHeaderStyle();
                } else {
                    ReportColumn reportColumn = (ReportColumn) report.getColumns().stream().filter(c -> c instanceof ReportColumn && ((ReportColumn) c).getField().getName().equals(styleName)).findFirst().orElse(null);
                    if (reportColumn.getFieldStyle() == null) {
                        reportColumn.setFieldStyle(new Style());
                    }
                    style = reportColumn.getFieldStyle();
                }
        }
        if (style.getFontColor() != null) {
            fontColor = style.getFontColor().toHexadecimal();
        }
        if (style.getBgColor() != null) {
            bgColor = style.getBgColor().toHexadecimal();
        }
        borderTop = style.getBorderTop();
        borderBottom = style.getBorderBottom();
        borderRight = style.getBorderRight();
        borderLeft = style.getBorderLeft();
        if (style.getBorderTop() != null) {
            if (style.getBorderTop().getColor() != null) {
                borderTopColor = style.getBorderTop().getColor().toHexadecimal();
            }
        }
        if (style.getBorderBottom() != null) {
            if (style.getBorderBottom().getColor() != null) {
                borderBottomColor = style.getBorderBottom().getColor().toHexadecimal();
            }
        }
        if (style.getBorderRight() != null) {
            if (style.getBorderRight().getColor() != null) {
                borderRightColor = style.getBorderRight().getColor().toHexadecimal();
            }
        }
        if (style.getBorderLeft() != null) {
            if (style.getBorderLeft().getColor() != null) {
                borderLeftColor = style.getBorderLeft().getColor().toHexadecimal();
            }
        }
    }

    public void btnCloseStyleClick() {
        if (fontColor != null) {
            style.setFontColor(new Color(fontColor));
        }
        if (bgColor != null) {
            style.setBgColor(new Color(bgColor));
        }
        if (borderTop != null) {
            if (borderTopColor != null) {
                borderTop.setColor(new Color(borderTopColor));
            }
            style.setBorderTop(borderTop);
        }
        if (borderBottom != null) {
            if (borderBottomColor != null) {
                borderBottom.setColor(new Color(borderBottomColor));
            }
            style.setBorderBottom(borderBottom);
        }
        if (borderRight != null) {
            if (borderRightColor != null) {
                borderRight.setColor(new Color(borderRightColor));
            }
            style.setBorderRight(borderRight);
        }
        if (borderLeft != null) {
            if (borderLeftColor != null) {
                borderLeft.setColor(new Color(borderLeftColor));
            }
            style.setBorderLeft(borderLeft);
        }
        fontColor = null;
        bgColor = null;
        borderTop = null;
        borderBottom = null;
        borderRight = null;
        borderLeft = null;
        borderTopColor = null;
        borderBottomColor = null;
        borderRightColor = null;
        borderLeftColor = null;
        style = null;
    }

    public DynamicReport generate() {
        report.setColumns(dualListModel.getTarget());
        report.setPage(getPage());
        if (getLogo().getFile() != null) {
            report.setLogo(getLogo());
        }
        return report;
    }

    public void changePaper() {
        report.setPage(selectedPaper.getPage());
    }

    public void uploadImage(FileUploadEvent event) {
        getLogo().setFile(event.getFile().getContents());
    }

    private String getNamePath(Path<?> path) {
        final StringBuilder sb = new StringBuilder(path.getMetadata().getName());
        while ((path = path.getMetadata().getParent()) != null) {
            if (path.getMetadata().getParent() != null) {
                sb.insert(0, path.getMetadata().getName() + ".");
            }
        }
        return sb.toString();
    }

    public static class ColumnModel implements Serializable {

        private String header;
        private String property;

        public ColumnModel(String header, String property) {
            this.header = header;
            this.property = property;
        }

        public String getHeader() {
            return header;
        }

        public String getProperty() {
            return property;
        }

    }

}
