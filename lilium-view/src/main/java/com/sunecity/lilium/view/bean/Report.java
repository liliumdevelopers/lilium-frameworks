package com.sunecity.lilium.view.bean;

import com.sunecity.lilium.data.report.Page;
import com.sunecity.lilium.data.report.ReportType;
import com.sunecity.lilium.data.report.enums.Paper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class Report implements Serializable {

    private static final long serialVersionUID = 6993880066371394171L;

    private String reportTitle;
    private Page page;
    private String reportPath;

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public void btnExportClick(ReportType type) {

    }

    public Paper[] getPages() {
        return Paper.values();
    }

}
