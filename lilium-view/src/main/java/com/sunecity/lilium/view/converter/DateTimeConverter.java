package com.sunecity.lilium.view.converter;

import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.core.localization.Locales;
import com.sunecity.lilium.core.time.DateTime;
import com.sunecity.lilium.core.util.Numbers;
import com.sunecity.lilium.core.validation.Validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;
import java.time.format.DateTimeFormatter;

@FacesConverter(forClass = DateTime.class)
public class DateTimeConverter implements Converter, Serializable {

    private static final long serialVersionUID = 2032209194285626527L;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            return null;
        }
        if (Locales.getLocale().equals(Locales.PERSIAN)) {
            if (!Validator.isPersianDate(value)) {
                FacesMessage facesMessage = new FacesMessage(Bundles.getValueDefault("validation.DateTime.message"));
                facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ConverterException(facesMessage);
            }
            return DateTime.parsePersian(value);
        } else {
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
                return DateTime.parse(value, formatter);
            } catch (Exception e) {
                throw new ConverterException(e);
            }
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }
        return Numbers.getTextEnglish(value.toString());
    }

}
