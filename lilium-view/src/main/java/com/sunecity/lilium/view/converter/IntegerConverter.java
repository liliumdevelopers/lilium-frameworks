package com.sunecity.lilium.view.converter;

import com.sunecity.lilium.core.util.Numbers;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = Integer.class)
public class IntegerConverter extends javax.faces.convert.IntegerConverter {

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return Numbers.getTextLocale(super.getAsString(context, component, value));
    }

}
