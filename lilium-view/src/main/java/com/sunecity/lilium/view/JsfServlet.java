package com.sunecity.lilium.view;

import org.primefaces.PrimeFaces;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <b>Title:</b> JsfServlet
 * Jsf Utilities.
 *
 * @author Shahram Goodarzi
 * @version 1.5.0
 */
public final class JsfServlet {

    private JsfServlet() {
    }

    public static FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    public static ExternalContext getExternalContext() {
        return getFacesContext().getExternalContext();
    }

    public static ServletContext getServletContext() {
        return (ServletContext) getExternalContext().getContext();
    }

    public static String getContextName() {
        return getFacesContext().getExternalContext().getContextName();
    }

    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) getExternalContext().getRequest();
    }

    public static HttpServletResponse getResponse() {
        return (HttpServletResponse) getExternalContext().getResponse();
    }

    public static void update(String id) {
        getFacesContext().getPartialViewContext().getRenderIds().add(id);
    }

    public static String getViewId() {
        return getFacesContext().getViewRoot().getViewId();
    }

    public static String getCurrentUrl() {
        return getRequest().getServletPath();
    }

    public static void addMessage(String msg, String id, FacesMessage.Severity severity, boolean isError) {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage(msg);
        message.setSeverity(severity);
        context.addMessage(id, message);
        PrimeFaces.current().ajax().addCallbackParam("error", isError);
    }

    public static String getLocalIp() {
        return getRequest().getLocalAddr();
    }

    public static int getLocalPort() {
        return getRequest().getLocalPort();
    }

    public static String getLocalAddress() {
        HttpServletRequest request = getRequest();
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
    }

    public static String getRequestParameter(String parameterName) {
        return getRequest().getParameter(parameterName);
    }

    public static void sendError(int errorCode) throws IOException {
        getResponse().sendError(errorCode);
    }

    public static String getValueBundle(String name, String key) {
        return getFacesContext().getApplication().getResourceBundle(getFacesContext(), name).getString(key);
    }

}
