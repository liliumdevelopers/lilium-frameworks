package com.sunecity.lilium.view.theme;

import com.sunecity.lilium.core.localization.Bundles;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//@FacesConverter
public class ThemeConverter implements Converter, Serializable {

	private static final long serialVersionUID = -3138537707536845750L;

	public List<LiliumTheme> getThemes() {
		List<LiliumTheme> themes = new ArrayList<>();
		themes.add(new LiliumTheme(Bundles.getValueDefault("flatui"), "flatui", "lilium.png"));
		themes.add(new LiliumTheme(Bundles.getValueDefault("spark"), "spark", "spark.png"));
		themes.add(new LiliumTheme(Bundles.getValueDefault("aristo"), "aristo", "aristo.png"));
		themes.add(new LiliumTheme(Bundles.getValueDefault("blitzer"), "blitzer", "blitzer.png"));
		themes.add(new LiliumTheme(Bundles.getValueDefault("bootstrap"), "bootstrap", "bootstrap.png"));
		themes.add(new LiliumTheme(Bundles.getValueDefault("home"), "home", "home.png"));
		themes.add(new LiliumTheme(Bundles.getValueDefault("hotsneaks"), "hot-sneaks", "hot-sneaks.png"));
		themes.add(new LiliumTheme(Bundles.getValueDefault("peppergrinder"), "pepper-grinder", "pepper-grinder.png"));


//		themes.add(new LiliumTheme(Bundles.getValueDefault("afterdark"), "afterdark", "afterdark.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("afternoon"), "afternoon", "afternoon.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("afterwork"), "afterwork", "afterwork.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("blacktie"), "black-tie", "black-tie.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("bluesky"), "bluesky", "bluesky.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("casablanca"), "casablanca", "casablanca.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("cruze"), "cruze", "cruze.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("cupertino"), "cupertino", "cupertino.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("darkhive"), "dark-hive", "dark-hive.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("dotluv"), "dot-luv", "dot-luv.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("eggplant"), "eggplant", "eggplant.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("excitebike"), "excite-bike", "excite-bike.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("flick"), "flick", "flick.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("glassx"), "glass-x", "glass-x.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("humanity"), "humanity", "humanity.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("lefrog"), "le-frog", "le-frog.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("midnight"), "midnight", "midnight.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("mintchoc"), "mint-choc", "mint-choc.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("overcast"), "overcast", "overcast.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("redmond"), "redmond", "redmond.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("rocket"), "rocket", "rocket.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("sam"), "sam", "sam.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("smoothness"), "smoothness", "smoothness.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("southstreet"), "south-street", "south-street.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("start"), "start", "start.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("sunny"), "sunny", "sunny.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("swankypurse"), "swanky-purse", "swanky-purse.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("trontastic"), "trontastic", "trontastic.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("uidarkness"), "ui-darkness", "ui-darkness.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("uilightness"), "ui-lightness", "ui-lightness.png"));
//		themes.add(new LiliumTheme(Bundles.getValueDefault("vader"), "vader", "vader.png"));
		return themes;
	}


	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		for (LiliumTheme theme : getThemes()) {
			if (theme.getValue().equals(value)) {
				return theme;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		LiliumTheme theme = (LiliumTheme) value;
		return String.valueOf(theme.getValue());
	}

}
