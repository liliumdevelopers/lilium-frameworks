package com.sunecity.lilium.view.filter;

import com.sunecity.lilium.security.Authentication;
import com.sunecity.lilium.security.PermissionCheck;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;

import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

/**
 * @author Shahram Goodarzi
 */
public class SecurityFilter extends AbstractFilter {

//    @Inject
//    private PermissionCheck permissionCheck;

    @Inject
    private Authentication authentication;

    @Override
    public void filter(ServletRequest request, ServletResponse response, FilterChain chain) throws Exception {
        Principal principal = httpServletRequest.getUserPrincipal();
//        if (!permissionCheck.isInitialized()) {
            if (principal instanceof KeycloakPrincipal) {
                KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
//                permissionCheck.initKeycloakPrincipal(keycloakPrincipal);
                authentication.init(keycloakPrincipal.getKeycloakSecurityContext().getRealm(), keycloakPrincipal.getKeycloakSecurityContext().getIdToken().getIssuedFor());
            }
//        }

        if (!httpServletRequest.getServletPath().equals("/pages/home.xhtml") && /*!permissionCheck.hasPageUrlPermission(httpServletRequest.getServletPath())*/
                authentication.getAllClientRoles(principal.getName()).stream().noneMatch(r -> (httpServletRequest).getServletPath().equals(r.getDescription()))) {
            httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
        } else {
            chain.doFilter(request, response);
        }
    }

}
