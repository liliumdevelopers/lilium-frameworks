package com.sunecity.lilium.view.theme;

import java.io.Serializable;

public class LiliumTheme implements Serializable {

	private static final long serialVersionUID = -7626199913625781101L;

	private String label;
	private String value;
	private String file;

	public LiliumTheme() {
	}

	public LiliumTheme(String label, String value, String file) {
		this.label = label;
		this.value = value;
		this.file = file;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof LiliumTheme)) {
			return false;
		}
		LiliumTheme liliumTheme = (LiliumTheme) obj;
		return liliumTheme.getValue().equals(value);
	}

}
