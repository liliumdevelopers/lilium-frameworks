package com.sunecity.lilium.view.abstractview;

import com.sunecity.lilium.core.exception.UniqueException;
import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.core.model.BaseIdentity;

public abstract class AbstractCrud<T extends BaseIdentity<?>> extends AbstractUpdate<T> {

    private static final long serialVersionUID = 4823285298800282430L;

    public void btnSaveClick() {
        try {
            if (saveValidate()) {
                save();
            }
            if (getDataModel() != null) {
                getDataModel().reset();
            }
        } catch (UniqueException e) {
            if (message.hasMessage(e.getConstraint())) {
                message.addError(message.getValueBundle(e.getConstraint()));
            } else {
                message.addError(Bundles.getValueDefault("message.UniqueException"));
            }
        } catch (Exception e) {
            logger.error("Create failed!", e);
        }
    }

    protected abstract boolean saveValidate() throws Exception;

    protected abstract void save() throws Exception;

    public void btnResetClick() {
        try {
            reset();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public void reset() throws Exception {

    }

}
