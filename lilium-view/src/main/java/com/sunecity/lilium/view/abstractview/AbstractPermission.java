package com.sunecity.lilium.view.abstractview;

import java.io.Serializable;
import java.util.List;

/**
 * <b>Title:</b> AbstractPermission
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public abstract class AbstractPermission implements Serializable {

    //    private Crud crud;
    private List<String> classFields;
    private List<String> pageFields;

//    public Crud getCrud() {
//        if (crud == null) {
//            crud = new Crud();
//        }
//        return crud;
//    }
//
//    public void setCrud(Crud crud) {
//        this.crud = crud;
//    }

    public List<String> getClassFields() {
        return classFields;
    }

    public void setClassFields(List<String> classFields) {
        this.classFields = classFields;
    }

    public List<String> getPageFields() {
        return pageFields;
    }

    public void setPageFields(List<String> pageFields) {
        this.pageFields = pageFields;
    }

//    public void grant(IdentityPermissionGrant identityPermissionGrant, IdentityType identityType) {
//        Class<?> clas = permission(identityPermissionGrant, identityType);
////        if (crud != null) {
//            if (Boolean.TRUE.equals(getCrud().getCreate())) {
//                identityPermissionGrant.grantPermission(identityType, clas, ClassScope.CREATE.name());
//            }
//            if (Boolean.TRUE.equals(getCrud().getRead())) {
//                identityPermissionGrant.grantPermission(identityType, clas, ClassScope.READ.name());
//            }
//            if (Boolean.TRUE.equals(getCrud().getUpdate())) {
//                identityPermissionGrant.grantPermission(identityType, clas, ClassScope.UPDATE.name());
//            }
//            if (Boolean.TRUE.equals(getCrud().getDelete())) {
//                identityPermissionGrant.grantPermission(identityType, clas, ClassScope.DELETE.name());
//            }
////        }
//        if (classFields != null) {
//            classFields.forEach(f -> identityPermissionGrant.grantPermission(identityType, new FieldPermission(f), FieldScope.CREATE.name()));
//        }
//        if (pageFields != null) {
//            pageFields.forEach(f -> identityPermissionGrant.grantPermission(identityType, new PagePermission(f), PageScope.GRANT.name()));
//        }
//        crud = null;
//        classFields = null;
//        pageFields = null;
//    }
//
//    protected String[] classFields() {
//        return null;
//    }
//
//    protected String[] pageFields() {
//        return null;
//    }
//
//    protected abstract Class<?> permission(IdentityPermissionGrant identityPermissionGrant, IdentityType identityType);

}
