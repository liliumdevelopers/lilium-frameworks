package com.sunecity.lilium.view.bean;

import com.sunecity.lilium.data.model.Menus;
import com.sunecity.lilium.view.JsfServlet;
import com.sunecity.lilium.view.abstractview.AbstractTemplate;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by shahram on 1/2/17.
 */
@Named
@RequestScoped
public class Breadcrumb {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Inject
    private AbstractTemplate template;

    private MenuModel model;

    public MenuModel getModel() {
        if (model == null) {
            model = new DefaultMenuModel();
            DefaultMenuItem home = new DefaultMenuItem();
            home.setId("itmHome");
            home.setUrl("/pages/home.xhtml");
            model.getElements().add(home);
            try {
                for (Menus menu : template.getMenus()) {
                    if (JsfServlet.getRequest().getServletPath().startsWith(menu.getPath())) {
                        DefaultMenuItem node = new DefaultMenuItem();
                        node.setValue(menu.getLabel());
                        model.getElements().add(node);
                    }
                }
            } catch (Exception e) {
                logger.error("Failed to load model", e);
            }
        }
        return model;
    }

}
