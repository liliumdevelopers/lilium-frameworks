package com.sunecity.lilium.view.facelet;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.faces.FacesWrapper;
import java.beans.FeatureDescriptor;
import java.util.Iterator;

/**
 * <b>Title:</b> ELResolverWrapper
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public class ELResolverWrapper extends ELResolver implements FacesWrapper<ELResolver> {

    private ELResolver elResolver;

    public ELResolverWrapper() {
    }

    public ELResolverWrapper(ELResolver elResolver) {
        this.elResolver = elResolver;
    }

    @Override
    public ELResolver getWrapped() {
        return elResolver;
    }

    @Override
    public Object getValue(ELContext context, Object base, Object property) {
        return getWrapped().getValue(context, base, property);
    }

    @Override
    public Object invoke(ELContext context, Object base, Object method, Class<?>[] paramTypes, Object[] params) {
        return getWrapped().invoke(context, base, method, paramTypes, params);
    }

    @Override
    public Class<?> getType(ELContext context, Object base, Object property) {
        return getWrapped().getType(context, base, property);
    }

    @Override
    public void setValue(ELContext context, Object base, Object property, Object value) {
        getWrapped().setValue(context, base, property, value);
    }

    @Override
    public boolean isReadOnly(ELContext context, Object base, Object property) {
        return getWrapped().isReadOnly(context, base, property);
    }

    @Override
    public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
        return getWrapped().getFeatureDescriptors(context, base);
    }

    @Override
    public Class<?> getCommonPropertyType(ELContext context, Object base) {
        return getWrapped().getCommonPropertyType(context, base);
    }

}
