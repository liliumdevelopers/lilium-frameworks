package com.sunecity.lilium.view.bean;

import com.sunecity.lilium.core.localization.MessageContext;
import com.sunecity.lilium.view.abstractview.AbstractView;
import org.primefaces.PrimeFaces;

import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Arrays;

@Dependent
public class JsfMessage implements Serializable {

    private static final long serialVersionUID = 2035464864016524342L;

    @Inject
    private Growl growl;

    @Inject
    private MessageContext messageContext;

    @Inject
    private Numbers numbers;

    private Class<? extends AbstractView> clas;

    public void setClass(Class<? extends AbstractView> clas) {
        this.clas = clas;
    }

    public Message msg(String message, Serializable... args) {
        return new Message(message, args);
    }

    public void setInfo(String msg, String id) {
        message(msg, null, id, FacesMessage.SEVERITY_INFO, false);
    }

    public void setInfo(String msg, String detail, String id) {
        growl.setShowDetail(detail != null);
        message(msg, detail, id, FacesMessage.SEVERITY_INFO, false);
    }

    public void addInfo(Message... messages) {
        growl.setShowDetail(false);
        growl.setSticky(false);
        growl.setLife(4000);
        message(messages, null, null, FacesMessage.SEVERITY_INFO, false);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addInfo(Message[] summaryMessages, Message[] detailMessages, Long life) {
        growl.setShowDetail(detailMessages != null);
        growl.setSticky(true);
        if (life != null) {
            growl.setLife(life);
        } else {
            growl.setLife(4000);
        }
        message(summaryMessages, detailMessages, null, FacesMessage.SEVERITY_INFO, false);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addStickyInfo(Message... messages) {
        addInfo(messages);
        growl.setSticky(true);
    }

    public void addStickyInfo(Message[] summaryMessages, Message[] detailMessages, Long life) {
        addInfo(summaryMessages, detailMessages, life);
        growl.setSticky(true);
    }

    public void addInfo(String summary, Serializable... args) {
        growl.setShowDetail(false);
        growl.setSticky(false);
        growl.setLife(4000);
        message(new Message[]{new Message(summary, args)}, null, null, FacesMessage.SEVERITY_INFO, false);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addInfo(String summary, String detail, Serializable[] summaryArgs, Serializable[] detailArgs, Long life) {
        growl.setShowDetail(detail != null);
        growl.setSticky(false);
        if (life != null) {
            growl.setLife(life);
        } else {
            growl.setLife(4000);
        }
        message(new Message[]{new Message(summary, summaryArgs)}, new Message[]{new Message(detail, detailArgs)}, null, FacesMessage.SEVERITY_INFO, false);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addStickyInfo(String summary, Serializable... args) {
        addInfo(summary, args);
        growl.setSticky(true);
    }

    public void addStickyInfo(String summary, String detail, Serializable[] summaryArgs, Serializable[] detailArgs, Long life) {
        addInfo(summary, detail, summaryArgs, detailArgs, life);
        growl.setSticky(true);
    }

    public void setWarn(String msg, String id) {
        message(msg, null, id, FacesMessage.SEVERITY_WARN, false);
    }

    public void setWarn(String msg, String detail, String id) {
        growl.setShowDetail(detail != null);
        message(msg, detail, id, FacesMessage.SEVERITY_WARN, false);
    }

    public void addWarn(Message... messages) {
        growl.setShowDetail(false);
        growl.setSticky(false);
        growl.setLife(4000);
        message(messages, null, null, FacesMessage.SEVERITY_WARN, false);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addWarn(Message[] summaryMessages, Message[] detailMessages, Long life) {
        growl.setShowDetail(detailMessages != null);
        growl.setSticky(false);
        if (life != null) {
            growl.setLife(life);
        } else {
            growl.setLife(4000);
        }
        message(summaryMessages, detailMessages, null, FacesMessage.SEVERITY_WARN, false);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addStickyWarn(Message... messages) {
        addWarn(messages);
        growl.setSticky(true);
    }

    public void addStickyWarn(Message[] summaryMessages, Message[] detailMessages, Long life) {
        addWarn(summaryMessages, detailMessages, life);
        growl.setSticky(true);
    }

    public void addWarn(String summary, Serializable... args) {
        growl.setShowDetail(false);
        growl.setSticky(false);
        growl.setLife(4000);
        message(new Message[]{new Message(summary, args)}, null, null, FacesMessage.SEVERITY_WARN, false);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addWarn(String summary, String detail, Serializable[] summaryArgs, Serializable[] detailArgs, Long life) {
        growl.setShowDetail(detail != null);
        growl.setSticky(false);
        if (life != null) {
            growl.setLife(life);
        } else {
            growl.setLife(4000);
        }
        message(new Message[]{new Message(summary, summaryArgs)}, new Message[]{new Message(detail, detailArgs)}, null, FacesMessage.SEVERITY_WARN, false);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addStickyWarn(String summary, Serializable... args) {
        addWarn(summary, args);
        growl.setSticky(true);
    }

    public void addStickyWarn(String summary, String detail, Serializable[] summaryArgs, Serializable[] detailArgs, Long life) {
        addWarn(summary, detail, summaryArgs, detailArgs, life);
        growl.setSticky(true);
    }

    public void setError(String msg, String id) {
        message(msg, null, id, FacesMessage.SEVERITY_ERROR, true);
    }

    public void setError(String msg, String detail, String id) {
        growl.setShowDetail(detail != null);
        message(msg, detail, id, FacesMessage.SEVERITY_ERROR, true);
    }

    public void addError(Message... messages) {
        growl.setShowDetail(false);
        growl.setSticky(false);
        growl.setLife(4000);
        message(messages, null, null, FacesMessage.SEVERITY_ERROR, true);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addError(Message[] summaryMessages, Message[] detailMessages, Long life) {
        growl.setShowDetail(detailMessages != null);
        growl.setSticky(false);
        if (life != null) {
            growl.setLife(life);
        } else {
            growl.setLife(4000);
        }
        message(summaryMessages, detailMessages, null, FacesMessage.SEVERITY_ERROR, true);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addStickyError(Message... messages) {
        addError(messages);
        growl.setSticky(true);
    }

    public void addStickyError(Message[] summaryMessages, Message[] detailMessages, Long life) {
        addError(summaryMessages, detailMessages, life);
        growl.setSticky(true);
    }

    public void addError(String summary, Serializable... args) {
        growl.setShowDetail(false);
        growl.setSticky(false);
        growl.setLife(4000);
        message(new Message[]{new Message(summary, args)}, null, null, FacesMessage.SEVERITY_ERROR, true);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addError(String summary, String detail, Serializable[] summaryArgs, Serializable[] detailArgs, Long life) {
        growl.setShowDetail(detail != null);
        growl.setSticky(false);
        if (life != null) {
            growl.setLife(life);
        } else {
            growl.setLife(4000);
        }
        message(new Message[]{new Message(summary, summaryArgs)}, new Message[]{new Message(detail, detailArgs)}, null, FacesMessage.SEVERITY_ERROR, true);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addStickyError(String summary, Serializable... args) {
        addError(summary, args);
        growl.setSticky(true);
    }

    public void addStickyError(String summary, String detail, Serializable[] summaryArgs, Serializable[] detailArgs, Long life) {
        addError(summary, detail, summaryArgs, detailArgs, life);
        growl.setSticky(true);
    }

    public void setFatal(String msg, String id) {
        message(msg, null, id, FacesMessage.SEVERITY_FATAL, true);
    }

    public void setFatal(String msg, String detail, String id) {
        growl.setShowDetail(detail != null);
        message(msg, detail, id, FacesMessage.SEVERITY_FATAL, true);
    }

    public void addFatal(Message... messages) {
        growl.setShowDetail(false);
        growl.setSticky(false);
        growl.setLife(4000);
        message(messages, null, null, FacesMessage.SEVERITY_FATAL, true);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addFatal(Message[] summaryMessages, Message[] detailMessages, Long life) {
        growl.setShowDetail(detailMessages != null);
        growl.setSticky(false);
        if (life != null) {
            growl.setLife(life);
        } else {
            growl.setLife(4000);
        }
        message(summaryMessages, detailMessages, null, FacesMessage.SEVERITY_FATAL, true);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addStickyFatal(Message... messages) {
        addFatal(messages);
        growl.setSticky(true);
    }

    public void addStickyFatal(Message[] summaryMessages, Message[] detailMessages, Long life) {
        addFatal(summaryMessages, detailMessages, life);
        growl.setSticky(true);
    }

    public void addFatal(String summary, Serializable... args) {
        growl.setShowDetail(false);
        growl.setSticky(false);
        growl.setLife(4000);
        message(new Message[]{new Message(summary, args)}, null, null, FacesMessage.SEVERITY_FATAL, true);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addFatal(String summary, String detail, Serializable[] summaryArgs, Serializable[] detailArgs, Long life) {
        growl.setShowDetail(detail != null);
        growl.setSticky(false);
        if (life != null) {
            growl.setLife(life);
        } else {
            growl.setLife(4000);
        }
        message(new Message[]{new Message(summary, summaryArgs)}, new Message[]{new Message(detail, detailArgs)}, null, FacesMessage.SEVERITY_FATAL, true);
        PrimeFaces.current().ajax().update("growl");
    }

    public void addStickyFatal(String summary, Serializable... args) {
        addFatal(summary, args);
        growl.setSticky(true);
    }

    public void addStickyFatal(String summary, String detail, Serializable[] summaryArgs, Serializable[] detailArgs, Long life) {
        addFatal(summary, detail, summaryArgs, detailArgs, life);
        growl.setSticky(true);
    }

    private void message(String msg, String detail, String id, Severity severity, boolean isError) {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage(msg);
        if (detail != null) {
            message.setDetail(detail);
        }
        message.setSeverity(severity);
        context.addMessage(id, message);
        PrimeFaces.current().ajax().addCallbackParam("error", isError);
    }

    private void message(Message[] summary, Message[] detail, String id, Severity severity, boolean isError) {
        com.sunecity.lilium.core.localization.Message message = messageContext.messageSource(clas.getPackage().getName().substring(0, clas.getPackage().getName().lastIndexOf("view")) + "view.message").message();
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage facesMessage = new FacesMessage();
        String className = clas.getSimpleName();
        if (className.contains("$")) {
            className = className.substring(0, className.indexOf('$'));
        }
        if (summary != null && summary.length > 0) {
            boolean bullet = summary.length > 1;
            StringBuilder sb = new StringBuilder();
            for (Message msg : summary) {
                if (bullet) {
                    sb.append("<span class=\"fa fa-circle\"/> ");
                }
                if (msg.getMessage().endsWith("Info") || msg.getMessage().endsWith("Warn") || msg.getMessage().endsWith("Error") || msg.getMessage().endsWith("Fatal")) {
                    sb.append(message.template("{" + className + "." + msg.getMessage() + "}").argument(msg.getArgs()).toString());
                } else {
                    sb.append(msg.getMessage());
                }
                sb.append("<br/>");
            }
            facesMessage.setSummary(sb.substring(0, sb.length() - 5));
        }
        if (detail != null && detail.length > 0) {
            boolean bullet = detail.length > 1;
            StringBuilder sb = new StringBuilder();
            for (Message msg : detail) {
                if (bullet) {
                    sb.append("<span class=\"fa fa-circle\"/> ");
                }
                if (msg.getMessage().endsWith("Info") || msg.getMessage().endsWith("Warn") || msg.getMessage().endsWith("Error") || msg.getMessage().endsWith("Fatal")) {
                    sb.append(message.template("{" + className + "." + msg.getMessage() + "}").argument(msg.getArgs()).toString());
                } else {
                    sb.append(msg.getMessage());
                }
                sb.append("<br/>");
            }
            facesMessage.setDetail(sb.substring(0, sb.length() - 5));
        }
        facesMessage.setSeverity(severity);
        context.addMessage(id, facesMessage);
        PrimeFaces.current().ajax().addCallbackParam("error", isError);
    }

    public boolean hasMessage(String msg) {
        com.sunecity.lilium.core.localization.Message message = messageContext.messageSource(clas.getPackage().getName().substring(0, clas.getPackage().getName().lastIndexOf("view")) + "view.message").message();
        String className = clas.getSimpleName();
        if (className.contains("$")) {
            className = className.substring(0, className.indexOf('$'));
        }
        if (msg.endsWith("Info") || msg.endsWith("Warn") || msg.endsWith("Error") || msg.endsWith("Fatal")) {
            message.template("{" + className + "." + msg + "}");
        } else {
            message.template("{" + msg + "}");
        }
        return !message.toString().startsWith("???");
    }

    public String getValueBundle(String key) {
        com.sunecity.lilium.core.localization.Message message = messageContext.messageSource(clas.getPackage().getName().substring(0, clas.getPackage().getName().lastIndexOf("view")) + "view.message").message();
        return message.template("{" + key + "}").toString();
    }

    public class Message {

        private final String message;
        private final String[] args;

        public Message(String message, Serializable... args) {
            this.message = message;
            this.args = Arrays.stream(args).map(a -> numbers.getText(String.valueOf(a))).toArray(String[]::new);
        }

        public String getMessage() {
            return message;
        }

        public String[] getArgs() {
            return args;
        }

    }

}
