package com.sunecity.lilium.view.abstractview;

import com.sunecity.lilium.core.exception.ConcurrentException;
import com.sunecity.lilium.core.exception.ForeignKeyException;
import com.sunecity.lilium.core.exception.UniqueException;
import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.view.model.AbstractDataModel;

import java.io.Serializable;

public abstract class AbstractUpdate<T extends BaseIdentity<?>> extends AbstractBase<T> {

    private static final long serialVersionUID = -3425318210480479421L;

    public AbstractUpdate() {
    }

    @Override
    public abstract AbstractDataModel<?> dataModel() throws Exception;

    public void btnUpdateClick() {
        try {
            if (updateValidate()) {
                update();
            }
        } catch (UniqueException e) {
            if (message.hasMessage(e.getConstraint())) {
                message.addError(message.getValueBundle(e.getConstraint()));
            } else {
                message.addError(Bundles.getValueDefault("message.UniqueException"));
            }
        } catch (ConcurrentException e) {
            if (message.hasMessage("concurrentError")) {
                message.addError("concurrentError");
            } else {
                message.addError(Bundles.getValueDefault("message.ConcurrentException"));
            }
        } catch (Exception e) {
            logger.error("Update failed!", e);
        }
    }

    public void btnDeleteClick() {
        try {
            if (deleteValidate()) {
                delete(getSelectedEntity().getId());
            }
            if (getDataModel() != null) {
                getDataModel().reset();
            }
        } catch (ForeignKeyException e) {
            if (message.hasMessage(e.getConstraint())) {
                message.addError(message.getValueBundle(e.getConstraint()));
            } else {
                message.addError(Bundles.getValueDefault("message.ForeignKeyException"));
            }
        } catch (ConcurrentException e) {
            if (message.hasMessage("concurrentError")) {
                message.addError("concurrentError");
            } else {
                message.addError(Bundles.getValueDefault("message.ConcurrentException"));
            }
        } catch (Exception e) {
            logger.error("Delete failed!", e);
        }
    }

    public void btnDeleteManyClick() {
        try {
            if (deleteValidate()) {
                delete(getSelectedIds());
                getSelectedEntities().clear();
                getSelectedItems().clear();
            }
            if (getDataModel() != null) {
                getDataModel().reset();
            }
        } catch (ForeignKeyException e) {
            if (message.hasMessage(e.getConstraint())) {
                message.addError(message.getValueBundle(e.getConstraint()));
            } else {
                message.addError(Bundles.getValueDefault("message.ForeignKeyException"));
            }
        } catch (ConcurrentException e) {
            if (message.hasMessage("concurrentError")) {
                message.addError("concurrentError");
            } else {
                message.addError(Bundles.getValueDefault("message.ConcurrentException"));
            }
        } catch (Exception e) {
            logger.error("Delete Many failed!", e);
        }
    }

    protected boolean updateValidate() throws Exception {
        return true;
    }

    protected abstract void update() throws Exception;

    protected boolean deleteValidate() throws Exception {
        return true;
    }

    protected abstract void delete(Serializable... ids) throws Exception;

}
