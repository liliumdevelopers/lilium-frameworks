package com.sunecity.lilium.view;

import com.sunecity.lilium.core.util.EncrypterDecrypter;

import javax.servlet.http.Cookie;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

public final class Cookies {

    public static final int UNTIL_BROWSER_OPEN = -1;
    public static final int MINUTE = 60;
    public static final int HOUR = MINUTE * 60;
    public static final int DAY = HOUR * 24;
    public static final int WEEK = DAY * 7;
    public static final int MONTH = DAY * 30;
    public static final int YEAR = DAY * 365;
    public static final int MAXIMUM = Integer.MAX_VALUE;

    private Cookies() {
    }

    private static String encode(String string) {
        try {
            return URLEncoder.encode(string, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String decode(String string) {
        try {
            return URLDecoder.decode(string, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String encrypt(String string) {
        try {
            return EncrypterDecrypter.encrypt(string, "liliumcookie");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String decrypt(String string) throws GeneralSecurityException {
        return EncrypterDecrypter.decrypt(string, "liliumcookie");
    }

    public static void save(String name, String value, int age) {
        name = encode(name);
        value = encode(value);
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(age);
        JsfServlet.getResponse().addCookie(cookie);
    }

    public static void saveEncrypt(String name, String value, int age) {
        save(encrypt(name), encrypt(value), age);
    }

    public static void saveWithPath(String name, String value, int age, String path) {
        name = encode(name);
        value = encode(value);
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(age);
        cookie.setPath(path);
        JsfServlet.getResponse().addCookie(cookie);
    }

    public static void saveWithPathEncrypt(String name, String value, int age, String path) {
        saveWithPath(encrypt(name), encrypt(value), age, path);
    }

    public static void saveWithDomain(String name, String value, int age, String domain) {
        name = encode(name);
        value = encode(value);
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(age);
        cookie.setDomain(domain);
        JsfServlet.getResponse().addCookie(cookie);
    }

    public static void saveWithDomainEncrypt(String name, String value, int age, String domain) {
        saveWithDomain(encrypt(name), encrypt(value), age, domain);
    }

    public static void save(String name, String value, int age, String path, String domain) {
        name = encode(name);
        value = encode(value);
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(age);
        cookie.setPath(path);
        cookie.setDomain(domain);
        JsfServlet.getResponse().addCookie(cookie);
    }

    public static void saveEncrypt(String name, String value, int age, String path, String domain) {
        save(encrypt(name), encrypt(value), age, path, domain);
    }

    public static void delete(String name) {
        name = encode(name);
        Cookie[] cookies = JsfServlet.getRequest().getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals(name)) {
                    c.setMaxAge(0);
                    c.setValue(null);
                    JsfServlet.getResponse().addCookie(c);
                    return;
                }
            }
        }
    }

    public static void deleteEncrypted(String name) {
        delete(encrypt(name));
    }

    public static String get(String name) {
        name = encode(name);
        Cookie[] cookies = JsfServlet.getRequest().getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName() != null && c.getName().trim().length() != 0 && c.getName().equals(name) && c.getValue() != null && c.getValue().trim().length() != 0) {
                    return decode(c.getValue());
                }
            }
        }
        return null;
    }

    public static String getEncrypted(String name) {
        name = encrypt(name);
        name = encode(name);
        Cookie[] cookies = JsfServlet.getRequest().getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName() != null && c.getName().trim().length() != 0 && c.getName().equals(name) && c.getValue() != null && c.getValue().trim().length() != 0) {
                    try {
                        return decrypt(decode(c.getValue()));
                    } catch (Exception e) {
                        delete(name);
                    }
                }
            }
        }
        return null;
    }

    public static Map<String, String> getAll() {
        Map<String, String> map = new HashMap<>();
        Cookie[] cookies = JsfServlet.getRequest().getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                map.put(decode(c.getName()), decode(c.getValue()));
            }
        }
        return map;
    }

    public static Map<String, String> getAllEncrypted() {
        Map<String, String> map = new HashMap<>();
        Cookie[] cookies = JsfServlet.getRequest().getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                try {
                    map.put(decrypt(decode(c.getName())), decrypt(decode(c.getValue())));
                } catch (Exception e) {
                    delete(c.getName());
                }
            }
        }
        return map;
    }

}
