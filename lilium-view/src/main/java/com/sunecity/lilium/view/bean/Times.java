package com.sunecity.lilium.view.bean;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.sunecity.lilium.core.util.Numbers;

@Named
@SessionScoped
public class Times implements Serializable {

	private static final long serialVersionUID = -8936141792466847051L;

	public String getTime(String hours, String minutes, String seconds) {
		return hours + ":" + minutes;
	}

	@SuppressWarnings("deprecation")
	public String getTime(Date date) {
		if (date != null) {
			return Numbers.getTextLocale(String.valueOf(date.getHours())) + ":"
					+ Numbers.getTextLocale(String.valueOf(date.getMinutes()));
		}
		return null;
	}

}
