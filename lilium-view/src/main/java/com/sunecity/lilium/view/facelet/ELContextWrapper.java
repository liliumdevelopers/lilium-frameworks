package com.sunecity.lilium.view.facelet;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.el.FunctionMapper;
import javax.el.VariableMapper;
import javax.faces.FacesWrapper;
import java.util.Locale;

/**
 * <b>Title:</b> ELContextWrapper
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public class ELContextWrapper extends ELContext implements FacesWrapper<ELContext> {

    private ELContext elContext;

    public ELContextWrapper() {
    }

    public ELContextWrapper(ELContext elContext) {
        this.elContext = elContext;
    }

    @Override
    public ELContext getWrapped() {
        return elContext;
    }

    @Override
    public boolean isPropertyResolved() {
        return getWrapped().isPropertyResolved();
    }

    @Override
    public void setPropertyResolved(boolean resolved) {
        getWrapped().setPropertyResolved(resolved);
    }

    @Override
    public void putContext(@SuppressWarnings("rawtypes") Class key, Object contextObject) {
        getWrapped().putContext(key, contextObject);
    }

    @Override
    public Object getContext(@SuppressWarnings("rawtypes") Class key) {
        return getWrapped().getContext(key);
    }

    @Override
    public ELResolver getELResolver() {
        return getWrapped().getELResolver();
    }

    @Override
    public FunctionMapper getFunctionMapper() {
        return getWrapped().getFunctionMapper();
    }

    @Override
    public Locale getLocale() {
        return getWrapped().getLocale();
    }

    @Override
    public void setLocale(Locale locale) {
        getWrapped().setLocale(locale);
    }

    @Override
    public VariableMapper getVariableMapper() {
        return getWrapped().getVariableMapper();
    }

}
