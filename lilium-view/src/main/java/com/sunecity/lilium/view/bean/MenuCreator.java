package com.sunecity.lilium.view.bean;

import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.data.model.Menus;
import com.sunecity.lilium.view.JsfServlet;
import org.primefaces.model.menu.DefaultMenuColumn;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.MenuModel;

import javax.enterprise.context.Dependent;
import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by shahram on 4/4/16.
 */
@Dependent
public class MenuCreator implements Serializable {

    private List<Menus> menus;

    public MenuModel megaMenu(List<Menus> menus, Predicate<Menus> showPredicate, Predicate<Menus> disabledPredicate) {
        this.menus = menus;
        MenuModel menuModel = new DefaultMenuModel();
        menuModel.getElements().add(getHome());
        getRootChildren().stream().filter(showPredicate).forEach(menuEntity -> {
            DefaultSubMenu menu = new DefaultSubMenu();
            menu.setLabel(menuEntity.getLabel());
            if (menuEntity.getIcon() != null) {
                menu.setIcon(menuEntity.getIcon());
            }
            Map<String, DefaultMenuColumn> columns = new HashMap<>();
            getChildren(menuEntity).stream().filter(showPredicate).forEach(subMenuEntity -> {
                DefaultSubMenu subMenu = new DefaultSubMenu();
                subMenu.setLabel(subMenuEntity.getLabel());
                DefaultMenuColumn column;
                String columnNumber = subMenuEntity.getDescription();
                if (columnNumber != null && columnNumber.trim().length() != 0 && columns.containsKey(columnNumber)) {
                    column = columns.get(columnNumber);
                } else {
                    column = new DefaultMenuColumn();
                    columns.put(columnNumber, column);
                }
                getChildren(subMenuEntity).stream().filter(showPredicate).forEach(menuItemEntity -> {
                    DefaultMenuItem menuItem = new DefaultMenuItem();
                    menuItem.setUrl(menuItemEntity.getPath().startsWith("http") ? menuItemEntity.getPath() : "/" + JsfServlet.getContextName() + menuItemEntity.getPath());
                    menuItem.setValue(menuItemEntity.getLabel());
                    if (menuItemEntity.getTarget() != null) {
                        menuItem.setTarget(menuItemEntity.getTarget());
                    }
                    if (menuItemEntity.getIcon() != null) {
                        menuItem.setIcon(menuItemEntity.getIcon());
                    }
                    menuItem.setDisabled(menuItemEntity.isDisabled() || disabledPredicate.test(menuItemEntity));
                    subMenu.getElements().add(menuItem);
                });
                column.getElements().add(subMenu);
            });
            columns.forEach((k, v) -> menu.getElements().add(v));
            menuModel.getElements().add(menu);
        });
        return menuModel;
    }

    public MenuModel menuBar(List<Menus> menus, Predicate<Menus> showPredicate, Predicate<Menus> disabledPredicate) {
        this.menus = menus;
        MenuModel menuModel = new DefaultMenuModel();
        menuModel.getElements().add(getHome());
        recursiveMenu(menuModel, null, getRootChildren(), showPredicate, disabledPredicate);
        return menuModel;
    }

    private void recursiveMenu(MenuModel menuModel, DefaultSubMenu defaultSubMenu, List<Menus> menus, Predicate<Menus> showPredicate, Predicate<Menus> disabledPredicate) {
        menus.stream().filter(showPredicate).forEach(menu -> {
            if (hasChildren(menu)) {
                DefaultSubMenu subMenu = new DefaultSubMenu();
                subMenu.setLabel(menu.getLabel());
                if (menu.getIcon() != null) {
                    subMenu.setIcon(menu.getIcon());
                }
                if (menuModel != null) {
                    menuModel.getElements().add(subMenu);
                } else {
                    defaultSubMenu.getElements().add(subMenu);
                }
                recursiveMenu(null, subMenu, getChildren(menu), showPredicate, disabledPredicate);
            } else {
                DefaultMenuItem menuItem = new DefaultMenuItem();
                menuItem.setUrl(menu.getPath().startsWith("http") ? menu.getPath() : "/" + JsfServlet.getContextName() + menu.getPath());
                menuItem.setValue(menu.getLabel());
                if (menu.getTarget() != null) {
                    menuItem.setTarget(menu.getTarget());
                }
                if (menu.getIcon() != null) {
                    menuItem.setIcon(menu.getIcon());
                }
                menuItem.setDisabled(menu.isDisabled() || disabledPredicate.test(menu));
                if (menuModel != null) {
                    menuModel.getElements().add(menuItem);
                } else {
                    defaultSubMenu.getElements().add(menuItem);
                }
            }
        });
    }

    private MenuItem getHome() {
        DefaultMenuItem home = new DefaultMenuItem();
        home.setId("itmHome");
        home.setCommand("/pages/home.xhtml?faces-redirect=true");
        home.setValue(Bundles.getValueDefault("homePage"));
        home.setIcon("fa fa-home");
        return home;
    }

    private boolean hasChildren(Menus menu) {
        return menus.stream().anyMatch(m -> m.getParentId() != null && m.getParentId().equals(menu.getId()));
    }

    private List<Menus> getChildren(Menus menu) {
        return menus.stream().filter(m -> m.getParentId() != null && m.getParentId().equals(menu.getId())).sorted(Comparator.comparing(Menus::getPriority)).collect(Collectors.toList());
    }

    private List<Menus> getRootChildren() {
        return menus.stream().filter(m -> m.getParentId() == null).sorted(Comparator.comparing(Menus::getPriority)).collect(Collectors.toList());
    }

}
