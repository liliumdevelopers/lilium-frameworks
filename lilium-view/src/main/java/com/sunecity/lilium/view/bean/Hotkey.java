package com.sunecity.lilium.view.bean;

import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;

/**
 * Created by shahram on 2/18/15.
 */
public class Hotkey extends org.primefaces.component.hotkey.Hotkey {

    private ActionListener actionListener;

    public void setActionListener(ActionListener actionListener) {
        this.actionListener = actionListener;
    }

    public void hotkeyActionListener(ActionEvent actionEvent) {
        if (actionListener != null) {
            actionListener.processAction(actionEvent);
        }
    }

}
