package com.sunecity.lilium.view.model;

import com.querydsl.core.types.Path;
import com.sunecity.lilium.data.field.Field;
import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.core.util.EncrypterDecrypter;
import com.sunecity.lilium.view.Cookies;
import com.sunecity.lilium.view.JsfServlet;
import com.sunecity.lilium.view.converter.FieldConverter;
import org.primefaces.component.api.DynamicColumn;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * <b>Title:</b> AbstractDataModel
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public abstract class AbstractDataModel<T extends BaseIdentity<?>> extends LazyDataModel<T> {

    private static final long serialVersionUID = 1668005175992199686L;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    //	private DataTable dataTable;
    protected Map<Path<?>, Boolean> sorts;
    private List<T> entities;
    private LoadCount loadCount = LoadCount.LOAD_100;
    private int first;
    private int lastFirst = 0;
    private int rowsPerPage;
    private List<DataModelField> fields;
    private List<Field> selectedFields;
    private Field selectedField;
    private FieldConverter fieldConverter;

    public AbstractDataModel() {
    }

    public AbstractDataModel(List<DataModelField> fields) {
        this.fields = fields;
    }

//	public DataTable getDataTable() {
//		if (dataTable == null) {
//			dataTable = new DataTable();
//		}
//		return dataTable;
//	}
//
//	public void setDataTable(DataTable dataTable) {
//		this.dataTable = dataTable;
//	}

    protected List<Field> getFields() {
        List<Field> fields = new ArrayList<>();
        if (this.fields != null) {
            fields.addAll(this.fields.stream().map(DataModelField::getField).collect(Collectors.toList()));
        }
        return fields;
    }

    public List<Field> getSearches() {
        List<Field> fields = new ArrayList<>();
        if (this.fields != null) {
            fields.addAll(this.fields.stream().filter(DataModelField::isSearch).map(DataModelField::getField).collect(Collectors.toList()));
        }
        return fields;
    }

    public List<Field> getExports() {
        List<Field> fields = new ArrayList<>();
        if (this.fields != null) {
            fields.addAll(this.fields.stream().filter(DataModelField::isExport).map(DataModelField::getField).collect(Collectors.toList()));
            fields.forEach(f -> f.setName(getNamePath(f.getPath())));
        }
        return fields;
    }

    public List<Field> getColumns() {
        List<Field> fields = new ArrayList<>();
        if (this.fields != null) {
            fields.addAll(this.fields.stream().filter(DataModelField::isColumn).map(DataModelField::getField).collect(Collectors.toList()));
        }
        return fields;
    }

    public List<Field> getInfos() {
        List<Field> fields = new ArrayList<>();
        if (this.fields != null) {
            fields.addAll(this.fields.stream().filter(DataModelField::isInfo).map(DataModelField::getField).collect(Collectors.toList()));
        }
        return fields;
    }

    public List<Field> getSelectedFields() {
        return selectedFields;
    }

    public void setSelectedFields(List<Field> selectedFields) {
        this.selectedFields = selectedFields;
    }

    public Field getSelectedField() {
        return selectedField;
    }

    public void setSelectedField(Field selectedField) {
        this.selectedField = selectedField;
    }

    public FieldConverter getFieldConverter() {
        List<Field> fields = new ArrayList<>();
        if (this.fields != null) {
            fields.addAll(this.fields.stream().map(DataModelField::getField).collect(Collectors.toList()));
        }
        fieldConverter = new FieldConverter(fields);
        return fieldConverter;
    }

    public void setFieldConverter(FieldConverter fieldConverter) {
        this.fieldConverter = fieldConverter;
    }

    public int getRowsPerPage() {
        try {
            String rows = getCookieProperty(Cookies.get("rows." + JsfServlet.getRequest().getServerName() + JsfServlet.getRequest().getContextPath()), this.getClass().getName());
            if (rows != null && rows.trim().length() > 0) {
                rowsPerPage = Integer.valueOf(rows);
            } else {
                rowsPerPage = 10;
            }
        } catch (Exception e) {
            e.printStackTrace();
            rowsPerPage = 10;
        }
        return rowsPerPage;
    }

    public void setRowsPerPage(int rowsPerPage) {
        try {
            Cookies.save("rows." + JsfServlet.getRequest().getServerName() + JsfServlet.getRequest().getContextPath(),
                    addCookieProperty(Cookies.get("rows." + JsfServlet.getRequest().getServerName() + JsfServlet.getRequest().getContextPath()), this.getClass().getName(), String.valueOf(rowsPerPage)), Cookies.MAXIMUM);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.rowsPerPage = rowsPerPage;
    }

    @Override
    public List<T> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        this.first = first;
        setRowsPerPage(pageSize);
        try {
            beforePaginate(((first % loadCount.getCount()) / pageSize) + 1, pageSize);
            if (getRowCount() == 0) {
                setRowCount(loadRowCount());
            }
//            if (shouldQuery()) {
            if (multiSortMeta != null) {
                sorts = new HashMap<>();
                for (SortMeta sortMeta : multiSortMeta) {
                    sorts.put(getFields().get(((DynamicColumn) sortMeta.getColumn()).getIndex()).getPath(), sortMeta.getSortOrder() == SortOrder.ASCENDING);
                }
            }
            entities = loadEntities(firstResult(), maxResult());
            load(firstResult(), maxResult());
//            }
            afterPaginate(entities.subList(0, first % loadCount.getCount()), getCurrentPageEntities(), ((first % loadCount.getCount()) / pageSize) + 1, pageSize);
        } catch (Exception e) {
            logger.error("loading data failed!", e);
        }
        lastFirst = first;
        if (entities != null && entities.size() > 0) {
            return getCurrentPageEntities();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public T getRowData(String rowKey) {
        return entities.stream().filter(e -> e.getId().equals(rowKey)).findFirst().orElse(null);
    }

    @Override
    public Object getRowKey(T t) {
        return t.getId();
    }

    public void reset() {
//		if (dataTable != null) {
//			dataTable.setValueExpression("sortBy", null);
//		}
        sorts = new HashMap<>();
        setRowCount(0);
        entities = null;
    }

    public LoadCount getLoadCount() {
        return loadCount;
    }

    public void setLoadCount(LoadCount loadCount) {
        this.loadCount = loadCount;
    }

    private int firstResult() {
        return (first / loadCount.getCount()) * loadCount.getCount();
    }

    private int maxResult() {
        return loadCount.getCount();
    }

    private boolean shouldQuery() {
        return (entities == null) || (first == lastFirst) || (first / loadCount.getCount() != lastFirst / loadCount.getCount());
    }

    private List<T> getCurrentPageEntities() {
        try {
            return entities.subList(first % loadCount.getCount(), first % loadCount.getCount() + rowsPerPage);
        } catch (IndexOutOfBoundsException e) {
            return entities.subList(first % loadCount.getCount(), first % loadCount.getCount() + (getRowCount() % rowsPerPage));
        }
    }

    private String getCookieProperty(String cookie, String key) throws Exception {
        if (cookie != null && cookie.trim().length() > 0) {
            Properties properties = new Properties();
            ByteArrayInputStream inputStream = new ByteArrayInputStream(cookie.getBytes());
            properties.load(inputStream);
            inputStream.close();
            return EncrypterDecrypter.decrypt(properties.getProperty(EncrypterDecrypter.encrypt(key, "sunecity")), "sunecity");
        }
        return null;
    }

    private String addCookieProperty(String oldCookie, String key, String value) throws Exception {
        Properties properties = new Properties();
        if (oldCookie != null && oldCookie.trim().length() > 0) {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(oldCookie.getBytes());
            properties.load(inputStream);
            inputStream.close();
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        properties.setProperty(EncrypterDecrypter.encrypt(key, "sunecity"), EncrypterDecrypter.encrypt(value, "sunecity"));
        properties.store(outputStream, null);
        String newCookie = new String(outputStream.toByteArray());
        outputStream.close();
        return newCookie;
    }

    public void beforePaginate(int currentPage, int pageSize) throws Exception {

    }

    public void afterPaginate(List<T> headerEntities, List<T> currentPageEntities, int currentPage, int pageSize) throws Exception {

    }

    public void load(int first, int max) throws Exception {
    }

    public void btnSearchClick() {
        try {
            search();
            reset();
        } catch (Exception e) {
            logger.error("Search failed!", e);
        }
    }

    public abstract void search() throws Exception;

    public abstract List<T> loadEntities(int first, int max) throws Exception;

    public abstract int loadRowCount() throws Exception;

    private String getNamePath(Path<?> path) {
        final StringBuilder sb = new StringBuilder(path.getMetadata().getName());
        while ((path = path.getMetadata().getParent()) != null) {
            if (path.getMetadata().getParent() != null) {
                sb.insert(0, path.getMetadata().getElement() + ".");
            }
        }
        return sb.toString();
    }

}
