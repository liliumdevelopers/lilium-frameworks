package com.sunecity.lilium.view.localization;

import java.util.HashMap;
import java.util.ListResourceBundle;
import java.util.Map;

public class Model extends ListResourceBundle {

    private Map<String, Object> properties = new HashMap<>();

    @Override
    protected Object[][] getContents() {
        return properties.entrySet().stream().toArray(Object[][]::new);
    }

    protected void add(String key, Object value) {
        properties.put(key, value);
    }

}
