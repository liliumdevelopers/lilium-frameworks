package com.sunecity.lilium.view.abstractview;

import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.view.Cookies;
import com.sunecity.lilium.view.JsfServlet;
import com.sunecity.lilium.view.Sessions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractLogin extends AbstractView {

    private static final long serialVersionUID = 919082469513552570L;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private boolean remember;
    private boolean cookieSet;

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }

    public boolean isCookieSet() {
        if (Cookies.getEncrypted("username." + JsfServlet.getRequest().getServerName() + JsfServlet.getRequest().getContextPath()) != null) {
            cookieSet = true;
        }
        return cookieSet;
    }

    public void setCookieSet(boolean cookieSet) {
        this.cookieSet = cookieSet;
    }

    protected void notActive() {
        message.addError(Bundles.getValueLocale("com.sunecity.lilium.core.localization.bundle", "notActive"));
    }

    protected void wrongUserpass() {
        message.addError(Bundles.getValueLocale("com.sunecity.lilium.core.localization.bundle", "userpassWrong"));
    }

    protected void duplicateLogin() {
        message.addError(Bundles.getValueLocale("com.sunecity.lilium.core.localization.bundle", "duplicateLogin"));
    }

    protected void saveCookieUsername(String username, int age) {
        Cookies.saveEncrypt("username." + JsfServlet.getRequest().getServerName() + JsfServlet.getRequest().getContextPath(), username, age);
    }

    public String getCookieUsername() {
        return Cookies.getEncrypted("username." + JsfServlet.getRequest().getServerName() + JsfServlet.getRequest().getContextPath());
    }

    protected String getLastUrl() {
        Object url = Sessions.get("url-dispatch");
        if (url != null) {
            Sessions.delete("url-dispatch");
            return url.toString() + "?faces-redirect=true";
        }
        return null;
    }

    public String btnResetClick() {
        Cookies.deleteEncrypted("username." + JsfServlet.getRequest().getServerName() + JsfServlet.getRequest().getContextPath());
        setCookieSet(false);
        return null;
    }

    public String btnLoginClick() {
        try {
            return login();
        } catch (Exception e) {
            logger.error("Login failed!", e);
            return null;
        }
    }

    protected abstract String login() throws Exception;

}
