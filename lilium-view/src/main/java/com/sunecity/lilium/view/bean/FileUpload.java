package com.sunecity.lilium.view.bean;

import org.primefaces.event.FileUploadEvent;
import com.sunecity.lilium.core.model.file.ArrayFile;
import org.primefaces.model.UploadedFile;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shahram on 2/23/15.
 */
@Named
@ViewScoped
public class FileUpload implements Serializable {

    private UploadedFile uploadedFile;
    private ArrayFile file;
    private List<ArrayFile> files = new ArrayList<>();

    public void upload(FileUploadEvent fileUploadEvent) {
        try {
            UploadedFile uploadedFile = fileUploadEvent.getFile();
            InputStream inputStream = uploadedFile.getInputstream();
            byte[] bs = new byte[inputStream.available()];
            inputStream.read(bs);
            inputStream.close();
            files.add(new ArrayFile(uploadedFile.getFileName(), uploadedFile.getContentType(), uploadedFile.getSize(), bs){});
            file = new ArrayFile(uploadedFile.getFileName(), uploadedFile.getContentType(), uploadedFile.getSize(), bs){};
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public ArrayFile getFile() {
        if (file != null) {
            return file;
        }
        if (uploadedFile != null) {
            try {
                InputStream inputStream = uploadedFile.getInputstream();
                byte[] bs = new byte[inputStream.available()];
                inputStream.read(bs);
                inputStream.close();
                return new ArrayFile(uploadedFile.getFileName(), uploadedFile.getContentType(), uploadedFile.getSize(), bs){};
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<ArrayFile> getFiles() {
        return files;
    }

    public void setFiles(List<ArrayFile> files) {
        this.files = files;
    }

}
