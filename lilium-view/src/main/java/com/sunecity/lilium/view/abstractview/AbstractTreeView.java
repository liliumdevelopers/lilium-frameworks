package com.sunecity.lilium.view.abstractview;

import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.view.model.AbstractDataModel;
import com.sunecity.lilium.view.util.Trees;
import org.primefaces.model.TreeNode;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractTreeView<T extends BaseIdentity<?>> extends AbstractBase<T> {

    private static final long serialVersionUID = -4766038416997833057L;

    @Inject
    protected Trees trees;

    private TreeNode root;

    private TreeNode[] selectedTreeNodes;
    private TreeNode selectedTreeNode;

    private String searchValue;

    public AbstractTreeView() {
    }

    @Override
    public AbstractDataModel<?> dataModel() throws Exception {
        return null;
    }

    protected abstract TreeNode root();

    public TreeNode getRoot() {
        if (root == null) {
            root = root();
        }
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode[] getSelectedTreeNodes() {
        return selectedTreeNodes;
    }

    public void setSelectedTreeNodes(TreeNode[] selectedTreeNodes) {
        this.selectedTreeNodes = selectedTreeNodes;
    }

    public TreeNode getSelectedTreeNode() {
        return selectedTreeNode;
    }

    public void setSelectedTreeNode(TreeNode selectedTreeNode) {
        this.selectedTreeNode = selectedTreeNode;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public T getSelectedEntity() {
        if (selectedTreeNode != null) {
            return (T) selectedTreeNode.getData();
        }
        return super.getSelectedEntity();
    }

    public List<T> getSelectedEntities() {
        if (selectedTreeNodes != null) {
            return Arrays.stream(selectedTreeNodes).map(t -> (T) t.getData()).collect(Collectors.toList());
        }
        return super.getSelectedEntities();
    }

    public void btnSearchClick() {
        try {
            root = trees.search(root, t -> {
                try {
                    return searchValue != null && search(searchValue).test((T) t);
                } catch (Exception e) {
                    logger.error("Search failed!", e);
                    return false;
                }
            });
        } catch (Exception e) {
            logger.error("Search failed!", e);
        }
    }

    public void reset() throws Exception {
        root = null;
        searchValue = null;
    }

    public boolean hasFound(T t) {
        try {
            return searchValue != null && search(searchValue).test(t);
        } catch (Exception e) {
            logger.error("Finding failed!", e);
            return false;
        }
    }

    protected abstract Predicate<T> search(String value) throws Exception;

}