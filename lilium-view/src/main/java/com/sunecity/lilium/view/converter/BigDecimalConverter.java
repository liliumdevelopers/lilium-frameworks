package com.sunecity.lilium.view.converter;

import com.sunecity.lilium.core.util.Numbers;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import java.math.BigDecimal;

@FacesConverter(forClass = BigDecimal.class)
public class BigDecimalConverter extends org.primefaces.convert.BigDecimalConverter {

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String number = super.getAsString(context, component, value);
        if (number.endsWith(".00")) {
            number = number.substring(0, number.length() - 3);
        }
        return Numbers.getTextLocale(number);
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        value = Numbers.getTextEnglish(value);
        return super.getAsObject(context, component, value);
    }

}
