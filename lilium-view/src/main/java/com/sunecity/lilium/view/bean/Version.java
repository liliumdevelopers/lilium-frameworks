package com.sunecity.lilium.view.bean;

import com.sunecity.lilium.view.JsfServlet;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.InputStream;
import java.io.Serializable;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

@Named
@ApplicationScoped
public class Version implements Serializable {

    private static final long serialVersionUID = 8394870181485297508L;

    private String version;

    public String getVersion() {
        if (version == null) {
            try (InputStream in = JsfServlet.getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF")) {
                Manifest manifest = new Manifest(in);
                Attributes attributes = manifest.getMainAttributes();
                String v = attributes.getValue("Implementation-Version");
                if (v == null) {
                    v = attributes.getValue("Specification-Version");
                }
                version = v != null ? " version: " + v : "version 1.0.0 Demo";
            } catch (Exception e) {
                version = "version 1.0.0 Demo";
            }
        }
        return version;
    }

}
