package com.sunecity.lilium.view.abstractview;

import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.view.Sessions;
import com.sunecity.lilium.view.model.AbstractDataModel;
import com.sunecity.lilium.view.model.DataModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractBase<T extends BaseIdentity<?>> extends AbstractView {

    private static final long serialVersionUID = -3381310584139913019L;

    private AbstractDataModel<?> dataModel;
    private Map<Serializable, Boolean> selectedItems;
    private List<T> selectedEntities;
    private T selectedEntity;

    protected AbstractDataModel<?> dataModel() throws Exception {
        return new DataModel<>();
    }

    public void setDataModelNull() {
        dataModel = null;
    }

    public AbstractDataModel<?> getDataModel() {
        if (dataModel == null) {
            try {
                dataModel = dataModel();
            } catch (Exception e) {
                logger.error("DataModel creation failed!", e);
            }
        }
        return dataModel;
    }

    public Map<Serializable, Boolean> getSelectedItems() {
        if (selectedItems == null) {
            selectedItems = new HashMap<>();
        }
        return selectedItems;
    }

    public void setSelectedItems(Map<Serializable, Boolean> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public Serializable[] getSelectedIds() {
        return getSelectedItems().entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).toArray(Serializable[]::new);
    }

    public List<T> getSelectedEntities() {
        if (selectedEntities == null) {
            selectedEntities = new ArrayList<>();
        }
        return selectedEntities;
    }

    public void setSelectedEntities(List<T> selectedEntities) {
        this.selectedEntities = selectedEntities;
        if (selectedEntities != null) {
            selectedEntities.forEach(e -> getSelectedItems().put(e, true));
        }
    }

    public T getSelectedEntity() {
        return selectedEntity;
    }

    public void setSelectedEntity(T selectedEntity) {
        this.selectedEntity = selectedEntity;
    }

    public void selectListener(T t) {
        String value = String.valueOf(getSelectedItems().get(t.getId()));
        if (value != null && Boolean.valueOf(value)) {
            getSelectedEntities().add(t);
        } else {
            getSelectedEntities().remove(t);
        }
    }

    public void selectClear() {
        getSelectedEntities().clear();
        getSelectedItems().clear();
    }

    public void btnCutClick() {
        Sessions.save(getClass().getName(), getSelectedEntity());
        try {
            cut(getSelectedEntity());
        } catch (Exception e) {
            logger.error("Cut failed!", e);
        }
    }

    public void btnCopyClick() {
        Sessions.save(getClass().getName(), getSelectedEntity());
        try {
            copy(getSelectedEntity());
        } catch (Exception e) {
            logger.error("Copy failed!", e);
        }
    }

    public void btnCopyManyClick() {
        Sessions.save(getClass().getName(), getSelectedEntities());
        try {
            copyMany(getSelectedEntities());
        } catch (Exception e) {
            logger.error("Copy many failed!", e);
        }
    }

    @SuppressWarnings("unchecked")
    public void btnPasteClick() {
        try {
            Object object = Sessions.get(getClass().getName());
            if (object != null) {
                paste((T) object);
            }
        } catch (Exception e) {
            logger.error("Paste failed!", e);
        }
    }

    @SuppressWarnings("unchecked")
    public void btnPasteManyClick() {
        try {
            Object object = Sessions.get(getClass().getName());
            if (object != null) {
                pasteMany((List<T>) object);
            }
        } catch (Exception e) {
            logger.error("Paste many failed!", e);
        }
    }

    protected void cut(T t) throws Exception {
    }

    protected void copy(T t) throws Exception {
    }

    protected void copyMany(List<T> ts) throws Exception {
    }

    protected void paste(T t) throws Exception {
    }

    protected void pasteMany(List<T> ts) throws Exception {
    }

}
