package com.sunecity.lilium.view;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.DeviceType;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import org.primefaces.PrimeFaces;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Objects;

@Named
@SessionScoped
public class ClientInfo implements Serializable {

    private static final long serialVersionUID = 577672835303993840L;

    public String getClientInfo() {
        return JsfServlet.getRequest().getHeader("user-agent");
    }

    public OperatingSystem getOperatingSystem() {
        UserAgent userAgent = new UserAgent(getClientInfo());
        return userAgent.getOperatingSystem();
    }

    public Browser getBrowser() {
        UserAgent userAgent = new UserAgent(getClientInfo());
        return userAgent.getBrowser();
    }

    public Version getBrowserVersion() {
        UserAgent userAgent = new UserAgent(getClientInfo());
        return userAgent.getBrowserVersion();
    }

    public DeviceType getDeviceType() {
        UserAgent userAgent = new UserAgent(getClientInfo());
        return userAgent.getOperatingSystem().getDeviceType();
    }

    public String getIp() {
        String ip = JsfServlet.getRequest().getHeader("X-Forwarded-For");
        if (ip == null || ip.trim().length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = JsfServlet.getRequest().getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.trim().length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = JsfServlet.getRequest().getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.trim().length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = JsfServlet.getRequest().getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.trim().length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = JsfServlet.getRequest().getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.trim().length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = JsfServlet.getRequest().getRemoteAddr();
        }
        return ip;
    }

    public void check() {
        Browser browser = getBrowser();
        Version version = getBrowserVersion();
        if (!((Objects.equals(browser.getGroup().getName(), Browser.FIREFOX.getName()) && new Version("46.0", null, null).compareTo(version) <= 0)
                || (Objects.equals(browser.getGroup().getName(), Browser.CHROME.getName()) && new Version("52.0", null, null).compareTo(version) <= 0)
                || (Objects.equals(browser.getGroup().getName(), Browser.OPERA.getName()) && new Version("38.0", null, null).compareTo(version) <= 0)
                || (Objects.equals(browser.getGroup().getName(), Browser.EDGE.getName()) && new Version("38.0", null, null).compareTo(version) <= 0)
                || (Objects.equals(browser.getGroup().getName(), Browser.SAFARI.getName())) && new Version("9.0", null, null).compareTo(version) <= 0)) {
            PrimeFaces.current().executeScript("PF('dlgBrowser').show()");
        }
    }

}
