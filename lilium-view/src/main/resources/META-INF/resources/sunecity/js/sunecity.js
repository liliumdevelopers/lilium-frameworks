function openDialog(xhr, status, args, widgetShow) {
    if ((!args.validationFailed) && (!args.error)) {
        PF(widgetShow).show();
    }
}

function openValidDialog(xhr, status, args, widgetShow1, widgetShow2) {
    if ((!args.validationFailed) && (!args.error)) {
        PF(widgetShow1).show();
    } else {
        PF(widgetShow2).show();
    }
}

function closeDialog(xhr, status, args, widgetHide) {
    if (args.validationFailed || args.error) {
    } else {
        PF(widgetHide).hide();
    }
}

function closeAndOpenDialog(xhr, status, args, widgetHide, widgetShow) {
    if (args.validationFailed || args.error) {
        jQuery('#' + widgetHide.id).effect("shake", {
            times: 2
        }, 100);
    } else {
        PF(widgetHide).hide();
        PF(widgetShow).show();
    }
}

function closeGrowl() {
    PF('wdgGrowl').hide();
}

function showFatal(xhr, status, args, widgetVar) {
    if (args.fatal) {
        PF(widgetVar).show();
    }
}

function showPopup(url) {
    window
        .open(url, 'ok',
        'height=700,width=1000,scrollbars=yes,resizable=yes');
}

function openTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}

function handleDrop(event, ui) {
    var droppedCar = ui.draggable;
    droppedCar.fadeOut('fast');
}

function statusStart() {
    dlgStatus.show();
}

function statusStop() {
    dlgStatus.hide();
}

function exportChart(chart) {
    $(document.getElementById(chart.id + '_output')).empty().append(
        chart.exportAsImage());
}

function downloadChart(chart) {
    img = chart.exportAsImage();
    document.getElementById(chart.id + '_ept').value = img.src;
}

function scrollTo(pixel) {
    $("html, body").animate({scrollTop: pixel}, 700);
}

PrimeFacesExt.locales.TimePicker['fa'] = {
    hourText: 'ساعت',
    minuteText: 'دقیقه',
    amPmText: ['صبح', 'عصر'],
    closeButtonText: 'تایید',
    nowButtonText: 'الان',
    deselectButtonText: 'لغو'
};

PrimeFaces.locales ['fa'] = {
    messages: {
        'javax.faces.component.UIInput.CONVERSION': 'خطا در تبدیل مقادیر\!',
        'javax.faces.component.UIInput.REQUIRED': 'مقدار نمیتواند خالی باشد.',
        'javax.faces.component.UIInput.UPDATE': 'خطا در پردازش اطلاعات وارد شده\!',
        'javax.faces.component.UISelectOne.INVALID': 'مقدار صحیح نمیباشد.',
        'javax.faces.component.UISelectMany.INVALID': 'مقدار صحیح نمیباشد.'
    }
};