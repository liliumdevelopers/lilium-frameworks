package com.sunecity.lilium.report.jasper;

import com.sunecity.lilium.data.report.Border;
import com.sunecity.lilium.data.report.DynamicReport;
import com.sunecity.lilium.data.report.Style;
import com.sunecity.lilium.data.report.enums.BorderType;
import com.sunecity.lilium.data.report.enums.HorizontalAlign;
import com.sunecity.lilium.data.report.enums.VerticalAlign;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.MarginBuilder;
import net.sf.dynamicreports.report.builder.style.BorderBuilder;
import net.sf.dynamicreports.report.builder.style.PaddingBuilder;
import net.sf.dynamicreports.report.builder.style.PenBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.LineSpacing;
import net.sf.dynamicreports.report.constant.LineStyle;
import net.sf.dynamicreports.report.constant.Rotation;
import net.sf.dynamicreports.report.constant.VerticalTextAlignment;

import java.awt.Color;

/**
 * @author Shahram Goodarzi
 */
public class ReportStyles {

    private ReportStyles() {
    }

    public static Style titleStyle() {
        Style style = new Style();
        style.setFontSize(18);
        style.setBold(true);
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        return style;
    }

    private static StyleBuilder titleStyle(Style style) {
//        StyleBuilder styleBuilder = DynamicReports.stl.style();
//        if (style.getFontSize() == null) {
//            styleBuilder.setFontSize(18);
//        } else {
//            styleBuilder.setFontSize(style.getFontSize());
//        }
//
//        if (style.isBold() == null) {
//            styleBuilder.setBold(true);
//        } else {
//            styleBuilder.setBold(style.isBold());
//        }
//
//        if (style.getHorizontalAlign() == null) {
//            styleBuilder.setHorizontalAlignment(HorizontalAlignment.CENTER);
//        } else {
//            switch (style.getHorizontalAlign()) {
//                case LEFT:
//                    styleBuilder.setHorizontalAlignment(HorizontalAlignment.LEFT);
//                    break;
//                case RIGHT:
//                    styleBuilder.setHorizontalAlignment(HorizontalAlignment.RIGHT);
//                    break;
//                case JUSTIFIED:
//                    styleBuilder.setHorizontalAlignment(HorizontalAlignment.JUSTIFIED);
//                default:
//                    styleBuilder.setHorizontalAlignment(HorizontalAlignment.CENTER);
//                    break;
//            }
//        }
//
//        if (style.getVerticalAlign() == null) {
//            styleBuilder.setVerticalAlignment(VerticalAlignment.MIDDLE);
//        } else {
//            switch (style.getVerticalAlign()) {
//                case BOTTOM:
//                    styleBuilder.setVerticalAlignment(VerticalAlignment.BOTTOM);
//                    break;
//                case TOP:
//                    styleBuilder.setVerticalAlignment(VerticalAlignment.TOP);
//                    break;
//                default:
//                    styleBuilder.setVerticalAlignment(VerticalAlignment.MIDDLE);
//            }
//        }
//
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setPadding(DynamicReports.stl.padding().setRight(10).setLeft(10));
        styleBuilder.setFont(DynamicReports.stl.font().bold().setFontSize(18));
        return styleBuilder;
    }

    public static Style subtitleStyle() {
        Style style = new Style();
        style.setHorizontalAlign(HorizontalAlign.RIGHT);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        return style;
    }

    private static StyleBuilder subtitleStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setPadding(DynamicReports.stl.padding().setRight(10).setLeft(10));
        return styleBuilder;
    }

    public static Style headerStyle() {
        Style style = new Style();
        style.setBold(true);
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        return style;
    }

    private static StyleBuilder headerStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setBold(true);
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        return styleBuilder;
    }

    public static Style footerStyle() {
        Style style = new Style();
        style.setBold(true);
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        return style;
    }

    private static StyleBuilder footerStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setBold(true);
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        return styleBuilder;
    }

    public static Style summaryStyle() {
        Style style = new Style();
        style.setBold(true);
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        return style;
    }

    private static StyleBuilder summaryStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setBold(true);
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        return styleBuilder;
    }

    public static Style columnSummaryStyle() {
        Style style = new Style();
        style.setBold(true);
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        style.setBorderTop(new Border(BorderType.SOLID, 1f));
        style.setBorderLeft(new Border(BorderType.SOLID, 1f));
        style.setBorderRight(new Border(BorderType.SOLID, 1f));
        style.setBorderBottom(new Border(BorderType.SOLID, 1f));
        return style;
    }

    private static StyleBuilder columnSummaryStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setBold(true);
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        styleBuilder.setBottomBorder(DynamicReports.stl.penThin());
        styleBuilder.setLeftBorder(DynamicReports.stl.penThin());
        styleBuilder.setRightBorder(DynamicReports.stl.penThin());
        styleBuilder.setTopBorder(DynamicReports.stl.penThin());
        return styleBuilder;
    }

    public static Style columnSummaryLabelStyle() {
        Style style = new Style();
        style.setBold(true);
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        return style;
    }

    private static StyleBuilder columnSummaryLabelStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setBold(true);
        styleBuilder.setBottomBorder(DynamicReports.stl.pen1Point());
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        styleBuilder.setBottomBorder(DynamicReports.stl.penThin());
        styleBuilder.setLeftBorder(DynamicReports.stl.penThin());
        styleBuilder.setRightBorder(DynamicReports.stl.penThin());
        styleBuilder.setTopBorder(DynamicReports.stl.penThin());
        return styleBuilder;
    }

    public static Style columnSubtotalStyle() {
        Style style = new Style();
        style.setBold(true);
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        return style;
    }

    private static StyleBuilder columnSubtotalStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setBold(true);
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        styleBuilder.setBottomBorder(DynamicReports.stl.penThin());
        styleBuilder.setLeftBorder(DynamicReports.stl.penThin());
        styleBuilder.setRightBorder(DynamicReports.stl.penThin());
        styleBuilder.setTopBorder(DynamicReports.stl.penThin());
        return styleBuilder;
    }

    public static Style columnSubtotalLabelStyle() {
        Style style = new Style();
        style.setBold(true);
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        return style;
    }

    private static StyleBuilder columnSubtotalLabelStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setBold(true);
        styleBuilder.setBottomBorder(DynamicReports.stl.pen1Point());
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        styleBuilder.setBottomBorder(DynamicReports.stl.penThin());
        styleBuilder.setLeftBorder(DynamicReports.stl.penThin());
        styleBuilder.setRightBorder(DynamicReports.stl.penThin());
        styleBuilder.setTopBorder(DynamicReports.stl.penThin());
        return styleBuilder;
    }

    public static Style subtotalStyle() {
        Style style = new Style();
        style.setBold(true);
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        return style;
    }

    private static StyleBuilder subtotalStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setBold(true);
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        styleBuilder.setTopBorder(DynamicReports.stl.pen1Point());
        return styleBuilder;
    }

    public static Style columnHeaderStyle() {
        Style style = new Style();
        style.setBold(true);
        style.setBgColor(com.sunecity.lilium.data.report.Color.GRAY);
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        style.setBorderTop(new Border(BorderType.SOLID, 1f));
        style.setBorderLeft(new Border(BorderType.SOLID, 1f));
        style.setBorderRight(new Border(BorderType.SOLID, 1f));
        style.setBorderBottom(new Border(BorderType.SOLID, 1f));
        return style;
    }

    private static StyleBuilder columnHeaderStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setBold(true);
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        BorderBuilder border = DynamicReports.stl.border();
        border.setRightPen(DynamicReports.stl.penThin());
        border.setTopPen(DynamicReports.stl.penThin());
        border.setLeftPen(DynamicReports.stl.penThin());
        border.setBottomPen(DynamicReports.stl.penThin());
        styleBuilder.setBorder(border);
        styleBuilder.setBackgroundColor(Color.LIGHT_GRAY);
        return styleBuilder;
    }

    public static Style columnStyle() {
        Style style = new Style();
        style.setHorizontalAlign(HorizontalAlign.RIGHT);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        style.setBorderTop(new Border(BorderType.SOLID, 1f));
        style.setBorderLeft(new Border(BorderType.SOLID, 1f));
        style.setBorderRight(new Border(BorderType.SOLID, 1f));
        style.setBorderBottom(new Border(BorderType.SOLID, 1f));
        return style;
    }

    private static StyleBuilder columnStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        BorderBuilder border = DynamicReports.stl.border();
        border.setRightPen(DynamicReports.stl.penThin());
        border.setTopPen(DynamicReports.stl.penThin());
        border.setLeftPen(DynamicReports.stl.penThin());
        border.setBottomPen(DynamicReports.stl.penThin());
        styleBuilder.setBorder(border);
        PaddingBuilder padding = DynamicReports.stl.padding();
        padding.setRight(2);
        padding.setLeft(2);
        styleBuilder.setPadding(padding);
        return styleBuilder;
    }

    public static Style rowColumnStyle() {
        Style style = new Style();
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        BorderBuilder border = DynamicReports.stl.border();
        border.setRightPen(DynamicReports.stl.penThin());
        border.setTopPen(DynamicReports.stl.penThin());
        border.setLeftPen(DynamicReports.stl.penThin());
        border.setBottomPen(DynamicReports.stl.penThin());
        PaddingBuilder padding = DynamicReports.stl.padding();
        padding.setRight(2);
        padding.setLeft(2);
        return style;
    }

    private static StyleBuilder rowColumnStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        BorderBuilder border = DynamicReports.stl.border();
        border.setRightPen(DynamicReports.stl.penThin());
        border.setTopPen(DynamicReports.stl.penThin());
        border.setLeftPen(DynamicReports.stl.penThin());
        border.setBottomPen(DynamicReports.stl.penThin());
        styleBuilder.setBorder(border);
        PaddingBuilder padding = DynamicReports.stl.padding();
        padding.setRight(2);
        padding.setLeft(2);
        styleBuilder.setPadding(padding);
        return styleBuilder;
    }

    public static Style pageNumberStyle() {
        Style style = new Style();
        style.setHorizontalAlign(HorizontalAlign.CENTER);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        Border border = new Border();
        border.setWidth(1f);
        border.setType(BorderType.SOLID);
        style.setBorderTop(border);
        return style;
    }

    private static StyleBuilder pageNumberStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setTopBorder(DynamicReports.stl.pen1Point());
        styleBuilder.bold();
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        return styleBuilder;
    }

    public static Style reportDateStyle() {
        Style style = new Style();
        style.setFontSize(8);
        style.setHorizontalAlign(HorizontalAlign.LEFT);
        style.setVerticalAlign(VerticalAlign.MIDDLE);
        return style;
    }

    private static StyleBuilder reportDateStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        styleBuilder.setFontSize(8);
        styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.LEFT);
        styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
        return styleBuilder;
    }

    static MarginBuilder getMargin(DynamicReport report) {
        MarginBuilder margin = DynamicReports.margin();
        if (report.getPage().getMarginRight() != null) {
            margin.setRight(report.getPage().getMarginRight());
        }

        if (report.getPage().getMarginTop() != null) {
            margin.setTop(report.getPage().getMarginTop());
        }

        if (report.getPage().getMarginLeft() != null) {
            margin.setLeft(report.getPage().getMarginLeft());
        }

        if (report.getPage().getMarginBottom() != null) {
            margin.setBottom(report.getPage().getMarginBottom());
        }

        return margin;
    }

    static StyleBuilder getStyle(Style style) {
        StyleBuilder styleBuilder = DynamicReports.stl.style();
        if (style.getBgColor() != null) {
            styleBuilder.setBackgroundColor(new Color(style.getBgColor().getRed(), style.getBgColor().getGreen(), style.getBgColor().getBlue()));
        }

        if (style.getFontColor() != null) {
            styleBuilder.setForegroundColor(new Color(style.getFontColor().getRed(), style.getFontColor().getGreen(), style.getFontColor().getBlue()));
        }

        if (style.getFontName() != null) {
            styleBuilder.setFontName(style.getFontName());
        }

        if (style.getFontSize() != null) {
            styleBuilder.setFontSize(style.getFontSize());
        }

        if (style.getBold() != null) {
            styleBuilder.setBold(style.getBold());
        }

        if (style.getItalic() != null) {
            styleBuilder.setItalic(style.getItalic());
        }

        if (style.getUnderline() != null) {
            styleBuilder.setUnderline(style.getUnderline());
        }

        if (style.getStrikeThrough() != null) {
            styleBuilder.setStrikeThrough(style.getStrikeThrough());
        }
        if (style.getPaddingRight() != null || style.getPaddingTop() != null || style.getPaddingLeft() != null || style.getPaddingBottom() != null) {
            PaddingBuilder padding = DynamicReports.stl.padding();
            if (style.getPaddingRight() != null) {
                padding.setRight(style.getPaddingRight());
            }

            if (style.getPaddingLeft() != null) {
                padding.setRight(style.getPaddingLeft());
            }

            if (style.getPaddingTop() != null) {
                padding.setRight(style.getPaddingTop());
            }

            if (style.getPaddingBottom() != null) {
                padding.setRight(style.getPaddingBottom());
            }

            styleBuilder.setPadding(padding);
        }

        if (style.getRotation() != null) {
            switch (style.getRotation()) {
                case LEFT:
                    styleBuilder.setRotation(Rotation.LEFT);
                    break;
                case RIGHT:
                    styleBuilder.setRotation(Rotation.RIGHT);
                    break;
                case UPSIDE_DOWN:
                    styleBuilder.setRotation(Rotation.UPSIDE_DOWN);
                    break;
                default:
                    styleBuilder.setRotation(Rotation.NONE);
            }
        }

        if (style.getLineSpacing() != null) {
            switch (style.getLineSpacing()) {
                case SINGLE:
                    styleBuilder.setLineSpacing(LineSpacing.SINGLE);
                    break;
                case DOUBLE:
                    styleBuilder.setLineSpacing(LineSpacing.DOUBLE);
                    break;
                default:
                    styleBuilder.setLineSpacing(LineSpacing.ONE_AND_HALF);
            }
        }

        if (style.getHorizontalAlign() != null) {
            switch (style.getHorizontalAlign()) {
                case LEFT:
                    styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.LEFT);
                    break;
                case CENTER:
                    styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
                    break;
                case RIGHT:
                    styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);
                    break;
                default:
                    styleBuilder.setHorizontalTextAlignment(HorizontalTextAlignment.JUSTIFIED);
            }
        }

        if (style.getVerticalAlign() != null) {
            switch (style.getVerticalAlign()) {
                case BOTTOM:
                    styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.BOTTOM);
                    break;
                case TOP:
                    styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.TOP);
                    break;
                default:
                    styleBuilder.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);
            }
        }

        if (style.getBorderRight() != null) {
            styleBuilder.setRightBorder(getPen(style.getBorderRight()));
        }

        if (style.getBorderLeft() != null) {
            styleBuilder.setLeftBorder(getPen(style.getBorderLeft()));
        }

        if (style.getBorderTop() != null) {
            styleBuilder.setTopBorder(getPen(style.getBorderTop()));
        }

        if (style.getBorderBottom() != null) {
            styleBuilder.setBottomBorder(getPen(style.getBorderBottom()));
        }

        return styleBuilder;
    }

    private static PenBuilder getPen(Border border) {
        PenBuilder pen = DynamicReports.stl.pen();
        if (border.getColor() != null) {
            pen.setLineColor(new Color(border.getColor().getRed(), border.getColor().getGreen(), border.getColor().getBlue()));
        }
        if (border.getType() != null) {
            switch (border.getType()) {
                case DOUBLE:
                    pen.setLineStyle(LineStyle.DOUBLE);
                    break;
                case DASH:
                    pen.setLineStyle(LineStyle.DASHED);
                    break;
                case DOT:
                    pen.setLineStyle(LineStyle.DOTTED);
                    break;
                default:
                    pen.setLineStyle(LineStyle.SOLID);
            }
        }
        if (border.getWidth() != null) {
            pen.setLineWidth(border.getWidth());
        }
        return pen;
    }

    public static Style rowStyle() {
        Style style = new Style();
        style.setBorderTop(new Border(BorderType.SOLID, 1f));
        style.setBorderBottom(new Border(BorderType.SOLID, 1f));
        style.setBorderRight(new Border(BorderType.SOLID, 1f));
        style.setBorderLeft(new Border(BorderType.SOLID, 1f));
        return style;
    }
}
