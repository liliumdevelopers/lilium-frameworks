package com.sunecity.lilium.report;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.table.RtfCell;
import org.apache.commons.beanutils.PropertyUtils;

import javax.enterprise.context.Dependent;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class Rtf implements Serializable {

    public <T> void export(List<T> values, List<String> fields, boolean rtl, OutputStream outputStream) throws DocumentException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Document document = new Document();
        RtfWriter2.getInstance(document, outputStream);
        document.open();
        Table table = new Table(fields.size(), values.size());
        if (rtl) {
            for (int i = fields.size() - 1; i >= 0; i--) {
                RtfCell cell = new RtfCell(fields.get(i));
                cell.setHorizontalAlignment(RtfCell.ALIGN_CENTER);
                table.addCell(cell);
            }
            for (T t : values) {
                for (int j = fields.size() - 1; j >= 0; j--) {
                    Object object = PropertyUtils.getProperty(t, fields.get(j));
                    if (object != null) {
                        RtfCell cell = new RtfCell(object.toString());
                        cell.setHorizontalAlignment(RtfCell.ALIGN_RIGHT);
                        table.addCell(cell);
                    } else {
                        table.addCell(new RtfCell());
                    }
                }
            }
        } else {
            for (String field : fields) {
                RtfCell cell = new RtfCell(field);
                cell.setHorizontalAlignment(RtfCell.ALIGN_CENTER);
                table.addCell(cell);
            }
            for (T t : values) {
                for (String field : fields) {
                    Object object = PropertyUtils.getProperty(t, field);
                    if (object != null) {
                        table.addCell(new RtfCell(object.toString()));
                    } else {
                        table.addCell(new RtfCell());
                    }
                }
            }
        }
        document.add(table);
        document.close();
    }

}
