package com.sunecity.lilium.report.barcode;

import java.io.IOException;

public interface Barcode {

    byte[] generate(String str, BarcodeType type) throws IOException;

}
