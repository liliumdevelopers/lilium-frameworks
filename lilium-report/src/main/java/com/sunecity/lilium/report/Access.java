package com.sunecity.lilium.report;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.ColumnBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.TableBuilder;
import org.apache.commons.beanutils.PropertyUtils;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class Access implements Serializable {

    public <T> void export(String tableName, List<T> values, Map<String, Integer> columns, OutputStream outputStream) throws IOException, SQLException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Path path = Files.createTempFile("AccessOutTemp", ".accdb");
        try (Database db = DatabaseBuilder.create(Database.FileFormat.V2010, path.toFile())) {
            TableBuilder tableBuilder = new TableBuilder(tableName);
            for (Entry<String, Integer> e : columns.entrySet()) {
                tableBuilder.addColumn(new ColumnBuilder(e.getKey()).setSQLType(e.getValue()));
            }
            Table table = tableBuilder.toTable(db);
            for (T t : values) {
                Object[] objects = new Object[columns.size()];
                int c = 0;
                for (Entry<String, Integer> e : columns.entrySet()) {
                    objects[c] = PropertyUtils.getProperty(t, e.getKey());
                    c++;
                }
                table.addRow(objects);
            }
            Files.copy(path, outputStream);
        }
        Files.delete(path);
    }

    public <T> List<T> getValues(String tableName, InputStream inputStream, Class<T> clas) throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        List<T> list = new ArrayList<>();
        Path path = Files.createTempFile("AccessInTemp", ".accdb");
        Files.copy(inputStream, path, StandardCopyOption.REPLACE_EXISTING);
        try (Database db = DatabaseBuilder.open(path.toFile())) {
            Table table = db.getTable(tableName);
            for (Map<String, Object> row : table) {
                T t = clas.getConstructor().newInstance();
                for (Column column : table.getColumns()) {
                    PropertyUtils.setProperty(t, column.getName(), row.get(column.getName()));
                }
                list.add(t);
            }
        }
        Files.delete(path);
        return list;
    }

}
