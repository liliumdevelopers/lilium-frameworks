package com.sunecity.lilium.report.jasper.interfaces;

import net.sf.dynamicreports.report.definition.expression.DRIExpression;

/**
 * @author Shahram Goodarzi
 */
public interface IExpression<T> {

    DRIExpression<T> getExpression();

}
