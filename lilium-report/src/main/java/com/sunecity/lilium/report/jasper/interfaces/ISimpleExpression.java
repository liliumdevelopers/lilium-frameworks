package com.sunecity.lilium.report.jasper.interfaces;

import net.sf.dynamicreports.report.base.expression.AbstractSimpleExpression;

/**
 * @author Shahram Goodarzi
 */
public interface ISimpleExpression<T> extends IExpression<T> {

    @Override
    AbstractSimpleExpression<T> getExpression();

}
