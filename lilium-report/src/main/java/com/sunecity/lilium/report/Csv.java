package com.sunecity.lilium.report;

import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import org.apache.commons.beanutils.PropertyUtils;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class Csv implements Serializable {

    public <T> void export(List<T> values, List<String> fields, OutputStream outputStream) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IOException {
        try (Writer writer = new OutputStreamWriter(outputStream, "UTF-8");
             CSVWriter csvWriter = new CSVWriter(writer)) {
            List<String[]> list = new ArrayList<>();
            for (T t : values) {
                String[] strings = new String[fields.size()];
                for (int i = 0; i < strings.length; i++) {
                    Object object = PropertyUtils.getProperty(t, fields.get(i));
                    if (object != null) {
                        strings[i] = object.toString();
                    } else {
                        strings[i] = "";
                    }
                }
                list.add(strings);
            }
            csvWriter.writeAll(list);
        }
    }

    public <T> List<T> getValues(InputStream inputStream, Class<T> clas, List<String> fields) throws IOException {
        try (Reader reader = new InputStreamReader(inputStream, "UTF-8")) {
            ColumnPositionMappingStrategy<T> strat = new ColumnPositionMappingStrategy<>();
            strat.setType(clas);
            strat.setColumnMapping(fields.toArray(new String[fields.size()]));
            CsvToBean<T> csv = new CsvToBean<>();
            csv.setMappingStrategy(strat);
            csv.setCsvReader(new CSVReaderBuilder(reader).build());
            return csv.parse();
        }
    }

}
