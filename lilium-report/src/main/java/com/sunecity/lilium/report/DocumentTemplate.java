package com.sunecity.lilium.report;

import com.sunecity.lilium.core.model.file.ImageFile;
import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.core.document.SyntaxKind;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.images.ByteArrayImageProvider;
import fr.opensagres.xdocreport.document.images.IImageProvider;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class DocumentTemplate {

    private IXDocReport report;
    private IContext context;
    private FieldsMetadata metadata;

    public DocumentTemplate() {
    }

    public void template(InputStream inputStream) throws IOException, XDocReportException {
        report = XDocReportRegistry.getRegistry().loadReport(inputStream, TemplateEngineKind.Freemarker);
        context = report.createContext();
        metadata = report.createFieldsMetadata();
    }

    public void add(String name, Object value) {
        context.put(name, value);
    }

    public void add(String name, String html) {
        metadata.addFieldAsTextStyling(name, SyntaxKind.Html, true);
        context.put(name, html);
    }

    public void add(String name, List<?> values) throws XDocReportException {
        if (values != null && values.size() > 0) {
            metadata.load(name, values.get(0).getClass(), true);
            context.put(name, values);
        }
    }

    public void add(String name, ImageFile image) throws IOException {
        metadata.addFieldAsImage(name);
        IImageProvider imageProvider = new ByteArrayImageProvider(image.getValue());
        if (image.getWidth() != null) {
            imageProvider.setWidth(image.getWidth().floatValue());
        }
        if (image.getHeight() != null) {
            imageProvider.setHeight(image.getHeight().floatValue());
        }
        if (image.getWidth() == null ^ image.getHeight() == null) {
            imageProvider.setUseImageSize(true);
            imageProvider.setResize(true);
        }
        context.put(name, imageProvider);
    }

    public void generate(OutputStream outputStream) throws IOException, XDocReportException {
        report.process(context, outputStream);
    }

    public void generateToPdf(OutputStream outputStream) throws IOException, XDocReportException {
        Options options = Options.getTo(ConverterTypeTo.PDF);
        report.convert(context, options, outputStream);
    }

    public void generateToXhtml(OutputStream outputStream) throws IOException, XDocReportException {
        Options options = Options.getTo(ConverterTypeTo.XHTML);
        report.convert(context, options, outputStream);
    }

}
