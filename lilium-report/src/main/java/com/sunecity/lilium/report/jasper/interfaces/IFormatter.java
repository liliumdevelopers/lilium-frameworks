package com.sunecity.lilium.report.jasper.interfaces;

import net.sf.dynamicreports.report.base.expression.AbstractValueFormatter;

/**
 * @author Shahram Goodarzi
 */
public interface IFormatter<S, H> {

    AbstractValueFormatter<S, H> getFormatter();

}
