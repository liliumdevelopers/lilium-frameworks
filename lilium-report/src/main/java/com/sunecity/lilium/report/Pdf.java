package com.sunecity.lilium.report;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sunecity.lilium.font.Font;
import org.apache.commons.beanutils.PropertyUtils;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class Pdf implements Serializable {

    public <T> void export(List<T> values, Map<String, String> fields, boolean rtl, Font font, Rectangle pageSize, float marginLeft, float marginRight, float marginTop, float marginBottom, OutputStream outputStream) throws DocumentException, IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Document document = new Document(pageSize, marginLeft, marginRight, marginTop, marginBottom);
        PdfWriter.getInstance(document, outputStream);
        document.open();

        BaseFont bf = BaseFont.createFont(font.getName(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED, true, font.getBytes(), null);
        com.lowagie.text.Font f = new com.lowagie.text.Font(bf);
        PdfPTable table = new PdfPTable(fields.size());
        if (rtl) {
            for (int i = fields.size() - 1; i >= 0; i--) {
                Paragraph paragraph = new Paragraph();
                paragraph.setAlignment(Element.ALIGN_CENTER);
                paragraph.setFont(f);
                if (fields.get(i) == null) {
                    paragraph.add("");
                } else {
                    paragraph.add(fields.get(i));
                }
                PdfPCell cell = new PdfPCell();
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.addElement(paragraph);
                cell.setRunDirection(PdfWriter.RUN_DIRECTION_LTR);
                table.addCell(cell);
            }
            for (T t : values) {
                for (int j = fields.size() - 1; j >= 0; j--) {
                    Paragraph paragraph = new Paragraph();
                    paragraph.setAlignment(Element.ALIGN_RIGHT);
                    paragraph.setFont(f);
                    Object object = PropertyUtils.getProperty(t, fields.get(j));
                    if (object == null) {
                        paragraph.add("");
                    } else {
                        paragraph.add(object.toString());
                    }
                    PdfPCell cell = new PdfPCell();
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.addElement(paragraph);
                    cell.setRunDirection(PdfWriter.RUN_DIRECTION_LTR);
                    table.addCell(cell);
                }
            }
        } else {
            for (Map.Entry<String, String> field : fields.entrySet()) {
                Paragraph paragraph = new Paragraph();
                paragraph.setFont(f);
                if (field == null) {
                    paragraph.add("");
                } else {
                    paragraph.add(field.getValue());
                }
                PdfPCell cell = new PdfPCell();
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.addElement(paragraph);
                table.addCell(cell);
            }
            for (T t : values) {
                for (Map.Entry<String, String> field : fields.entrySet()) {
                    Paragraph paragraph = new Paragraph();
                    paragraph.setFont(f);
                    Object object = PropertyUtils.getProperty(t, field.getKey());
                    if (object == null) {
                        paragraph.add("");
                    } else {
                        paragraph.add(object.toString());
                    }
                    PdfPCell cell = new PdfPCell();
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.addElement(paragraph);
                    table.addCell(cell);
                }
            }
        }
        document.add(table);
        document.close();
    }

    public <T> void export(List<T> values, Map<String, String> fields, boolean rtl, Font font, OutputStream outputStream) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException, DocumentException, IOException {
        export(values, fields, rtl, font, PageSize.A4, 10, 10, 10, 10, outputStream);
    }

}
