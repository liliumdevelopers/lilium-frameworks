package com.sunecity.lilium.report.jasper;

import com.sunecity.lilium.core.localization.Locales;
import com.sunecity.lilium.core.time.Date;
import com.sunecity.lilium.core.time.DateTime;
import com.sunecity.lilium.core.time.Time;
import com.sunecity.lilium.data.field.Field;
import com.sunecity.lilium.data.field.SimpleField;
import com.sunecity.lilium.data.report.AbstractColumn;
import com.sunecity.lilium.data.report.DynamicReport;
import com.sunecity.lilium.data.report.GroupColumn;
import com.sunecity.lilium.data.report.Report;
import com.sunecity.lilium.data.report.ReportColumn;
import com.sunecity.lilium.data.report.ReportType;
import com.sunecity.lilium.data.report.Row;
import com.sunecity.lilium.data.report.RowColumn;
import com.sunecity.lilium.data.report.chart.AreaChart;
import com.sunecity.lilium.data.report.chart.BarChart;
import com.sunecity.lilium.data.report.chart.Chart;
import com.sunecity.lilium.data.report.chart.LineChart;
import com.sunecity.lilium.data.report.chart.PieChart;
import com.sunecity.lilium.data.report.enums.LogoPosition;
import com.sunecity.lilium.report.jasper.interfaces.IExpression;
import com.sunecity.lilium.report.jasper.interfaces.IFormatter;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.FieldBuilder;
import net.sf.dynamicreports.report.builder.chart.AbstractCategoryChartBuilder;
import net.sf.dynamicreports.report.builder.chart.AbstractPieChartBuilder;
import net.sf.dynamicreports.report.builder.chart.CategoryChartSerieBuilder;
import net.sf.dynamicreports.report.builder.chart.LineChartBuilder;
import net.sf.dynamicreports.report.builder.column.ColumnBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.column.ValueColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.component.ImageBuilder;
import net.sf.dynamicreports.report.builder.component.PageNumberBuilder;
import net.sf.dynamicreports.report.builder.component.TextFieldBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.grid.ColumnGridComponentBuilder;
import net.sf.dynamicreports.report.builder.grid.ColumnTitleGroupBuilder;
import net.sf.dynamicreports.report.builder.subtotal.AggregationSubtotalBuilder;
import net.sf.dynamicreports.report.constant.PageOrientation;
import net.sf.dynamicreports.report.constant.Position;
import net.sf.dynamicreports.report.constant.RunDirection;
import net.sf.dynamicreports.report.constant.WhenNoDataType;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRVirtualizationHelper;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static net.sf.dynamicreports.report.builder.DynamicReports.cht;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.sbt;

/**
 * @author Shahram Goodarzi
 */
public class DynamicReports {

    private DynamicReports() {
    }

    public static JasperPrint generateJasperPrint(DynamicReport dynamicReport, ReportType reportType) throws DRException, IOException {
        return new DynamicReportGenerator(dynamicReport, reportType).generateJasperPrint();
    }

    public static JasperPrint generateJasperPrint(DynamicReport dynamicReport, Collection<?> collection, ReportType reportType) throws DRException, IOException {
        return new DynamicReportGenerator(dynamicReport, reportType).generateJasperPrint(collection);
    }

    public static JasperPrint generateJasperPrint(DynamicReport dynamicReport, Connection connection, ReportType reportType) throws DRException, IOException {
        return new DynamicReportGenerator(dynamicReport, reportType).generateJasperPrint(connection, null);
    }

    public static JasperPrint generateJasperPrint(DynamicReport dynamicReport, Connection connection, Map<String, Object> parameters, ReportType reportType) throws DRException, IOException {
        return new DynamicReportGenerator(dynamicReport, reportType).generateJasperPrint(connection, parameters);
    }

    public static JasperPrint generateJasperPrint(DynamicReport dynamicReport, EntityManager entityManager, ReportType reportType) throws DRException, IOException {
        return new DynamicReportGenerator(dynamicReport, reportType).generateJasperPrint(entityManager, null);
    }

    public static JasperPrint generateJasperPrint(DynamicReport dynamicReport, EntityManager entityManager, Map<String, Object> parameters, ReportType reportType) throws DRException, IOException {
        return new DynamicReportGenerator(dynamicReport, reportType).generateJasperPrint(entityManager, parameters);
    }

    private static JasperReportBuilder generateJasperReportBuilder(DynamicReport dynamicReport, Collection<?> collection, ReportType reportType) throws IOException {
        DynamicReportGenerator dynamicReportGenerator = new DynamicReportGenerator(dynamicReport, reportType);
        dynamicReportGenerator.reportBuilder = net.sf.dynamicreports.report.builder.DynamicReports.report();
        dynamicReportGenerator.reportBuilder.setDataSource(collection);
        dynamicReportGenerator.generate();
        return dynamicReportGenerator.reportBuilder;
    }

    private static class ChartGenerator {
        private Map<ReportColumn, ColumnBuilder<?, ?>> columns;
        private ValueColumnBuilder<?, String> category;
        private ValueColumnBuilder<?, Number> serie;
        private List<ValueColumnBuilder<?, Number>> series;

        private ChartGenerator(Map<ReportColumn, ColumnBuilder<?, ?>> columns) {
            this.columns = columns;
            this.series = new ArrayList<>();
        }

        private ComponentBuilder<?, ?> getChart(Chart chart) {
            if (this.columns.containsKey(chart.getCategory())) {
                this.category = (ValueColumnBuilder) this.columns.get(chart.getCategory());
            } else {
                this.category = col.column(chart.getCategory().getField().getDisplayName(), chart.getCategory().getField().getName(), String.class);
            }

            ReportColumn pie;
            Iterator chartSeries;
            int i;
            Object var10;
            CategoryChartSerieBuilder[] var11;
            if (chart instanceof AreaChart) {
                AreaChart var8 = (AreaChart) chart;
                chartSeries = var8.getSeries().iterator();

                while (chartSeries.hasNext()) {
                    pie = (ReportColumn) chartSeries.next();
                    if (this.columns.containsKey(pie)) {
                        this.series.add((ValueColumnBuilder) this.columns.get(pie));
                    } else {
                        this.series.add(net.sf.dynamicreports.report.builder.DynamicReports.col.column(pie.getField().getDisplayName(), pie.getField().getName(), Number.class));
                    }
                }

                var10 = var8.isStacked() ? cht.stackedAreaChart() : cht.areaChart();
                ((AbstractCategoryChartBuilder) var10).setTitle(var8.getTitle());
                ((AbstractCategoryChartBuilder) var10).setCategory(this.category);
                var11 = new CategoryChartSerieBuilder[this.series.size()];

                for (i = 0; i < var11.length; ++i) {
                    var11[i] = cht.serie(this.series.get(i));
                }

                ((AbstractCategoryChartBuilder) var10).series(var11);
                ((AbstractCategoryChartBuilder) var10).setCategoryAxisFormat(cht.axisFormat().setLabel(var8.getLabel()));
                return (ComponentBuilder) var10;
            } else if (chart instanceof BarChart) {
                BarChart var7 = (BarChart) chart;
                chartSeries = var7.getSeries().iterator();

                while (chartSeries.hasNext()) {
                    pie = (ReportColumn) chartSeries.next();
                    if (this.columns.containsKey(pie)) {
                        this.series.add((ValueColumnBuilder) this.columns.get(pie));
                    } else {
                        this.series.add(col.column(pie.getField().getDisplayName(), pie.getField().getName(), Number.class));
                    }
                }

                var10 = var7.isThreeDimension() ? (var7.isStacked() ? cht.stackedBar3DChart() : cht.bar3DChart()) : (var7.isStacked() ? cht.stackedBarChart() : cht.barChart());
                ((AbstractCategoryChartBuilder) var10).setTitle(var7.getTitle());
                ((AbstractCategoryChartBuilder) var10).setCategory(this.category);
                var11 = new CategoryChartSerieBuilder[this.series.size()];

                for (i = 0; i < var11.length; ++i) {
                    var11[i] = cht.serie(this.series.get(i));
                }

                ((AbstractCategoryChartBuilder) var10).series(var11);
                ((AbstractCategoryChartBuilder) var10).setCategoryAxisFormat(cht.axisFormat().setLabel(var7.getLabel()));
                return (ComponentBuilder) var10;
            } else if (!(chart instanceof LineChart)) {
                if (chart instanceof PieChart) {
                    PieChart var6 = (PieChart) chart;
                    if (this.columns.containsKey(var6.getSerie())) {
                        this.serie = (ValueColumnBuilder) this.columns.get(var6.getSerie());
                    } else {
                        this.serie = col.column(var6.getSerie().getField().getDisplayName(), var6.getSerie().getField().getName(), Number.class);
                    }

                    var10 = var6.isThreeDimension() ? cht.pie3DChart() : cht.pieChart();
                    ((AbstractPieChartBuilder) var10).setTitle(var6.getTitle());
                    ((AbstractPieChartBuilder) var10).setKey(this.category);
                    ((AbstractPieChartBuilder) var10).series(cht.serie(this.serie));
                    return (ComponentBuilder) var10;
                } else {
                    return null;
                }
            } else {
                LineChart pieChart = (LineChart) chart;
                chartSeries = pieChart.getSeries().iterator();

                while (chartSeries.hasNext()) {
                    pie = (ReportColumn) chartSeries.next();
                    if (this.columns.containsKey(pie)) {
                        this.series.add((ValueColumnBuilder) this.columns.get(pie));
                    } else {
                        this.series.add(col.column(pie.getField().getDisplayName(), pie.getField().getName(), Number.class));
                    }
                }

                LineChartBuilder var9 = cht.lineChart();
                var9.setTitle(pieChart.getTitle());
                var9.setCategory(this.category);
                var11 = new CategoryChartSerieBuilder[this.series.size()];

                for (i = 0; i < var11.length; ++i) {
                    var11[i] = cht.serie(this.series.get(i));
                }

                var9.series(var11);
                var9.setCategoryAxisFormat(cht.axisFormat().setLabel(pieChart.getLabel()));
                return var9;
            }
        }
    }

    private static class DynamicReportGenerator {
        private JasperReportBuilder reportBuilder;
        private DynamicReport report;
        private ReportType reportType;
        private List<ColumnBuilder<?, ?>> columns;
        private List<ColumnGridComponentBuilder> columnGrids;
        private List<ColumnTitleGroupBuilder> columnGroups;
        private List<AggregationSubtotalBuilder<?>> subtotals;
        private List<FieldBuilder<?>> fieldBuilders;

        private DynamicReportGenerator(DynamicReport report, ReportType reportType) {
            this.report = report;
            this.reportType = reportType;
            this.columns = new ArrayList<>();
            this.columnGrids = new ArrayList<>();
            this.columnGroups = new ArrayList<>();
            this.subtotals = new ArrayList<>();
            this.fieldBuilders = new ArrayList<>();
        }

        private JasperPrint generateJasperPrint() throws DRException, IOException {
            this.reportBuilder = net.sf.dynamicreports.report.builder.DynamicReports.report();
            this.reportBuilder.setDataSource(new JREmptyDataSource());
            this.generate();
            return this.reportBuilder.toJasperPrint();
        }

        private JasperPrint generateJasperPrint(Collection<?> collection) throws DRException, IOException {
            this.reportBuilder = net.sf.dynamicreports.report.builder.DynamicReports.report();
            this.reportBuilder.setDataSource(collection);
            this.generate();
            return this.reportBuilder.toJasperPrint();
        }

        private JasperPrint generateJasperPrint(Connection connection, Map<String, Object> parameters) throws DRException, IOException {
            this.reportBuilder = net.sf.dynamicreports.report.builder.DynamicReports.report();
            if (parameters != null && !parameters.isEmpty()) {

                for (Object o : parameters.entrySet()) {
                    Entry entry = (Entry) o;
                    this.reportBuilder.addParameter((String) entry.getKey(), entry.getValue());
                }
            }

            this.reportBuilder.setQuery(this.report.getReportQuery().getQuery(), "sql").setConnection(connection);
            this.generate();
            return this.reportBuilder.toJasperPrint();
        }

        private JasperPrint generateJasperPrint(EntityManager entityManager, Map<String, Object> parameters) throws DRException, IOException {
            this.reportBuilder = net.sf.dynamicreports.report.builder.DynamicReports.report();
            if (parameters != null && !parameters.isEmpty()) {

                for (Object o : parameters.entrySet()) {
                    Entry entry = (Entry) o;
                    this.reportBuilder.addParameter((String) entry.getKey(), entry.getValue());
                }
            }

            this.reportBuilder.setQuery(this.report.getReportQuery().getQuery(), "ejbql").addParameter("JPA_ENTITY_MANAGER", entityManager);
            this.generate();
            return this.reportBuilder.toJasperPrint();
        }

        private void generate() throws IOException {
            JRFileVirtualizer virtualizer = new JRFileVirtualizer(5);
            JRVirtualizationHelper.setThreadVirtualizer(virtualizer);
            this.reportBuilder.setWhenNoDataType(WhenNoDataType.BLANK_PAGE).setSummaryWithPageHeaderAndFooter(true);
            this.reportBuilder.setVirtualizer(virtualizer);
            this.reportBuilder.setColumnDirection(RunDirection.RIGHT_TO_LEFT);
            reportBuilder.setDataSource(report.getValues());
            this.title(this.report);
            this.subtitle(this.report);
            this.header(this.report);
            this.reportDate(this.report);
            this.footer(this.report);
            this.reportSummary(this.report);
            if (report.getSubReports() != null) {
                subReport(report.getSubReports());
            }
            this.pageNumber(this.report);
            if (report.getRows() != null) {
                this.rows(report.getRows());
            }
            if (this.report.isHighlightEvenRows()) {
                this.reportBuilder.highlightDetailEvenRows();
            }

            if (!this.reportType.isPaginate()) {
                this.reportBuilder.ignorePagination();
            }

            HashMap columns = new HashMap();
            Iterator column;
            if (this.report.getColumns() != null) {
                if (Locales.isRtl() && this.reportType.isPaginate()) {
                    Collections.reverse(this.report.getColumns());
                }
                column = this.report.getColumns().iterator();

                while (column.hasNext()) {
                    AbstractColumn chart = (AbstractColumn) column.next();
                    if (chart instanceof ReportColumn) {
                        ReportColumn groupColumn = (ReportColumn) chart;
                        ColumnBuilder valueColumn = this.getColumn(groupColumn);
                        this.columns.add(valueColumn);
                        this.columnGrids.add(valueColumn);
                        columns.put(groupColumn, valueColumn);
                    } else if (chart instanceof GroupColumn) {
                        GroupColumn var10 = (GroupColumn) chart;
                        this.columnGrids.add(this.columnTitleGroup(var10));
                    }
                }

                this.reportBuilder.fields((FieldBuilder[]) this.fieldBuilders.toArray(new FieldBuilder[this.fieldBuilders.size()]));
                ColumnGridComponentBuilder[] var7 = new ColumnGridComponentBuilder[this.columnGrids.size() + this.columnGroups.size()];

                int var9;
                for (var9 = 0; var9 < this.columnGrids.size(); ++var9) {
                    var7[var9] = this.columnGrids.get(var9);
                }

                for (var9 = 0; var9 < this.columnGroups.size(); ++var9) {
                    var7[var9 + this.columnGrids.size()] = this.columnGroups.get(var9);
                }

                this.reportBuilder.columnGrid(var7);

                for (Object column1 : this.columns) {
                    ColumnBuilder var12 = (ColumnBuilder) column1;
                    this.reportBuilder.addColumn(var12);
                }

//                this.group(this.report.getGroupColumn());
            }

//            if (this.report.getCharts() != null) {
//                column = this.report.getCharts().iterator();
//                while (column.hasNext()) {
//                    Chart var8 = (Chart) column.next();
//                    this.reportBuilder.addSummary((new ChartGenerator(columns, (ChartGenerator) null)).getChart(var8));
//                }
//            }

            this.reportBuilder.setPageFormat(this.report.getPage().getWidth(), this.report.getPage().getHeight(), this.report.getPage().isPortrait() ? PageOrientation.PORTRAIT : PageOrientation.LANDSCAPE);
            if (this.report.getPage().getMarginRight() != null || this.report.getPage().getMarginTop() != null || this.report.getPage().getMarginLeft() != null || this.report.getPage().getMarginBottom() != null) {
                this.reportBuilder.setPageMargin(ReportStyles.getMargin(this.report));
            }

//            this.reportBuilder.show();
        }

        private void subReport(List<Report> subReports) throws IOException {
            for (Report report : subReports) {
                DynamicReport dr = (DynamicReport) report;
                reportBuilder.addSummary(cmp.subreport(DynamicReports.generateJasperReportBuilder(dr, dr.getValues(), reportType)));
            }
        }

        private ColumnTitleGroupBuilder columnTitleGroup(GroupColumn groupColumn) {
            List valueColumns = new ArrayList();

            for (Object abstractColumn : groupColumn.getColumns()) {
                if (abstractColumn instanceof GroupColumn) {
                    valueColumns.add(this.columnTitleGroup((GroupColumn) abstractColumn));
                } else {
                    ColumnBuilder valueColumn = this.getColumn((ReportColumn) abstractColumn);
                    valueColumns.add(valueColumn);
                    this.reportBuilder.addColumn(valueColumn);
                }
            }

            return net.sf.dynamicreports.report.builder.DynamicReports.grid.titleGroup(groupColumn.getTitle(), (ColumnGridComponentBuilder[]) valueColumns.toArray(new ColumnGridComponentBuilder[valueColumns.size()])).setTitleFixedWidth(groupColumn.getWidth());
        }

//        private void group(ReportColumn groupedColumn) {
//            if (groupedColumn != null) {
//                CustomGroupBuilder itemGroup = ((CustomGroupBuilder) net.sf.dynamicreports.report.builder.DynamicReports.grp.group(groupedColumn.getField().getName(), groupedColumn.getField().getTypeClass()).setHeaderLayout(GroupHeaderLayout.VALUE)).setPadding(Integer.valueOf(0)).setHorizontalAlignment(HorizontalAlignment.RIGHT);
//                if (this.report.getSubtotalPosition() == SubtotalPosition.HEADER) {
//                    itemGroup.setStyle(net.sf.dynamicreports.report.builder.DynamicReports.stl.style().setTopPadding(10));
//                    this.reportBuilder.groupBy(itemGroup).subtotalsAtGroupHeader(itemGroup, (SubtotalBuilder[]) this.subtotals.toArray(new AggregationSubtotalBuilder[this.subtotals.size()]));
//                } else {
//                    this.reportBuilder.groupBy(itemGroup).subtotalsAtGroupFooter(itemGroup, (SubtotalBuilder[]) this.subtotals.toArray(new AggregationSubtotalBuilder[this.subtotals.size()]));
//                }
//            }
//        }

        private AggregationSubtotalBuilder<?> subtotal(ReportColumn column, TextColumnBuilder<Number> textColumnBuilder) {
            AggregationSubtotalBuilder subtotal;
            switch (column.getSubtotal()) {
                case AVG:
                    subtotal = sbt.avg(textColumnBuilder).setStyle(net.sf.dynamicreports.report.builder.DynamicReports.stl.style().setBorder(net.sf.dynamicreports.report.builder.DynamicReports.stl.border().setTopPen(net.sf.dynamicreports.report.builder.DynamicReports.stl.penThin())));
                    break;
                case SUM:
                    subtotal = sbt.sum(textColumnBuilder).setStyle(net.sf.dynamicreports.report.builder.DynamicReports.stl.style().setBorder(net.sf.dynamicreports.report.builder.DynamicReports.stl.border().setTopPen(net.sf.dynamicreports.report.builder.DynamicReports.stl.penThin())));
                    break;
                case MIN:
                    subtotal = sbt.min(textColumnBuilder).setStyle(net.sf.dynamicreports.report.builder.DynamicReports.stl.style().setBorder(net.sf.dynamicreports.report.builder.DynamicReports.stl.border().setTopPen(net.sf.dynamicreports.report.builder.DynamicReports.stl.penThin())));
                    break;
                case MAX:
                    subtotal = sbt.max(textColumnBuilder).setStyle(net.sf.dynamicreports.report.builder.DynamicReports.stl.style().setBorder(net.sf.dynamicreports.report.builder.DynamicReports.stl.border().setTopPen(net.sf.dynamicreports.report.builder.DynamicReports.stl.penThin())));
                    break;
                default:
                    subtotal = sbt.count(textColumnBuilder).setStyle(net.sf.dynamicreports.report.builder.DynamicReports.stl.style().setBorder(net.sf.dynamicreports.report.builder.DynamicReports.stl.border().setTopPen(net.sf.dynamicreports.report.builder.DynamicReports.stl.penThin())));
                    break;
            }

            if (column.getSubtotalLabel() != null) {
                subtotal.setLabel(column.getSubtotalLabel());
                if (column.getSubtotalLabelStyle() != null) {
                    subtotal.setLabelStyle(ReportStyles.getStyle(column.getSubtotalLabelStyle()));
                }
            }

            subtotal.setLabelPosition(Position.RIGHT);
            if (column.getSubtotalStyle() != null) {
                subtotal.setStyle(ReportStyles.getStyle(column.getSubtotalStyle()));
            }

            return subtotal;
        }

        private void columnSummary(ReportColumn column, TextColumnBuilder<Number> textColumnBuilder) {
            AggregationSubtotalBuilder aggregationSubtotal;
            switch (column.getSummary()) {
                case AVG:
                    aggregationSubtotal = sbt.avg(textColumnBuilder);
                    break;
                case SUM:
                    aggregationSubtotal = sbt.sum(textColumnBuilder);
                    break;
                case MIN:
                    aggregationSubtotal = sbt.min(textColumnBuilder);
                    break;
                case MAX:
                    aggregationSubtotal = sbt.max(textColumnBuilder);
                    break;
                default:
                    aggregationSubtotal = sbt.count(textColumnBuilder);
                    break;
            }

            if (column.getSummaryLabel() != null) {
                aggregationSubtotal.setLabel(column.getSummaryLabel());
                if (column.getSummaryLabelStyle() != null) {
                    aggregationSubtotal.setLabelStyle(ReportStyles.getStyle(column.getSummaryLabelStyle()));
                }
            }

            aggregationSubtotal.setLabelPosition(Position.RIGHT);
            if (column.getSummaryStyle() != null) {
                aggregationSubtotal.setStyle(ReportStyles.getStyle(column.getSummaryStyle()));
            }

            this.reportBuilder.subtotalsAtSummary(aggregationSubtotal);
        }

        private void title(DynamicReport report) throws IOException {
            if (report.getLogo() != null) {
                BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(report.getLogo().getFile()));
                ImageBuilder image = cmp.image(bufferedImage).setFixedDimension(70, 70).setStyle(net.sf.dynamicreports.report.builder.DynamicReports.stl.style().setPadding(3));
                if (report.getLogo().getPosition() == LogoPosition.LEFT || report.getLogo().getPosition() == LogoPosition.RIGHT) {
                    image.setFixedDimension(bufferedImage.getWidth(), bufferedImage.getHeight());
                }
                if (report.getTitle() != null) {
                    TextFieldBuilder textField = cmp.text(report.getTitle());
                    if (report.getTitleStyle() != null) {
                        textField.setStyle(ReportStyles.getStyle(report.getTitleStyle()));
                    } else {
                        textField.setStyle(ReportStyles.getStyle(ReportStyles.titleStyle()));
                    }
                    if (report.getLogo().getPosition() != LogoPosition.LEFT && report.getLogo().getPosition() != LogoPosition.LEFT_FIT) {
                        this.reportBuilder.addTitle(cmp.horizontalList(textField, image));
                    } else {
                        this.reportBuilder.addTitle(cmp.horizontalList(image, textField));
                    }
                } else {
                    this.reportBuilder.addTitle(image);
                }
            } else if (report.getTitle() != null) {
                TextFieldBuilder textField = cmp.text(report.getTitle());
                if (report.getTitleStyle() != null) {
                    textField.setStyle(ReportStyles.getStyle(report.getTitleStyle()));
                } else {
                    textField.setStyle(ReportStyles.getStyle(ReportStyles.titleStyle()));
                }
                this.reportBuilder.addTitle(textField);
            }
        }

        private void subtitle(DynamicReport report) {
            if (report.getSubtitle() != null) {
                TextFieldBuilder textField = cmp.text(report.getSubtitle());
                if (report.getSubtitleStyle() != null) {
                    textField.setStyle(ReportStyles.getStyle(report.getSubtitleStyle()));
                } else {
                    textField.setStyle(ReportStyles.getStyle(ReportStyles.subtitleStyle()));
                }
                this.reportBuilder.addTitle(textField);
            }

        }

        private void header(DynamicReport report) {
//            if (report.getLogo() != null) {
//                BufferedImage textField = ImageIO.read(new ByteArrayInputStream(report.getLogo().getFile()));
//                ImageBuilder image = cmp.image(textField).setFixedDimension(70, 70).setStyle(net.sf.dynamicreports.report.builder.DynamicReports.stl.style().setPadding(3));
//                if (report.getLogo().getPosition() == LogoPosition.LEFT || report.getLogo().getPosition() == LogoPosition.RIGHT) {
//                    image.setFixedDimension(textField.getWidth(), textField.getHeight());
//                }
//
//                if (report.getHeader() != null) {
//                    TextFieldBuilder textField1 = cmp.text(report.getHeader());
//                    if (report.getHeaderStyle() != null) {
//                        textField1.setStyle(ReportStyles.getStyle(report.getHeaderStyle()));
//                    } else {
//                        textField1.setStyle(ReportStyles.headerStyle(report.getHeaderStyle()));
//                    }
//
//                    if (report.getLogo().getPosition() != LogoPosition.LEFT && report.getLogo().getPosition() != LogoPosition.LEFT_FIT) {
//                        this.reportBuilder.addPageHeader(cmp.horizontalList(new ComponentBuilder[]{textField1, image}));
//                    } else {
//                        this.reportBuilder.addPageHeader(cmp.horizontalList(new ComponentBuilder[]{image, textField1}));
//                    }
//                } else {
//                    this.reportBuilder.addPageHeader(image);
//                }
//            } else if (report.getHeader() != null) {
            TextFieldBuilder textField2 = cmp.text(report.getHeader());
            if (report.getHeaderStyle() != null) {
                textField2.setStyle(ReportStyles.getStyle(report.getHeaderStyle()));
            } else {
                textField2.setStyle(ReportStyles.getStyle(ReportStyles.headerStyle()));
            }

            this.reportBuilder.addPageHeader(textField2);
//            }

        }

        private void footer(DynamicReport report) {
            if (report.getFooter() != null) {
                TextFieldBuilder textField = cmp.text(report.getFooter());
                if (report.getFooterStyle() != null) {
                    textField.setStyle(ReportStyles.getStyle(report.getFooterStyle()));
                } else {
                    textField.setStyle(ReportStyles.getStyle(ReportStyles.footerStyle()));
                }

                this.reportBuilder.addPageFooter(textField);
            }

        }

        private void reportSummary(DynamicReport report) {
            if (report.getSummary() != null) {
                TextFieldBuilder textField = cmp.text(report.getSummary());
                if (report.getSummaryStyle() != null) {
                    textField.setStyle(ReportStyles.getStyle(report.getSummaryStyle()));
                } else {
                    textField.setStyle(ReportStyles.getStyle(ReportStyles.summaryStyle()));
                }

                this.reportBuilder.addSummary(textField);
            }

        }

        private void rows(List<Row> rows) {
            if (Locales.isRtl()) {
                rows.forEach(r -> Collections.reverse(r.getColumns()));
            }
            VerticalListBuilder verticalListBuilder = cmp.verticalList();
            for (Row row : rows) {
                HorizontalListBuilder horizontalListBuilder = cmp.horizontalList();
                for (RowColumn rowColumn : row.getColumns()) {
                    TextFieldBuilder textField = cmp.text(rowColumn.getValue());
                    if (rowColumn.getStyle() != null) {
                        textField.setStyle(ReportStyles.getStyle(rowColumn.getStyle()));
                    } else {
                        textField.setStyle(ReportStyles.getStyle(ReportStyles.rowStyle()));
                    }
                    horizontalListBuilder.add(textField);
                }
                verticalListBuilder.add(horizontalListBuilder);
            }
            reportBuilder.addTitle(verticalListBuilder);
        }

        private void reportDate(DynamicReport report) {
            if (report.getReportDate() != null) {
                DateTime dateTime = DateTime.now();
                Date date = Date.now();
                Time time = Time.now();
                TextFieldBuilder textField;
                switch (report.getReportDate()) {
                    case DATE_TOP:
                        textField = cmp.text(date.toString());
                        this.reportBuilder.addPageHeader(textField);
                        break;
                    case TIME_TOP:
                        textField = cmp.text(time.toString());
                        this.reportBuilder.addPageHeader(textField);
                        break;
                    case DATE_TIME_TOP:
                        textField = cmp.text(dateTime.toString());
                        this.reportBuilder.addPageHeader(textField);
                        break;
                    case DATE_BOTTOM:
                        textField = cmp.text(date.toString());
                        this.reportBuilder.addPageFooter(textField);
                        break;
                    case TIME_BOTTOM:
                        textField = cmp.text(time.toString());
                        this.reportBuilder.addPageFooter(textField);
                        break;
                    default:
                        textField = cmp.text(dateTime.toString());
                        this.reportBuilder.addPageFooter(textField);
                }

                if (report.getReportDateStyle() != null) {
                    textField.setStyle(ReportStyles.getStyle(report.getReportDateStyle()));
                } else {
                    textField.setStyle(ReportStyles.getStyle(ReportStyles.reportDateStyle()));
                }
            }

        }

        private void pageNumber(DynamicReport report) {
            if (report.getPage().isPageNumber()) {
                PageNumberBuilder pageNumber = cmp.pageNumber();
                if (report.getPageNumberStyle() != null) {
                    pageNumber.setStyle(ReportStyles.getStyle(report.getPageNumberStyle()));
                } else {
                    pageNumber.setStyle(ReportStyles.getStyle(ReportStyles.pageNumberStyle()));
                }

                this.reportBuilder.addPageFooter(pageNumber);
            }

        }

        private ColumnBuilder<?, ?> getColumn(ReportColumn column) {
            Object columnBuilder = null;
            if (column.getField().getName() != null) {
                this.fieldBuilders.add(net.sf.dynamicreports.report.builder.DynamicReports.field(column.getField().getName(), column.getField().getTypeClass()));
            }

            Field valueColumnBuilder;
//            if(column.getField() instanceof SimpleField) {
            valueColumnBuilder = column.getField();
//                switch($SWITCH_TABLE$ir$sso$setadiservices$model$enums$SimpleFieldType()[valueColumnBuilder.getFieldType().ordinal()]) {
//                    case 12:
//                        columnBuilder = col.percentageColumn(valueColumnBuilder.getDisplayName(), valueColumnBuilder.getName(), column.getField().getTypeClass());
//                        break;
//                    case 13:
//                    default:
            columnBuilder = col.column(valueColumnBuilder.getDisplayName(), valueColumnBuilder.getName(), valueColumnBuilder.getTypeClass());
//                        break;
//                    case 14:
//                        columnBuilder = col.reportRowNumberColumn(valueColumnBuilder.getDisplayName());
//                }
//
            if (column.getWidth() != null) {
                ((ValueColumnBuilder) columnBuilder).setFixedWidth(column.getWidth());
            }
//            } else if(column.getField() instanceof BooleanField && ((BooleanField)column.getField()).getBooleanType() != BooleanType.CUSTOM) {
//                BooleanField valueColumnBuilder2 = (BooleanField)column.getField();
//                BooleanColumnBuilder booleanColumnBuilder = col.booleanColumn(valueColumnBuilder2.getDisplayName(), valueColumnBuilder2.getName());
//                booleanColumnBuilder.setEmptyWhenNullValue(Boolean.valueOf(true));
//                switch($SWITCH_TABLE$ir$sso$setadiservices$model$enums$BooleanType()[((BooleanField)column.getField()).getBooleanType().ordinal()]) {
//                    case 2:
//                        booleanColumnBuilder.setComponentType(BooleanComponentType.TEXT_YES_NO);
//                        break;
//                    case 3:
//                        booleanColumnBuilder.setComponentType(BooleanComponentType.TEXT_TRUE_FALSE);
//                        break;
//                    case 4:
//                        booleanColumnBuilder.setComponentType(BooleanComponentType.IMAGE_STYLE_1);
//                        break;
//                    case 5:
//                        booleanColumnBuilder.setComponentType(BooleanComponentType.IMAGE_STYLE_2);
//                        break;
//                    case 6:
//                        booleanColumnBuilder.setComponentType(BooleanComponentType.IMAGE_STYLE_3);
//                        break;
//                    case 7:
//                        booleanColumnBuilder.setComponentType(BooleanComponentType.IMAGE_STYLE_4);
//                        break;
//                    case 8:
//                        booleanColumnBuilder.setComponentType(BooleanComponentType.IMAGE_CHECKBOX_1);
//                        break;
//                    case 9:
//                        booleanColumnBuilder.setComponentType(BooleanComponentType.IMAGE_CHECKBOX_2);
//                        break;
//                    case 10:
//                        booleanColumnBuilder.setComponentType(BooleanComponentType.IMAGE_BALL);
//                }

//                if(column.getWidth() != null) {
//                    booleanColumnBuilder.setFixedWidth(column.getWidth());
//                }
//
//                booleanColumnBuilder.setHorizontalAlignment(HorizontalAlignment.CENTER);
//                columnBuilder = booleanColumnBuilder;
//            } else {
            valueColumnBuilder = null;
            TextColumnBuilder valueColumnBuilder1;
            if (column.getField() instanceof IExpression) {
                valueColumnBuilder1 = col.column(column.getField().getDisplayName(), ((IExpression) column.getField()).getExpression());
            } else {
                valueColumnBuilder1 = col.column(column.getField().getDisplayName(), column.getField().getName(), column.getField().getTypeClass());
            }

            if (column.getField() instanceof IFormatter) {
                valueColumnBuilder1.setValueFormatter(((IFormatter) column.getField()).getFormatter());
            }

            if (column.getWidth() != null) {
                valueColumnBuilder1.setFixedWidth(column.getWidth());
            }

            columnBuilder = valueColumnBuilder1;
//            }

            if (column.getHeaderStyle() != null) {
                ((ColumnBuilder) columnBuilder).setTitleStyle(ReportStyles.getStyle(column.getHeaderStyle()));
            } else {
                ((ColumnBuilder) columnBuilder).setTitleStyle(ReportStyles.getStyle(ReportStyles.columnHeaderStyle()));
            }

            if (column.getFieldStyle() != null) {
                ((ColumnBuilder) columnBuilder).setStyle(ReportStyles.getStyle(column.getFieldStyle()));
            } else {
                ((ColumnBuilder) columnBuilder).setStyle(ReportStyles.getStyle(ReportStyles.columnStyle()));
            }

//            if(column.getSummary() != null && column.getField().isNumber()) {
//                this.columnSummary(column, (TextColumnBuilder)columnBuilder);
//            }

//            if(!(column.getField() instanceof SimpleField) || ((SimpleField)column.getField()).getFieldType() != SimpleFieldType.ROW_NUMBER) {
//                this.fieldBuilders.add(net.sf.dynamicreports.report.builder.DynamicReports.field(column.getField().getName(), column.getField().getTypeClass()));
//            }
            if (!(column.getField() instanceof SimpleField)) {
                this.fieldBuilders.add(net.sf.dynamicreports.report.builder.DynamicReports.field(column.getField().getName(), column.getField().getTypeClass()));
            }

//            if(column.getSubtotal() != null && column.getField().isNumber()) {
//                this.subtotals.add(this.subtotal(column, (TextColumnBuilder)columnBuilder));
//            }

            return (ColumnBuilder) columnBuilder;
        }
    }
}
