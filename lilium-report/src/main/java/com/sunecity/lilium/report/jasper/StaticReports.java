package com.sunecity.lilium.report.jasper;

import com.sunecity.lilium.data.report.StaticReport;
import net.sf.jasperreports.engine.JRVirtualizationHelper;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;

import javax.persistence.EntityManager;
import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Shahram Goodarzi
 */
public class StaticReports {

    private StaticReports() {
    }

    public static JasperPrint getJasperPrint(StaticReport staticReport, Collection<?> collection) throws Exception {
        if (staticReport.isCompiled()) {
            return JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), parameters(null), new JRBeanCollectionDataSource(collection));
        } else {
            JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
            return JasperFillManager.fillReport(jasperReport, parameters(null), new JRBeanCollectionDataSource(collection));
        }
    }

    public static JasperPrint getJasperPrint(StaticReport staticReport, Collection<?> collection, Map<String, Object> parameters) throws Exception {
        if (staticReport.isCompiled()) {
            return JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), parameters(parameters), new JRBeanCollectionDataSource(collection));
        } else {
            JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
            return JasperFillManager.fillReport(jasperReport, parameters(parameters), new JRBeanCollectionDataSource(collection));
        }
    }

    public static JasperPrint getJasperPrint(StaticReport staticReport, Connection connection) throws Exception {
        if (staticReport.isCompiled()) {
            return JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), parameters(null), connection);
        } else {
            JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
            return JasperFillManager.fillReport(jasperReport, parameters(null), connection);
        }
    }

    public static JasperPrint getJasperPrint(StaticReport staticReport, Connection connection, Map<String, Object> parameters) throws Exception {
        if (staticReport.isCompiled()) {
            return JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), parameters(parameters), connection);
        } else {
            JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
            return JasperFillManager.fillReport(jasperReport, parameters(parameters), connection);
        }
    }

    public static JasperPrint getJasperPrint(StaticReport staticReport, EntityManager entityManager) throws Exception {
        if (staticReport.isCompiled()) {
            return JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), parameter("JPA_ENTITY_MANAGER", entityManager));
        } else {
            JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
            return JasperFillManager.fillReport(jasperReport, parameter("JPA_ENTITY_MANAGER", entityManager));
        }
    }

    public static JasperPrint getJasperPrint(StaticReport staticReport, EntityManager entityManager, Map<String, Object> parameters) throws Exception {
        if (staticReport.isCompiled()) {
            return JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), parameter("JPA_ENTITY_MANAGER", entityManager));
        } else {
            JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
            return JasperFillManager.fillReport(jasperReport, parameter("JPA_ENTITY_MANAGER", entityManager));
        }
    }

    private static Map<String, Object> parameters(Map<String, Object> params) {
        HashMap<String, Object> parameters = new HashMap<>();
        JRFileVirtualizer virtualizer = new JRFileVirtualizer(5);
        JRVirtualizationHelper.setThreadVirtualizer(virtualizer);
        parameters.put("REPORT_VIRTUALIZER", virtualizer);
        if (params != null) {
            parameters.putAll(params);
        }

        return parameters;
    }

    private static Map<String, Object> parameter(String name, Object value) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(name, value);
        return parameters(parameters);
    }

}