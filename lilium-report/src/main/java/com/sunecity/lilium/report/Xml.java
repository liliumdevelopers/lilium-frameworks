package com.sunecity.lilium.report;

import org.apache.commons.beanutils.PropertyUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class Xml implements Serializable {

    public <T> void export(List<T> values, List<String> fields, OutputStream outputStream) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IOException {
        Element root = new Element("root");
        Document doc = new Document(root);
        for (T t : values) {
            Element element = new Element(t.getClass().getSimpleName());
            for (String field : fields) {
                Element e = new Element(field);
                Object object = PropertyUtils.getProperty(t, field);
                e.addContent(object != null ? object.toString() : "");
                element.addContent(e);
            }
            root.addContent(element);
        }
        XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
        xout.output(doc, outputStream);
    }

    public <T> List<T> getValues(InputStream inputStream, Class<T> clas, List<String> fields) throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, JDOMException {
        List<T> list = new ArrayList<>();
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(inputStream);
        Element root = doc.getRootElement();
        for (Element element : root.getChildren()) {
            T t = clas.getConstructor().newInstance();
            for (int i = 0; i < fields.size(); i++) {
                Element fieldElement = element.getChildren().get(i);
                PropertyUtils.setProperty(t, fields.get(i), fieldElement.getValue());
            }
            list.add(t);
        }
        return list;
    }

}
