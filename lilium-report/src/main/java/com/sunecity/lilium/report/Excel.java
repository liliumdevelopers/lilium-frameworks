package com.sunecity.lilium.report;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class Excel implements Serializable {

    private String sheetName;
    private int sheetNumber;
    private boolean rtl;

    public void setFile(ExcelType type, InputStream stream) {

    }

    public void setFile(ExcelType type, OutputStream stream) {

    }

    public void export(int from, int to) {

    }

    public void getValues(int from, int to) {

    }

    public <T> void export(String sheetName, boolean rtl, ExcelType type, List<T> values, List<String> fields, OutputStream outputStream) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IOException {
        Workbook wb = getWorkbook(type);
        Sheet sheet = wb.createSheet(sheetName);
        sheet.setRightToLeft(rtl);
        Row header = sheet.createRow(0);
        CellStyle headerStyle = wb.createCellStyle();
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        for (int i = 0; i < fields.size(); i++) {
            Cell head = header.createCell(i);
            head.setCellStyle(headerStyle);
            head.setCellValue(fields.get(i));
        }
        CellStyle style = wb.createCellStyle();
        style.setAlignment(rtl ? HorizontalAlignment.RIGHT : HorizontalAlignment.LEFT);
        for (int i = 0; i < values.size(); i++) {
            T t = values.get(i);
            Row row = sheet.createRow(i + 1);
            for (int j = 0; j < fields.size(); j++) {
                Cell cell = row.createCell(j);
                cell.setCellStyle(style);
                Object object = PropertyUtils.getProperty(t, fields.get(j));
                if (object != null) {
                    if (object instanceof Number) {
                        cell.setCellValue(((Number) object).doubleValue());
                    } else if (object instanceof Boolean) {
                        cell.setCellValue((Boolean) object);
                    } else if (object instanceof Calendar) {
                        CreationHelper createHelper = wb.getCreationHelper();
//                        cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
                        cell.setCellValue((Calendar) object);
                    } else if (object instanceof Date) {
                        cell.setCellValue((Date) object);
                    } else {
                        cell.setCellValue(object.toString());
                    }
                } else {
                    cell.setBlank();
                }
            }
        }
        for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {
            sheet.autoSizeColumn(i);
        }
        wb.write(outputStream);
    }

    public <T> List<T> getValues(InputStream inputStream, ExcelType type, int sheetNumber, Class<T> clas, List<String> fields) throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        List<T> list = new ArrayList<>();
        Workbook workbook = getWorkbook(type, inputStream);
        Sheet sheet = workbook.getSheetAt(sheetNumber);
        for (Row row : sheet) {
            T t = clas.getConstructor().newInstance();
            int c = 0;
            for (Cell cell : row) {
                switch (cell.getCellType()) {
                    case STRING:
                        PropertyUtils.setProperty(t, fields.get(c), cell.getStringCellValue());
                        break;
                    case NUMERIC:
                        PropertyUtils.setProperty(t, fields.get(c), cell.getNumericCellValue());
                        break;
                    case BOOLEAN:
                        PropertyUtils.setProperty(t, fields.get(c), cell.getBooleanCellValue());
                        break;
                    case FORMULA:
                        PropertyUtils.setProperty(t, fields.get(c), cell.getCellFormula());
                        break;
                }
                c++;
            }
            list.add(t);
        }
        return list;
    }

    private Workbook getWorkbook(ExcelType type, InputStream stream) throws IOException {
        Workbook workbook = null;
        switch (type) {
            case XLS:
                workbook = new HSSFWorkbook(stream);
                break;
            case XLSX:
                workbook = new XSSFWorkbook(stream);
                break;
        }
        return workbook;
    }

    private Workbook getWorkbook(ExcelType type) {
        Workbook workbook = null;
        switch (type) {
            case XLS:
                workbook = new HSSFWorkbook();
                break;
            case XLSX:
                workbook = new XSSFWorkbook();
                break;
        }
        return workbook;
    }

    public enum ExcelType {
        XLS, XLSX
    }

}
