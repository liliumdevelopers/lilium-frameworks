package com.sunecity.lilium.report.barcode;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.output.OutputException;

import javax.enterprise.context.Dependent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Dependent
public class Barbecue implements com.sunecity.lilium.report.barcode.Barcode {

    public Barbecue() {
    }

    @Override
    public byte[] generate(String str, BarcodeType type) throws IOException {
        if (!(type instanceof BarbecueType)) {
            throw new IllegalArgumentException("Type is not a BarbecueType!");
        }
        try {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            generate(outputStream, str, (BarbecueType) type);
            final byte[] bs = outputStream.toByteArray();
            outputStream.close();
            return bs;
        } catch (OutputException | BarcodeException e) {
            throw new IOException(e);
        }
    }

    public void generate(OutputStream outputStream, String str, BarbecueType type) throws OutputException, BarcodeException {
        final Barcode barcode = getBarcode(str, type);
        BarcodeImageHandler.writeJPEG(barcode, outputStream);
    }

    private Barcode getBarcode(String str, BarbecueType type) throws BarcodeException {
        Barcode barcode = null;
        switch (type) {
            case TOW_OF7:
                barcode = BarcodeFactory.create2of7(str);
                break;
            case THREE_OF9:
                barcode = BarcodeFactory.create3of9(str, false);
                break;
            case BOOKLAND:
                barcode = BarcodeFactory.createBookland(str);
                break;
            case CODABAR:
                barcode = BarcodeFactory.createCodabar(str);
                break;
            case CODE_128:
                barcode = BarcodeFactory.createCode128(str);
                break;
            case CODE_128A:
                barcode = BarcodeFactory.createCode128A(str);
                break;
            case CODE_128B:
                barcode = BarcodeFactory.createCode128B(str);
                break;
            case CODE_128C:
                barcode = BarcodeFactory.createCode128C(str);
                break;
            case CODE_39:
                barcode = BarcodeFactory.createCode39(str, false);
                break;
            case EAN_128:
                barcode = BarcodeFactory.createEAN128(str);
                break;
            case EAN_13:
                barcode = BarcodeFactory.createEAN13(str);
                break;
            case GLOBAL_TRADE_ITEM_NUMBER:
                barcode = BarcodeFactory.createGlobalTradeItemNumber(str);
                break;
            case INT_2OF5:
                barcode = BarcodeFactory.createInt2of5(str);
                break;
            case MONARCH:
                barcode = BarcodeFactory.createMonarch(str);
                break;
            case NW7:
                barcode = BarcodeFactory.createNW7(str);
                break;
            case PDF417:
                barcode = BarcodeFactory.createPDF417(str);
                break;
            case POST_NET:
                barcode = BarcodeFactory.createPostNet(str);
                break;
            case RANDOM_WEIGHT_UPCA:
                barcode = BarcodeFactory.createRandomWeightUPCA(str);
                break;
            case SCC14_SHIPPING_CODE:
                barcode = BarcodeFactory.createSCC14ShippingCode(str);
                break;
            case SHIPMENT_IDENTIFICATION_NUMBER:
                barcode = BarcodeFactory.createShipmentIdentificationNumber(str);
                break;
            case SSCC18:
                barcode = BarcodeFactory.createSSCC18(str);
                break;
            case STD_2OF5:
                barcode = BarcodeFactory.createStd2of5(str);
                break;
            case UPCA:
                barcode = BarcodeFactory.createUPCA(str);
                break;
            case USD3:
                barcode = BarcodeFactory.createUSD3(str, false);
                break;
            case USD4:
                barcode = BarcodeFactory.createUSD4(str);
                break;
            case USPS:
                barcode = BarcodeFactory.createUSPS(str);
                break;
        }
        return barcode;
    }

}
