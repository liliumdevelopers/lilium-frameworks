package com.sunecity.lilium.report.jasper.interfaces;

import net.sf.dynamicreports.report.builder.expression.AbstractComplexExpression;

/**
 * @author Shahram Goodarzi
 */
public interface IComplexExpression<T> extends IExpression<T> {

    @Override
    AbstractComplexExpression<T> getExpression();

}
