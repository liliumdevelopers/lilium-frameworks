package com.sunecity.lilium.report;

import org.artofsolving.jodconverter.OfficeDocumentConverter;
import org.artofsolving.jodconverter.office.DefaultOfficeManagerConfiguration;
import org.artofsolving.jodconverter.office.OfficeManager;

import javax.enterprise.context.Dependent;
import java.nio.file.Path;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class Converter implements AutoCloseable {

    private OfficeManager officeManager;
    private OfficeDocumentConverter documentConverter;

    public Converter() {
    }

    public void connect(int... ports) throws Exception {
        officeManager = new DefaultOfficeManagerConfiguration().setPortNumbers(ports).buildOfficeManager();
        officeManager.start();
        documentConverter = new OfficeDocumentConverter(officeManager);
    }

    public void convert(Path input, Path output) throws Exception {
        documentConverter.convert(input.toFile(), output.toFile());
    }

    @Override
    public void close() throws Exception {
        officeManager.stop();
    }

}
