package com.sunecity.lilium.report;

import org.apache.commons.beanutils.PropertyUtils;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class Text implements Serializable {

    public <T> void export(List<T> values, List<String> fields, OutputStream outputStream) throws IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        try (Writer writer = new OutputStreamWriter(outputStream, "UTF-8")) {
            for (T t : values) {
                for (String field : fields) {
                    Object object = PropertyUtils.getProperty(t, field);
                    if (object != null) {
                        writer.write(object.toString());
                    } else {
                        writer.write("");
                    }
                    writer.write("\t");
                }
                writer.write("\r\n");
            }
        }
    }

    public <T> List<T> getValues(InputStream inputStream, Class<T> clas, List<String> fields) throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        List<T> list = new ArrayList<>();
        try (Reader reader = new InputStreamReader(inputStream, "UTF-8");
             Scanner scanner = new Scanner(reader)) {
            while (scanner.hasNextLine()) {
                T t = clas.getConstructor().newInstance();
                String[] strings = scanner.nextLine().split("\t");
                for (int i = 0; i < strings.length; i++) {
                    PropertyUtils.setProperty(t, fields.get(i), strings[i]);
                }
                list.add(t);
            }
        }
        return list;
    }

}
