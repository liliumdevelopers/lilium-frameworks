package com.sunecity.lilium.report.barcode;

public enum Barcode4jType implements BarcodeType {

    INTERLEAVED_2OF5, ITF_14, CODE_39, CODE_128, CODABAR, UPC_A, UPC_E, EAN_13, EAN_8, EAN_128, POSTNET, ROYAL_MAIL, USPS, PDF417, DATA_MATRIX

}