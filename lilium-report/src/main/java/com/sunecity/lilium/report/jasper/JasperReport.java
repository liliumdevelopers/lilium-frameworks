package com.sunecity.lilium.report.jasper;

import com.sunecity.lilium.core.localization.Locales;
import com.sunecity.lilium.data.report.DynamicReport;
import com.sunecity.lilium.data.report.Report;
import com.sunecity.lilium.data.report.ReportType;
import com.sunecity.lilium.data.report.StaticReport;
import com.sunecity.lilium.report.util.ReportUtils;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRVirtualizationHelper;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.query.JRJpaQueryExecuterFactory;
import net.sf.jasperreports.engine.type.RunDirectionEnum;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterConfiguration;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

import javax.persistence.EntityManager;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Dynamically create reports based on jasper.
 *
 * @author Shahram Goodarzi
 */
public class JasperReport {

    private JasperReport() {
    }

    public static String createReport(Report report, ReportType reportType, Map<String, Object> params) throws Exception {
        if (report instanceof DynamicReport) {
            JasperPrint print = DynamicReports.generateJasperPrint((DynamicReport) report, reportType);
            return generateReport(report, reportType, print);
        } else {
            StaticReport staticReport = (StaticReport) report;
            if (staticReport.isCompiled()) {
                JasperPrint print = JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), setSubReport(staticReport, parameter(params)), new JREmptyDataSource());
                return generateReport(report, reportType, print);
            } else {
                net.sf.jasperreports.engine.JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
                JasperPrint print = JasperFillManager.fillReport(jasperReport, compileSubReport(staticReport, parameter(params)), new JREmptyDataSource());
                return generateReport(report, reportType, print);
            }
        }
    }

    public static String createReport(Report report, ReportType reportType, Connection connection, Map<String, Object> params) throws Exception {
        if (report instanceof DynamicReport) {
            JasperPrint print = DynamicReports.generateJasperPrint((DynamicReport) report, connection, reportType);
            return generateReport(report, reportType, print);
        } else {
            StaticReport staticReport = (StaticReport) report;
            if (staticReport.isCompiled()) {
                JasperPrint print = JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), setSubReport(staticReport, parameter(params)), connection);
                return generateReport(report, reportType, print);
            } else {
                net.sf.jasperreports.engine.JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
                JasperPrint print = JasperFillManager.fillReport(jasperReport, compileSubReport(staticReport, parameter(params)), connection);
                return generateReport(report, reportType, print);
            }
        }
    }

    public static String createReport(Report report, ReportType reportType, EntityManager entityManager, Map<String, Object> params) throws Exception {
        if (report instanceof DynamicReport) {
            JasperPrint print = DynamicReports.generateJasperPrint((DynamicReport) report, entityManager, reportType);
            return generateReport(report, reportType, print);
        } else {
            StaticReport staticReport = (StaticReport) report;
            Map<String, Object> parameters = parameter(params);
            parameters.put(JRJpaQueryExecuterFactory.PARAMETER_JPA_ENTITY_MANAGER, entityManager);
            if (staticReport.isCompiled()) {
                JasperPrint print = JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), setSubReport(staticReport, parameters));
                return generateReport(report, reportType, print);
            } else {
                net.sf.jasperreports.engine.JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
                JasperPrint print = JasperFillManager.fillReport(jasperReport, compileSubReport(staticReport, parameters));
                return generateReport(report, reportType, print);
            }
        }
    }

    public static String createReport(Report report, ReportType reportType, Collection<?> collection, Map<String, Object> params) throws Exception {
        if (report instanceof DynamicReport) {
            JasperPrint print = DynamicReports.generateJasperPrint((DynamicReport) report, collection, reportType);
            return generateReport(report, reportType, print);
        } else {
            StaticReport staticReport = (StaticReport) report;
            if (staticReport.isCompiled()) {
                JasperPrint print = JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), setSubReport(staticReport, parameter(params)), new JRBeanCollectionDataSource(collection));
                return generateReport(report, reportType, print);
            } else {
                net.sf.jasperreports.engine.JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
                JasperPrint print = JasperFillManager.fillReport(jasperReport, compileSubReport(staticReport, parameter(params)), new JRBeanCollectionDataSource(collection));
                return generateReport(report, reportType, print);
            }
        }
    }

    public static void createReport(Report report, ReportType reportType, OutputStream outputStream, Map<String, Object> params) throws Exception {
        if (report instanceof DynamicReport) {
            JasperPrint print = DynamicReports.generateJasperPrint((DynamicReport) report, reportType);
            generateReport(reportType, print, outputStream);
        } else {
            StaticReport staticReport = (StaticReport) report;
            if (staticReport.isCompiled()) {
                JasperPrint print = JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), setSubReport(staticReport, parameter(params)), new JREmptyDataSource());
                generateReport(reportType, print, outputStream);
            } else {
                net.sf.jasperreports.engine.JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
                JasperPrint print = JasperFillManager.fillReport(jasperReport, compileSubReport(staticReport, parameter(params)), new JREmptyDataSource());
                generateReport(reportType, print, outputStream);
            }
        }
    }

    public static void createReport(Report report, ReportType reportType, Connection connection, OutputStream outputStream, Map<String, Object> params) throws Exception {
        if (report instanceof DynamicReport) {
            JasperPrint print = DynamicReports.generateJasperPrint((DynamicReport) report, connection, reportType);
            generateReport(reportType, print, outputStream);
        } else {
            StaticReport staticReport = (StaticReport) report;
            if (staticReport.isCompiled()) {
                JasperPrint print = JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), setSubReport(staticReport, parameter(params)), connection);
                generateReport(reportType, print, outputStream);
            } else {
                net.sf.jasperreports.engine.JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
                JasperPrint print = JasperFillManager.fillReport(jasperReport, compileSubReport(staticReport, parameter(params)), connection);
                generateReport(reportType, print, outputStream);
            }
        }
    }

    public static void createReport(Report report, ReportType reportType, EntityManager entityManager, OutputStream outputStream, Map<String, Object> params) throws Exception {
        if (report instanceof DynamicReport) {
            JasperPrint print = DynamicReports.generateJasperPrint((DynamicReport) report, entityManager, reportType);
            generateReport(reportType, print, outputStream);
        } else {
            StaticReport staticReport = (StaticReport) report;
            Map<String, Object> parameters = parameter(params);
            parameters.put(JRJpaQueryExecuterFactory.PARAMETER_JPA_ENTITY_MANAGER, entityManager);
            if (staticReport.isCompiled()) {
                JasperPrint print = JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), setSubReport(staticReport, parameter(params)));
                generateReport(reportType, print, outputStream);
            } else {
                net.sf.jasperreports.engine.JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
                JasperPrint print = JasperFillManager.fillReport(jasperReport, compileSubReport(staticReport, parameter(params)));
                generateReport(reportType, print, outputStream);
            }
        }
    }

    public static void createReport(Report report, ReportType reportType, Collection<?> collection, OutputStream outputStream, Map<String, Object> params) throws Exception {
        if (report instanceof DynamicReport) {
            JasperPrint print = DynamicReports.generateJasperPrint((DynamicReport) report, collection, reportType);
            generateReport(reportType, print, outputStream);
        } else {
            StaticReport staticReport = (StaticReport) report;
            if (staticReport.isCompiled()) {
                JasperPrint print = JasperFillManager.fillReport(new ByteArrayInputStream(staticReport.getFile()), setSubReport(staticReport, parameter(params)), new JRBeanCollectionDataSource(collection));
                generateReport(reportType, print, outputStream);
            } else {
                net.sf.jasperreports.engine.JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(staticReport.getFile()));
                JasperPrint print = JasperFillManager.fillReport(jasperReport, compileSubReport(staticReport, parameter(params)), new JRBeanCollectionDataSource(collection));
                generateReport(reportType, print, outputStream);
            }
        }
    }

    private static Map<String, Object> setSubReport(StaticReport staticReport, Map<String, Object> params) throws JRException {
        if (staticReport.getSubReports() != null) {
            for (StaticReport subReport : staticReport.getSubReports()) {
                net.sf.jasperreports.engine.JasperReport jasperReport = (net.sf.jasperreports.engine.JasperReport) JRLoader.loadObject(new ByteArrayInputStream(subReport.getFile()));
                params.put(subReport.getName(), jasperReport);
            }
        }
        return params;
    }

    private static Map<String, Object> compileSubReport(StaticReport staticReport, Map<String, Object> params) throws JRException {
        if (staticReport.getSubReports() != null) {
            for (StaticReport subReport : staticReport.getSubReports()) {
                net.sf.jasperreports.engine.JasperReport jasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(subReport.getFile()));
                params.put(subReport.getName(), jasperReport);
            }
        }
        return params;
    }

    private static Map<String, Object> parameter(Map<String, Object> params) {
        Map<String, Object> parameters = new HashMap<>();
        JRFileVirtualizer virtualizer = new JRFileVirtualizer(5, "tempreport");
        JRVirtualizationHelper.setThreadVirtualizer(virtualizer);
        parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
        parameters.put(JRParameter.REPORT_LOCALE, Locales.getLocale());
        if (params != null) {
            parameters.putAll(params);
        }
        return parameters;
    }

    private static Exporter getExporter(ReportType type) {
        Exporter exporter = null;
        switch (type) {
            case PDF:
                exporter = new JRPdfExporter();
                break;
            case XLSX:
                exporter = new JasperXlsxExporter();
                SimpleXlsxReportConfiguration xlsReportConfiguration = new SimpleXlsxReportConfiguration();
                xlsReportConfiguration.setOnePagePerSheet(false);
                xlsReportConfiguration.setRemoveEmptySpaceBetweenRows(true);
                xlsReportConfiguration.setRemoveEmptySpaceBetweenColumns(true);
                xlsReportConfiguration.setWhitePageBackground(false);
                xlsReportConfiguration.setDetectCellType(true);
                xlsReportConfiguration.setSheetDirection(RunDirectionEnum.RTL);
                exporter.setConfiguration(xlsReportConfiguration);
                break;
            case DOCX:
                exporter = new JRDocxExporter();
                break;
            case PPTX:
                exporter = new JRPptxExporter();
                break;
            case RTF:
                exporter = new JRRtfExporter();
                break;
            case HTML:
                exporter = new HtmlExporter();
                String header = "<html>\n";
                header += "<head>\n";
                header += "<title>";
                header += "report";
                header += "</title>\n";
                header += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n";
                header += "<style type=\"text/css\">\n";
                header += "table {border-collapse: collapse}\n";
                header += "a {text-decoration: none}\n";
                header += "</style>\n";
                header += "</head>\n";
                header += "<body dir=\"rtl\" text=\"#000000\" link=\"#000000\" alink=\"#000000\" vlink=\"#000000\">\n";
                header += "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
                header += "<tr><td width=\"50%\">&nbsp;</td><td align=\"center\">\n";
                SimpleHtmlExporterConfiguration htmlExporterConfiguration = new SimpleHtmlExporterConfiguration();
                htmlExporterConfiguration.setHtmlHeader(header);
                exporter.setConfiguration(htmlExporterConfiguration);
                break;
            case HTML_PRINT:
                exporter = new HtmlExporter();
                header = "<html>\n";
                header += "<head>\n";
                header += "<title>";
                header += "report";
                header += "</title>\n";
                header += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n";
                header += "<style type=\"text/css\">\n";
                header += "table {border-collapse: collapse}\n";
                header += "a {text-decoration: none}\n";
                header += "</style>\n";
                header += "</head>\n";
                header += "<body text=\"#000000\" link=\"#000000\" alink=\"#000000\" vlink=\"#000000\" onload=\"window.print();\">\n";
                header += "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
                header += "<tr><td width=\"50%\">&nbsp;</td><td align=\"center\">\n";
                htmlExporterConfiguration = new SimpleHtmlExporterConfiguration();
                htmlExporterConfiguration.setHtmlHeader(header);
                exporter.setConfiguration(htmlExporterConfiguration);
                break;
            case PRINT:
                exporter = new JRPrintServiceExporter();
                PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
                MediaSizeName mediaSizeName = MediaSizeName.ISO_A4;
                printRequestAttributeSet.add(mediaSizeName);
                printRequestAttributeSet.add(OrientationRequested.PORTRAIT);
                printRequestAttributeSet.add(new MediaPrintableArea(10, 10, MediaSize.getMediaSizeForName(mediaSizeName).getX(MediaPrintableArea.MM) - 20, MediaSize.getMediaSizeForName(mediaSizeName).getY(MediaPrintableArea.MM) - 20, MediaPrintableArea.MM));
                PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
                SimplePrintServiceExporterConfiguration printServiceExporterConfiguration = new SimplePrintServiceExporterConfiguration();
                printServiceExporterConfiguration.setPrintRequestAttributeSet(printRequestAttributeSet);
                printServiceExporterConfiguration.setPrintServiceAttributeSet(printServiceAttributeSet);
                printServiceExporterConfiguration.setDisplayPageDialog(false);
                printServiceExporterConfiguration.setDisplayPrintDialog(true);
                exporter.setConfiguration(printServiceExporterConfiguration);
                break;
        }
//		exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
        return exporter;
    }

    private static String generateReport(Report report, ReportType reportType, JasperPrint print) throws Exception {
        String reportName = report.getName() + "_" + ReportUtils.random() + "." + reportType.getExtension();
        File file = new File(ReportUtils.getPath() + File.separator + reportName);
        Exporter exporter = getExporter(reportType);
        exporter.setExporterInput(new SimpleExporterInput(print));
        if (reportType == ReportType.HTML || reportType == ReportType.HTML_PRINT) {
            exporter.setExporterOutput(new SimpleHtmlExporterOutput(file));
        } else if (reportType == ReportType.RTF) {
            exporter.setExporterOutput(new SimpleWriterExporterOutput(file));
        } else {
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
        }
        exporter.exportReport();
        ReportUtils.scheduleRemoval(file.getAbsolutePath());
        return ReportUtils.generatePath(reportName, file);
    }

    private static void generateReport(ReportType reportType, JasperPrint print, OutputStream outputStream) throws Exception {
        Exporter exporter = getExporter(reportType);
        exporter.setExporterInput(new SimpleExporterInput(print));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
        exporter.exportReport();
    }

}
