package com.sunecity.lilium.report.util;

import com.sunecity.lilium.core.util.IO;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by shahram on 7/12/16.
 */
public class ReportUtils {

    public static void scheduleRemoval(String file) {
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.schedule(() -> {
            try {
                Path path = Paths.get(file);
                if (path.toFile().exists()) {
                    delete(path);
                }
                path = Paths.get(file + "_files");
                if (path.toFile().exists()) {
                    delete(path);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, 30, TimeUnit.MINUTES);
        scheduledExecutorService.shutdown();
    }

    private static void delete(Path path) throws IOException {
        IO.delete(path);
    }

    private static boolean isWebApp() {
        try {
            Class.forName("javax.faces.context.FacesContext");
            return FacesContext.getCurrentInstance() != null;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public static String random() {
//        return new BigInteger(130, new SecureRandom()).toString(32);
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public static String generatePath(String reportName, File file) {
        if (ReportUtils.isWebApp()) {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            HttpServletRequest request = ((HttpServletRequest) externalContext.getRequest());
            return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + externalContext.getRequestContextPath() + "/report/" + reportName;
        } else {
            return file.getAbsolutePath();
        }
    }

    public static String getPath() throws IOException {
        String path;
        if (ReportUtils.isWebApp()) {
            path = URLDecoder.decode(FacesContext.getCurrentInstance().getExternalContext().getResource("/WEB-INF/").getFile(), "UTF-8");
            path = path.substring(0, path.length() - 9);
        } else {
            path = System.getProperty("user.dir");
        }
        path += FileSystems.getDefault().getSeparator() + "report";
        Path p = new File(path).toPath();
        if (!Files.exists(p)) {
            Files.createDirectories(p);
        }
        return path;
    }

}
