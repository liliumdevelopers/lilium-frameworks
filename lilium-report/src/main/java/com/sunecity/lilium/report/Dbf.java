package com.sunecity.lilium.report;

import org.apache.commons.beanutils.PropertyUtils;
import org.xBaseJ.DBF;
import org.xBaseJ.fields.CharField;
import org.xBaseJ.fields.Field;
import org.xBaseJ.fields.PictureField;
import org.xBaseJ.xBaseJException;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class Dbf implements Serializable {

    public <T> void export(List<T> values, Map<String, Type> fields, OutputStream outputStream) throws IOException, xBaseJException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Path path = Files.createTempFile("DbfOutTemp", ".dbf");
        try (DBF dbf = new DBF(path.toFile().getAbsolutePath(), true, "UTF-8")) {
            for (Map.Entry<String, Type> entry : fields.entrySet()) {
                Field field = new CharField(entry.getKey(), 20);
                switch (entry.getValue()) {
                    case BLOB:
                        field = new PictureField(entry.getKey());
                        break;
                    case NUM:
//                        field = new NumField(entry.getKey());
                        break;
                    default:
                        field = new CharField(entry.getKey(), 20);
                }
                dbf.addField(field);
            }
            for (T t : values) {
                for (Map.Entry<String, Type> entry : fields.entrySet()) {
                    Object object = PropertyUtils.getProperty(t, entry.getKey());
                    if (object != null) {
                        dbf.getField(entry.getKey()).put(object.toString().getBytes());
                    } else {
                        dbf.getField(entry.getKey()).put("");
                    }
                }
                dbf.write();
            }
            Files.copy(path, outputStream);
        }
        Files.delete(path);
    }

    public <T> List<T> getValues(InputStream inputStream, Class<T> clas, Collection<String> fields) throws IOException, xBaseJException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        List<T> list = new ArrayList<>();
        Path path = Files.createTempFile("DbfInTemp", ".dbf");
        Files.copy(inputStream, path, StandardCopyOption.REPLACE_EXISTING);
        try (DBF dbf = new DBF(path.toFile().getAbsolutePath(), "UTF-8")) {
            for (int i = 0; i < dbf.getRecordCount(); i++) {
                dbf.read();
                T t = clas.getConstructor().newInstance();
                for (String f : fields) {
                    Field field = dbf.getField(f);
                    PropertyUtils.setProperty(t, f, field.get());
                }
                list.add(t);
            }
        }
        Files.delete(path);
        return list;
    }

    public enum Type {
        CHAR, NUM, BLOB
    }

}
