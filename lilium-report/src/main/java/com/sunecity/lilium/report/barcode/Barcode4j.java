package com.sunecity.lilium.report.barcode;

import org.krysalis.barcode4j.impl.AbstractBarcodeBean;
import org.krysalis.barcode4j.impl.codabar.CodabarBean;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.impl.code128.EAN128Bean;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.impl.datamatrix.DataMatrixBean;
import org.krysalis.barcode4j.impl.fourstate.RoyalMailCBCBean;
import org.krysalis.barcode4j.impl.fourstate.USPSIntelligentMailBean;
import org.krysalis.barcode4j.impl.int2of5.ITF14Bean;
import org.krysalis.barcode4j.impl.int2of5.Interleaved2Of5Bean;
import org.krysalis.barcode4j.impl.pdf417.PDF417Bean;
import org.krysalis.barcode4j.impl.postnet.POSTNETBean;
import org.krysalis.barcode4j.impl.upcean.EAN13Bean;
import org.krysalis.barcode4j.impl.upcean.EAN8Bean;
import org.krysalis.barcode4j.impl.upcean.UPCABean;
import org.krysalis.barcode4j.impl.upcean.UPCEBean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

import javax.enterprise.context.Dependent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Dependent
public class Barcode4j implements Barcode {

    public Barcode4j() {
    }

    @Override
    public byte[] generate(String str, BarcodeType type) throws IOException {
        if (!(type instanceof Barcode4jType)) {
            throw new IllegalArgumentException("Type is not a Barcode4jType!");
        }
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        generate(outputStream, str, (Barcode4jType) type);
        final byte[] bs = outputStream.toByteArray();
        outputStream.close();
        return bs;
    }

    public void generate(OutputStream outputStream, String str, Barcode4jType type) throws IOException {
        final AbstractBarcodeBean barcodeBean = getBarcodeBean(type);
        barcodeBean.setModuleWidth(UnitConv.in2mm(1.0f / 150));
        barcodeBean.doQuietZone(false);
        final BitmapCanvasProvider canvas = new BitmapCanvasProvider(outputStream, "image/jpeg", 150, BufferedImage.TYPE_BYTE_BINARY, false, 0);
        barcodeBean.generateBarcode(canvas, str);
        canvas.finish();
    }

    public byte[] generate(String str, Barcode4jType type, int dpi, int height, boolean quietZone) throws IOException {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        generate(outputStream, str, type, dpi, height, quietZone);
        final byte[] bs = outputStream.toByteArray();
        outputStream.close();
        return bs;
    }

    public void generate(OutputStream outputStream, String str, Barcode4jType type, int dpi, int height, boolean quietZone) throws IOException {
        final AbstractBarcodeBean barcodeBean = getBarcodeBean(type);
        barcodeBean.setModuleWidth(UnitConv.in2mm(1.0f / dpi));
        barcodeBean.doQuietZone(quietZone);
        barcodeBean.setHeight(height);
        final BitmapCanvasProvider canvas = new BitmapCanvasProvider(outputStream, "image/jpeg", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
        barcodeBean.generateBarcode(canvas, str);
        canvas.finish();
    }

    private AbstractBarcodeBean getBarcodeBean(Barcode4jType type) {
        AbstractBarcodeBean barcodeBean = null;
        switch (type) {
            case INTERLEAVED_2OF5:
                barcodeBean = new Interleaved2Of5Bean();
                break;
            case ITF_14:
                barcodeBean = new ITF14Bean();
                break;
            case CODE_39:
                barcodeBean = new Code39Bean();
                break;
            case CODE_128:
                barcodeBean = new Code128Bean();
                break;
            case CODABAR:
                barcodeBean = new CodabarBean();
                break;
            case UPC_A:
                barcodeBean = new UPCABean();
                break;
            case UPC_E:
                barcodeBean = new UPCEBean();
                break;
            case EAN_13:
                barcodeBean = new EAN13Bean();
                break;
            case EAN_8:
                barcodeBean = new EAN8Bean();
                break;
            case EAN_128:
                barcodeBean = new EAN128Bean();
                break;
            case POSTNET:
                barcodeBean = new POSTNETBean();
                break;
            case ROYAL_MAIL:
                barcodeBean = new RoyalMailCBCBean();
                break;
            case USPS:
                barcodeBean = new USPSIntelligentMailBean();
                break;
            case PDF417:
                barcodeBean = new PDF417Bean();
                break;
            case DATA_MATRIX:
                barcodeBean = new DataMatrixBean();
                break;
        }
        return barcodeBean;
    }

}
