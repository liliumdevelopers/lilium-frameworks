package com.sunecity.lilium.report;

import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Paths;

/**
 * Created by shahram on 9/3/16.
 */
public class ConverterTest {

    @Test
    public void convert() {
        try (Converter converter = new Converter()) {
            converter.connect(8200);
            converter.convert(Paths.get("src/test/resources/template/letter.docx"), Paths.get("target/test.pdf"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
