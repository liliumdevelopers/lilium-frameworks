package com.sunecity.lilium.report;

import com.sunecity.lilium.report.model.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by shahram on 9/3/16.
 */
public class RtfTest {

    private Rtf rtf;
    private List<String> fields;

    @Before
    public void init() {
        fields = new ArrayList<>();
        fields.add("name");
        fields.add("family");
        rtf = new Rtf();
    }

    @Test
    public void export() {
        try {
            Person person = new Person("Shahram", "Goodarzi");
            rtf.export(Collections.singletonList(person), fields, true, new FileOutputStream("target/person.rtf"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
