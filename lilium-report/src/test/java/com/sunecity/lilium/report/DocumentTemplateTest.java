package com.sunecity.lilium.report;

import com.sunecity.lilium.core.model.file.ImageFile;
import com.sunecity.lilium.report.model.Contact;
import com.sunecity.lilium.report.model.Person;
import com.sunecity.lilium.report.model.Phone;
import fr.opensagres.xdocreport.core.XDocReportException;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DocumentTemplateTest {

    @Test
    public void odt() {
        try {
            DocumentTemplate documentTemplate = new DocumentTemplate();
            documentTemplate.template(DocumentTemplateTest.class.getResourceAsStream("/template/letter.odt"));

            ImageFile imageFile = new ImageFile();
            imageFile.setValue(Files.readAllBytes(Paths.get(DocumentTemplateTest.class.getResource("/template/logo.png").getFile())));
            List<Contact> contacts = new ArrayList<>();
            contacts.add(new Contact("کرج", Files.readAllBytes(Paths.get(DocumentTemplateTest.class.getResource("/template/ramin.jpg").getFile()))).addPhone(new Phone("رامین", "۰۹۱۲۶۵۳۴۷۹")).addPhone(new Phone("صفا", "۰۹۳۶۵۴۴۲۴۷۳")));
            contacts.add(new Contact("تهران", Files.readAllBytes(Paths.get(DocumentTemplateTest.class.getResource("/template/shahab.jpg").getFile()))).addPhone(new Phone("شهاب", "۰۹۳۶۵۴۲۵۷۸۹")).addPhone(new Phone("مرتضی", "۰۹۳۹۵۴۲۹۸۷۴۲")));
            documentTemplate.add("person", new Person("شهرام", "گودرزی"));
            documentTemplate.add("html", "<html><p style=\"text-align: center;\"><a href=\"http://www.oracle.com\">شهرام گودرزی</a></p></html>");
            documentTemplate.add("logo", imageFile);
            documentTemplate.add("contacts", contacts);
//            documentTemplate.generate(new FileOutputStream("/home/shahram/Pictures/letter.docx"));
            documentTemplate.generateToPdf(new FileOutputStream("/home/shahram/Pictures/letter.pdf"));
//            documentTemplate.generateToXhtml(new FileOutputStream("/home/shahram/Pictures/letter.html"));
        } catch (IOException | XDocReportException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void ods() {
        try {
            DocumentTemplate documentTemplate = new DocumentTemplate();
            documentTemplate.template(DocumentTemplateTest.class.getResourceAsStream("/template/letter.docx"));

            ImageFile imageFile = new ImageFile();
            imageFile.setValue(Files.readAllBytes(Paths.get(DocumentTemplateTest.class.getResource("/template/logo.png").getFile())));
            List<Contact> contacts = new ArrayList<>();
            documentTemplate.add("person", new Person("شهرام", "گودرزی"));
            documentTemplate.generate(new FileOutputStream("/home/shahram/Pictures/letter.docx"));
            documentTemplate.generateToPdf(new FileOutputStream("/home/shahram/Pictures/letter.pdf"));
            documentTemplate.generateToXhtml(new FileOutputStream("/home/shahram/Pictures/letter.html"));
        } catch (IOException | XDocReportException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
