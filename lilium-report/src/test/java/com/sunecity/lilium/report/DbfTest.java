package com.sunecity.lilium.report;

import com.sunecity.lilium.report.model.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shahram on 9/3/16.
 */
public class DbfTest {

    private Dbf dbf;
    private Map<String, Dbf.Type> fields;

    @Before
    public void init() {
        fields = new LinkedHashMap<>();
        fields.put("name", Dbf.Type.CHAR);
        fields.put("family", Dbf.Type.CHAR);
        dbf = new Dbf();
    }

    @Test
    public void export() {
        try {
            Person person = new Person("Shahram", "Goodarzi");
            dbf.export(Collections.singletonList(person), fields, new FileOutputStream("target/person.dbf"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void getValues() {
        try {
            List<Person> persons = dbf.getValues(new FileInputStream("target/person.dbf"), Person.class, fields.keySet());
            Assert.assertEquals(persons.get(0).getName(), "Shahram");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
