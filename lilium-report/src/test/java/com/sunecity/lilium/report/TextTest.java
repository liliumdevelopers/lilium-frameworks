package com.sunecity.lilium.report;

import com.sunecity.lilium.report.model.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by shahram on 9/3/16.
 */
public class TextTest {

    private Text text;
    private List<String> fields;

    @Before
    public void init() {
        fields = new ArrayList<>();
        fields.add("name");
        fields.add("family");
        text = new Text();
    }

    @Test
    public void export() {
        try {
            Person person = new Person("Shahram", "Goodarzi");
            text.export(Collections.singletonList(person), fields, new FileOutputStream("target/person.txt"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void getValues() {
        try {
            List<Person> persons = text.getValues(new FileInputStream("target/person.txt"), Person.class, fields);
            Assert.assertEquals(persons.get(0).getName(), "Shahram");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
