package com.sunecity.lilium.report.model;

import fr.opensagres.xdocreport.document.images.ByteArrayImageProvider;
import fr.opensagres.xdocreport.document.images.IImageProvider;

import java.util.ArrayList;
import java.util.List;

public class Contact {

    private String address;
    private byte[] image;
    private List<Phone> phones;

    public Contact() {
    }

    public Contact(String address, byte[] image) {
        this.address = address;
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public Contact addPhone(Phone phone) {
        if (phones == null) {
            phones = new ArrayList<>();
        }
        phones.add(phone);
        return this;
    }

    //    @FieldMetadata(images = {@ImageMetadata(name = "imageField")})
    public IImageProvider getImageField() {
        return new ByteArrayImageProvider(image);
    }

}
