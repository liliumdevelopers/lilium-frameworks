package com.sunecity.lilium.report;

import com.sunecity.lilium.report.model.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Types;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shahram on 9/3/16.
 */
public class AccessTest {

    private Access access;
    private Map<String, Integer> coloums;

    @Before
    public void init() {
        coloums = new HashMap<>();
        coloums.put("name", Types.VARCHAR);
        coloums.put("family", Types.VARCHAR);
        access = new Access();
    }

    @Test
    public void export() {
        try {
            Person person = new Person("Shahram", "Goodarzi");
            access.export("person", Collections.singletonList(person), coloums, new FileOutputStream("target/person.accdb"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void getValues() {
        try {
            List<Person> persons = access.getValues("person", new FileInputStream("target/person.accdb"), Person.class);
            Assert.assertEquals(persons.get(0).getName(), "Shahram");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
