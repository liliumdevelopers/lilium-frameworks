package com.sunecity.lilium.report;

import com.sunecity.lilium.report.barcode.Barbecue;
import com.sunecity.lilium.report.barcode.BarbecueType;
import com.sunecity.lilium.report.barcode.Barcode;
import com.sunecity.lilium.report.barcode.Barcode4j;
import com.sunecity.lilium.report.barcode.Barcode4jType;
import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by shahram on 9/3/16.
 */
public class BarcodeTest {

    @Test
    public void barbecue() {
        try {
            Barcode barcode = new Barbecue();
            byte[] bs = barcode.generate("0410138304", BarbecueType.CODABAR);
            Files.write(Paths.get("target/barbecue.png"), bs);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void barcode4j() {
        try {
            Barcode barcode = new Barcode4j();
            byte[] bs = barcode.generate("0410138304", Barcode4jType.CODABAR);
            Files.write(Paths.get("target/barcode4j.png"), bs);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
