package com.sunecity.lilium.bpm;

import com.sunecity.lilium.core.config.BpmConfig;
import org.aeonbits.owner.ConfigFactory;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.SequenceFlow;
import org.flowable.cdi.CdiJtaProcessEngineConfiguration;
import org.flowable.cdi.CdiStandaloneProcessEngineConfiguration;
import org.flowable.engine.DynamicBpmnService;
import org.flowable.engine.FormService;
import org.flowable.engine.HistoryService;
import org.flowable.engine.IdentityService;
import org.flowable.engine.ManagementService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.impl.RepositoryServiceImpl;
import org.flowable.engine.impl.persistence.entity.ExecutionEntity;
import org.flowable.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.flowable.engine.task.Comment;
import org.flowable.image.impl.DefaultProcessDiagramGenerator;
import org.flowable.task.api.Task;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.TransactionManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by shahram on 9/26/16.
 */
public class Flowable {

    private ProcessEngine processEngine;
    private IdentityService identityService;
    private RepositoryService repositoryService;
    private RuntimeService runtimeService;
    private HistoryService historyService;
    private TaskService taskService;
    private String userId;

    public Flowable(String username) {
        this.userId = username;
        processEngine = ProcessEngines.getProcessEngine("lilium");
        identityService = processEngine.getIdentityService();
        identityService.setAuthenticatedUserId(username);
        repositoryService = processEngine.getRepositoryService();
        runtimeService = processEngine.getRuntimeService();
        historyService = processEngine.getHistoryService();
        taskService = processEngine.getTaskService();
        ManagementService managementService = processEngine.getManagementService();
        FormService formService = processEngine.getFormService();
        DynamicBpmnService dynamicBpmnService = processEngine.getDynamicBpmnService();
    }

    public static ProcessEngine init() throws NamingException {
        BpmConfig bpmConfig = ConfigFactory.create(BpmConfig.class);
        if (bpmConfig.jndi() != null) {
            TransactionManager transactionManager = (TransactionManager) new InitialContext().lookup("java:/TransactionManager");
            CdiJtaProcessEngineConfiguration engineConfiguration = new CdiJtaProcessEngineConfiguration();
            engineConfiguration.setTransactionManager(transactionManager);
            engineConfiguration.setDataSourceJndiName(bpmConfig.jndi());
            ProcessEngineConfiguration configuration = engineConfiguration
                    .setJpaPersistenceUnitName(bpmConfig.persistenceUnit())
                    .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
                    .setAsyncExecutorActivate(false)
                    .setEngineName("lilium")
                    .getProcessEngineConfiguration();
//            configuration = ((ProcessEngineConfigurationImpl) configuration)
//                    .addConfigurator(new KeycloakConfigurator("lilium", "infrastructure-ui", "http://localhost:8080/auth", "admin", "adminadmin"));
            return configuration.buildProcessEngine();
        } else {
            ProcessEngineConfiguration configuration = new CdiStandaloneProcessEngineConfiguration()
                    .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
                    .setJpaPersistenceUnitName(bpmConfig.persistenceUnit())
                    .setJdbcUrl(bpmConfig.url())
                    .setJdbcUsername(bpmConfig.username())
                    .setJdbcPassword(bpmConfig.password())
                    .setJdbcDriver(bpmConfig.driver())
                    .setAsyncExecutorActivate(false)
                    .setEngineName("lilium")
                    .getProcessEngineConfiguration();
//            configuration = ((ProcessEngineConfigurationImpl) configuration)
//                    .addConfigurator(new KeycloakConfigurator("lilium", "infrastructure-ui", "http://localhost:8080/auth", "admin", "adminadmin"));
            return configuration.buildProcessEngine();
        }
    }

    public String startProcess(String processId, Map<String, Object> variables) {
        return runtimeService.startProcessInstanceByKey(processId, variables).getId();
    }

    public void abortProcess(String processInstanceId, String abortReason) {
        runtimeService.deleteProcessInstance(processInstanceId, abortReason);
    }

    public List<Task> getTasks(String processInstanceId) {
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstanceId).list();
        return tasks;
    }

    public void claimTask(String processInstanceId) {
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstanceId).list();
        Task task = tasks.get(0);
        taskService.claim(task.getId(), userId);
    }

    public void completeTask(String processInstanceId, Map<String, Object> taskVariables) {
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstanceId).list();
        Task task = tasks.get(0);
        taskService.complete(task.getId(), taskVariables);
    }

    public void doTask(String processInstanceId, Map<String, Object> taskVariables, String comment) {
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstanceId).list();
        Task task = tasks.get(0);
        taskService.claim(task.getId(), userId);
        taskService.complete(task.getId(), taskVariables);
    }

    public Comment comment(String processInstanceId, String comment) {
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstanceId).list();
        Task task = tasks.get(0);
        return taskService.addComment(task.getId(), processInstanceId, comment);
    }

    public Comment getComment(String commentId) {
        return taskService.getComment(commentId);
    }

    public List<Comment> getComments(String taskId) {
        return taskService.getTaskComments(taskId);
    }

    public void deleteComment(String commentId) {
        taskService.deleteComment(commentId);
    }

    public Object getVariable(String taskId, String varName) {
        return taskService.getVariable(taskId, varName);
    }

    public Map<String, Object> getVariables(String taskId) {
        return taskService.getVariables(taskId);
    }

    public List<HistoricProcessInstance> getProcessLogs(String processId) {
        return historyService.createHistoricProcessInstanceQuery().list();
    }

    public byte[] getProcessInstanceImage(String processInstanceId) throws IOException {
        ExecutionEntity executionEntity = (ExecutionEntity) runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService).getDeployedProcessDefinition(executionEntity.getProcessDefinitionId());
        if (processDefinitionEntity != null && processDefinitionEntity.isGraphicalNotationDefined()) {
            BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinitionEntity.getId());

            List<String> activeActivityIds = runtimeService.getActiveActivityIds(processInstanceId);
            List<String> activeAndHistoricActivityIds = historyService.createHistoricActivityInstanceQuery().processInstanceId(processInstanceId).list().stream().map(HistoricActivityInstance::getActivityId).collect(Collectors.toList());
            activeAndHistoricActivityIds.addAll(activeActivityIds);

            List<String> flowIds = new LinkedList<>();
            for (Process process : bpmnModel.getProcesses()) {
                process.getFlowElements().stream().filter(flowElement -> flowElement instanceof SequenceFlow).forEach(flowElement -> {
                    SequenceFlow sequenceFlow = (SequenceFlow) flowElement;
                    String from = sequenceFlow.getSourceRef();
                    String to = sequenceFlow.getTargetRef();
                    if (activeAndHistoricActivityIds.contains(from) && activeAndHistoricActivityIds.contains(to)) {
                        flowIds.add(sequenceFlow.getId());
                    }
                });
            }

            InputStream inputStream = new DefaultProcessDiagramGenerator().generateDiagram(bpmnModel, "png", activeActivityIds, flowIds, true);
            return getBytes(inputStream);
        }
        return null;
    }

    public byte[] getProcessImage(String processDefinitionId) throws IOException {
        ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) repositoryService.createProcessDefinitionQuery().processDefinitionKey(processDefinitionId).singleResult();
        if (processDefinitionEntity != null && processDefinitionEntity.isGraphicalNotationDefined()) {
            InputStream inputStream = repositoryService.getResourceAsStream(processDefinitionEntity.getDeploymentId(), processDefinitionEntity.getDiagramResourceName());
            return getBytes(inputStream);
        }
        return null;
    }

    private byte[] getBytes(InputStream inputStream) throws IOException {
        try (inputStream; ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            inputStream.transferTo(outputStream);
            return outputStream.toByteArray();
        }
    }

}
