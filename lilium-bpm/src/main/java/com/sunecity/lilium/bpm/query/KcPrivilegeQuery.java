package com.sunecity.lilium.bpm.query;

import org.flowable.idm.api.Privilege;
import org.flowable.idm.engine.impl.PrivilegeQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.PrivilegeEntity;
import org.flowable.idm.engine.impl.persistence.entity.PrivilegeEntityImpl;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.RoleRepresentation;

import java.util.List;
import java.util.stream.Collectors;

public class KcPrivilegeQuery extends PrivilegeQueryImpl {

    private RolesResource rolesResource;
    private UsersResource usersResource;

    @Override
    public Privilege singleResult() {
        return toPrivilege(rolesResource.get(getId()).toRepresentation());
    }

    @Override
    public List<Privilege> list() {
        return usersResource.get(getUserId()).roles().realmLevel().listAll().stream().map(this::toPrivilege).collect(Collectors.toList());
//        return rolesResource.list().stream().map(this::toPrivilege).collect(Collectors.toList());
    }

    @Override
    public List<Privilege> listPage(int firstResult, int maxResults) {
        return rolesResource.list().stream().map(this::toPrivilege).collect(Collectors.toList());
    }

    @Override
    public long count() {
        return super.count();
    }

    private Privilege toPrivilege(RoleRepresentation role) {
        if (role == null) {
            return null;
        }
        PrivilegeEntity privilegeEntity = new PrivilegeEntityImpl();
        privilegeEntity.setId(role.getId());
        privilegeEntity.setName(role.getName());
        return privilegeEntity;
    }

}
