package com.sunecity.lilium.bpm;

import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.flowable.idm.engine.configurator.IdmEngineConfigurator;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UsersResource;

public class KeycloakConfigurator extends IdmEngineConfigurator {

    private Keycloak client;
    private String realm;

    public KeycloakConfigurator(String realm, String clientId, String url, String username, String password) {
        client = Keycloak.getInstance(url, realm, username, password, clientId);
        this.realm = realm;
    }

    public void configure(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super.configure(processEngineConfiguration);
        RealmResource realmResource = client.realm(realm);
        UsersResource usersResource = realmResource.users();
        RolesResource rolesResource = realmResource.roles();
//        getIdmEngineConfiguration().setIdmIdentityService(new KeycloakIdmIdentityService());
        getIdmEngineConfiguration().setUserEntityManager(new KeycloakUserManager(processEngineConfiguration, usersResource));
        getIdmEngineConfiguration().setGroupEntityManager(new KeycloakGroupManager(processEngineConfiguration, usersResource, rolesResource));
        getIdmEngineConfiguration().setMembershipEntityManager(new KeycloakMembershipManager(processEngineConfiguration, usersResource, rolesResource));
    }

}
