package com.sunecity.lilium.bpm.query;

import org.flowable.idm.api.Group;
import org.flowable.idm.engine.impl.GroupQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntity;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntityImpl;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.representations.idm.GroupRepresentation;

import java.util.List;
import java.util.stream.Collectors;

public class KcGroupQuery extends GroupQueryImpl {

    private GroupsResource groupsResource;

    @Override
    public long count() {
        return groupsResource.count().get("count");
    }

    @Override
    public Group singleResult() {
        return toGroup(groupsResource.group(getId()).toRepresentation());
    }

    @Override
    public List<Group> list() {
        return groupsResource.groups().stream().map(this::toGroup).collect(Collectors.toList());
    }

    @Override
    public List<Group> listPage(int firstResult, int maxResults) {
        return groupsResource.groups(firstResult, maxResults).stream().map(this::toGroup).collect(Collectors.toList());
    }

    private Group toGroup(GroupRepresentation group) {
        if (group == null) {
            return null;
        }
        GroupEntity groupEntity = new GroupEntityImpl();
        groupEntity.setId(group.getId());
        groupEntity.setName(group.getName());
        return groupEntity;
    }

}
