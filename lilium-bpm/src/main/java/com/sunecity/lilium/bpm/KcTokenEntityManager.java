package com.sunecity.lilium.bpm;

import org.flowable.idm.api.Token;
import org.flowable.idm.api.TokenQuery;
import org.flowable.idm.engine.IdmEngineConfiguration;
import org.flowable.idm.engine.impl.TokenQueryImpl;
import org.flowable.idm.engine.impl.persistence.AbstractManager;
import org.flowable.idm.engine.impl.persistence.entity.TokenEntity;
import org.flowable.idm.engine.impl.persistence.entity.TokenEntityManager;

import java.util.List;
import java.util.Map;

public class KcTokenEntityManager extends AbstractManager implements TokenEntityManager {

    public KcTokenEntityManager(IdmEngineConfiguration idmEngineConfiguration) {
        super(idmEngineConfiguration);
    }

    @Override
    public Token createNewToken(String tokenId) {
        return null;
    }

    @Override
    public void updateToken(Token updatedToken) {

    }

    @Override
    public boolean isNewToken(Token token) {
        return false;
    }

    @Override
    public List<Token> findTokenByQueryCriteria(TokenQueryImpl query) {
        return null;
    }

    @Override
    public long findTokenCountByQueryCriteria(TokenQueryImpl query) {
        return 0;
    }

    @Override
    public TokenQuery createNewTokenQuery() {
        return null;
    }

    @Override
    public List<Token> findTokensByNativeQuery(Map<String, Object> parameterMap) {
        return null;
    }

    @Override
    public long findTokenCountByNativeQuery(Map<String, Object> parameterMap) {
        return 0;
    }

    @Override
    public TokenEntity create() {
        return null;
    }

    @Override
    public TokenEntity findById(String s) {
        return null;
    }

    @Override
    public void insert(TokenEntity entity) {

    }

    @Override
    public void insert(TokenEntity entity, boolean b) {

    }

    @Override
    public TokenEntity update(TokenEntity entity) {
        return null;
    }

    @Override
    public TokenEntity update(TokenEntity entity, boolean b) {
        return null;
    }

    @Override
    public void delete(String s) {

    }

    @Override
    public void delete(TokenEntity entity) {

    }

    @Override
    public void delete(TokenEntity entity, boolean b) {

    }

}
