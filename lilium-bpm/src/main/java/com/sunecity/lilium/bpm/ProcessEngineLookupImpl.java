package com.sunecity.lilium.bpm;

import org.flowable.cdi.spi.ProcessEngineLookup;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngines;

/**
 * Created by shahram on 5/7/17.
 */
public class ProcessEngineLookupImpl implements ProcessEngineLookup {

    @Override
    public int getPrecedence() {
        return 5;
    }

    @Override
    public ProcessEngine getProcessEngine() {
        try {
            return Flowable.init();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    @Override
    public void ungetProcessEngine() {
        ProcessEngines.getProcessEngine("lilium").close();
    }

}
