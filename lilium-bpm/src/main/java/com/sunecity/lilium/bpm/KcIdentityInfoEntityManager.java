package com.sunecity.lilium.bpm;

import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.flowable.engine.impl.persistence.AbstractManager;
import org.flowable.idm.engine.impl.persistence.entity.IdentityInfoEntity;
import org.flowable.idm.engine.impl.persistence.entity.IdentityInfoEntityManager;

import java.util.List;
import java.util.Map;

public class KcIdentityInfoEntityManager extends AbstractManager implements IdentityInfoEntityManager{

    public KcIdentityInfoEntityManager(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    @Override
    public IdentityInfoEntity findUserInfoByUserIdAndKey(String userId, String key) {
        return null;
    }

    @Override
    public List<String> findUserInfoKeysByUserIdAndType(String userId, String type) {
        return null;
    }

    @Override
    public List<IdentityInfoEntity> findIdentityInfoByUserId(String userId) {
        return null;
    }

    @Override
    public void updateUserInfo(String userId, String userPassword, String type, String key, String value, String accountPassword, Map<String, String> accountDetails) {

    }

    @Override
    public void deleteUserInfoByUserIdAndKey(String userId, String key) {

    }

    @Override
    public IdentityInfoEntity create() {
        return null;
    }

    @Override
    public IdentityInfoEntity findById(String s) {
        return null;
    }

    @Override
    public void insert(IdentityInfoEntity entity) {

    }

    @Override
    public void insert(IdentityInfoEntity entity, boolean b) {

    }

    @Override
    public IdentityInfoEntity update(IdentityInfoEntity entity) {
        return null;
    }

    @Override
    public IdentityInfoEntity update(IdentityInfoEntity entity, boolean b) {
        return null;
    }

    @Override
    public void delete(String s) {

    }

    @Override
    public void delete(IdentityInfoEntity entity) {

    }

    @Override
    public void delete(IdentityInfoEntity entity, boolean b) {

    }

}
