package com.sunecity.lilium.bpm;

import com.sunecity.lilium.core.exception.RuleValidationException;
import com.sunecity.lilium.core.exception.RuleVerificationException;
import com.sunecity.lilium.core.model.Rule;
import com.sunecity.lilium.core.model.kie.RuleMessage;
import org.drools.core.base.RuleNameMatchesAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.ObjectFilter;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Title:</b> RuleRunner
 * Utility class for running rule files.
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public class RuleRunner implements AutoCloseable {

    private KieContainer kieContainer;
    private KieSession kieSession;

    public RuleRunner(Rule... rules) throws RuleVerificationException {
        KieServices kieServices = KieServices.Factory.get();
        KieFileSystem kfs = kieServices.newKieFileSystem();
        for (Rule rule : rules) {
            try (Reader reader = new InputStreamReader(new ByteArrayInputStream(rule.getRuleFile()), "UTF-8")) {
                kfs.write("src/main/resources/" + rule.getName() + ".drl", kieServices.getResources().newReaderResource(reader, "UTF-8"));
            } catch (Exception e) {
                throw new RuleVerificationException(e);
            }
        }
        verify(kieServices.newKieBuilder(kfs).buildAll());
        kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
    }

    public int runRules(String regex, Object... facts) throws RuleValidationException {
        kieSession = kieContainer.newKieSession();
        for (Object o : facts) {
            kieSession.insert(o);
        }
        int i;
        if (regex != null) {
            i = kieSession.fireAllRules(new RuleNameMatchesAgendaFilter(regex));
        } else {
            i = kieSession.fireAllRules();
        }
        List<RuleMessage> results = getResult(RuleMessage.class);
        if (results.size() != 0) {
            List<RuleMessage> messages = new ArrayList<>();
            StringBuilder sb = new StringBuilder("\n");
            results.forEach(r -> {
                messages.add(r);
                sb.append("[").append(r.getMessage()).append("]\n");
            });
            throw new RuleValidationException(messages, sb.substring(0, sb.length() - 1));
        }
        return i;
    }

    /**
     * Get result objects from rule file
     *
     * @param factClass class type to be returned
     * @return List of objects from rule file
     */
    public <T> List<T> getResult(final Class<T> factClass) {
        ObjectFilter filter = object -> object.getClass().equals(factClass);
        List<T> ts = new ArrayList<>();
        kieSession.getObjects(filter).forEach(o -> ts.add((T) o));
        return ts;
    }

    /**
     * Checks if rule file is valid
     *
     * @throws RuleVerificationException
     */
    private void verify(KieBuilder kieBuilder) throws RuleVerificationException {
        Results results = kieBuilder.getResults();
        if (results.hasMessages(Message.Level.ERROR)) {
            List<RuleMessage> messages = new ArrayList<>();
            StringBuilder sb = new StringBuilder("\n");
            results.getMessages().forEach(m -> {
                messages.add(new RuleMessage(m.getText()));
                sb.append("[").append(m.getText()).append("]\n");
            });
            throw new RuleVerificationException(messages, sb.substring(0, sb.length() - 1));
        }
    }

    @Override
    public void close() throws Exception {
        kieSession.dispose();
        kieContainer.dispose();
    }

}
