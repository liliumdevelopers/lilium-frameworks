package com.sunecity.lilium.bpm;

import org.flowable.idm.api.Privilege;
import org.flowable.idm.engine.IdmEngineConfiguration;
import org.flowable.idm.engine.impl.PrivilegeQueryImpl;
import org.flowable.idm.engine.impl.persistence.AbstractManager;
import org.flowable.idm.engine.impl.persistence.entity.PrivilegeEntity;
import org.flowable.idm.engine.impl.persistence.entity.data.PrivilegeDataManager;

import java.util.List;
import java.util.Map;

public class KcPrivilegeDataManager extends AbstractManager implements PrivilegeDataManager {

    public KcPrivilegeDataManager(IdmEngineConfiguration idmEngineConfiguration) {
        super(idmEngineConfiguration);
    }

    @Override
    public List<Privilege> findPrivilegeByQueryCriteria(PrivilegeQueryImpl query) {
        return null;
    }

    @Override
    public long findPrivilegeCountByQueryCriteria(PrivilegeQueryImpl query) {
        return 0;
    }

    @Override
    public List<Privilege> findPrivilegeByNativeQuery(Map<String, Object> parameterMap) {
        return null;
    }

    @Override
    public long findPrivilegeCountByNativeQuery(Map<String, Object> parameterMap) {
        return 0;
    }

    @Override
    public PrivilegeEntity create() {
        return null;
    }

    @Override
    public PrivilegeEntity findById(String s) {
        return null;
    }

    @Override
    public void insert(PrivilegeEntity entity) {

    }

    @Override
    public PrivilegeEntity update(PrivilegeEntity entity) {
        return null;
    }

    @Override
    public void delete(String s) {

    }

    @Override
    public void delete(PrivilegeEntity entity) {

    }

}
