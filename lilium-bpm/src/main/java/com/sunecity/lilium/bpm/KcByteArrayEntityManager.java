package com.sunecity.lilium.bpm;

import org.flowable.idm.engine.IdmEngineConfiguration;
import org.flowable.idm.engine.impl.persistence.AbstractManager;
import org.flowable.idm.engine.impl.persistence.entity.ByteArrayEntityManager;
import org.flowable.idm.engine.impl.persistence.entity.IdmByteArrayEntity;

import java.util.List;

public class KcByteArrayEntityManager extends AbstractManager implements ByteArrayEntityManager {

    public KcByteArrayEntityManager(IdmEngineConfiguration idmEngineConfiguration) {
        super(idmEngineConfiguration);
    }

    @Override
    public List<IdmByteArrayEntity> findAll() {
        return null;
    }

    @Override
    public void deleteByteArrayById(String byteArrayEntityId) {

    }

    @Override
    public IdmByteArrayEntity create() {
        return null;
    }

    @Override
    public IdmByteArrayEntity findById(String s) {
        return null;
    }

    @Override
    public void insert(IdmByteArrayEntity entity) {

    }

    @Override
    public void insert(IdmByteArrayEntity entity, boolean b) {

    }

    @Override
    public IdmByteArrayEntity update(IdmByteArrayEntity entity) {
        return null;
    }

    @Override
    public IdmByteArrayEntity update(IdmByteArrayEntity entity, boolean b) {
        return null;
    }

    @Override
    public void delete(String s) {

    }

    @Override
    public void delete(IdmByteArrayEntity entity) {

    }

    @Override
    public void delete(IdmByteArrayEntity entity, boolean b) {

    }

}
