package com.sunecity.lilium.bpm;

import org.flowable.idm.engine.IdmEngineConfiguration;
import org.flowable.idm.engine.impl.persistence.AbstractManager;
import org.flowable.idm.engine.impl.persistence.entity.IdmPropertyEntity;
import org.flowable.idm.engine.impl.persistence.entity.PropertyEntityManager;

import java.util.List;

public class KcPropertyEntityManager extends AbstractManager implements PropertyEntityManager {

    public KcPropertyEntityManager(IdmEngineConfiguration idmEngineConfiguration) {
        super(idmEngineConfiguration);
    }

    @Override
    public List<IdmPropertyEntity> findAll() {
        return null;
    }

    @Override
    public IdmPropertyEntity create() {
        return null;
    }

    @Override
    public IdmPropertyEntity findById(String s) {
        return null;
    }

    @Override
    public void insert(IdmPropertyEntity entity) {

    }

    @Override
    public void insert(IdmPropertyEntity entity, boolean b) {

    }

    @Override
    public IdmPropertyEntity update(IdmPropertyEntity entity) {
        return null;
    }

    @Override
    public IdmPropertyEntity update(IdmPropertyEntity entity, boolean b) {
        return null;
    }

    @Override
    public void delete(String s) {

    }

    @Override
    public void delete(IdmPropertyEntity entity) {

    }

    @Override
    public void delete(IdmPropertyEntity entity, boolean b) {

    }

}
