package com.sunecity.lilium.bpm.query;

import org.flowable.idm.api.User;
import org.flowable.idm.engine.impl.UserQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.UserEntity;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityImpl;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.List;
import java.util.stream.Collectors;

public class KcUserQuery extends UserQueryImpl {

    private UsersResource usersResource;

    @Override
    public User singleResult() {
        return toUser(usersResource.get(getId()).toRepresentation());
    }

    @Override
    public List<User> list() {
        return usersResource.search("").stream().map(this::toUser).collect(Collectors.toList());
    }

    @Override
    public List<User> listPage(int firstResult, int maxResults) {
        return usersResource.search("", firstResult, maxResults).stream().map(this::toUser).collect(Collectors.toList());
    }

    @Override
    public long count() {
        return usersResource.count();
    }

    private User toUser(UserRepresentation user) {
        if (user == null) {
            return null;
        }
        UserEntity userEntity = new UserEntityImpl();
        userEntity.setId(user.getId());
        userEntity.setEmail(user.getEmail());
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        return userEntity;
    }

}
