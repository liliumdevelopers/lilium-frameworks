package com.sunecity.lilium.bpm;

import org.flowable.common.engine.api.FlowableException;
import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.flowable.engine.impl.persistence.AbstractManager;
import org.flowable.idm.api.PasswordEncoder;
import org.flowable.idm.api.PasswordSalt;
import org.flowable.idm.api.Picture;
import org.flowable.idm.api.User;
import org.flowable.idm.api.UserQuery;
import org.flowable.idm.engine.impl.UserQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.UserEntity;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityManager;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class KeycloakUserManager extends AbstractManager implements UserEntityManager {

    private static Logger logger = LoggerFactory.getLogger(KeycloakUserManager.class);

    private UsersResource usersResource;

    public KeycloakUserManager(ProcessEngineConfigurationImpl processEngineConfiguration, UsersResource usersResource) {
        super(processEngineConfiguration);
        this.usersResource = usersResource;
    }

    @Override
    public long findUserCountByQueryCriteria(UserQueryImpl query) {
        return findUserByQueryCriteria(query).size();
    }

//    @Override
//    public List<Group> findGroupsByUser(String userId) {
//        List<Group> groups = new ArrayList<>();
//        List<String> roles = new ArrayList<>();
//        UserRepresentation userRepresentation = usersResource.get(userId).toRepresentation();
//        roles.addAll(userRepresentation.getRealmRoles());
//        roles.addAll(userRepresentation.getClientRoles().values().stream().flatMap(List::stream).collect(Collectors.toList()));
//        for (String role : roles) {
//            GroupEntityImpl entity = new GroupEntityImpl();
//            entity.setName(role);
//        }
//        return groups;
//    }

    @Override
    public UserQuery createNewUserQuery() {
        return new UserQueryImpl();
    }

    @Override
    public Boolean checkPassword(String s, String s1, PasswordEncoder passwordEncoder, PasswordSalt passwordSalt) {
        return null;
    }

    @Override
    public List<User> findUsersByNativeQuery(Map<String, Object> map) {
        return null;
    }

    @Override
    public long findUserCountByNativeQuery(Map<String, Object> parameterMap) {
        throw new FlowableException("Native queries are not supported by Keycloak user manager");
    }

    @Override
    public UserEntity findById(String id) {
        UserRepresentation user = usersResource.get(id).toRepresentation();
        return toUser(user);
    }

    @Override
    public boolean isNewUser(User user) {
        logger.warn("");
        return false;
    }

    @Override
    public Picture getUserPicture(User user) {
        return null;//FIXME NEW
    }

    @Override
    public void setUserPicture(User user, Picture picture) {
//FIXME NEW
    }

    @Override
    public void deletePicture(User user) {
        logger.warn("");
    }

    @Override
    public List<User> findUsersByPrivilegeId(String s) {
        return null;
    }

    @Override
    public UserEntity create() {
        return new UserEntityImpl();
    }

    @Override
    public User createNewUser(String userId) {
        UserEntityImpl user = new UserEntityImpl();
        user.setId(userId);
        return user;
    }

    @Override
    public void updateUser(User updatedUser) {
        UserRepresentation userRepresentation = new UserRepresentation();
        //FIXME
        usersResource.get(updatedUser.getId()).update(userRepresentation);
    }

    @Override
    public List<User> findUserByQueryCriteria(UserQueryImpl userQuery) {
        List<User> ret = new ArrayList<>();
        if (userQuery.getId() != null) {
            UserEntity byId = findById(userQuery.getId());
            if (byId != null) ret.add(byId);
        } else {
            logger.warn("Unknown query type, returning all users");
            List<UserRepresentation> users = usersResource.search("", userQuery.getFirstResult(), userQuery.getMaxResults());
            for (UserRepresentation r : users)
                ret.add(toUser(r));
        }
        return ret;
    }

    @Override
    public void insert(UserEntity entity) {
        UserRepresentation userRepresentation = new UserRepresentation();
        //FIXME
        usersResource.create(userRepresentation);
    }

    @Override
    public void insert(UserEntity entity, boolean fireUpdateEvent) {
        insert(entity);
    }

    @Override
    public UserEntity update(UserEntity entity) {
        UserRepresentation userRepresentation = usersResource.get(entity.getId()).toRepresentation();

        return entity;
    }

    @Override
    public UserEntity update(UserEntity entity, boolean fireUpdateEvent) {
        return update(entity);
    }

    @Override
    public void delete(String id) {
        usersResource.get(id).remove();
    }

    @Override
    public void delete(UserEntity entity) {
        delete(entity.getId());
    }

    @Override
    public void delete(UserEntity entity, boolean fireDeleteEvent) {
        delete(entity.getId());
    }

    private UserEntity toUser(UserRepresentation user) {
        if (user == null) {
            return null;
        }
        UserEntity entity = new UserEntityImpl();
        entity.setId(user.getId());
        entity.setEmail(user.getEmail());
        entity.setFirstName(user.getFirstName());
        entity.setLastName(user.getLastName());
        return entity;
    }

}
