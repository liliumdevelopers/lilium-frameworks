package com.sunecity.lilium.bpm;

import com.sunecity.lilium.bpm.query.KcGroupQuery;
import com.sunecity.lilium.bpm.query.KcTokenQuery;
import com.sunecity.lilium.bpm.query.KcUserQuery;
import org.flowable.idm.api.Group;
import org.flowable.idm.api.GroupQuery;
import org.flowable.idm.api.IdmIdentityService;
import org.flowable.idm.api.NativeGroupQuery;
import org.flowable.idm.api.NativeTokenQuery;
import org.flowable.idm.api.NativeUserQuery;
import org.flowable.idm.api.Picture;
import org.flowable.idm.api.Privilege;
import org.flowable.idm.api.PrivilegeMapping;
import org.flowable.idm.api.PrivilegeQuery;
import org.flowable.idm.api.Token;
import org.flowable.idm.api.TokenQuery;
import org.flowable.idm.api.User;
import org.flowable.idm.api.UserQuery;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntity;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.PrivilegeEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.PrivilegeMappingEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.TokenEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.UserEntity;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityImpl;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class KeycloakIdmIdentityService implements IdmIdentityService {

    private UsersResource usersResource;
    private GroupsResource groupsResource;
    private RolesResource rolesResource;

    public KeycloakIdmIdentityService(UsersResource usersResource) {
        this.usersResource = usersResource;
    }

    @Override
    public User newUser(String userId) {
        UserEntityImpl user = new UserEntityImpl();
        user.setId(userId);
        return user;
    }

    @Override
    public void saveUser(User user) {
        usersResource.create(toUserRepresentation(user));
    }

    @Override
    public void updateUserPassword(User user) {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(user.getPassword());
        credentialRepresentation.setTemporary(false);
        usersResource.get(user.getId()).resetPassword(credentialRepresentation);
    }

    @Override
    public UserQuery createUserQuery() {
        return new KcUserQuery();
    }

    @Override
    public NativeUserQuery createNativeUserQuery() {
        return null;
    }

    @Override
    public void deleteUser(String userId) {
        usersResource.delete(userId);
    }

    @Override
    public Group newGroup(String groupId) {
        GroupEntityImpl group = new GroupEntityImpl();
        group.setId(groupId);
        return group;
    }

    @Override
    public GroupQuery createGroupQuery() {
        return new KcGroupQuery();
    }

    @Override
    public NativeGroupQuery createNativeGroupQuery() {
        return null;
    }

    @Override
    public void saveGroup(Group group) {
        groupsResource.add(toGroupRepresentation(group));
    }

    @Override
    public void deleteGroup(String groupId) {
        groupsResource.group(groupId).remove();
    }

    @Override
    public void createMembership(String userId, String groupId) {
        usersResource.get(userId).joinGroup(groupId);
    }

    @Override
    public void deleteMembership(String userId, String groupId) {
        usersResource.get(userId).leaveGroup(groupId);
    }

    @Override
    public boolean checkPassword(String userId, String password) {
        return false;
    }

    @Override
    public void setAuthenticatedUserId(String s) {

    }

    @Override
    public void setUserPicture(String userId, Picture picture) {

    }

    @Override
    public Picture getUserPicture(String userId) {
        return null;
    }

    @Override
    public Token newToken(String id) {
        TokenEntityImpl token = new TokenEntityImpl();
        token.setId(id);
        return token;
    }

    @Override
    public void saveToken(Token token) {

    }

    @Override
    public void deleteToken(String tokenId) {

    }

    @Override
    public TokenQuery createTokenQuery() {
        return new KcTokenQuery();
    }

    @Override
    public NativeTokenQuery createNativeTokenQuery() {
        return null;
    }

    @Override
    public void setUserInfo(String userId, String key, String value) {

    }

    @Override
    public String getUserInfo(String userId, String key) {
        return null;
    }

    @Override
    public List<String> getUserInfoKeys(String userId) {
        return null;
    }

    @Override
    public void deleteUserInfo(String userId, String key) {

    }

    @Override
    public Privilege createPrivilege(String privilegeName) {
        PrivilegeEntityImpl privilege = new PrivilegeEntityImpl();
        privilege.setName(privilegeName);
        return privilege;
    }

    @Override
    public void addUserPrivilegeMapping(String privilegeId, String userId) {
        usersResource.get(userId).roles().realmLevel().add(Collections.singletonList(rolesResource.get(privilegeId).toRepresentation()));//FIXME role by name not id
    }

    @Override
    public void deleteUserPrivilegeMapping(String privilegeId, String userId) {
        usersResource.get(userId).roles().realmLevel().remove(Collections.singletonList(rolesResource.get(privilegeId).toRepresentation()));//FIXME role by name not id
    }

    @Override
    public void addGroupPrivilegeMapping(String privilegeId, String groupId) {
        groupsResource.group(groupId).roles().realmLevel().add(Collections.singletonList(rolesResource.get(privilegeId).toRepresentation()));//FIXME role by name not id
    }

    @Override
    public void deleteGroupPrivilegeMapping(String privilegeId, String groupId) {
        groupsResource.group(groupId).roles().realmLevel().remove(Collections.singletonList(rolesResource.get(privilegeId).toRepresentation()));//FIXME role by name not id
    }

    @Override
    public List<PrivilegeMapping> getPrivilegeMappingsByPrivilegeId(String privilegeId) {
        List<PrivilegeMapping> privilegeMappings = new ArrayList<>();
        PrivilegeMappingEntityImpl privilegeMapping = new PrivilegeMappingEntityImpl();//TODO
        privilegeMappings.add(privilegeMapping);
        return privilegeMappings;
    }

    @Override
    public void deletePrivilege(String privilegeId) {
        rolesResource.deleteRole(privilegeId);
    }

    @Override
    public List<User> getUsersWithPrivilege(String privilegeId) {
        return null;
    }

    @Override
    public List<Group> getGroupsWithPrivilege(String privilegeId) {
        return null;
    }

    @Override
    public PrivilegeQuery createPrivilegeQuery() {
        return null;
    }

    private User toUser(UserRepresentation user) {
        if (user == null) {
            return null;
        }
        UserEntity userEntity = new UserEntityImpl();
        userEntity.setId(user.getId());
        userEntity.setEmail(user.getEmail());
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        return userEntity;
    }

    private UserRepresentation toUserRepresentation(User user) {
        if (user == null) {
            return null;
        }
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setId(user.getId());
        userRepresentation.setEmail(user.getEmail());
        userRepresentation.setFirstName(user.getFirstName());
        userRepresentation.setLastName(user.getLastName());
        return userRepresentation;
    }

    private Group toGroup(GroupRepresentation group) {
        if (group == null) {
            return null;
        }
        GroupEntity groupEntity = new GroupEntityImpl();
        groupEntity.setId(group.getId());
        groupEntity.setName(group.getName());
        return groupEntity;
    }

    private GroupRepresentation toGroupRepresentation(Group group) {
        if (group == null) {
            return null;
        }
        GroupRepresentation groupRepresentation = new GroupRepresentation();
        groupRepresentation.setId(group.getId());
        groupRepresentation.setName(group.getName());
        return groupRepresentation;
    }

}
