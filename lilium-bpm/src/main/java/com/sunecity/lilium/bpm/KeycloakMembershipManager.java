package com.sunecity.lilium.bpm;

import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.flowable.engine.impl.persistence.AbstractManager;
import org.flowable.idm.engine.impl.persistence.entity.MembershipEntity;
import org.flowable.idm.engine.impl.persistence.entity.MembershipEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.MembershipEntityManager;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

public class KeycloakMembershipManager extends AbstractManager implements MembershipEntityManager {

    private static Logger logger = LoggerFactory.getLogger(KeycloakMembershipManager.class);

    private UsersResource usersResource;
    private RolesResource rolesResource;

    public KeycloakMembershipManager(ProcessEngineConfigurationImpl processEngineConfiguration, UsersResource usersResource, RolesResource rolesResource) {
        super(processEngineConfiguration);
        this.usersResource = usersResource;
        this.rolesResource = rolesResource;
    }

    @Override
    public void createMembership(String userId, String groupId) {
        usersResource.get(userId).roles().realmLevel().add(Collections.singletonList(rolesResource.get(groupId).toRepresentation()));
    }

    @Override
    public void deleteMembership(String userId, String groupId) {
        usersResource.get(userId).roles().realmLevel().remove(Collections.singletonList(rolesResource.get(groupId).toRepresentation()));
    }

    @Override
    public void deleteMembershipByGroupId(String groupId) {
        logger.warn("");
    }

    @Override
    public void deleteMembershipByUserId(String userId) {
        logger.warn("");
    }

    @Override
    public MembershipEntity findById(String entityId) {
        logger.warn("");
        MembershipEntityImpl entity = new MembershipEntityImpl();
        entity.setId(entityId);
        return entity;
    }

    @Override
    public MembershipEntity create() {
        return new MembershipEntityImpl();
    }

    @Override
    public void insert(MembershipEntity entity) {
        logger.warn("");
    }

    @Override
    public void insert(MembershipEntity entity, boolean fireCreateEvent) {
        logger.warn("");
    }

    @Override
    public MembershipEntity update(MembershipEntity entity) {
        logger.warn("");
        return entity;
    }

    @Override
    public MembershipEntity update(MembershipEntity entity, boolean fireUpdateEvent) {
        logger.warn("");
        return entity;
    }

    @Override
    public void delete(String id) {
        logger.warn("");
    }

    @Override
    public void delete(MembershipEntity entity) {
        logger.warn("");
    }

    @Override
    public void delete(MembershipEntity entity, boolean fireDeleteEvent) {
        logger.warn("");
    }

}
