package com.sunecity.lilium.bpm;

import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.flowable.engine.impl.persistence.AbstractManager;
import org.flowable.idm.api.Group;
import org.flowable.idm.api.GroupQuery;
import org.flowable.idm.engine.impl.GroupQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntity;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntityManager;
import org.keycloak.admin.client.resource.RoleMappingResource;
import org.keycloak.admin.client.resource.RoleScopeResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class KeycloakGroupManager extends AbstractManager implements GroupEntityManager {

    private static Logger logger = LoggerFactory.getLogger(KeycloakGroupManager.class);

    private final UsersResource usersResource;
    private final RolesResource rolesResource;

    public KeycloakGroupManager(ProcessEngineConfigurationImpl processEngineConfiguration, UsersResource usersResource, RolesResource rolesResource) {
        super(processEngineConfiguration);
        this.usersResource = usersResource;
        this.rolesResource = rolesResource;
    }

    @Override
    public GroupQuery createNewGroupQuery() {
        return new GroupQueryImpl();
    }

    @Override
    public List<Group> findGroupByQueryCriteria(GroupQueryImpl groupQuery) {
        return filterGroupsByQuery(getAllGroups(), groupQuery); // TODO: paging
    }

    @Override
    public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
        return filterGroupsByQuery(getAllGroups(), query).size();
    }

    @Override
    public List<Group> findGroupsByUser(String userId) {
        return filterGroupsByQuery(getAllGroups(), (GroupQueryImpl) new GroupQueryImpl().groupMember(userId));
    }

    @Override
    public List<Group> findGroupsByNativeQuery(Map<String, Object> map) {
        return null;
    }

    @Override
    public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
        logger.warn("Native queries are not supported by Keycloak group manager");
        return 0;
    }

    @Override
    public boolean isNewGroup(Group group) {
        logger.warn("");
        return false;
    }

    @Override
    public List<Group> findGroupsByPrivilegeId(String s) {
        return null;//FIXME NEW
    }

    @Override
    public GroupEntity findById(String id) {
        rolesResource.get(id).toRepresentation();
        //FIXME
        return null;
    }

    @Override
    public Group createNewGroup(String groupId) {
        GroupEntityImpl group = new GroupEntityImpl();
        group.setId(groupId);
        return group;
    }

    @Override
    public GroupEntity create() {
        return new GroupEntityImpl();
    }

    @Override
    public void insert(GroupEntity entity) {
        RoleRepresentation roleRepresentation = new RoleRepresentation();
        //FIXME
        rolesResource.create(roleRepresentation);
    }

    @Override
    public void insert(GroupEntity entity, boolean fireDeleteEvent) {
        insert(entity);
    }

    @Override
    public GroupEntity update(GroupEntity entity) {
        RoleRepresentation roleRepresentation = new RoleRepresentation();
        //FIXME
        rolesResource.create(roleRepresentation);
        return entity;
    }

    @Override
    public GroupEntity update(GroupEntity entity, boolean fireDeleteEvent) {
        return update(entity);
    }

    @Override
    public void delete(String id) {
        rolesResource.get(id).remove();
    }

    @Override
    public void delete(GroupEntity entity) {
        delete(entity.getId());
    }

    @Override
    public void delete(GroupEntity entity, boolean fireDeleteEvent) {
        delete(entity.getId());
    }

    private List<Group> getAllGroups() {
        List<Group> ret = new ArrayList<>();
        List<RoleRepresentation> roles = rolesResource.list();
        for (RoleRepresentation role : roles) {
            Group g = new GroupEntityImpl();//role.getId()
            g.setName(role.getName());
            g.setType("assignment");
            ret.add(g);
        }
        return ret;
    }

    private List<Group> filterGroupsByQuery(List<Group> groups, GroupQueryImpl query) {
        Set<Group> ret = new HashSet<>();
        Set<String> clientRoles = query.getUserId() == null ? null : getUserRoles(query.getUserId());

        for (Group g : groups)
            if ((query.getId() == null || g.getId().equals(query.getId())) &&
                    (query.getName() == null || g.getName().equals(query.getName())) &&
                    (query.getNameLike() == null || g.getName().contains(query.getNameLike().replace("%", ""))) &&
                    (clientRoles == null || clientRoles.contains(g.getId())) &&
                    (query.getType() == null || g.getType().equals(query.getType())))
                ret.add(g);
        return new ArrayList<>(ret);

    }

    private Set<String> getUserRoles(String userId) {
        Set<String> ret = new HashSet<>();
        UserResource user = usersResource.get(userId);
        RoleMappingResource mapping = user.roles();
        RoleScopeResource rsc = mapping.realmLevel();

        List<RoleRepresentation> clientRoles = rsc.listAll();
        for (RoleRepresentation role : clientRoles) {
            ret.add(role.getId());
        }
        return ret;
    }

}
