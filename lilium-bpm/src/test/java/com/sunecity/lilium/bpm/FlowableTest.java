package com.sunecity.lilium.bpm;

import org.junit.Assert;
import org.junit.Test;

import javax.naming.NamingException;
import java.util.HashMap;

/**
 * Created by shahram on 5/3/17.
 */
public class FlowableTest {

    @Test
    public void test() {
        try {
            Flowable.init();
            Flowable flowable = new Flowable("admin");
            System.out.println(flowable.startProcess("accdoc", new HashMap<>()));
//            flowable.claimTask("17501");
//            flowable.completeTask("17501", new HashMap<>());
        } catch (NamingException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
