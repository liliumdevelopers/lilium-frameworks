package com.sunecity.lilium.bpm;

import com.sunecity.lilium.core.model.Rule;
import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by shahram on 11/12/16.
 */
public class RuleTest {

    @Test
    public void test() {
        try {
            Rule rule = new Rule("rule1", Files.readAllBytes(Paths.get("src/test/resources/sample.drl")));
            rule.setName("rule1");
            RuleRunner ruleRunner = new RuleRunner(rule);

            Person person = new Person();
            person.setId(10);
            ruleRunner.runRules(null, person);
            Assert.assertEquals(person.getName(), "شهرام");

            person = new Person();
            person.setId(9);
            ruleRunner.runRules(null, person);
            Assert.assertNull(person.getName());

            ruleRunner.close();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }

    }

}
