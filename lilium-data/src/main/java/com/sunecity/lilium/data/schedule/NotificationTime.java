package com.sunecity.lilium.data.schedule;

import com.sunecity.lilium.core.model.BaseEntitySimple;
import com.sunecity.lilium.data.schedule.enums.TimeType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "NOTIFICATION_TIMES")
public class NotificationTime extends BaseEntitySimple<Long> {

    private static final long serialVersionUID = -743369417248055402L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIME_TYPE")
    private TimeType timeType;

    @Column(name = "TIME")
    private int time;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private NotificationHour notificationHour;

    public NotificationTime() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    // @NotNull
    public TimeType getTimeType() {
        return timeType;
    }

    public void setTimeType(TimeType timeType) {
        this.timeType = timeType;
    }

    @Min(1)
    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public NotificationHour getNotificationHour() {
        return notificationHour;
    }

    public void setNotificationHour(NotificationHour notificationHour) {
        this.notificationHour = notificationHour;
    }

}
