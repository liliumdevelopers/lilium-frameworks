package com.sunecity.lilium.data.schedule;

import com.sunecity.lilium.core.model.BaseEntitySimple;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "NOTIFICATION_HOURS")
@NamedQueries({
        @NamedQuery(name = "selectAllNotificationHoursByCount", query = "SELECT COUNT(n) FROM NotificationHour n"),
        @NamedQuery(name = "selectAllNotificationHours", query = "SELECT n FROM NotificationHour n")})
public class NotificationHour extends BaseEntitySimple<Long> {

    private static final long serialVersionUID = 8408839479460885754L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "NOTIFICATION_DAY_ID")
    private Long notificationDayId;

    private int hour;

    private int minute;

    private int second;

    public NotificationHour() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getNotificationDayId() {
        return notificationDayId;
    }

    public void setNotificationDayId(Long notificationDayId) {
        this.notificationDayId = notificationDayId;
    }

    @Min(0)
    @Max(23)
    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    @Min(0)
    @Max(59)
    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    @Min(0)
    @Max(59)
    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

}
