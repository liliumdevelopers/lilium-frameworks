package com.sunecity.lilium.data.report;

import com.sunecity.lilium.core.model.BaseEntitySimple;
import com.sunecity.lilium.data.field.Field;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "REPORT_QUERY_FIELDS")
public class ReportQueryField extends BaseEntitySimple<Long> {

    private static final long serialVersionUID = -6154551709535851946L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ReportQueryFieldSeq")
    @SequenceGenerator(name = "ReportQueryFieldSeq", sequenceName = "REPORT_QUERY_FIELD_SEQ")
    @Column(name = "REPORT_QUERY_FIELD_ID")
    private Long id;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_QUERY_FIELD_ID")
    private Field field;

    @Column(name = "PARAM")
    private boolean param;

    public ReportQueryField() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Field getField() {
        return this.field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public boolean isParam() {
        return this.param;
    }

    public void setParam(boolean param) {
        this.param = param;
    }

}
