package com.sunecity.lilium.data.field;

/**
 * Created by shahram on 6/2/15.
 */
public enum BooleanType {

    CUSTOM,
    YES_NO,
    TRUE_FALSE,
    IMAGE_1,
    IMAGE_2,
    IMAGE_3,
    IMAGE_4,
    CHECKBOX_1,
    CHECKBOX_2,
    IMAGE_BALL

}
