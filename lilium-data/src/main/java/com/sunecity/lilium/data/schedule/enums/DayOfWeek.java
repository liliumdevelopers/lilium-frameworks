package com.sunecity.lilium.data.schedule.enums;

import com.sunecity.lilium.core.localization.Bundles;

public enum DayOfWeek {

	SATURDAY, SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY;

	public String toString() {
		String name = this.getClass().getSimpleName();
		return Bundles.getValueLocale(
				this.getClass().getName().replaceAll(".enums." + name, "")
						+ ".language." + name.toLowerCase().charAt(0)
						+ name.substring(1), this.name());
	}

}
