package com.sunecity.lilium.data.field;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.sunecity.lilium.core.model.Search;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

public abstract class Field implements Serializable, Cloneable {

    private static final long serialVersionUID = -2646117923711989705L;

    private final String displayName;
    private final Path<?> path;
    private String name;
    private Constraint constraint;

    public Field(String name, String displayName) {
        this.displayName = displayName;
        this.name = name;
        path = null;
    }

    public Field(Path<?> path, String displayName) {
        this.displayName = displayName;
        this.path = path;
        name = null;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Path<?> getPath() {
        return path;
    }

    public Constraint getConstraint() {
        return constraint;
    }

    public void setConstraint(Constraint constraint) {
        this.constraint = constraint;
    }

    @Override
    public Field clone() {
        try {
            return (Field) super.clone();
        } catch (CloneNotSupportedException e) {
            return this;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Field field = (Field) o;

        if (displayName != null ? !displayName.equals(field.displayName) : field.displayName != null) return false;
        if (name != null ? !name.equals(field.name) : field.name != null) return false;
        if (constraint != field.constraint) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getConstraints(), field.getConstraints())) return false;
        if (getFieldType() != field.getFieldType()) return false;
        return !(getTypeClass() != null ? !getTypeClass().equals(field.getTypeClass()) : field.getTypeClass() != null);

    }

    @Override
    public int hashCode() {
        int result = displayName != null ? displayName.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (constraint != null ? constraint.hashCode() : 0);
        result = 31 * result + (getConstraints() != null ? Arrays.hashCode(getConstraints()) : 0);
        result = 31 * result + (getFieldType() != null ? getFieldType().hashCode() : 0);
        result = 31 * result + (getTypeClass() != null ? getTypeClass().hashCode() : 0);
        return result;
    }

    public abstract Constraint[] getConstraints();

    public abstract FieldType getFieldType();

    public boolean isList() {
        return getSearch().getValue() instanceof Collection;
    }

    public abstract Predicate toPredicate();

    public abstract Search getSearch();

    public abstract Class<?> getTypeClass();

    public abstract void clear();

}
