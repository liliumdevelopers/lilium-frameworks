package com.sunecity.lilium.data.schedule;

import com.sunecity.lilium.data.schedule.enums.DayOfMonth;
import com.sunecity.lilium.data.schedule.enums.Month;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "NOTIFICATION_YEARS")
@NamedQueries({
        @NamedQuery(name = "selectAllNotificationYearsByCount", query = "SELECT COUNT(n) FROM NotificationYear n"),
        @NamedQuery(name = "selectAllNotificationYears", query = "SELECT n FROM NotificationYear n")})
public class NotificationYear extends NotificationTime {

    private static final long serialVersionUID = 1188528590436526277L;

    @Enumerated(EnumType.STRING)
    @Column(name = "MONTH")
    private Month month;

    @Enumerated(EnumType.STRING)
    @Column(name = "DAY_OF_MONTH")
    private DayOfMonth dayOfMonth;

    public NotificationYear() {
    }

    @NotNull
    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    @NotNull
    public DayOfMonth getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(DayOfMonth dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

}
