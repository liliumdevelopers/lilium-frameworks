package com.sunecity.lilium.data.field;

import com.querydsl.core.types.Predicate;
import com.sunecity.lilium.core.model.Search;

public class SizeField extends Field {

    public SizeField(String name, String displayName) {
        super(name, displayName);
    }

    @Override
    public Constraint[] getConstraints() {
        Constraint[] constraints = new Constraint[6];
        constraints[0] = Constraint.EQUALS;
        constraints[1] = Constraint.NOT_EQUALS;
        constraints[2] = Constraint.GREATER_THAN;
        constraints[3] = Constraint.GREATER_THAN_OR_EQUALS;
        constraints[4] = Constraint.LESS_THAN;
        constraints[5] = Constraint.LESS_THAN_OR_EQUALS;
        return constraints;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.SIZE;
    }

    @Override
    public Predicate toPredicate() {
        return null;
    }

    @Override
    public Search getSearch() {
        return null;
    }

    @Override
    public Class<?> getTypeClass() {
        return null;
    }

    @Override
    public void clear() {
    }

}