package com.sunecity.lilium.data.report;

/**
 * @author Shahram Goodarzi
 */
public enum ReportType {

    PDF("pdf", "application/pdf", true),
    XLSX("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", false),
    DOCX("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", true),
    PPTX("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation", true),
    RTF("rtf", "text/rtf", true),
    HTML("html", "text/html", false),
    HTML_PRINT("html", "text/html", false),
    PRINT(null, null, true);

    private final String extension;
    private final String mimetype;
    private final boolean paginate;

    ReportType(String extension, String mimetype, boolean paginate) {
        this.extension = extension;
        this.mimetype = mimetype;
        this.paginate = paginate;
    }

    public String getExtension() {
        return extension;
    }

    public String getMimetype() {
        return mimetype;
    }

    public boolean isPaginate() {
        return paginate;
    }

}
