package com.sunecity.lilium.data.field;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.ComparablePath;
import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.core.model.Search;
import com.sunecity.lilium.core.time.Time;

public class TimeField extends Field implements SimpleField {

    private Time value;
    private Time value2;

    public TimeField(String name, String displayName) {
        super(name, displayName);
    }

    public TimeField(Path<?> path, String displayName) {
        super(path, displayName);
    }


    public Time getValue() {
        return value;
    }

    public void setValue(Time value) {
        this.value = value;
    }

    public Time getValue2() {
        return value2;
    }

    public void setValue2(Time value2) {
        this.value2 = value2;
    }

    @Override
    public Constraint[] getConstraints() {
        Constraint[] constraints = new Constraint[6];
        constraints[0] = Constraint.BETWEEN;
        constraints[1] = Constraint.NOT_BETWEEN;
        constraints[2] = Constraint.GREATER_THAN;
        constraints[3] = Constraint.LESS_THAN;
        constraints[4] = Constraint.IS_NULL;
        constraints[5] = Constraint.IS_NOT_NULL;
        return constraints;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.TIME;
    }

    @Override
    public Predicate toPredicate() {
        ComparablePath<Time> path = (ComparablePath<Time>) getPath();
        switch (getConstraint()) {
            case IS_NULL:
                return path.isNull();
            case IS_NOT_NULL:
                return path.isNotNull();
            case GREATER_THAN:
                return path.gt(value);
            case LESS_THAN:
                return path.lt(value);
            case BETWEEN:
                return path.between(value, value2);
            case NOT_BETWEEN:
                return path.notBetween(value, value2);
            default:
                return null;
        }
    }

    @Override
    public Search getSearch() {
        String name = getDisplayName();
        String condition = getConstraint().toString();
        Object value = null;
        switch (getConstraint()) {
            case BETWEEN:
            case NOT_BETWEEN:
                value = getValue().toString()
                        + Bundles.getValueDefault("search.and")
                        + getValue2().toString();
                break;
            case GREATER_THAN:
            case LESS_THAN:
                value = getValue().toString();
                break;
            case IS_NULL:
            case IS_NOT_NULL:
            default:
                break;
        }
        return new Search(name, condition, value);
    }

    @Override
    public Class<?> getTypeClass() {
        return Time.class;
    }

    @Override
    public void clear() {
        value = null;
        value2 = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TimeField timeField = (TimeField) o;

        if (value != null ? !value.equals(timeField.value) : timeField.value != null) return false;
        return !(value2 != null ? !value2.equals(timeField.value2) : timeField.value2 != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (value2 != null ? value2.hashCode() : 0);
        return result;
    }

}
