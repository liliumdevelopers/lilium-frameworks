package com.sunecity.lilium.data.field;

import com.sunecity.lilium.core.localization.Bundles;

public enum Constraint {

    EQUALS, NOT_EQUALS, GREATER_THAN, LESS_THAN, GREATER_THAN_OR_EQUALS, LESS_THAN_OR_EQUALS, BETWEEN, NOT_BETWEEN, IS_NULL, IS_NOT_NULL, CONTAINS, NOT_CONTAINS, STARTS_WITH, ENDS_WITH, IN, NOT_IN;

    public String toString() {
        return Bundles.getValueDefault("search." + name());
    }

}
