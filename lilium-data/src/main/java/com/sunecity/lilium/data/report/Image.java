package com.sunecity.lilium.data.report;

import com.sunecity.lilium.core.model.BaseEntitySimple;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

@MappedSuperclass
public class Image extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ImageSeq")
    @SequenceGenerator(name = "ImageSeq", sequenceName = "IMAGE_SEQ")
    @Column(name = "IMAGE_ID")
    private long id;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

}
