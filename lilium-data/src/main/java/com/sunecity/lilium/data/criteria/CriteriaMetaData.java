package com.sunecity.lilium.data.criteria;

import com.querydsl.core.QueryMetadata;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Path;

/**
 * @author Shahram Goodarzi
 */
public class CriteriaMetaData extends MetaData {

    private QueryMetadata metadata;
    private String graph;
    private Expression<?> result;
    private Path<?>[] paths;

    CriteriaMetaData() {
    }

    public CriteriaMetaData(CriteriaMetaData metaData, Path<?>... paths) {
        this.metadata = metaData.getMetadata();
        this.graph = metaData.getGraph();
        this.result = metaData.getResult();
        this.paths = paths;
    }

    public QueryMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(QueryMetadata metadata) {
        this.metadata = metadata;
    }

    public String getGraph() {
        return graph;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }

    public Expression<?> getResult() {
        return result;
    }

    public void setResult(Expression<?> result) {
        this.result = result;
    }

    public Path<?>[] getPaths() {
        return paths;
    }

    public void setPaths(Path<?>[] paths) {
        this.paths = paths;
    }

}
