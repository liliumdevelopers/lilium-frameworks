package com.sunecity.lilium.data.criteria;

import com.sunecity.lilium.data.field.Constraint;

import java.util.List;

/**
 * Created by shahram on 1/29/17.
 */
public class Search {

    private String field;
    private Class type;
    private Constraint constraint;
    private Object value;
    private Object another;
    private List<Object> values;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    public Constraint getConstraint() {
        return constraint;
    }

    public void setConstraint(Constraint constraint) {
        this.constraint = constraint;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getAnother() {
        return another;
    }

    public void setAnother(Object another) {
        this.another = another;
    }

    public List<Object> getValues() {
        return values;
    }

    public void setValues(List<Object> values) {
        this.values = values;
    }

}
