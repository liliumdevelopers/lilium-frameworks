package com.sunecity.lilium.data.report;

import com.sunecity.lilium.data.field.Field;
import com.sunecity.lilium.data.report.enums.Function;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "REPORT_COLUMNS")
public class ReportColumn extends AbstractColumn {

    private static final long serialVersionUID = -6006063872880728398L;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "COLUMN_ID")
    private Field field;

    @Column(name = "FIELD_STYLE")
    private Style fieldStyle;

    @Column(name = "SUBTOTAL")
    @Enumerated(EnumType.STRING)
    private Function subtotal;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "SUBTOTAL_STYLE_ID")
    private Style subtotalStyle;

    @Column(name = "SUBTOTAL_LABEL")
    private String subtotalLabel;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "SUBTOTAL_LABEL_STYLE_ID")
    private Style subtotalLabelStyle;

    @Column(name = "SUMMARY")
    @Enumerated(EnumType.STRING)
    private Function summary;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "SUMMARY_STYLE_ID")
    private Style summaryStyle;

    @Column(name = "SUMMARY_LABEL")
    private String summaryLabel;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "SUMMARY_LABEL_STYLE_ID")
    private Style summaryLabelStyle;

    public ReportColumn() {
    }

    public ReportColumn(Field field) {
        this.field = field;
    }

    public Field getField() {
        return this.field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public Style getFieldStyle() {
        return this.fieldStyle;
    }

    public void setFieldStyle(Style fieldStyle) {
        this.fieldStyle = fieldStyle;
    }

    public Function getSubtotal() {
        return this.subtotal;
    }

    public void setSubtotal(Function subtotal) {
        this.subtotal = subtotal;
    }

    public Style getSubtotalStyle() {
        return this.subtotalStyle;
    }

    public void setSubtotalStyle(Style subtotalStyle) {
        this.subtotalStyle = subtotalStyle;
    }

    public String getSubtotalLabel() {
        return this.subtotalLabel;
    }

    public void setSubtotalLabel(String subtotalLabel) {
        this.subtotalLabel = subtotalLabel;
    }

    public Style getSubtotalLabelStyle() {
        return this.subtotalLabelStyle;
    }

    public void setSubtotalLabelStyle(Style subtotalLabelStyle) {
        this.subtotalLabelStyle = subtotalLabelStyle;
    }

    public Function getSummary() {
        return this.summary;
    }

    public void setSummary(Function summary) {
        this.summary = summary;
    }

    public Style getSummaryStyle() {
        return this.summaryStyle;
    }

    public void setSummaryStyle(Style summaryStyle) {
        this.summaryStyle = summaryStyle;
    }

    public String getSummaryLabel() {
        return this.summaryLabel;
    }

    public void setSummaryLabel(String summaryLabel) {
        this.summaryLabel = summaryLabel;
    }

    public Style getSummaryLabelStyle() {
        return this.summaryLabelStyle;
    }

    public void setSummaryLabelStyle(Style summaryLabelStyle) {
        this.summaryLabelStyle = summaryLabelStyle;
    }

}
