package com.sunecity.lilium.data.field;

import com.querydsl.core.types.Predicate;
import com.sunecity.lilium.core.model.Search;

public class CollectionField extends Field {

    private String collectionName;
    private Field field;

    public CollectionField(String collectionName, Field field) {
        super(field.getName(), field.getDisplayName());
        this.collectionName = collectionName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    @Override
    public Constraint[] getConstraints() {
        return field.getConstraints();
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.COLLECTION;
    }

    @Override
    public Predicate toPredicate() {
        return null;
    }

    @Override
    public Search getSearch() {
        return null;
    }

    @Override
    public Class<?> getTypeClass() {
        return null;
    }

    @Override
    public void clear() {
    }

}
