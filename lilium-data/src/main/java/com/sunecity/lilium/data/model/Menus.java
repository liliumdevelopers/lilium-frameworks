package com.sunecity.lilium.data.model;

import com.sunecity.lilium.core.model.BaseEntitySimple;
import com.sunecity.lilium.core.model.Treeable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Shahram Goodarzi
 */
@Entity
@Table(name = "SK_MENUS", uniqueConstraints = {
        @UniqueConstraint(name = "UK_MENUS_LABEL", columnNames = "LABEL")
})
public class Menus extends BaseEntitySimple<Long> implements Treeable<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MenuSeq")
    @SequenceGenerator(name = "MenuSeq", sequenceName = "SK_MENUS_SEQ")
    @Column(name = "MENU_ID")
    private Long id;

    @Column(name = "LABEL")
    @Size(max = 100)
    private String label;

    @Column(name = "ICON")
    @Size(max = 100)
    private String icon;

    @Column(name = "IS_DISABLED")
    private boolean disabled;

    @Column(name = "TARGET")
    @Size(max = 20)
    private String target;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_ID", foreignKey = @ForeignKey(name = "FK_MENUS_PARENT"))
    private Menus parent;

    @Column(name = "PARENT_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "PATH")
    @Size(max = 200)
    @NotBlank
    private String path;

    @Column(name = "IS_LEAF")
    private boolean leaf;

    @Column(name = "PRIORITY")
    private int priority;

    @Column(name = "DESCRIPTION")
    @Size(max = 100)
    private String description;

    public Menus() {
    }

    public Menus(int priority) {
        this.description = "separator";
        this.priority = priority;
    }

    public Menus(String label, String path, int priority, String icon, boolean disabled) {
        this.label = label;
        this.path = path;
        this.priority = priority;
        this.icon = icon;
        this.disabled = disabled;
    }

    public Menus(String label, String path, Menus parent, int priority, boolean disabled) {
        this.label = label;
        this.path = path;
        this.parent = parent;
        this.priority = priority;
        this.disabled = disabled;
    }

    public Menus(String label, String path, Menus parent, int priority, boolean disabled, String target) {
        this.label = label;
        this.path = path;
        this.parent = parent;
        this.priority = priority;
        this.disabled = disabled;
        this.target = target;
    }

    public Menus(String label, String path, Menus parent, int priority, String icon, boolean disabled) {
        this.label = label;
        this.path = path;
        this.parent = parent;
        this.priority = priority;
        this.icon = icon;
        this.disabled = disabled;
    }

    public Menus(String label, String path, Menus parent, String description, int priority, boolean disabled) {
        this.label = label;
        this.path = path;
        this.parent = parent;
        this.description = description;
        this.priority = priority;
        this.disabled = disabled;
    }

    public Menus(String label, String path, Menus parent, int priority, String icon, boolean disabled, String target) {
        this.label = label;
        this.path = path;
        this.parent = parent;
        this.priority = priority;
        this.icon = icon;
        this.disabled = disabled;
        this.target = target;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Menus getParent() {
        return parent;
    }

    public void setParent(Menus parent) {
        this.parent = parent;
    }

    @Override
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Menus menus = (Menus) o;

        if (disabled != menus.disabled) return false;
        if (leaf != menus.leaf) return false;
        if (priority != menus.priority) return false;
        if (id != null ? !id.equals(menus.id) : menus.id != null) return false;
        if (label != null ? !label.equals(menus.label) : menus.label != null) return false;
        if (icon != null ? !icon.equals(menus.icon) : menus.icon != null) return false;
        if (target != null ? !target.equals(menus.target) : menus.target != null) return false;
        if (parentId != null ? !parentId.equals(menus.parentId) : menus.parentId != null) return false;
        if (path != null ? !path.equals(menus.path) : menus.path != null) return false;
        return description != null ? description.equals(menus.description) : menus.description == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (disabled ? 1 : 0);
        result = 31 * result + (target != null ? target.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (leaf ? 1 : 0);
        result = 31 * result + priority;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

}