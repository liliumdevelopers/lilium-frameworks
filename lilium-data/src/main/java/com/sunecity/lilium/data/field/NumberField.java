package com.sunecity.lilium.data.field;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.NumberPath;
import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.core.model.Search;

import java.util.List;

public class NumberField extends Field implements SimpleField {

    private Number value;
    private Number value2;
    private String addValue;
    private List<Number> values;
    private List<Number> selectedValues;

    public NumberField(String name, String displayName) {
        super(name, displayName);
    }

    public NumberField(Path<?> path, String displayName) {
        super(path, displayName);
    }

    public Number getValue() {
        return value;
    }

    public void setValue(Number value) {
        this.value = value;
    }

    public Number getValue2() {
        return value2;
    }

    public void setValue2(Number value2) {
        this.value2 = value2;
    }

    public String getAddValue() {
        return addValue;
    }

    public void setAddValue(String addValue) {
        this.addValue = addValue;
    }

    public List<Number> getValues() {
        return values;
    }

    public void setValues(List<Number> values) {
        this.values = values;
    }

    public List<Number> getSelectedValues() {
        return selectedValues;
    }

    public void setSelectedValues(List<Number> selectedValues) {
        this.selectedValues = selectedValues;
    }

    @Override
    public Constraint[] getConstraints() {
        Constraint[] constraints = new Constraint[12];
        constraints[0] = Constraint.EQUALS;
        constraints[1] = Constraint.NOT_EQUALS;
        constraints[2] = Constraint.BETWEEN;
        constraints[3] = Constraint.NOT_BETWEEN;
        constraints[4] = Constraint.GREATER_THAN;
        constraints[5] = Constraint.LESS_THAN;
        constraints[6] = Constraint.GREATER_THAN_OR_EQUALS;
        constraints[7] = Constraint.LESS_THAN_OR_EQUALS;
        constraints[8] = Constraint.IN;
        constraints[9] = Constraint.NOT_IN;
        constraints[10] = Constraint.IS_NULL;
        constraints[11] = Constraint.IS_NOT_NULL;
        return constraints;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.NUMBER;
    }

    @Override
    public Predicate toPredicate() {
        NumberPath numberPath = (NumberPath) getPath();
        switch (getConstraint()) {
            case EQUALS:
                return numberPath.eq(value);
            case NOT_EQUALS:
                return numberPath.ne(value);
            case BETWEEN:
                return numberPath.between(value, value2);
            case NOT_BETWEEN:
                return numberPath.notBetween(value, value2);
            case GREATER_THAN:
                return numberPath.gt(value);
            case LESS_THAN:
                return numberPath.lt(value);
            case GREATER_THAN_OR_EQUALS:
                return numberPath.goe(value);
            case LESS_THAN_OR_EQUALS:
                return numberPath.loe(value);
            case IN:
                return numberPath.in(selectedValues);
            case NOT_IN:
                return numberPath.notIn(selectedValues);
            case IS_NULL:
                return numberPath.isNull();
            case IS_NOT_NULL:
                return numberPath.isNotNull();
            default:
                return null;
        }
    }

    @Override
    public Search getSearch() {
        String name = getDisplayName();
        String condition = getConstraint().toString();
        Object value = null;
        switch (getConstraint()) {
            case EQUALS:
            case NOT_EQUALS:
            case GREATER_THAN:
            case LESS_THAN:
            case GREATER_THAN_OR_EQUALS:
            case LESS_THAN_OR_EQUALS:
                value = getValue().toString();
                break;
            case BETWEEN:
            case NOT_BETWEEN:
                value = getValue().toString() + Bundles.getValueDefault("search.and") + getValue2().toString();
                break;
            case IN:
            case NOT_IN:
                value = getSelectedValues();
                break;
            case IS_NULL:
            case IS_NOT_NULL:
            default:
                break;
        }
        return new Search(name, condition, value);
    }

    @Override
    public Class<?> getTypeClass() {
        return Number.class;
    }

    @Override
    public void clear() {
        value = null;
        value2 = null;
        addValue = null;
        values = null;
        selectedValues = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        NumberField that = (NumberField) o;

        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (value2 != null ? !value2.equals(that.value2) : that.value2 != null) return false;
        if (addValue != null ? !addValue.equals(that.addValue) : that.addValue != null) return false;
        if (values != null ? !values.equals(that.values) : that.values != null) return false;
        return !(selectedValues != null ? !selectedValues.equals(that.selectedValues) : that.selectedValues != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (value2 != null ? value2.hashCode() : 0);
        result = 31 * result + (addValue != null ? addValue.hashCode() : 0);
        result = 31 * result + (values != null ? values.hashCode() : 0);
        result = 31 * result + (selectedValues != null ? selectedValues.hashCode() : 0);
        return result;
    }

}
