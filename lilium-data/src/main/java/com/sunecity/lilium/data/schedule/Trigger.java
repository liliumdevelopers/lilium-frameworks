package com.sunecity.lilium.data.schedule;

import com.sunecity.lilium.core.time.DateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

//@Entity
@Table(name = "TRIGGERS")
@NamedQueries({
        @NamedQuery(name = "selectAllTriggersByCount", query = "SELECT COUNT(t) FROM Trigger t"),
        @NamedQuery(name = "selectAllTriggers", query = "SELECT t FROM Trigger t"),
        @NamedQuery(name = "selectNotificationByTimeType", query = "SELECT n FROM Trigger t JOIN t.notificationTime n where n.timeType=:timeType"),
        @NamedQuery(name = "selectAllNotificationYearsByTriggerIdCount", query = "SELECT COUNT(n) FROM Trigger t JOIN t.notificationTime n where n.id=:id"),
        @NamedQuery(name = "selectAllNotificationYearsByTriggerId", query = "SELECT n FROM Trigger t JOIN t.notificationTime n where n.id=:id"),
        @NamedQuery(name = "selectAllNotificationMonthsByTriggerIdCount", query = "SELECT COUNT(n) FROM Trigger t JOIN t.notificationTime n where n.id=:id"),
        @NamedQuery(name = "selectAllNotificationMonthsByTriggerId", query = "SELECT n FROM Trigger t JOIN t.notificationTime n where n.id=:id"),
        @NamedQuery(name = "selectAllNotificationWeeksByTriggerIdCount", query = "SELECT COUNT(n) FROM Trigger t JOIN t.notificationTime n where n.id=:id"),
        @NamedQuery(name = "selectAllNotificationWeeksByTriggerId", query = "SELECT n FROM Trigger t JOIN t.notificationTime n where n.id=:id"),
        @NamedQuery(name = "selectAllNotificationDaysByTriggerIdCount", query = "SELECT COUNT(n) FROM Trigger t JOIN t.notificationTime n where n.id=:id"),
        @NamedQuery(name = "selectAllNotificationDaysByTriggerId", query = "SELECT n FROM Trigger t JOIN t.notificationTime n where n.id=:id")})
public class Trigger extends AbstractTrigger {

    private static final long serialVersionUID = -2945357031365808490L;

    @Column(name = "GROUP_ID")
    private Long groupId;

    @Column(name = "REPEAT")
    private boolean repeat;

    @Column(name = "START_DATE")
    private DateTime startDate;

    @Column(name = "END_DATE")
    private DateTime endDate;

    @OneToOne(cascade = CascadeType.ALL)
    private NotificationTime notificationTime;

    @Column(name = "PAUSE")
    private boolean pause;

    @Min(1)
    @Max(23)
    @Column(name = "STARTDATE_HOUR")
    private int startDateHour;

    @Min(1)
    @Max(59)
    @Column(name = "STARTDATE_MINUTE")
    private int startDateMinute;

    @Min(1)
    @Max(59)
    @Column(name = "STARTDATE_SECOND")
    private int startDateSecond;

    @Min(1)
    @Max(23)
    @Column(name = "ENDDATE_HOUR")
    private int endDateHour;

    @Min(1)
    @Max(59)
    @Column(name = "ENDDATE_MINUTE")
    private int endDateMinute;

    @Min(1)
    @Max(59)
    @Column(name = "ENDDATE_SECOND")
    private int endDateSecond;

    public Trigger() {
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public DateTime getStartDate() {
        if (startDate == null) {
            startDate = DateTime.now();
        }
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public NotificationTime getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(NotificationTime notificationTime) {
        this.notificationTime = notificationTime;
    }

    public boolean isPause() {
        return pause;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }

    public int getStartDateHour() {
        return startDateHour;
    }

    public void setStartDateHour(int startDateHour) {
        this.startDateHour = startDateHour;
    }

    public int getStartDateMinute() {
        return startDateMinute;
    }

    public void setStartDateMinute(int startDateMinute) {
        this.startDateMinute = startDateMinute;
    }

    public int getStartDateSecond() {
        return startDateSecond;
    }

    public void setStartDateSecond(int startDateSecond) {
        this.startDateSecond = startDateSecond;
    }

    public int getEndDateHour() {
        return endDateHour;
    }

    public void setEndDateHour(int endDateHour) {
        this.endDateHour = endDateHour;
    }

    public int getEndDateMinute() {
        return endDateMinute;
    }

    public void setEndDateMinute(int endDateMinute) {
        this.endDateMinute = endDateMinute;
    }

    public int getEndDateSecond() {
        return endDateSecond;
    }

    public void setEndDateSecond(int endDateSecond) {
        this.endDateSecond = endDateSecond;
    }

}
