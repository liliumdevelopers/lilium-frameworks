package com.sunecity.lilium.data.report;

import com.sunecity.lilium.core.model.BaseEntitySimple;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "REPORT_QUERIES")
public class ReportQuery extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ReportQuerySeq")
    @SequenceGenerator(name = "ReportQuerySeq", sequenceName = "REPORT_QUERY_SEQ")
    @Column(name = "REPORT_QUERY_ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "QUERY")
    private String query;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_ENTITY_ID")
    @OrderColumn
    private List<ReportQueryField> fields;

    public ReportQuery() {
    }

    public ReportQuery(long id) {
        this.id = id;
    }

    public ReportQuery(String name, String query) {
        this.name = name;
        this.query = query;
    }

    public ReportQuery(String name, String query, List<ReportQueryField> fields) {
        this.name = name;
        this.query = query;
        this.fields = fields;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ReportQueryField> getFields() {
        return this.fields;
    }

    public void setFields(List<ReportQueryField> fields) {
        this.fields = fields;
    }

    public void addField(ReportQueryField field) {
        if (this.getFields() == null) {
            this.fields = new ArrayList<>();
        }
        this.fields.add(field);
    }

}
