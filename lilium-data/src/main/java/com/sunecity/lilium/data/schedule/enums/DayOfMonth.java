package com.sunecity.lilium.data.schedule.enums;

import com.sunecity.lilium.core.localization.Bundles;

public enum DayOfMonth {

	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, _30, _31, LAST_DAY_OF_MONTH;

	public String toString() {
		String name = this.getClass().getSimpleName();
		return Bundles.getValueLocale(
				this.getClass().getName().replaceAll(".enums." + name, "")
						+ ".language." + name.toLowerCase().charAt(0)
						+ name.substring(1), this.name());
	}

}
