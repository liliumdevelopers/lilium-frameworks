package com.sunecity.lilium.data.field;

/**
 * <b>Title:</b> FieldType
 * Different types of fields for data model.
 *
 * @author Shahram Goodarzi
 * @version 1.1.0
 */
public enum FieldType {

    MULTIPLE, BOOLEAN, ENUM, NUMBER, STRING, COLLECTION, SIZE, DATE, DATE_TIME, TIME

}
