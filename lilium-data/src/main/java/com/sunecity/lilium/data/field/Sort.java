package com.sunecity.lilium.data.field;

import java.io.Serializable;

/**
 * Created by shahram on 10/16/16.
 */
public class Sort implements Serializable {

    private String field;
    private SortType type;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public SortType getType() {
        return type;
    }

    public void setType(SortType type) {
        this.type = type;
    }

    public enum SortType {
        ASC, DESC
    }

}
