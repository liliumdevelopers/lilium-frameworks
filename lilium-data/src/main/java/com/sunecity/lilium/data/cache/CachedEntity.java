package com.sunecity.lilium.data.cache;

import com.sunecity.lilium.core.time.DateTime;

import java.io.Serializable;

/**
 * Created by shahram on 2/12/17.
 */
public class CachedEntity implements Serializable {

    private int version;
    private DateTime modifiedTimestamp;

    public CachedEntity() {
    }

    public CachedEntity(int version, DateTime modifiedTimestamp) {
        this.version = version;
        this.modifiedTimestamp = modifiedTimestamp;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public DateTime getModifiedTimestamp() {
        return modifiedTimestamp;
    }

    public void setModifiedTimestamp(DateTime modifiedTimestamp) {
        this.modifiedTimestamp = modifiedTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CachedEntity that = (CachedEntity) o;

        if (version != that.version) return false;
        return modifiedTimestamp != null ? modifiedTimestamp.equals(that.modifiedTimestamp) : that.modifiedTimestamp == null;
    }

    @Override
    public int hashCode() {
        int result = version;
        result = 31 * result + (modifiedTimestamp != null ? modifiedTimestamp.hashCode() : 0);
        return result;
    }

}
