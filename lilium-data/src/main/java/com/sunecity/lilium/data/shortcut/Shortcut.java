package com.sunecity.lilium.data.shortcut;

import com.sunecity.lilium.core.model.BaseEntitySimple;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "SK_SHORTCUTS",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "UK_SHORTCUTS_COMB_CODE_ACTION",
                        columnNames = {"KEY_COMBINATION", "KEY_CODE", "ACTION"
                        }
                )
        }
)
public class Shortcut extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ShortcutSeq")
    @SequenceGenerator(name = "ShortcutSeq", sequenceName = "SK_SHORTCUTS_SEQ")
    @Column(name = "SHORTCUT_ID")
    private Long id;

    @Column(name = "KEY_COMBINATION", length = 17)
    @Enumerated(EnumType.STRING)
    private KeyCombination keyCombination;

    @Column(name = "KEY_CODE", length = 3)
    @Enumerated(EnumType.STRING)
    private KeyCode keyCode;

    @Column(name = "ACTION")
    private String action;

    @Column(name = "DESCRIPTION")
    private String description;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public KeyCombination getKeyCombination() {
        return keyCombination;
    }

    public void setKeyCombination(KeyCombination keyCombination) {
        this.keyCombination = keyCombination;
    }

    @NotNull
    public KeyCode getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(KeyCode keyCode) {
        this.keyCode = keyCode;
    }

    @Size(max = 50)
    @NotBlank
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Size(max = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    public String getBind() {
        return keyCombination.getName() + "+" + keyCode.getName();
    }

}
