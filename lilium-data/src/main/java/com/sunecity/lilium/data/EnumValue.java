package com.sunecity.lilium.data;

import java.io.Serializable;

/**
 * @author Shahram Goodarzi
 */
public interface EnumValue<T extends Serializable> {

    String name();

    int ordinal();

    T value();

}
