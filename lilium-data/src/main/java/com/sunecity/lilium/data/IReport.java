package com.sunecity.lilium.data;

import com.sunecity.lilium.data.criteria.CriteriaMetaData;
import com.sunecity.lilium.data.report.Report;
import com.sunecity.lilium.data.report.ReportType;

/**
 * @author Shahram Goodarzi
 */
public interface IReport {

    String createReport(Report report, ReportType reportType, CriteriaMetaData metaData) throws Exception;

}
