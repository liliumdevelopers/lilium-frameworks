package com.sunecity.lilium.data.cache;

import javax.cache.annotation.GeneratedCacheKey;

/**
 * Created by shahram on 3/15/17.
 */
public class EntityGeneratedCacheKey implements GeneratedCacheKey {

    private final Object id;

    public EntityGeneratedCacheKey(Object id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityGeneratedCacheKey that = (EntityGeneratedCacheKey) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

}
