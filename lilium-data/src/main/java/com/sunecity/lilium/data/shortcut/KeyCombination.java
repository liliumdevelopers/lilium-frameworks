package com.sunecity.lilium.data.shortcut;

public enum KeyCombination {

    NONE("none"),
    ALT("alt"),
    CONTROL("crtl"),
    SHIFT("shift"),
    ALT_CONTROL("alt+crtl"),
    ALT_SHIFT("alt+shift"),
    CONTROL_SHIFT("crtl+shift"),
    ALT_CONTROL_SHIFT("alt+crtl+shift");

    private final String name;

    KeyCombination(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
