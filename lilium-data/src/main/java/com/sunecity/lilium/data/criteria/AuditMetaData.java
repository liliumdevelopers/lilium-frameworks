package com.sunecity.lilium.data.criteria;

import com.sunecity.lilium.core.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Shahram Goodarzi
 */
public class AuditMetaData extends MetaData {

    private final Class<?> clas;
    private Serializable id;
    private String author;
    private DateTime from;
    private DateTime to;
    private Map<String, String> commitProperties;
    private List<String> changeProperties;

    public AuditMetaData(Class<?> clas) {
        this.clas = clas;
    }

    public AuditMetaData(Class<?> clas, Serializable id) {
        this.clas = clas;
        this.id = id;
    }

    public Class<?> getClas() {
        return clas;
    }

    public Serializable getId() {
        return id;
    }

    public void setId(Serializable id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public DateTime getFrom() {
        return from;
    }

    public void setFrom(DateTime from) {
        this.from = from;
    }

    public DateTime getTo() {
        return to;
    }

    public void setTo(DateTime to) {
        this.to = to;
    }

    public Map<String, String> getCommitProperties() {
        return commitProperties;
    }

    public void setCommitProperties(Map<String, String> commitProperties) {
        this.commitProperties = commitProperties;
    }

    public void addCommitProperty(String name, String value) {
        if (commitProperties == null) {
            commitProperties = new HashMap<>();
        }
        commitProperties.put(name, value);
    }

    public List<String> getChangeProperties() {
        return changeProperties;
    }

    public void setChangeProperties(List<String> changeProperties) {
        this.changeProperties = changeProperties;
    }

    public void addChangeProperty(String propertyChange) {
        if (changeProperties == null) {
            changeProperties = new ArrayList<>();
        }
        changeProperties.add(propertyChange);
    }

}
