package com.sunecity.lilium.data.report.enums;

import com.sunecity.lilium.data.report.Page;

/**
 * Created by shahram on 7/23/15.
 */
public enum Paper {

    A0(new Page(2380, 3368)),
    A1(new Page(1684, 2380)),
    A2(new Page(1190, 1684)),
    A3(new Page(842, 1190)),
    A4(new Page(595, 842)),
    A5(new Page(421, 595)),
    A6(new Page(297, 421)),
    A7(new Page(210, 297)),
    A8(new Page(148, 210)),
    A9(new Page(105, 148)),
    A10(new Page(74, 105)),
    B0(new Page(2836, 4008)),
    B1(new Page(2004, 2836)),
    B2(new Page(1418, 2004)),
    B3(new Page(1002, 1418)),
    B4(new Page(709, 1002)),
    B5(new Page(501, 709)),
    LETTER(new Page(612, 792)),
    LEGAL(new Page(612, 1008)),
    LEDGER(new Page(1224, 792)),
    CUSTOM(new Page(595, 842));

    private final Page page;

    Paper(Page page) {
        this.page = page;
    }

    public static Paper valueOf(int height, int width) {
        return Paper.A0;
    }

    public Page getPage() {
        return page;
    }

}
