package com.sunecity.lilium.data.sysinfo;

public class Network {

	private String name;
	private String macAddress;

	public Network() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

}
