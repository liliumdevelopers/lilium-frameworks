package com.sunecity.lilium.data.report.chart;

import com.sunecity.lilium.core.model.BaseEntitySimple;
import com.sunecity.lilium.data.report.ReportColumn;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

@MappedSuperclass
public abstract class Chart extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ChartSeq")
    @SequenceGenerator(name = "ChartSeq", sequenceName = "CHART_SEQ")
    @Column(name = "CHART_ID")
    private long id;

    @Column(name = "TITLE")
    private String title;

    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "COLUMN_CATEGORY_ID")
    private ReportColumn category;

    public Chart() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ReportColumn getCategory() {
        return this.category;
    }

    public void setCategory(ReportColumn category) {
        this.category = category;
    }

}
