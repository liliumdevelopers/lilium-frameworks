package com.sunecity.lilium.data.field;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.ComparablePath;
import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.core.model.Search;
import com.sunecity.lilium.core.time.Date;

/**
 * <b>Title:</b> DateTimeField
 * Date Field type for data model.
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public class DateField extends Field implements SimpleField {

    private Date value;
    private Date value2;

    public DateField(String name, String displayName) {
        super(name, displayName);
    }

    public DateField(Path<?> path, String displayName) {
        super(path, displayName);
    }

    @Override
    public Constraint[] getConstraints() {
        Constraint[] constraints = new Constraint[6];
        constraints[0] = Constraint.BETWEEN;
        constraints[1] = Constraint.NOT_BETWEEN;
        constraints[2] = Constraint.GREATER_THAN;
        constraints[3] = Constraint.LESS_THAN;
        constraints[4] = Constraint.IS_NULL;
        constraints[5] = Constraint.IS_NOT_NULL;
        return constraints;
    }

    public Date getValue() {
        return value;
    }

    public void setValue(Date value) {
        this.value = value;
    }

    public Date getValue2() {
        return value2;
    }

    public void setValue2(Date value2) {
        this.value2 = value2;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.DATE;
    }

    @Override
    public Predicate toPredicate() {
        ComparablePath<Date> path = (ComparablePath<Date>) getPath();
        switch (getConstraint()) {
            case IS_NULL:
                return path.isNull();
            case IS_NOT_NULL:
                return path.isNotNull();
            case GREATER_THAN:
                return path.gt(value);
            case LESS_THAN:
                return path.lt(value);
            case BETWEEN:
                return path.between(value, value2);
            case NOT_BETWEEN:
                return path.notBetween(value, value2);
            default:
                return null;
        }
    }

    @Override
    public Search getSearch() {
        String name = getDisplayName();
        String condition = getConstraint().toString();
        Object value = null;
        switch (getConstraint()) {
            case BETWEEN:
            case NOT_BETWEEN:
                value = getValue().toString() + Bundles.getValueDefault("search.and") + getValue2().toString();
                break;
            case GREATER_THAN:
            case LESS_THAN:
                value = getValue().toString();
                break;
            case IS_NULL:
            case IS_NOT_NULL:
            default:
                break;
        }
        return new Search(name, condition, value);
    }

    @Override
    public Class<?> getTypeClass() {
        return Date.class;
    }

    @Override
    public void clear() {
        value = null;
        value2 = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        DateField dateField = (DateField) o;

        if (value != null ? !value.equals(dateField.value) : dateField.value != null) return false;
        return !(value2 != null ? !value2.equals(dateField.value2) : dateField.value2 != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (value2 != null ? value2.hashCode() : 0);
        return result;
    }

}
