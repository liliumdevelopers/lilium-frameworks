package com.sunecity.lilium.data.report;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "GROUP_COLUMNS")
public class GroupColumn extends AbstractColumn {

    @Column(name = "TITLE")
    private String title;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "GROUP_COLUMN_ID")
    private Set<AbstractColumn> columns;

    public GroupColumn() {
    }

    public GroupColumn(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<AbstractColumn> getColumns() {
        return this.columns;
    }

    public void setColumns(Set<AbstractColumn> columns) {
        this.columns = columns;
    }

    public void addColumn(AbstractColumn column) {
        if (this.getColumns() == null) {
            this.columns = new HashSet<>();
        }
        this.columns.add(column);
    }

}
