package com.sunecity.lilium.data.schedule;

import com.sunecity.lilium.core.model.BaseEntitySimple;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Inheritance(strategy = InheritanceType.JOINED)
@Entity
@Table(name = "ABSTRACT_TRIGGERS")
public abstract class AbstractTrigger extends BaseEntitySimple<Long> {

    private static final long serialVersionUID = 7319131591723163673L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "NAME")
    private String name;

    public AbstractTrigger() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @NotBlank
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
