package com.sunecity.lilium.data.repository;

import java.io.Serializable;
import java.util.HashMap;

public class BaseNode<T extends BaseNode> implements Serializable {

    private String name;
    private String repositoryPath;
    private HashMap<String, NodeProperty> properties = new HashMap<>();
    private String creator;

    public String getRepositoryPath() {
        return repositoryPath;
    }

    public void setRepositoryPath(String repositoryPath) {
        this.repositoryPath = repositoryPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public NodeProperty getProperty(String key) {
        return properties.get(key);
    }

    public void addProperty(String key, NodeProperty value) {
        properties.put(key, value);
    }

}
