package com.sunecity.lilium.data.report;

import com.sunecity.lilium.data.report.chart.Chart;
import com.sunecity.lilium.data.report.enums.Paper;
import com.sunecity.lilium.data.report.enums.ReportDate;
import com.sunecity.lilium.data.report.enums.SubtotalPosition;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "DYNAMIC_REPORTS")
public final class DynamicReport extends Report {

    @Column(name = "TITLE")
    private String title;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_DYNAMIC_ID")
    private Style titleStyle;

    @Column(name = "SUB_TITLE")
    private String subtitle;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_DYNAMIC_ID")
    private Style subtitleStyle;

    @Column(name = "HEADER")
    private String header;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_DYNAMIC_ID")
    private Style headerStyle;

    @Column(name = "FOOTER")
    private String footer;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_DYNAMIC_ID")
    private Style footerStyle;

    @Column(name = "SUMMARY")
    private String summary;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_DYNAMIC_ID")
    private Style summaryStyle;

    @Column(name = "HIGHLIGHT_EVEN_ROWS")
    private boolean highlightEvenRows = true;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_DYNAMIC_ID")
    private Style pageNumberStyle;

    @Column(name = "REPORT_DATE")
    @Enumerated(EnumType.STRING)
    private ReportDate reportDate = ReportDate.DATE_TIME_TOP;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_DYNAMIC_ID")
    private Style reportDateStyle;

    @Column(name = "SUBTOTAL_POSITION")
    @Enumerated(EnumType.STRING)
    private SubtotalPosition subtotalPosition = SubtotalPosition.FOOTER;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_QUERY_ID")
    private Page page = Paper.A4.getPage();

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "DYNAMIC_REPORT_ID")
    private Logo logo;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "DYNAMIC_REPORT_ID")
    private List<Row> rows;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "DYNAMIC_REPORT_ID")
    private List<Chart> charts;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "DYNAMIC_REPORT_ID")
    private List<AbstractColumn> columns;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "DYNAMIC_REPORT_ID")
    private List<Report> subReports;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "REPORT_QUERY_ID")
    private ReportQuery reportQuery;

    @Transient
    private Collection<?> values;

    public DynamicReport() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Style getTitleStyle() {
        return titleStyle;
    }

    public void setTitleStyle(Style titleStyle) {
        this.titleStyle = titleStyle;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Style getSubtitleStyle() {
        return subtitleStyle;
    }

    public void setSubtitleStyle(Style subtitleStyle) {
        this.subtitleStyle = subtitleStyle;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Style getHeaderStyle() {
        return headerStyle;
    }

    public void setHeaderStyle(Style headerStyle) {
        this.headerStyle = headerStyle;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public Style getFooterStyle() {
        return footerStyle;
    }

    public void setFooterStyle(Style footerStyle) {
        this.footerStyle = footerStyle;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Style getSummaryStyle() {
        return summaryStyle;
    }

    public void setSummaryStyle(Style summaryStyle) {
        this.summaryStyle = summaryStyle;
    }

    public boolean isHighlightEvenRows() {
        return highlightEvenRows;
    }

    public void setHighlightEvenRows(boolean highlightEvenRows) {
        this.highlightEvenRows = highlightEvenRows;
    }

    public Style getPageNumberStyle() {
        return pageNumberStyle;
    }

    public void setPageNumberStyle(Style pageNumberStyle) {
        this.pageNumberStyle = pageNumberStyle;
    }

    public ReportDate getReportDate() {
        return reportDate;
    }

    public void setReportDate(ReportDate reportDate) {
        this.reportDate = reportDate;
    }

    public Style getReportDateStyle() {
        return reportDateStyle;
    }

    public void setReportDateStyle(Style reportDateStyle) {
        this.reportDateStyle = reportDateStyle;
    }

    public SubtotalPosition getSubtotalPosition() {
        return subtotalPosition;
    }

    public void setSubtotalPosition(SubtotalPosition subtotalPosition) {
        this.subtotalPosition = subtotalPosition;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Logo getLogo() {
        return logo;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public List<Chart> getCharts() {
        return charts;
    }

    public void setCharts(List<Chart> charts) {
        this.charts = charts;
    }

    public List<AbstractColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<AbstractColumn> columns) {
        this.columns = columns;
    }

    public List<Report> getSubReports() {
        return subReports;
    }

    public void setSubReports(List<Report> subReports) {
        this.subReports = subReports;
    }

    public ReportQuery getReportQuery() {
        return reportQuery;
    }

    public void setReportQuery(ReportQuery reportQuery) {
        this.reportQuery = reportQuery;
    }

    public Collection<?> getValues() {
        return values;
    }

    public void setValues(Collection<?> values) {
        this.values = values;
    }

    public void addColumn(AbstractColumn column) {
        if (getColumns() == null) {
            columns = new ArrayList<>();
        }
        columns.add(column);
    }

    public void addRow(Row row) {
        if (getRows() == null) {
            rows = new ArrayList<>();
        }
        rows.add(row);
    }

    public void addRow(String... values) {
        if (getRows() == null) {
            rows = new ArrayList<>();
        }
        Row row = new Row();
        for (String s : values) {
            row.addColumn(s);
        }
        rows.add(row);
    }

    public void addSubReport(Report subReport) {
        if (getSubReports() == null) {
            subReports = new ArrayList<>();
        }
        subReports.add(subReport);
    }
}