package com.sunecity.lilium.data.cache;

import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.data.criteria.SearchMetaData;

import javax.cache.annotation.CacheInvocationParameter;
import javax.cache.annotation.CacheKeyGenerator;
import javax.cache.annotation.CacheKeyInvocationContext;
import javax.cache.annotation.GeneratedCacheKey;
import java.lang.annotation.Annotation;

/**
 * Created by shahram on 3/15/17.
 */
public class EntityCacheKeyGenerator implements CacheKeyGenerator {

    @Override
    public GeneratedCacheKey generateCacheKey(CacheKeyInvocationContext<? extends Annotation> cacheKeyInvocationContext) {
        CacheInvocationParameter[] allParameters = cacheKeyInvocationContext.getAllParameters();
        for (CacheInvocationParameter parameter : allParameters) {
            if (BaseIdentity.class.isAssignableFrom(parameter.getRawType())) {
                BaseIdentity<?> baseIdentity = (BaseIdentity<?>) parameter.getValue();
                return new EntityGeneratedCacheKey(baseIdentity.getId());
            } else if (SearchMetaData.class.isAssignableFrom(parameter.getRawType())) {
                SearchMetaData<?> searchMetaData = (SearchMetaData<?>) parameter.getValue();

            }
        }
        return new EntityGeneratedCacheKey(allParameters[0].getValue());
    }

}