package com.sunecity.lilium.data.criteria;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
public interface ResultList<T extends Serializable> extends Serializable {

    @GET
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
//    @Paging
//    @VersionTag
    List<T> search(@QueryParam("metaData") SearchMetaData<T> metaData,
                   @QueryParam("first") @DefaultValue("1") @Min(1) int first,
                   @QueryParam("max") @DefaultValue("10") @Min(1) @Max(1000) int max) throws Exception;

    @GET
    @Path("/search/count")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    long searchCount(@QueryParam("metaData") SearchMetaData<T> metaData) throws Exception;

}
