package com.sunecity.lilium.data.schedule;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "NOTIFICATION_DAYS")
@NamedQueries({
        @NamedQuery(name = "selectAllNotificationDaysByCount", query = "SELECT COUNT(n) FROM NotificationDay n"),
        @NamedQuery(name = "selectAllNotificationDays", query = "SELECT n FROM NotificationDay n")})
public class NotificationDay extends NotificationTime {

    private static final long serialVersionUID = -5578321043286500424L;

    public NotificationDay() {
    }

}
