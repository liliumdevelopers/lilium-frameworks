package com.sunecity.lilium.data.sysinfo;

public class Cpu {

	private String vendor;
	private String model;
	private int mhz;
	private long cacheSize;
	private int totalCpus;
	private int physicalCpus;
	private int coresPerCpus;

	public Cpu() {
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getMhz() {
		return mhz;
	}

	public void setMhz(int mhz) {
		this.mhz = mhz;
	}

	public long getCacheSize() {
		return cacheSize;
	}

	public void setCacheSize(long cacheSize) {
		this.cacheSize = cacheSize;
	}

	public int getTotalCpus() {
		return totalCpus;
	}

	public void setTotalCpus(int totalCpus) {
		this.totalCpus = totalCpus;
	}

	public int getPhysicalCpus() {
		return physicalCpus;
	}

	public void setPhysicalCpus(int physicalCpus) {
		this.physicalCpus = physicalCpus;
	}

	public int getCoresPerCpus() {
		return coresPerCpus;
	}

	public void setCoresPerCpus(int coresPerCpus) {
		this.coresPerCpus = coresPerCpus;
	}

}
