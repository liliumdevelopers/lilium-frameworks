package com.sunecity.lilium.data.field;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.EnumPath;
import com.sunecity.lilium.core.model.Search;
import com.sunecity.lilium.core.util.Pair;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <b>Title:</b> EnumField
 * Represent enums in data model.
 *
 * @author Shahram Goodarzi
 * @version 1.1.0
 */
public class EnumField extends Field implements SimpleField {

    private Enum<?> value;
    private List<Pair<String, Object>> values;
    private List<Pair<String, Object>> selectedValues;

    public EnumField(String name, String displayName, List<Pair<String, Object>> values) {
        super(name, displayName);
        this.values = values;
    }

    public EnumField(Path<?> path, String displayName, List<Pair<String, Object>> values) {
        super(path, displayName);
        this.values = values;
    }

    public Enum<?> getValue() {
        return value;
    }

    public void setValue(Enum<?> value) {
        this.value = value;
    }

    public List<Pair<String, Object>> getValues() {
        return values;
    }

    public void setValues(List<Pair<String, Object>> values) {
        this.values = values;
    }

    public List<Pair<String, Object>> getSelectedValues() {
        return selectedValues;
    }

    public void setSelectedValues(List<Pair<String, Object>> selectedValues) {
        this.selectedValues = selectedValues;
    }

    @Override
    public Constraint[] getConstraints() {
        Constraint[] constraints = new Constraint[4];
        constraints[0] = Constraint.IN;
        constraints[1] = Constraint.NOT_IN;
        constraints[2] = Constraint.IS_NULL;
        constraints[3] = Constraint.IS_NOT_NULL;
        return constraints;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.ENUM;
    }

    @Override
    public Predicate toPredicate() {
        EnumPath path = (EnumPath) getPath();
        switch (getConstraint()) {
            case EQUALS:
                return path.eq(value);
            case NOT_EQUALS:
                return path.ne(value);
            case IS_NULL:
                return path.isNull();
            case IS_NOT_NULL:
                return path.isNotNull();
            case IN:
                return path.in(selectedValues);
            case NOT_IN:
                return path.notIn(selectedValues);
            default:
                return null;
        }
    }

    @Override
    public Search getSearch() {
        String name = getDisplayName();
        String condition = getConstraint().toString();
        Object value = null;
        switch (getConstraint()) {
            case IN:
            case NOT_IN:
                value = getSelectedValues().stream().map(Pair::getKey).collect(Collectors.toList());
                break;
            case IS_NULL:
            case IS_NOT_NULL:
            default:
                break;
        }
        return new Search(name, condition, value);
    }

    @Override
    public Class<?> getTypeClass() {
        return Enum.class;
    }

    @Override
    public void clear() {
        value = null;
        values = null;
        selectedValues = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        EnumField enumField = (EnumField) o;

        if (value != null ? !value.equals(enumField.value) : enumField.value != null) return false;
        if (values != null ? !values.equals(enumField.values) : enumField.values != null) return false;
        return !(selectedValues != null ? !selectedValues.equals(enumField.selectedValues) : enumField.selectedValues != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (values != null ? values.hashCode() : 0);
        result = 31 * result + (selectedValues != null ? selectedValues.hashCode() : 0);
        return result;
    }

}
