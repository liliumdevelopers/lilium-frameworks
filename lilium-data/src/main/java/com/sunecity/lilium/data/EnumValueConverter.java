package com.sunecity.lilium.data;

import javax.persistence.AttributeConverter;
import java.io.Serializable;
import java.util.EnumSet;

/**
 * Converter helper for enums based on EnumCode.
 *
 * @author Shahram Goodarzi
 */
public abstract class EnumValueConverter<E extends Enum<E> & EnumValue<T>, T extends Serializable> implements AttributeConverter<E, T> {

    private final Class<E> enumClass;

    public EnumValueConverter(Class<E> enumClass) {
        this.enumClass = enumClass;
    }

    /**
     * @param enumValue from enum
     * @return database type
     */
    @Override
    public T convertToDatabaseColumn(E enumValue) {
        return enumValue != null ? enumValue.value() : null;
    }

    /**
     * @param value from database
     * @return entity type
     */
    @Override
    public E convertToEntityAttribute(T value) {
        return EnumSet.allOf(enumClass).stream().filter(e -> e.value().equals(value)).findAny().orElse(null);
    }

}
