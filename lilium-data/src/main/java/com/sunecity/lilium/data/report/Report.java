package com.sunecity.lilium.data.report;

import com.sunecity.lilium.core.model.BaseEntitySimple;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

@Entity
@MappedSuperclass
public abstract class Report extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ReportSeq")
    @SequenceGenerator(name = "ReportSeq", sequenceName = "REPORT_SEQ")
    @Column(name = "REPORT_ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
