package com.sunecity.lilium.data.field;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.Visitor;

import javax.annotation.Nullable;
import javax.el.MethodExpression;
import java.lang.reflect.AnnotatedElement;

/**
 * Created by shahram on 5/1/15.
 */
public class ExpressionPath<T> implements Path<T> {

    private Path<T> path;

    public ExpressionPath(Path<T> path, MethodExpression expression) {
        this.path = path;
    }

    @Override
    public PathMetadata getMetadata() {
        return path.getMetadata();
    }

    @Override
    public Path<?> getRoot() {
        return path.getRoot();
    }

    @Override
    public AnnotatedElement getAnnotatedElement() {
        return path.getAnnotatedElement();
    }

    @Nullable
    @Override
    public <R, C> R accept(Visitor<R, C> v, C context) {
        return path.accept(v, context);
    }

    @Override
    public Class<? extends T> getType() {
        return path.getType();
    }

}
