package com.sunecity.lilium.data.schedule;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

//@Entity
@Table(name = "GROUPS")
@NamedQueries({
        @NamedQuery(name = "selectAllGroupsCount", query = "SELECT COUNT(g) FROM TriggerGroup g"),
        @NamedQuery(name = "selectAllGroups", query = "SELECT g FROM TriggerGroup g")})
public class TriggerGroup extends AbstractTrigger {

    private static final long serialVersionUID = 8305542073299308892L;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUP_ID")
    private Set<Trigger> triggers;

    public TriggerGroup() {
    }

    public Set<Trigger> getTriggers() {
        return triggers;
    }

    public void setTriggers(Set<Trigger> triggers) {
        this.triggers = triggers;
    }

}
