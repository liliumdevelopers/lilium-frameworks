package com.sunecity.lilium.data.report.chart;

import com.sunecity.lilium.data.report.ReportColumn;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

@Entity
@Table(name = "PIE_CHARTS")
public class PieChart extends Chart {

    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "COLUMN_SERIE_ID")
    @OrderColumn
    private ReportColumn serie;

    @Column(name = "THREE_DIMENSION")
    private boolean threeDimension;

    public PieChart() {
    }

    public ReportColumn getSerie() {
        return this.serie;
    }

    public void setSerie(ReportColumn serie) {
        this.serie = serie;
    }

    public boolean isThreeDimension() {
        return this.threeDimension;
    }

    public void setThreeDimension(boolean threeDimension) {
        this.threeDimension = threeDimension;
    }

}
