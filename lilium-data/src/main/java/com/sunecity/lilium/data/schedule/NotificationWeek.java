package com.sunecity.lilium.data.schedule;

import com.sunecity.lilium.data.schedule.enums.DayOfWeek;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "NOTIFICATION_WEEKS")
@NamedQueries({
        @NamedQuery(name = "selectAllNotificationWeeksByCount", query = "SELECT COUNT(n) FROM NotificationWeek n"),
        @NamedQuery(name = "selectAllNotificationWeeks", query = "SELECT n FROM NotificationWeek n")})
public class NotificationWeek extends NotificationTime {

    private static final long serialVersionUID = -2187267251806164514L;

    @Enumerated(EnumType.STRING)
    @Column(name = "DAY_OF_WEEK")
    private DayOfWeek dayOfWeek;

    public NotificationWeek() {
    }

    @NotNull
    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

}
