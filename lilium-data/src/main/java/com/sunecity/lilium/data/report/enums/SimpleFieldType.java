package com.sunecity.lilium.data.report.enums;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * Created by shahram on 6/2/15.
 */
public enum SimpleFieldType {

    BYTE(Byte.class),
    SHORT(Short.class),
    INTEGER(Integer.class),
    LONG(Long.class),
    FLOAT(Float.class),
    DOUBLE(Double.class),
    CHARACTER(Character.class),
    STRING(String.class),
    BIGDECIMAL(BigDecimal.class),
    LIST(List.class),
    SET(Set.class),
    PERCENT(Integer.class),
    ENUM(Enum.class),
    ROW_NUMBER;

    private Class<?> typeClass;

    SimpleFieldType() {
    }

    SimpleFieldType(Class<?> typeClass) {
        this.typeClass = typeClass;
    }

    public Class<?> getTypeClass() {
        return this.typeClass;
    }

}
