package com.sunecity.lilium.data.criteria;

/**
 * Created by shahram on 10/16/16.
 */
public enum SortType {

    ASC, DESC

}
