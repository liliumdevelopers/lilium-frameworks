package com.sunecity.lilium.data.report.enums;

import com.sunecity.lilium.core.localization.Bundles;

/**
 * Created by shahram on 6/2/15.
 */
public enum ReportDate {

    DATE_TIME_TOP,
    DATE_TOP,
    TIME_TOP,
    DATE_TIME_BOTTOM,
    DATE_BOTTOM,
    TIME_BOTTOM;

    public String toString() {
        return Bundles.getValueLocale(getClass().getName().substring(0, getClass().getName().lastIndexOf("data")) + "data.model",
                getClass().getSimpleName() + "." + name());
    }

}
