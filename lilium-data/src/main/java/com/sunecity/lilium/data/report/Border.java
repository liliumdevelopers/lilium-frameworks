package com.sunecity.lilium.data.report;

import com.sunecity.lilium.core.model.BaseEntitySimple;
import com.sunecity.lilium.data.report.enums.BorderType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BORDERS")
public class Border extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BorderSeq")
    @SequenceGenerator(name = "BorderSeq", sequenceName = "BORDER_SEQ")
    @Column(name = "BORDER_ID")
    private Long id;

    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private BorderType type;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "BORDER_ID")
    private Color color;

    @Column(name = "WIDTH")
    private Float width;

    public Border() {
    }

    public Border(BorderType type, Float width) {
        this.type = type;
        this.width = width;
    }

    public Border(BorderType type, Color color, Float width) {
        this.type = type;
        this.color = color;
        this.width = width;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public BorderType getType() {
        return this.type;
    }

    public void setType(BorderType type) {
        this.type = type;
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Float getWidth() {
        return this.width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

}
