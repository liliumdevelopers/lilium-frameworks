package com.sunecity.lilium.data.field;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanPath;
import com.sunecity.lilium.core.model.Search;

public class BooleanField extends Field implements SimpleField {

    private final String trueName;
    private final String falseName;
    private final String allName;
    private Boolean value;

    public BooleanField(String name, String displayName, String trueName, String falseName, String allName) {
        super(name, displayName);
        this.trueName = trueName;
        this.falseName = falseName;
        this.allName = allName;
    }

    public BooleanField(Path<?> path, String displayName, String trueName, String falseName, String allName) {
        super(path, displayName);
        this.trueName = trueName;
        this.falseName = falseName;
        this.allName = allName;
    }

    public String getTrueName() {
        return trueName;
    }

    public String getFalseName() {
        return falseName;
    }

    public String getAllName() {
        return allName;
    }

    public Boolean getValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

    @Override
    public Constraint[] getConstraints() {
        Constraint[] constraints = new Constraint[3];
        constraints[0] = Constraint.EQUALS;
        constraints[1] = Constraint.IS_NULL;
        constraints[2] = Constraint.IS_NOT_NULL;
        return constraints;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.BOOLEAN;
    }

    @Override
    public Predicate toPredicate() {
        BooleanPath path = (BooleanPath) getPath();
        switch (getConstraint()) {
            case EQUALS:
                return path.eq(value);
            case IS_NULL:
                return path.isNull();
            case IS_NOT_NULL:
                return path.isNotNull();
            default:
                return null;
        }
    }

    @Override
    public Search getSearch() {
        String name = getDisplayName();
        String condition = getConstraint().toString();
        Object value = null;
        if (getValue() == null) {
            if (getConstraint() == Constraint.EQUALS) {
                value = getAllName();
            }
        } else {
            value = getValue() ? getTrueName() : getFalseName();
        }
        return new Search(name, condition, value);
    }

    @Override
    public Class<?> getTypeClass() {
        return Boolean.class;
    }

    @Override
    public void clear() {
        value = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BooleanField that = (BooleanField) o;

        if (trueName != null ? !trueName.equals(that.trueName) : that.trueName != null) return false;
        if (falseName != null ? !falseName.equals(that.falseName) : that.falseName != null) return false;
        if (allName != null ? !allName.equals(that.allName) : that.allName != null) return false;
        return !(value != null ? !value.equals(that.value) : that.value != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (trueName != null ? trueName.hashCode() : 0);
        result = 31 * result + (falseName != null ? falseName.hashCode() : 0);
        result = 31 * result + (allName != null ? allName.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

}
