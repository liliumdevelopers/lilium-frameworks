package com.sunecity.lilium.data.repository;

public class FileNode extends BaseNode {

    private long Size;
    private String mimeType;
    private byte[] content;
    private Object data;

    public long getSize() {
        return Size;
    }

    public void setSize(long Size) {
        this.Size = Size;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
