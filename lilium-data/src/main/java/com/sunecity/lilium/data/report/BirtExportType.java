package com.sunecity.lilium.data.report;

/**
 * @author Shahram Goodarzi
 */
public enum BirtExportType {

    PDF("pdf", "application/pdf"),
    XLSX("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
    DOCX("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
    PPTX("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"),
    XLS("xls", "application/vnd.ms-excel"),
    DOC("doc", "application/msword"),
    PPT("ppt", "application/vnd.ms-powerpoint"),
    ODT("odt", "application/vnd.oasis.opendocument.text"),
    ODS("ods", "application/vnd.oasis.opendocument.spreadsheet"),
    ODP("odp", "application/vnd.oasis.opendocument.presentation"),
    HTML("html", "text/html");

    private final String extension;
    private final String mimetype;

    BirtExportType(String extension, String mimetype) {
        this.extension = extension;
        this.mimetype = mimetype;
    }

    public String getExtension() {
        return extension;
    }

    public String getMimetype() {
        return mimetype;
    }

}
