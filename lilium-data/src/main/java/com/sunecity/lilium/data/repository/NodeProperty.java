package com.sunecity.lilium.data.repository;

public class NodeProperty {

    private String name;
    private String value;
    private String[] values;
    private boolean isMultiple = false;
    private int type;

    public NodeProperty() {
    }

    public NodeProperty(String name, String value, boolean isMultiple, int type) {
        this.name = name;
        this.value = value;
        this.isMultiple = isMultiple;
        this.type = type;
    }

    public NodeProperty(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public NodeProperty(String name, String values[], boolean isMultiple) {
        this.name = name;
        this.values = values;
        this.isMultiple = isMultiple;
    }

    public NodeProperty(String name, String[] values) {
        this.name = name;
        this.values = values;
    }

    public boolean isMultiple() {
        return isMultiple;
    }

    public void setMultiple(boolean isMultiple) {
        this.isMultiple = isMultiple;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

}
