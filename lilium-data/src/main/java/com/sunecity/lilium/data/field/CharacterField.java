package com.sunecity.lilium.data.field;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.ComparablePath;
import com.sunecity.lilium.core.localization.Bundles;
import com.sunecity.lilium.core.model.Search;

import java.util.List;

public class CharacterField extends Field implements SimpleField {

    private Character value;
    private Character value2;
    private Character addValue;
    private List<Character> values;
    private List<Character> selectedValues;

    public CharacterField(String name, String displayName) {
        super(name, displayName);
    }

    public CharacterField(Path<?> path, String displayName) {
        super(path, displayName);
    }

    public Character getValue() {
        return value;
    }

    public void setValue(Character value) {
        this.value = value;
    }

    public Character getValue2() {
        return value2;
    }

    public void setValue2(Character value2) {
        this.value2 = value2;
    }

    public Character getAddValue() {
        return addValue;
    }

    public void setAddValue(Character addValue) {
        this.addValue = addValue;
    }

    public List<Character> getValues() {
        return values;
    }

    public void setValues(List<Character> values) {
        this.values = values;
    }

    public List<Character> getSelectedValues() {
        return selectedValues;
    }

    public void setSelectedValues(List<Character> selectedValues) {
        this.selectedValues = selectedValues;
    }

    @Override
    public Constraint[] getConstraints() {
        Constraint[] constraints = new Constraint[12];
        constraints[0] = Constraint.EQUALS;
        constraints[1] = Constraint.NOT_EQUALS;
        constraints[2] = Constraint.IS_NULL;
        constraints[3] = Constraint.IS_NOT_NULL;
        constraints[4] = Constraint.GREATER_THAN;
        constraints[5] = Constraint.GREATER_THAN_OR_EQUALS;
        constraints[6] = Constraint.LESS_THAN;
        constraints[7] = Constraint.LESS_THAN_OR_EQUALS;
        constraints[8] = Constraint.BETWEEN;
        constraints[9] = Constraint.NOT_BETWEEN;
        constraints[10] = Constraint.IN;
        constraints[11] = Constraint.NOT_IN;
        return constraints;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.STRING;
    }

    @Override
    public Predicate toPredicate() {
        ComparablePath<Character> path = (ComparablePath<Character>) getPath();
        switch (getConstraint()) {
            case EQUALS:
                return path.eq(value);
            case NOT_EQUALS:
                return path.ne(value);
            case IS_NULL:
                return path.isNull();
            case IS_NOT_NULL:
                return path.isNotNull();
            case GREATER_THAN:
                return path.gt(value);
            case GREATER_THAN_OR_EQUALS:
                return path.goe(value);
            case LESS_THAN:
                return path.lt(value);
            case LESS_THAN_OR_EQUALS:
                return path.loe(value);
            case BETWEEN:
                return path.between(value, value2);
            case NOT_BETWEEN:
                return path.notBetween(value, value2);
            case IN:
                return path.in(selectedValues);
            case NOT_IN:
                return path.notIn(selectedValues);
            default:
                return null;
        }
    }

    @Override
    public Search getSearch() {
        String name = getDisplayName();
        String condition = getConstraint().toString();
        Object value = null;
        switch (getConstraint()) {
            case EQUALS:
            case NOT_EQUALS:
            case GREATER_THAN:
            case GREATER_THAN_OR_EQUALS:
            case LESS_THAN:
            case LESS_THAN_OR_EQUALS:
                value = getValue();
                break;
            case BETWEEN:
            case NOT_BETWEEN:
                value = getValue() + Bundles.getValueDefault("search.and") + getValue2();
                break;
            case IN:
            case NOT_IN:
                value = getSelectedValues();
                break;
            case IS_NULL:
            case IS_NOT_NULL:
            default:
                break;
        }
        return new Search(name, condition, value);
    }

    @Override
    public Class<?> getTypeClass() {
        return Character.class;
    }

    @Override
    public void clear() {
        value = null;
        value2 = null;
        addValue = null;
        values = null;
        selectedValues = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CharacterField that = (CharacterField) o;

        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (value2 != null ? !value2.equals(that.value2) : that.value2 != null) return false;
        if (addValue != null ? !addValue.equals(that.addValue) : that.addValue != null) return false;
        if (values != null ? !values.equals(that.values) : that.values != null) return false;
        return !(selectedValues != null ? !selectedValues.equals(that.selectedValues) : that.selectedValues != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (value2 != null ? value2.hashCode() : 0);
        result = 31 * result + (addValue != null ? addValue.hashCode() : 0);
        result = 31 * result + (values != null ? values.hashCode() : 0);
        result = 31 * result + (selectedValues != null ? selectedValues.hashCode() : 0);
        return result;
    }

}
