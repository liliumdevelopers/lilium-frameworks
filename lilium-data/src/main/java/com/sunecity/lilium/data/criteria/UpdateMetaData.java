package com.sunecity.lilium.data.criteria;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.Expressions;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Shahram Goodarzi
 */
public class UpdateMetaData implements Serializable {

    private final EntityPath<?> entityPath;
    private final Predicate[] predicates;
    private final Map<Path<?>, Expression<?>> updates;

    public UpdateMetaData(EntityPath<?> entityPath, Predicate... predicates) {
        this.entityPath = entityPath;
        this.predicates = predicates;
        this.updates = new LinkedHashMap<>();
    }

    public EntityPath<?> getEntityPath() {
        return entityPath;
    }

    public Predicate[] getPredicates() {
        return predicates;
    }

    public Map<Path<?>, Expression<?>> getUpdates() {
        return updates;
    }

    public void set(Path<?> path, Object value) {
        if (value != null) {
            this.updates.put(path, Expressions.constant(value));
        } else {
            this.setNull(path);
        }
    }

    public void set(Path<?> path, Expression<?> expression) {
        if (expression != null) {
            this.updates.put(path, expression);
        } else {
            this.setNull(path);
        }
    }

    public void set(List<? extends Path<?>> paths, List<?> values) {
        for (int i = 0; i < paths.size(); ++i) {
            if (values.get(i) != null) {
                this.updates.put(paths.get(i), Expressions.constant(values.get(i)));
            } else {
                this.updates.put(paths.get(i), Expressions.nullExpression((Path<?>) paths.get(i)));
            }
        }
    }

    public void setNull(Path<?> path) {
        this.updates.put(path, Expressions.nullExpression(path));
    }

}
