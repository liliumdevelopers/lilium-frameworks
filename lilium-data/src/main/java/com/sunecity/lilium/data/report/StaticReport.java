package com.sunecity.lilium.data.report;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "STATIC_REPORTS")
public final class StaticReport extends Report {

    @Column(name = "FILE")
    @Lob
    private byte[] file;

    @Column(name = "COMPILED")
    private boolean compiled = true;

    @OneToMany
    private Set<StaticReport> subReports;

    public StaticReport(String name, byte[] file) {
        super.setName(name);
        this.file = file;
    }

    public StaticReport(String name, byte[] file, boolean compiled) {
        super.setName(name);
        this.file = file;
        this.compiled = compiled;
    }

    public StaticReport(String name, InputStream file) throws IOException {
        super.setName(name);
        byte[] bs = new byte[file.available()];
        file.read(bs);
        file.close();
        this.file = bs;
    }

    public StaticReport(String name, InputStream file, boolean compiled) throws IOException {
        super.setName(name);
        byte[] bs = new byte[file.available()];
        file.read(bs);
        file.close();
        this.file = bs;
        this.compiled = compiled;
    }

    public StaticReport(String name, byte[] file, boolean compiled, StaticReport... subReports) {
        super.setName(name);
        this.file = file;
        this.compiled = compiled;
        this.subReports = Arrays.stream(subReports).collect(Collectors.toSet());
    }

    public StaticReport(String name, InputStream file, boolean compiled, StaticReport... subReports) throws IOException {
        super.setName(name);
        byte[] bs = new byte[file.available()];
        file.read(bs);
        file.close();
        this.file = bs;
        this.compiled = compiled;
        this.subReports = Arrays.stream(subReports).collect(Collectors.toSet());
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public boolean isCompiled() {
        return compiled;
    }

    public void setCompiled(boolean compiled) {
        this.compiled = compiled;
    }

    public Set<StaticReport> getSubReports() {
        return subReports;
    }

    public void setSubReports(Set<StaticReport> subReports) {
        this.subReports = subReports;
    }

    public void addSubReport(StaticReport subReport) {
        if (subReports == null) {
            subReports = new HashSet<>();
        }
        subReports.add(subReport);
    }

}
