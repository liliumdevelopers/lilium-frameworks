package com.sunecity.lilium.data.report;

import com.sunecity.lilium.core.model.BaseEntitySimple;
import com.sunecity.lilium.data.report.enums.HorizontalAlign;
import com.sunecity.lilium.data.report.enums.LineSpacing;
import com.sunecity.lilium.data.report.enums.Rotation;
import com.sunecity.lilium.data.report.enums.VerticalAlign;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "STYLES")
public class Style extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "StyleSeq")
    @SequenceGenerator(name = "StyleSeq", sequenceName = "STYLE_SEQ")
    @Column(name = "STYLE_ID")
    private Long id;

    @Column(name = "FONT_NAME")
    private String fontName;

    @Column(name = "FONT_SIZE")
    private Integer fontSize;

    @Column(name = "FONT_COLOR")
    @JoinColumn(name = "fontColor_ID")
    private Color fontColor;

    @Column(name = "BG_COLOR")
    @JoinColumn(name = "bgColor_ID")
    private Color bgColor;

    @Column(name = "HORIZONTAL_ALIGN")
    @Enumerated(EnumType.STRING)
    private HorizontalAlign horizontalAlign;

    @Column(name = "VERTICAL_ALIGN")
    @Enumerated(EnumType.STRING)
    private VerticalAlign verticalAlign;

    @Column(name = "BOLD")
    private Boolean bold;

    @Column(name = "ITALIC")
    private Boolean italic;

    @Column(name = "UNDERLINE")
    private Boolean underline;

    @Column(name = "STRIKE_THROUGH")
    private Boolean strikeThrough;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "borderRight_ID")
    private Border borderRight;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "borderLeft_ID")
    private Border borderLeft;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "borderTop_ID")
    private Border borderTop;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "borderBottom_ID")
    private Border borderBottom;

    @Column(name = "PADDIN_RIGHT")
    private Integer paddingRight;

    @Column(name = "PADDING_LEFT")
    private Integer paddingLeft;

    @Column(name = "PADDING_TOP")
    private Integer paddingTop;

    @Column(name = "PADDING_BOTTOM")
    private Integer paddingBottom;

    @Column(name = "INDENT_RIGHT")
    private Integer indentRight;

    @Column(name = "INDENT_LEFT")
    private Integer indentLeft;

    @Column(name = "ROTATION")
    @Enumerated(EnumType.STRING)
    private Rotation rotation;

    @Column(name = "LINE_SPACING")
    @Enumerated(EnumType.STRING)
    private LineSpacing lineSpacing;

    public Style() {
        this.horizontalAlign = HorizontalAlign.RIGHT;
        this.verticalAlign = VerticalAlign.MIDDLE;
        this.bold = false;
        this.italic = false;
        this.underline = false;
        this.strikeThrough = false;
        this.paddingRight = 2;
        this.paddingLeft = 2;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getFontName() {
        return this.fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public Integer getFontSize() {
        return this.fontSize;
    }

    public void setFontSize(Integer fontSize) {
        this.fontSize = fontSize;
    }

    public Color getFontColor() {
        return this.fontColor;
    }

    public void setFontColor(Color fontColor) {
        this.fontColor = fontColor;
    }

    public Color getBgColor() {
        return this.bgColor;
    }

    public void setBgColor(Color bgColor) {
        this.bgColor = bgColor;
    }

    public HorizontalAlign getHorizontalAlign() {
        return this.horizontalAlign;
    }

    public void setHorizontalAlign(HorizontalAlign horizontalAlign) {
        this.horizontalAlign = horizontalAlign;
    }

    public VerticalAlign getVerticalAlign() {
        return this.verticalAlign;
    }

    public void setVerticalAlign(VerticalAlign verticalAlign) {
        this.verticalAlign = verticalAlign;
    }

    public Boolean getBold() {
        return bold;
    }

    public void setBold(Boolean bold) {
        this.bold = bold;
    }

    public Boolean getItalic() {
        return italic;
    }

    public void setItalic(Boolean italic) {
        this.italic = italic;
    }

    public Boolean getUnderline() {
        return underline;
    }

    public void setUnderline(Boolean underline) {
        this.underline = underline;
    }

    public Boolean getStrikeThrough() {
        return strikeThrough;
    }

    public void setStrikeThrough(Boolean strikeThrough) {
        this.strikeThrough = strikeThrough;
    }

    public Border getBorderRight() {
        return this.borderRight;
    }

    public void setBorderRight(Border borderRight) {
        this.borderRight = borderRight;
    }

    public Border getBorderLeft() {
        return this.borderLeft;
    }

    public void setBorderLeft(Border borderLeft) {
        this.borderLeft = borderLeft;
    }

    public Border getBorderTop() {
        return this.borderTop;
    }

    public void setBorderTop(Border borderTop) {
        this.borderTop = borderTop;
    }

    public Border getBorderBottom() {
        return this.borderBottom;
    }

    public void setBorderBottom(Border borderBottom) {
        this.borderBottom = borderBottom;
    }

    public Integer getPaddingRight() {
        return this.paddingRight;
    }

    public void setPaddingRight(Integer paddingRight) {
        this.paddingRight = paddingRight;
    }

    public Integer getPaddingLeft() {
        return this.paddingLeft;
    }

    public void setPaddingLeft(Integer paddingLeft) {
        this.paddingLeft = paddingLeft;
    }

    public Integer getPaddingTop() {
        return this.paddingTop;
    }

    public void setPaddingTop(Integer paddingTop) {
        this.paddingTop = paddingTop;
    }

    public Integer getPaddingBottom() {
        return this.paddingBottom;
    }

    public void setPaddingBottom(Integer paddingBottom) {
        this.paddingBottom = paddingBottom;
    }

    public Integer getIndentRight() {
        return this.indentRight;
    }

    public void setIndentRight(Integer indentRight) {
        this.indentRight = indentRight;
    }

    public Integer getIndentLeft() {
        return this.indentLeft;
    }

    public void setIndentLeft(Integer indentLeft) {
        this.indentLeft = indentLeft;
    }

    public Rotation getRotation() {
        return this.rotation;
    }

    public void setRotation(Rotation rotation) {
        this.rotation = rotation;
    }

    public LineSpacing getLineSpacing() {
        return this.lineSpacing;
    }

    public void setLineSpacing(LineSpacing lineSpacing) {
        this.lineSpacing = lineSpacing;
    }

}
