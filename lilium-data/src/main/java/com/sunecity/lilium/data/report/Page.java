package com.sunecity.lilium.data.report;

import com.sunecity.lilium.core.model.BaseEntitySimple;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PAGES")
public class Page extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PageSeq")
    @SequenceGenerator(name = "PageSeq", sequenceName = "PAGE_SEQ")
    @Column(name = "PAGE_ID")
    private Long id;

    private int width;

    private int height;

    private boolean portrait = true;

    private Integer marginTop;

    private Integer marginLeft;

    private Integer marginRight;

    private Integer marginBottom;

    private boolean pageNumber = true;

    public Page() {
    }

    public Page(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isPortrait() {
        return portrait;
    }

    public void setPortrait(boolean portrait) {
        this.portrait = portrait;
    }

    public Integer getMarginTop() {
        return marginTop;
    }

    public void setMarginTop(Integer marginTop) {
        this.marginTop = marginTop;
    }

    public Integer getMarginLeft() {
        return marginLeft;
    }

    public void setMarginLeft(Integer marginLeft) {
        this.marginLeft = marginLeft;
    }

    public Integer getMarginRight() {
        return marginRight;
    }

    public void setMarginRight(Integer marginRight) {
        this.marginRight = marginRight;
    }

    public Integer getMarginBottom() {
        return marginBottom;
    }

    public void setMarginBottom(Integer marginBottom) {
        this.marginBottom = marginBottom;
    }

    public boolean isPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(boolean pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setMargins(Integer marginTop, Integer marginLeft, Integer marginRight, Integer marginBottom) {
        this.marginTop = marginTop;
        this.marginLeft = marginLeft;
        this.marginRight = marginRight;
        this.marginBottom = marginBottom;
    }

}
