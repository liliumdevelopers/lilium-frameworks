package com.sunecity.lilium.data.criteria;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Predicate;

import java.io.Serializable;

/**
 * @author Shahram Goodarzi
 */
public class DeleteMetaData implements Serializable {

    private final EntityPath<?> entityPath;
    private final Predicate[] predicates;

    public DeleteMetaData(EntityPath<?> entityPath, Predicate... predicates) {
        this.entityPath = entityPath;
        this.predicates = predicates;
    }

    public EntityPath<?> getEntityPath() {
        return entityPath;
    }

    public Predicate[] getPredicates() {
        return predicates;
    }

}
