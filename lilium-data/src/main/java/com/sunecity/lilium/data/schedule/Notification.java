package com.sunecity.lilium.data.schedule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "NOTIFICATIONS")
public class Notification {

    private static final long serialVersionUID = 2301459742130199289L;

    @Column(name = "TITLE")
    private String title;

    public Notification() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
