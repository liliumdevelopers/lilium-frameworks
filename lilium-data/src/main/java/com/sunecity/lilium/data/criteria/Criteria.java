package com.sunecity.lilium.data.criteria;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Expression;
import com.querydsl.jpa.JPQLTemplates;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * @author Shahram Goodarzi
 */
public class Criteria<T> extends JPAQuery<T> {

    private final CriteriaMetaData metaData;

    public Criteria(EntityPath<?>... entityPaths) {
        from(entityPaths);
        metaData = new CriteriaMetaData();
    }

    public Criteria(CriteriaMetaData metadata) {
        super(null, JPQLTemplates.DEFAULT, metadata.getMetadata());
        metaData = metadata;
    }

    public CriteriaMetaData getMetaData() {
        metaData.setMetadata(super.getMetadata());
        return metaData;
    }

    public Expression<?> getResult() {
        return metaData.getResult();
    }

    public void setResult(Expression<?> result) {
        metaData.setResult(result);
    }

    public String getGraph() {
        return metaData.getGraph();
    }

    public void setGraph(String graph) {
        metaData.setGraph(graph);
    }

    public Integer getFirst() {
        return metaData.getFirst();
    }

    public void setFirst(Integer first) {
        metaData.setFirst(first);
    }

    public Integer getMax() {
        return metaData.getMax();
    }

    public void setMax(Integer max) {
        metaData.setMax(max);
    }

    public void clearOrderBy() {
        getMetadata().clearOrderBy();
    }

    public void clearWhere() {
        getMetadata().clearWhere();
    }

    public String getJpqlQuery() {
        return serialize(false).toString();
    }

}
