package com.sunecity.lilium.data.cache;

import com.sunecity.lilium.core.model.BaseEntity;

import javax.cache.Cache;
import java.io.Serializable;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by shahram on 3/16/17.
 */
public class GeneralCache {

    private Cache<Serializable, CachedEntity> cache;

    public void put(BaseEntity<?> entity) {
        cache.put(entity.getId(), new CachedEntity(entity.getVersion(), entity.getUpdateTimestamp() != null ? entity.getUpdateTimestamp() : entity.getCreateTimestamp()));
    }

    public void put(Collection<BaseEntity<?>> entities) {
        cache.putAll(entities.stream().collect(Collectors.toMap(BaseEntity::getId, e -> new CachedEntity(e.getVersion(), e.getUpdateTimestamp() != null ? e.getUpdateTimestamp() : e.getCreateTimestamp()))));
    }

    public void remove(BaseEntity<?> entity) {
        cache.remove(entity.getId());
    }

    public void remove(Collection<BaseEntity<?>> entities) {
        cache.removeAll(entities.stream().map(BaseEntity::getId).collect(Collectors.toSet()));
    }

    public boolean isNew(BaseEntity<?> entity) {
        return new CachedEntity(entity.getVersion(), entity.getUpdateTimestamp() != null ? entity.getUpdateTimestamp() : entity.getCreateTimestamp()).equals(cache.get(entity.getId()));
    }

    public boolean isNew(Collection<BaseEntity<?>> entities) {
        return entities.stream().map(e -> new CachedEntity(e.getVersion(), e.getUpdateTimestamp() != null ? e.getUpdateTimestamp() : e.getCreateTimestamp()))
                .collect(Collectors.toList()).equals(cache.getAll(entities.stream().map(BaseEntity::getId).collect(Collectors.toSet())));//FIXME equals
    }

}
