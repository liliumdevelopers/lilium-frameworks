package com.sunecity.lilium.data.criteria;

import com.sunecity.lilium.data.IReport;

import java.io.Serializable;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
public interface CriteriaSelect extends IReport {

    <T extends Serializable> List<T> select(CriteriaMetaData metaData) throws Exception;

    int selectCount(CriteriaMetaData metaData) throws Exception;

}
