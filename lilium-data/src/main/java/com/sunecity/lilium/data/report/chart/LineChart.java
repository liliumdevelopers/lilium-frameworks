package com.sunecity.lilium.data.report.chart;

import com.sunecity.lilium.data.report.ReportColumn;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "LINE_CHARTS")
public class LineChart extends Chart {

    @Column(name = "LABEL")
    private String label;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "LINE_CHART_ID")
    @OrderColumn
    private List<ReportColumn> series;

    public LineChart() {
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<ReportColumn> getSeries() {
        return this.series;
    }

    public void setSeries(List<ReportColumn> series) {
        this.series = series;
    }

    public void addSerie(ReportColumn serie) {
        if (this.getSeries() == null) {
            this.series = new ArrayList<>();
        }
        this.series.add(serie);
    }

}
