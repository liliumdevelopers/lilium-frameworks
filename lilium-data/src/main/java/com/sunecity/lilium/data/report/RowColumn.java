package com.sunecity.lilium.data.report;

/**
 * Created by shahram on 7/17/15.
 */
public class RowColumn {

    private String value;
    private Style style;

    public RowColumn() {
    }

    public RowColumn(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

}
