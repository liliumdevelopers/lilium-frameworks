package com.sunecity.lilium.data.criteria;

import com.querydsl.core.types.Expression;
import com.querydsl.sql.SQLQuery;

public class SqlCriteria<T> extends SQLQuery<T> {

    private CriteriaMetaData metaData;

    public SqlCriteria(Expression<?>... entityPaths) {
        from(entityPaths);
//        metaData = new CriteriaMetaData(entityPaths);
    }

    public static void main(String[] args) {
//        SQLQueryFactory sqlQueryFactory = new SQLQueryFactory()
        SQLQuery sqlQuery = new SQLQuery();
//        sqlQuery.from()
//        sqlQuery.fetch()
//        sqlQuery.getSQL().getSQL()
    }

    public CriteriaMetaData getMetaData() {
        metaData.setMetadata(super.getMetadata());
        return metaData;
    }

    public String getSqlQuery() {
        return serialize(false).toString();
    }

}
