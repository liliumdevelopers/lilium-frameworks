package com.sunecity.lilium.data.schedule.enums;

import com.sunecity.lilium.core.localization.Bundles;

public enum Month {

	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12;

	public String toString() {
		String name = this.getClass().getSimpleName();
		return Bundles.getValueLocale(
				this.getClass().getName().replaceAll(".enums." + name, "")
						+ ".language." + name.toLowerCase().charAt(0)
						+ name.substring(1), this.name());
	}

}
