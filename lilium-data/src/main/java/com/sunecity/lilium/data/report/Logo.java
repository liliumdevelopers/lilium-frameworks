package com.sunecity.lilium.data.report;

import com.sunecity.lilium.core.model.BaseEntitySimple;
import com.sunecity.lilium.data.report.enums.LogoPosition;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOGOS")
public class Logo extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LogoSeq")
    @SequenceGenerator(name = "LogoSeq", sequenceName = "LOGO_SEQ")
    @Column(name = "LOGO_ID")
    private Long id;

    @Column(name = "LOGO_FILE")
    @Lob
    private byte[] file;

    @Column(name = "POSITION")
    @Enumerated(EnumType.STRING)
    private LogoPosition position;

    @Column(name = "WIDTH")
    private Integer width;

    @Column(name = "HEIGHT")
    private Integer height;

    public Logo() {
    }

    public Logo(byte[] file, LogoPosition position) {
        this.file = file;
        this.position = position;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getFile() {
        return this.file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public LogoPosition getPosition() {
        return this.position;
    }

    public void setPosition(LogoPosition position) {
        this.position = position;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

}
