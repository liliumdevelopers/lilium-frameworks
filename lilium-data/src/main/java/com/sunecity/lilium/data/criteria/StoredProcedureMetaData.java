package com.sunecity.lilium.data.criteria;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Shahram Goodarzi
 */
public class StoredProcedureMetaData extends MetaData {

    private final String query;
    private Map<String, Object> inParams;
    private Map<String, Object> outParams;

    public StoredProcedureMetaData(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public Map<String, Object> getInParams() {
        return inParams;
    }

    public void setInParams(Map<String, Object> inParams) {
        this.inParams = inParams;
    }

    public void addInParam(String name, Object value) {
        if (inParams == null) {
            inParams = new HashMap<>();
        }
        inParams.put(name, value);
    }

    public Map<String, Object> getOutParams() {
        return outParams;
    }

    public void setOutParams(Map<String, Object> outParams) {
        this.outParams = outParams;
    }

    public void addOutParam(String name) {
        if (outParams == null) {
            outParams = new HashMap<>();
        }
        outParams.put(name, null);
    }

}
