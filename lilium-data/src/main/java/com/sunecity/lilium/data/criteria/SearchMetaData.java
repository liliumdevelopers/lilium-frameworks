package com.sunecity.lilium.data.criteria;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by shahram on 12/22/16.
 */
public class SearchMetaData<T> implements Serializable {

    private Class<T> clas;
    private List<Search> searches;
    private Map<String, SortType> sorts;

    public SearchMetaData() {
    }

    public SearchMetaData(Class<T> clas) {
        this.clas = clas;
    }

    public static <T> SearchMetaData<T> valueOf(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, SearchMetaData.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Class<T> getClas() {
        return clas;
    }

    public void setClas(Class<T> clas) {
        this.clas = clas;
    }

    public List<Search> getSearches() {
        return searches;
    }

    public void setSearches(List<Search> searches) {
        this.searches = searches;
    }

    public Map<String, SortType> getSorts() {
        return sorts;
    }

    public void setSorts(Map<String, SortType> sorts) {
        this.sorts = sorts;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
