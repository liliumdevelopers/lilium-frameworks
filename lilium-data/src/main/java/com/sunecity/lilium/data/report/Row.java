package com.sunecity.lilium.data.report;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shahram on 7/17/15.
 */
public class Row {

    private List<RowColumn> columns;

    public List<RowColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<RowColumn> columns) {
        this.columns = columns;
    }

    public void addColumn(RowColumn column) {
        if (getColumns() == null) {
            columns = new ArrayList<>();
        }
        columns.add(column);
    }

    public void addColumn(String value) {
        if (getColumns() == null) {
            columns = new ArrayList<>();
        }
        columns.add(new RowColumn(value));
    }

}
