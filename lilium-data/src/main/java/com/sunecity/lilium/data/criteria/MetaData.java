package com.sunecity.lilium.data.criteria;

import java.io.Serializable;

/**
 * @author Shahram Goodarzi
 */
public class MetaData implements Serializable {

    private Integer first;
    private Integer max;

    public MetaData() {
    }

    public Integer getFirst() {
        return first;
    }

    public void setFirst(Integer first) {
        this.first = first;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

}
