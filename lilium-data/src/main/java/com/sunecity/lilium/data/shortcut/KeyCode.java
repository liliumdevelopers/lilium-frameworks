package com.sunecity.lilium.data.shortcut;

public enum KeyCode {

    A("a"), B("b"), C("c"), D("d"), E("e"), F("f"), G("g"), H("h"), I("i"), J("j"), K("k"), L("l"), M("m"), N("n"), O("o"), P("p"), Q("q"), R("r"), S("s"), T("t"), U("u"), V("v"), W("w"), X("x"), Y("y"), Z("z"),
    _0("0"), _1("1"), _2("2"), _3("3"), _4("4"), _5("5"), _6("6"), _7("7"), _8("8"), _9("9"),
    F1("f1"), F2("f2"), F3("f3"), F4("f4"), F5("f5"), F6("f6"), F7("f7"), F8("f8"), F9("f9"), F10("f10"), F11("f11"), F12("f12");

    private final String name;

    KeyCode(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}