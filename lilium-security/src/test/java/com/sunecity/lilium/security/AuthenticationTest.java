package com.sunecity.lilium.security;

import com.sunecity.lilium.security.authentication.Credential;
import com.sunecity.lilium.security.authentication.Group;
import com.sunecity.lilium.security.authentication.Role;
import com.sunecity.lilium.security.authentication.User;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by shahram on 10/11/16.
 */
public class AuthenticationTest {

    private Authentication authentication;

    @Before
    public void init() throws Exception {
        authentication = new Authentication();
        authentication.init("lilium");
    }

    @Test
    public void saveUser() throws Exception {
        User user = new User("shahram", new Credential("shahram"));
        authentication.save(user);
    }


    @Test
    public void saveRole() throws Exception {
        Role role = new Role("role");
        role = authentication.save(role);
        System.out.println(role.getId());
    }


    @Test
    public void saveGroup() throws Exception {
        Group group = new Group("group");
        group = authentication.save(group);
        System.out.println(group.getId());
    }

}
