package com.sunecity.lilium.security;

import com.sunecity.lilium.security.authentication.Role;
import com.sunecity.lilium.security.authorization.Resource;
import com.sunecity.lilium.security.authorization.permission.Permission;
import com.sunecity.lilium.security.authorization.permission.ResourcePermission;
import com.sunecity.lilium.security.authorization.policy.Policy;
import com.sunecity.lilium.security.authorization.policy.RolePolicy;
import com.sunecity.lilium.security.authorization.scope.PageScope;
import com.sunecity.lilium.security.authorization.scope.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by shahram on 10/11/16.
 */
public class AuthorizationTest {

    private Authorization authorization;

    @Before
    public void init() throws Exception {
        authorization = new Authorization();
        authorization.init("lilium", "accounting");
    }

    @Test
    public void saveScope() throws Exception {
        Scope scope = new Scope(PageScope.DISABLE.scope(), "ddd");
        scope = authorization.save(scope);
        System.out.println(scope.getId());
    }

    @Test
    public void saveResource() throws Exception {
        Resource resource = new Resource("res30", "/res30");
        resource = authorization.save(resource);
        System.out.println(resource.getId());
    }

    @Test
    public void savePolicy() throws Exception {
        Role role = new Role("role");
        role.setId("af164ae7-0af0-4999-9c19-279d9e302dd9");
        Policy policy = new RolePolicy("policy", role);
        policy = authorization.save(policy);
        System.out.println(policy.getId());
    }

    @Test
    public void savePermission() throws Exception {
        Set<Resource> resources = new HashSet<>();
        Resource resource = new Resource("resource1", "/resource1");
        resource = authorization.save(resource);
        resources.add(resource);
        Permission permission = new ResourcePermission("permission1", resources, new RolePolicy("policy"));
        permission = authorization.save(permission);
        System.out.println(permission.getId());
    }

    @Test
    public void hasPermission() {

    }

}
