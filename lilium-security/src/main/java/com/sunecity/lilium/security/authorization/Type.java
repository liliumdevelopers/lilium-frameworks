package com.sunecity.lilium.security.authorization;

import java.io.Serializable;

/**
 * Created by shahram on 10/9/16.
 */
public interface Type extends Serializable {

    static String type(Class<? extends Type> clas) {
        return "urn:type:" + clas.getSimpleName().substring(0, clas.getSimpleName().lastIndexOf("Scope"));
    }

    default String scope() {
        return "urn:" + getClass().getSimpleName() + ":" + name();
    }

    String name();

}
