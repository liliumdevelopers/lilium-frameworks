package com.sunecity.lilium.security;

import com.sunecity.lilium.security.authentication.Credential;
import com.sunecity.lilium.security.authentication.Group;
import com.sunecity.lilium.security.authentication.Role;
import com.sunecity.lilium.security.authentication.User;
import org.aeonbits.owner.ConfigFactory;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RoleByIdResource;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.admin.client.resource.RoleScopeResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import javax.enterprise.context.Dependent;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by shahram on 10/11/16.
 */
@Dependent
public class Authentication implements Serializable {

    private UsersResource users;
    private GroupsResource groups;
    private RolesResource roles;
    private RoleByIdResource rolesId;
    private RolesResource clientRoles;
    private String client;

    public void init(String realm) throws Exception {
        KeycloakConfig keycloakConfig = ConfigFactory.create(KeycloakConfig.class, System.getProperties(), System.getenv());
        String url = keycloakConfig.url();
        Keycloak kc = KeycloakBuilder.builder().serverUrl(url).realm(realm).username(keycloakConfig.username())
                .password(keycloakConfig.password()).clientId("admin-cli")
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).register(new CustomJacksonProvider()).build())
                .build();
        RealmResource resource = kc.realm(realm);
        users = resource.users();
        groups = resource.groups();
        roles = resource.roles();
        rolesId = resource.rolesById();
    }

    public void init(String realm, String client) throws Exception {
        KeycloakConfig keycloakConfig = ConfigFactory.create(KeycloakConfig.class, System.getProperties(), System.getenv());
        String url = keycloakConfig.url();
        Keycloak kc = KeycloakBuilder.builder().serverUrl(url).realm(realm).username(keycloakConfig.username())
                .password(keycloakConfig.password()).clientId("admin-cli")
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).register(new CustomJacksonProvider()).build())
        .build();
        RealmResource resource = kc.realm(realm);
        users = resource.users();
        groups = resource.groups();
        roles = resource.roles();
        rolesId = resource.rolesById();
        List<ClientRepresentation> clientRepresentations = resource.clients().findByClientId(client);
        if (clientRepresentations != null && !clientRepresentations.isEmpty()) {
            clientRoles = resource.clients().get(clientRepresentations.get(0).getId()).roles();
            this.client = clientRepresentations.get(0).getId();
        } else {
            throw new Exception("Client Not Found!");
        }
    }

    public User save(User user) throws Exception {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setUsername(user.getUsername());
        userRepresentation.setEnabled(user.getEnabled());
        userRepresentation.setFirstName(user.getFirstName());
        userRepresentation.setLastName(user.getLastName());
        userRepresentation.setEmail(user.getEmail());
        Response response = users.create(userRepresentation);
        user.setId(getId(response));
        UserResource userResource = users.get(user.getId());
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(user.getPassword().getValue());
        credentialRepresentation.setTemporary(user.getPassword().isTemporary());
        userResource.resetPassword(credentialRepresentation);
        return user;
    }

    public void resetPassword(String userId, String password) throws Exception {
        resetPassword(userId, password, true);
    }

    public void resetPassword(String userId, String password, boolean temporary) throws Exception {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(password);
        credentialRepresentation.setTemporary(temporary);
        users.get(userId).resetPassword(credentialRepresentation);
    }

    public void resetPassword(String userId, Credential credential) {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setValue(credential.getValue());
        credentialRepresentation.setTemporary(credential.isTemporary());
        users.get(userId).resetPassword(credentialRepresentation);
    }

    public void update(User user) throws Exception {
        UserResource userResource = users.get(user.getId());
        UserRepresentation userRepresentation = userResource.toRepresentation();
        userRepresentation.setUsername(user.getUsername());
        userRepresentation.setEnabled(user.getEnabled());
        userRepresentation.setFirstName(user.getFirstName());
        userRepresentation.setLastName(user.getLastName());
        userRepresentation.setEmail(user.getEmail());
        userResource.update(userRepresentation);
    }

    public void deleteUser(String userId) throws Exception {
        users.get(userId).remove();
    }

    public User getUser(String userId) throws Exception {
        try {
            UserRepresentation userRepresentation = users.get(userId).toRepresentation();
            return getUser(userRepresentation);
        } catch (Exception e) {
            return null;
        }
    }

    public User getUserByUsername(String username) throws Exception {
        List<UserRepresentation> userRepresentations = users.search(username, 0, 1);
        if (userRepresentations != null && !userRepresentations.isEmpty()) {
            UserRepresentation userRepresentation = userRepresentations.get(0);
            return getUser(userRepresentation);
        }
        return null;
    }

    public List<User> getAllUsers() throws Exception {
        List<User> users = new ArrayList<>();
        try {
            List<UserRepresentation> userRepresentations = this.users.search("", 0, this.users.count());
            for (UserRepresentation userRepresentation : userRepresentations) {
                users.add(getUser(userRepresentation));
            }
        } catch (Exception ignored) {
        }
        return users;
    }

    public Group save(Group group) throws Exception {
        GroupRepresentation groupRepresentation = new GroupRepresentation();
        groupRepresentation.setName(group.getName());
        Response response = groups.add(groupRepresentation);
        group.setId(getId(response));
        return group;
    }

    public void update(Group group) throws Exception {
        GroupResource groupResource = groups.group(group.getId());
        GroupRepresentation groupRepresentation = groupResource.toRepresentation();
        groupRepresentation.setName(group.getName());
        groupResource.update(groupRepresentation);
    }

    public void deleteGroup(String groupId) throws Exception {
        groups.group(groupId).remove();
    }

    public Group getGroup(String groupId) throws Exception {
        try {
            GroupRepresentation groupRepresentation = groups.group(groupId).toRepresentation();
            Group group = getGroup(groupRepresentation);
            List<Group> subGroups = new ArrayList<>();
            for (GroupRepresentation representation : groupRepresentation.getSubGroups()) {
                subGroups.add(getGroup(representation));
            }
            group.setSubGroups(subGroups);
            return group;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Group> getAllGroups() throws Exception {
        List<Group> groups = new ArrayList<>();
        try {
            List<GroupRepresentation> groupRepresentations = this.groups.groups();
            for (GroupRepresentation groupRepresentation : groupRepresentations) {
                Group group = getGroup(groupRepresentation);
                List<Group> subGroups = new ArrayList<>();
                for (GroupRepresentation representation : groupRepresentation.getSubGroups()) {
                    subGroups.add(getGroup(representation));
                }
                group.setSubGroups(subGroups);
                groups.add(group);
            }
        } catch (Exception ignored) {
        }
        return groups;
    }

    public Role save(Role role) throws Exception {
        RoleRepresentation roleRepresentation = new RoleRepresentation();
        roleRepresentation.setName(role.getName());
        roleRepresentation.setDescription(role.getDescription());
        roles.create(roleRepresentation);
        role.setId(roles.get(role.getName()).toRepresentation().getId());
        return role;
    }

    public void update(Role role) throws Exception {
        RoleResource roleResource = roles.get(role.getId());
        RoleRepresentation roleRepresentation = roleResource.toRepresentation();
        roleRepresentation.setName(role.getName());
        roleRepresentation.setDescription(role.getDescription());
        roleResource.update(roleRepresentation);
    }

    public void deleteRole(String roleId) throws Exception {
        roles.get(roleId).remove();
    }

    public Role getRole(String roleName) throws Exception {
        try {
            RoleRepresentation roleRepresentation = roles.get(roleName).toRepresentation();
            return getRole(roleRepresentation);
        } catch (Exception e) {
            return null;
        }
    }

    public Role getRoleById(String roleId) throws Exception {
        try {
            RoleRepresentation roleRepresentation = rolesId.getRole(roleId);
            return getRole(roleRepresentation);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Role> getAllRoles() throws Exception {
        List<Role> roles = new ArrayList<>();
        try {
            List<RoleRepresentation> roleRepresentations = this.roles.list();
            for (RoleRepresentation roleRepresentation : roleRepresentations) {
                roles.add(getRole(roleRepresentation));
            }
            return roles;
        } catch (Exception ignored) {

        }
        return roles;
    }

    public List<Role> getUserRoles(User user) throws Exception {
        List<Role> roles = new ArrayList<>();
        try {
            List<RoleRepresentation> roleRepresentations = this.users.get(user.getId()).roles().realmLevel().listAll();
            for (RoleRepresentation roleRepresentation : roleRepresentations) {
                roles.add(getRole(roleRepresentation));
            }
            return roles;
        } catch (Exception ignored) {

        }
        return roles;
    }

    public List<Role> getUserClientRoles(User user) throws Exception {
        List<Role> roles = new ArrayList<>();
        try {
            List<RoleRepresentation> roleRepresentations = this.users.get(user.getId()).roles().clientLevel(client).listAll();
            for (RoleRepresentation roleRepresentation : roleRepresentations) {
                roles.add(getRole(roleRepresentation));
            }
            return roles;
        } catch (Exception ignored) {

        }
        return roles;
    }

    public List<Role> getGroupRoles(Group group) throws Exception {
        List<Role> roles = new ArrayList<>();
        try {
            List<RoleRepresentation> roleRepresentations = this.groups.group(group.getId()).roles().realmLevel().listAll();
            for (RoleRepresentation roleRepresentation : roleRepresentations) {
                roles.add(getRole(roleRepresentation));
            }
            return roles;
        } catch (Exception ignored) {

        }
        return roles;
    }

    public List<Role> getGroupClientRoles(Group group) throws Exception {
        List<Role> roles = new ArrayList<>();
        try {
            List<RoleRepresentation> roleRepresentations = this.groups.group(group.getId()).roles().clientLevel(client).listAll();
            for (RoleRepresentation roleRepresentation : roleRepresentations) {
                roles.add(getRole(roleRepresentation));
            }
            return roles;
        } catch (Exception ignored) {

        }
        return roles;
    }

    public Role saveClient(Role role) throws Exception {
        RoleRepresentation roleRepresentation = new RoleRepresentation();
        roleRepresentation.setName(role.getName());
        roleRepresentation.setDescription(role.getDescription());
        clientRoles.create(roleRepresentation);
        role.setId(clientRoles.get(role.getName()).toRepresentation().getId());
        return role;
    }

    public void updateClient(Role role) throws Exception {
        RoleResource roleResource = clientRoles.get(role.getId());
        RoleRepresentation roleRepresentation = roleResource.toRepresentation();
        roleRepresentation.setName(role.getName());
        roleRepresentation.setDescription(role.getDescription());
        roleResource.update(roleRepresentation);
    }

    public void deleteClientRole(String roleId) throws Exception {
        clientRoles.get(roleId).remove();
    }

    public Role getClientRole(String roleName) throws Exception {
        try {
            RoleRepresentation roleRepresentation = clientRoles.get(roleName).toRepresentation();
            return getRole(roleRepresentation);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Role> getAllClientRoles() throws Exception {
        List<Role> roles = new ArrayList<>();
        try {
            List<RoleRepresentation> roleRepresentations = this.clientRoles.list();
            for (RoleRepresentation roleRepresentation : roleRepresentations) {
                roles.add(getRole(roleRepresentation));
            }
            return roles;
        } catch (Exception ignored) {

        }
        return roles;
    }

    public List<Role> getAllClientRoles(String userId) throws Exception {
        List<Role> roles = new ArrayList<>();
        try {
            RoleScopeResource clientRole = this.users.get(userId).roles().clientLevel(client);
            List<RoleRepresentation> roleRepresentations = clientRole.listAll();
            roleRepresentations.addAll(clientRole.listEffective());
            for (RoleRepresentation roleRepresentation : roleRepresentations) {
                roles.add(getRole(roleRepresentation));
            }
            return roles;
        } catch (Exception ignored) {

        }
        return roles;
    }

    private User getUser(UserRepresentation userRepresentation) {
        User user = new User(userRepresentation.getUsername());
        user.setId(userRepresentation.getId());
        user.setEnabled(userRepresentation.isEnabled());
        user.setFirstName(userRepresentation.getFirstName());
        user.setLastName(userRepresentation.getLastName());
        user.setEmail(userRepresentation.getEmail());
        user.setRealmRoles(userRepresentation.getRealmRoles());
        user.setClientRoles(userRepresentation.getClientRoles());
        user.setAttributes(userRepresentation.getAttributes());
        user.setGroups(userRepresentation.getGroups());
        return user;
    }

    private Group getGroup(GroupRepresentation groupRepresentation) {
        Group group = new Group(groupRepresentation.getName());
        group.setId(groupRepresentation.getId());
        group.setPath(groupRepresentation.getPath());
        group.setRealmRoles(groupRepresentation.getRealmRoles());
        group.setClientRoles(groupRepresentation.getClientRoles());
        return group;
    }

    private Role getRole(RoleRepresentation roleRepresentation) {
        Role role = new Role(roleRepresentation.getName(), roleRepresentation.getDescription());
        role.setId(roleRepresentation.getId());
        role.setComposite(roleRepresentation.isComposite());
        role.setClientRole(roleRepresentation.getClientRole());
        role.setContainerId(roleRepresentation.getContainerId());
//        role.setComposites(roleRepresentation.getComposites());//TODO
        return role;
    }

    public void assignUsersToGroup(String groupId, String... userIds) throws Exception {
        Arrays.stream(userIds).forEach(u -> users.get(u).joinGroup(groupId));
    }

    public void dissociateUsersFromGroup(String groupId, String... userIds) throws Exception {
        Arrays.stream(userIds).forEach(u -> users.get(u).leaveGroup(groupId));
    }

    public void assignRolesToUser(String userId, String... roleNames) throws Exception {
        users.get(userId).roles().realmLevel().add(Arrays.stream(roleNames).map(r -> roles.get(r).toRepresentation()).collect(Collectors.toList()));
    }

    public void dissociateRolesFromUser(String userId, String... roleNames) throws Exception {
        users.get(userId).roles().realmLevel().remove(Arrays.stream(roleNames).map(r -> roles.get(r).toRepresentation()).collect(Collectors.toList()));
    }

    public void assignClientRolesToUser(String userId, String... roleNames) throws Exception {
        users.get(userId).roles().clientLevel(client).add(Arrays.stream(roleNames).map(r -> clientRoles.get(r).toRepresentation()).collect(Collectors.toList()));
    }

    public void dissociateClientRolesFromUser(String userId, String... roleNames) throws Exception {
        users.get(userId).roles().clientLevel(client).remove(Arrays.stream(roleNames).map(r -> clientRoles.get(r).toRepresentation()).collect(Collectors.toList()));
    }

    public void assignRolesToGroup(String groupId, String... roleNames) throws Exception {
        groups.group(groupId).roles().realmLevel().add(Arrays.stream(roleNames).map(r -> roles.get(r).toRepresentation()).collect(Collectors.toList()));
    }

    public void dissociateRolesFromGroup(String groupId, String... roleNames) throws Exception {
        groups.group(groupId).roles().realmLevel().remove(Arrays.stream(roleNames).map(r -> roles.get(r).toRepresentation()).collect(Collectors.toList()));
    }

    public void assignClientRolesToGroup(String groupId, String... roleNames) throws Exception {
        groups.group(groupId).roles().clientLevel(client).add(Arrays.stream(roleNames).map(r -> clientRoles.get(r).toRepresentation()).collect(Collectors.toList()));
    }

    public void dissociateClientRolesFromGroup(String groupId, String... roleNames) throws Exception {
        groups.group(groupId).roles().clientLevel(client).remove(Arrays.stream(roleNames).map(r -> clientRoles.get(r).toRepresentation()).collect(Collectors.toList()));
    }

    private String getId(Response response) throws Exception {
        if (response.getStatus() != 201) {
            String error = response.readEntity(String.class);
            throw new Exception(error);
        }
        String path = response.getLocation().getPath();
        return path.substring(path.lastIndexOf('/') + 1);
    }

}
