package com.sunecity.lilium.security.authorization.scope;

import com.sunecity.lilium.security.authorization.Type;

public enum ObjectScope implements Type {

    READ, READ_GRANT_OPTION, DENY_READ,
    UPDATE, UPDATE_GRANT_OPTION, DENY_UPDATE,
    DELETE, DELETE_GRANT_OPTION, DENY_DELETE

}