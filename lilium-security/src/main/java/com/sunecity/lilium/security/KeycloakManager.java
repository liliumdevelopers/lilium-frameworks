package com.sunecity.lilium.security;

import com.sunecity.lilium.security.authentication.User;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.resource.ClientResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RealmsResource;
import org.keycloak.admin.client.resource.ServerInfoResource;
import org.keycloak.admin.client.token.TokenManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Created by shahram on 3/5/17.
 */
public interface KeycloakManager {

    static KeycloakManager proxy(String url) {
        ResteasyClient client = new ResteasyClientBuilder().connectionPoolSize(10).build();
        return client.target(url).proxy(KeycloakManager.class);
    }

    static String checkId(Response response) throws Exception {
        if (response.getStatus() != 201) {
            String error = response.readEntity(String.class);
            throw new Exception(error);
        }
        Map<String, String> params = response.readEntity(Map.class);
        String id = params.get("id");
        if (id == null) {
            id = params.get("_id");
        }
        if (id == null) {
            String path = response.getLocation().getPath();
            id = path.substring(path.lastIndexOf('/') + 1);
        }
        return id;
    }

    @Path("/tokenManager")
    @GET
    TokenManager tokenManager();

    @Path("/serverInfo")
    @GET
    ServerInfoResource serverInfo();

    @Path("/realm/{name}")
    @GET
    RealmResource realm(@PathParam("name") String name);

    @Path("/realms")
    @GET
    RealmsResource realms();

    @Path("/realm")
    @GET
    RealmResource getRealm();

    @Path("/client")
    @GET
    ClientResource getClient();

    @Path("/user/{username}")
    @GET
    User getUser(@PathParam("username") String username) throws Exception;

    @Path("/checkAuthenticationId")
    @GET
    String checkAuthenticationId(Response response) throws Exception;

    @Path("/checkAuthorizationId")
    @GET
    String checkAuthorizationId(Response response) throws Exception;

}
