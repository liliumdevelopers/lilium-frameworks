package com.sunecity.lilium.security.authorization;

/**
 * The decision strategy dictates how the policies associated with a given policy are evaluated and how a final decision
 * is obtained.
 */
public enum DecisionStrategy {

    /**
     * Defines that at least one policy must evaluate to a positive decision in order to the overall decision be also positive.
     */
    AFFIRMATIVE,

    /**
     * Defines that all policies must evaluate to a positive decision in order to the overall decision be also positive.
     */
    UNANIMOUS,

    /**
     * Defines that the number of positive decisions must be greater than the number of negative decisions. If the number of positive and negative is the same,
     * the final decision will be negative.
     */
    CONSENSUS
}
