package com.sunecity.lilium.security.authorization.policy;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shahram on 10/11/16.
 */
public class DroolsPolicy extends Policy {

    private String mavenArtifact;

    public DroolsPolicy(String name, String mavenArtifact) {
        super(name);
        this.mavenArtifact = mavenArtifact;
    }

    public String getMavenArtifact() {
        return mavenArtifact;
    }

    public void setMavenArtifact(String mavenArtifact) {
        this.mavenArtifact = mavenArtifact;
    }

    @Override
    public String getType() {
        return "drools";
    }

    @Override
    public Map<String, String> getConfig() throws Exception {
        Map<String, String> config = new HashMap<>();
        return config;
    }

}
