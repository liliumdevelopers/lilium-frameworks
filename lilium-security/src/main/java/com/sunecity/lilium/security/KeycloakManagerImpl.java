package com.sunecity.lilium.security;

import com.sunecity.lilium.security.authentication.User;
import com.sunecity.lilium.security.authorization.permission.Permission;
import com.sunecity.lilium.security.authorization.policy.Policy;
import org.aeonbits.owner.ConfigFactory;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.ClientResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RealmsResource;
import org.keycloak.admin.client.resource.ServerInfoResource;
import org.keycloak.admin.client.token.TokenManager;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.representations.idm.authorization.PolicyRepresentation;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;
import java.util.Map;

/**
 * Created by shahram on 10/11/16.
 */
@Path("/keycloak")
public class KeycloakManagerImpl implements KeycloakManager, AutoCloseable {

    @Context
    private SecurityContext securityContext;

    private Keycloak kc;
    private String realm;
    private String client;

    public KeycloakManagerImpl() {
    }

    public KeycloakManagerImpl(String realm, String client) {
        this.realm = realm;
        this.client = client;
        KeycloakConfig keycloakConfig = ConfigFactory.create(KeycloakConfig.class, System.getProperties(), System.getenv());
        String url = keycloakConfig.url();
        kc = Keycloak.getInstance(url, realm, keycloakConfig.username(), keycloakConfig.password(), "admin-cli");
    }

    @PostConstruct
    public void init() {
        KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) securityContext.getUserPrincipal();
        realm = keycloakPrincipal.getKeycloakSecurityContext().getRealm();
        client = keycloakPrincipal.getKeycloakSecurityContext().getIdToken().getIssuedFor();

        KeycloakConfig keycloakConfig = ConfigFactory.create(KeycloakConfig.class, System.getProperties(), System.getenv());
        String url = keycloakConfig.url();
        kc = Keycloak.getInstance(url, realm, keycloakConfig.username(), keycloakConfig.password(), "admin-cli");
    }

    @Override
    @PreDestroy
    public void close() {
        kc.close();
    }

    @Override
    public TokenManager tokenManager() {
        return kc.tokenManager();
    }

    @Override
    public ServerInfoResource serverInfo() {
        return kc.serverInfo();
    }

    @Override
    public RealmResource realm(String name) {
        return kc.realm(name);
    }

    @Override
    public RealmsResource realms() {
        return kc.realms();
    }

    @Override
    public RealmResource getRealm() {
        return kc.realm(realm);
    }

    @Override
    public ClientResource getClient() {
        ClientRepresentation representation = realm(realm).clients().findByClientId(client).get(0);
        return realm(realm).clients().get(representation.getId());
    }

    @Override
    public User getUser(String username) throws Exception {
        List<UserRepresentation> representations = getRealm().users().search(username, 0, 1);
        if (representations.size() == 0) {
            throw new Exception("User not found");
        }
        UserRepresentation representation = representations.get(0);
        User user = new User(representation.getUsername());
        user.setId(representation.getId());
        user.setEnabled(representation.isEnabled());
        user.setFirstName(representation.getFirstName());
        user.setLastName(representation.getLastName());
        user.setEmail(representation.getEmail());
        user.setRealmRoles(representation.getRealmRoles());
        user.setClientRoles(representation.getClientRoles());
        user.setAttributes(representation.getAttributes());
        user.setGroups(representation.getGroups());
        return user;
    }

    public Permission getPermission(PolicyRepresentation representation) {
//         representation.getType().equals("resource") ? new ResourcePermission(representation) : new ScopePermission(representation);
        return null;
    }

    public Policy getPolicy(PolicyRepresentation representation) {
        return null;
//        return representation.getType().equals("resource") ? new ResourcePermission(representation) : new ScopePermission(representation);
    }

    public String checkAuthenticationId(Response response) throws Exception {
        if (response.getStatus() != 201) {
            String error = response.readEntity(String.class);
            throw new Exception(error);
        }
        String path = response.getLocation().getPath();
        return path.substring(path.lastIndexOf('/') + 1);
    }

    public String checkAuthorizationId(Response response) throws Exception {
        if (response.getStatus() != 201) {
            String error = response.readEntity(String.class);
            throw new Exception(error);
        }
        Map<String, String> params = response.readEntity(Map.class);
        return params.get("id") != null ? params.get("id") : params.get("_id");
    }

}
