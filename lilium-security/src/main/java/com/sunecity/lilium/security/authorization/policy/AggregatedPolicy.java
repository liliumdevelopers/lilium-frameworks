package com.sunecity.lilium.security.authorization.policy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sunecity.lilium.security.authorization.DecisionStrategy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by shahram on 10/11/16.
 */
public class AggregatedPolicy extends Policy {

    private Set<Policy> policies;
    private DecisionStrategy decisionStrategy = DecisionStrategy.UNANIMOUS;

    public AggregatedPolicy(String name, Policy... policies) {
        super(name);
        this.policies = Arrays.stream(policies).collect(Collectors.toSet());
    }

    public AggregatedPolicy(String name, Set<Policy> policies) {
        super(name);
        this.policies = policies;
    }

    public Set<Policy> getPolicies() {
        return policies;
    }

    public void setPolicies(Set<Policy> policies) {
        this.policies = policies;
    }

    public DecisionStrategy getDecisionStrategy() {
        return decisionStrategy;
    }

    public void setDecisionStrategy(DecisionStrategy decisionStrategy) {
        this.decisionStrategy = decisionStrategy;
    }

    @Override
    public String getType() {
        return "aggregated";
    }

    @Override
    public Map<String, String> getConfig() throws Exception {
        Map<String, String> config = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        config.put("applyPolicies", mapper.writeValueAsString(policies.stream().map(Policy::getName).collect(Collectors.toSet())));
        return config;
    }

}
