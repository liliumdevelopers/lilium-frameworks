package com.sunecity.lilium.security.authorization.permission;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sunecity.lilium.security.authorization.Resource;
import com.sunecity.lilium.security.authorization.policy.Policy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by shahram on 10/11/16.
 */
public class ResourcePermission extends Permission {

    private String resourceType;
    private Set<Resource> resources;

    public ResourcePermission(String name, Set<Resource> resources, Policy... policies) {
        super(name, policies);
        this.resources = resources;
    }

    public ResourcePermission(String name, Set<Resource> resources, Set<Policy> policies) {
        super(name, policies);
        this.resources = resources;
    }

    public ResourcePermission(String name, String resourceType, Policy... policies) {
        super(name, policies);
        this.resourceType = resourceType;
    }

    public ResourcePermission(String name, String resourceType, Set<Policy> policies) {
        super(name, policies);
        this.resourceType = resourceType;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Set<Resource> getResources() {
        return resources;
    }

    public void setResources(Set<Resource> resources) {
        this.resources = resources;
    }

    public void addResource(Resource resource) {
        if (resources == null) {
            resources = new HashSet<>();
        }
        resources.add(resource);
    }

    @Override
    public String getType() {
        return "resource";
    }

    @Override
    public Map<String, String> getConfig() throws Exception {
        Map<String, String> config = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        if (resources != null) {
            config.put("resources", mapper.writeValueAsString(resources.stream().map(Resource::getId).collect(Collectors.toSet())));
        }
        if (resourceType != null) {
            config.put("defaultResourceType", resourceType);
        }
        config.put("applyPolicies", mapper.writeValueAsString(getPolicies().stream().map(Policy::getName).collect(Collectors.toSet())));
        return config;
    }

}
