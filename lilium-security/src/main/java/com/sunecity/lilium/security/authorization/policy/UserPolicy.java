package com.sunecity.lilium.security.authorization.policy;

import com.sunecity.lilium.security.authentication.User;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

/**
 * Created by shahram on 10/11/16.
 */
public class UserPolicy extends Policy {

    private Set<User> users;

    public UserPolicy(String name, User... users) {
        super(name);
        this.users = Arrays.stream(users).collect(Collectors.toSet());
    }

    public UserPolicy(String name, Set<User> users) {
        super(name);
        this.users = users;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public void addUser(User user) {
        if (users == null) {
            users = new HashSet<>();
        }
        users.add(user);
    }

    @Override
    public String getType() {
        return "user";
    }

    @Override
    public Map<String, String> getConfig() throws Exception {
        Map<String, String> config = new HashMap<>();
        StringJoiner joiner = new StringJoiner(",", "[", "]");
        users.forEach(user -> joiner.add("\"" + user.getId() + "\""));
        config.put("users", joiner.toString());
        return config;
    }

}
