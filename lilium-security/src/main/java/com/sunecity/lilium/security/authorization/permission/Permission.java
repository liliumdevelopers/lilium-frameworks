package com.sunecity.lilium.security.authorization.permission;

import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.security.authorization.DecisionStrategy;
import com.sunecity.lilium.security.authorization.policy.Policy;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class Permission implements BaseIdentity<String> {

    private String id;
    private String name;
    private String description;
    private DecisionStrategy decisionStrategy = DecisionStrategy.AFFIRMATIVE;
    private Set<Policy> policies;

    public Permission(String name, Policy... policies) {
        this.name = name;
        this.policies = Arrays.stream(policies).collect(Collectors.toSet());
    }

    public Permission(String name, Set<Policy> policies) {
        this.name = name;
        this.policies = policies;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DecisionStrategy getDecisionStrategy() {
        return decisionStrategy;
    }

    public void setDecisionStrategy(DecisionStrategy decisionStrategy) {
        this.decisionStrategy = decisionStrategy;
    }

    public Set<Policy> getPolicies() {
        return policies;
    }

    public void setPolicies(Set<Policy> policies) {
        this.policies = policies;
    }

    public abstract String getType();

    public abstract Map<String, String> getConfig() throws Exception;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Permission permission = (Permission) o;
        return Objects.equals(getId(), permission.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

}