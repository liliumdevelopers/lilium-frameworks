package com.sunecity.lilium.security.authorization.scope;

import com.sunecity.lilium.security.authorization.Type;

public enum PageScope implements Type {

    VIEW, GRANT_OPTION, DISABLE, DENY

}