package com.sunecity.lilium.security.authorization.policy;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shahram on 10/11/16.
 */
public class TimePolicy extends Policy {

    private String year;

    public TimePolicy(String name) {
        super(name);
    }

    public TimePolicy(String name, String year) {
        super(name);
        this.year = year;
    }

    @Override
    public String getType() {
        return "time";
    }

    @Override
    public Map<String, String> getConfig() throws Exception {
        Map<String, String> config = new HashMap<>();

        return config;
    }

}
