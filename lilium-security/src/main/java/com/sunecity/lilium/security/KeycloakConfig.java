package com.sunecity.lilium.security;

import org.aeonbits.owner.Config;

/**
 * Created by shahram on 5/31/16.
 */
@Config.Sources({"classpath:META-INF/keycloak.properties"})
public interface KeycloakConfig extends Config {

    String username();

    String password();

    @DefaultValue("${keycloakUrl}")
    default String url() {
        return System.getProperty("keycloakUrl");
    }

}
