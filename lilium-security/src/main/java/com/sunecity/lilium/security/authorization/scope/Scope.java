package com.sunecity.lilium.security.authorization.scope;

import com.sunecity.lilium.core.model.BaseIdentity;

import java.util.Objects;

public class Scope implements BaseIdentity<String> {

    private String id;
    private String name;
    private String iconUri;

    public Scope(String name) {
        this(name, null);
    }

    public Scope(String name, String iconUri) {
        this.name = name;
        this.iconUri = iconUri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUri() {
        return iconUri;
    }

    public void setIconUri(String iconUri) {
        this.iconUri = iconUri;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Scope scope = (Scope) o;
        return Objects.equals(getName(), scope.getName());
    }

    public int hashCode() {
        return Objects.hash(getName());
    }

}