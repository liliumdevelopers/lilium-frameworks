package com.sunecity.lilium.security.authorization.policy;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shahram on 10/11/16.
 */
public class JsPolicy extends Policy {

    private String code;

    public JsPolicy(String name, String code) {
        super(name);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getType() {
        return "js";
    }

    @Override
    public Map<String, String> getConfig() throws Exception {
        Map<String, String> config = new HashMap<>();
        config.put("code", code);
        return config;
    }

}
