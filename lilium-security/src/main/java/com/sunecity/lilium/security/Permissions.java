package com.sunecity.lilium.security;

import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.security.authorization.scope.ClassScope;
import com.sunecity.lilium.security.authorization.scope.FieldScope;
import com.sunecity.lilium.security.authorization.scope.ObjectScope;
import com.sunecity.lilium.security.authorization.scope.PageScope;
import org.keycloak.AuthorizationContext;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Path("/permissions")
public class Permissions implements Serializable {

    @Context
    private SecurityContext securityContext;

    private AuthorizationContext authorizationContext;

    @PostConstruct
    public void init() {
        KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) securityContext.getUserPrincipal();
        authorizationContext = keycloakPrincipal.getKeycloakSecurityContext().getAuthorizationContext();
    }

    @GET
    @Path("/{resource}")
    public boolean hasPermission(@PathParam("resource") String resource) {
        return authorizationContext.hasResourcePermission(resource);
    }

    @GET
    @Path("/{resource}/{scope}")
    public boolean hasPermission(@PathParam("resource") String resource, @PathParam("scope") String scope) {
        return authorizationContext.hasPermission(resource, scope);
    }

    @GET
    @Path("/page/{page}")
    public boolean hasPagePermission(@PathParam("page") String page) {
        return authorizationContext.hasPermission(page, PageScope.VIEW.scope());
    }

    public boolean isPageDisabled(String page) {
        return authorizationContext.hasPermission(page, PageScope.DISABLE.scope());
    }

    @GET
    @Path("/field/{field}")
    public boolean hasCreateFieldPermission(@PathParam("field") String field) {
        return authorizationContext.hasPermission(field, FieldScope.CREATE.scope()) && !authorizationContext.hasPermission(field, FieldScope.DENY_CREATE.scope());
    }

    public boolean hasReadFieldPermission(String field) {
        return authorizationContext.hasPermission(field, FieldScope.READ.scope()) && !authorizationContext.hasPermission(field, FieldScope.DENY_READ.scope());
    }

    public boolean hasUpdateFieldPermission(String field) {
        return authorizationContext.hasPermission(field, FieldScope.UPDATE.scope()) && !authorizationContext.hasPermission(field, FieldScope.DENY_UPDATE.scope());
    }

    public boolean hasDeleteFieldPermission(String field) {
        return authorizationContext.hasPermission(field, FieldScope.DELETE.scope()) && !authorizationContext.hasPermission(field, FieldScope.DENY_DELETE.scope());
    }

    public boolean isFieldDisabled(String field) {
        return authorizationContext.hasPermission(field, FieldScope.DISABLE.scope());
    }

    public boolean hasCreateClassPermission(String className) {
        try {
            Class<?> clas = Class.forName(className);
            return hasCreateClassPermission(clas);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public boolean hasCreateClassPermission(Class<?> clas) {
        return authorizationContext.hasPermission(clas.getName(), ClassScope.CREATE.scope()) && !authorizationContext.hasPermission(clas.getName(), ClassScope.DENY_CREATE.scope());
    }

    public boolean hasReadClassPermission(String className) {
        try {
            Class<?> clas = Class.forName(className);
            return hasReadClassPermission(clas);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public boolean hasReadClassPermission(Class<?> clas) {
        return authorizationContext.hasPermission(clas.getName(), ClassScope.READ.scope()) && !authorizationContext.hasPermission(clas.getName(), ClassScope.DENY_READ.scope());
    }

    public boolean hasUpdateClassPermission(String className) {
        try {
            Class<?> clas = Class.forName(className);
            return hasUpdateClassPermission(clas);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public boolean hasUpdateClassPermission(Class<?> clas) {
        return authorizationContext.hasPermission(clas.getName(), ClassScope.UPDATE.scope()) && !authorizationContext.hasPermission(clas.getName(), ClassScope.DENY_UPDATE.scope());
    }

    public boolean hasDeleteClassPermission(String className) {
        try {
            Class<?> clas = Class.forName(className);
            return hasDeleteClassPermission(clas);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public boolean hasDeleteClassPermission(Class<?> clas) {
        return authorizationContext.hasPermission(clas.getName(), ClassScope.DELETE.scope()) && !authorizationContext.hasPermission(clas.getName(), ClassScope.DENY_DELETE.scope());
    }

    public boolean hasReadObjectPermission(BaseIdentity<? extends Serializable> object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.READ.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.DENY_READ.scope());
    }

    public boolean hasUpdateObjectPermission(BaseIdentity<? extends Serializable> object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.UPDATE.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.DENY_UPDATE.scope());
    }

    public boolean hasDeleteObjectPermission(BaseIdentity<? extends Serializable> object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.DELETE.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.DENY_DELETE.scope());
    }

    public boolean hasReadObjectPermission(Object object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.READ.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.DENY_READ.scope());
    }

    public boolean hasUpdateObjectPermission(Object object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.UPDATE.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.DENY_UPDATE.scope());
    }

    public boolean hasDeleteObjectPermission(Object object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.DELETE.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.DENY_DELETE.scope());
    }

    public List<?> getReadPermittedObjects(List<?> objects) {
        return objects.stream().filter(o -> o instanceof BaseIdentity ? this.hasReadObjectPermission((BaseIdentity<?>) o) : this.hasReadObjectPermission(o)).collect(Collectors.toList());
    }

    public List<?> getUpdatePermittedObjects(List<?> objects) {
        return objects.stream().filter(o -> o instanceof BaseIdentity ? this.hasUpdateObjectPermission((BaseIdentity<?>) o) : this.hasUpdateObjectPermission(o)).collect(Collectors.toList());
    }

    public List<?> getDeletePermittedObjects(List<?> objects) {
        return objects.stream().filter(o -> o instanceof BaseIdentity ? this.hasDeleteObjectPermission((BaseIdentity<?>) o) : this.hasDeleteObjectPermission(o)).collect(Collectors.toList());
    }

}