package com.sunecity.lilium.security.authorization.permission;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sunecity.lilium.security.authorization.policy.Policy;
import com.sunecity.lilium.security.authorization.scope.Scope;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by shahram on 10/11/16.
 */
public class ScopePermission extends Permission {

    private Set<Scope> scopes;

    public ScopePermission(String name, Set<Scope> scopes, Policy... policies) {
        super(name, policies);
        this.scopes = scopes;
    }

    public ScopePermission(String name, Set<Scope> scopes, Set<Policy> policies) {
        super(name, policies);
        this.scopes = scopes;
    }

    public Set<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(Set<Scope> scopes) {
        this.scopes = scopes;
    }

    public void addScope(Scope scope) {
        if (scopes == null) {
            scopes = new HashSet<>();
        }
        scopes.add(scope);
    }

    @Override
    public String getType() {
        return "scope";
    }

    @Override
    public Map<String, String> getConfig() throws Exception {
        Map<String, String> config = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        config.put("scopes", mapper.writeValueAsString(scopes.stream().map(Scope::getId).collect(Collectors.toSet())));
        config.put("applyPolicies", mapper.writeValueAsString(getPolicies().stream().map(Policy::getName).collect(Collectors.toSet())));
        return config;
    }

}
