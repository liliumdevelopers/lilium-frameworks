package com.sunecity.lilium.security.authorization;

import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.security.authorization.scope.Scope;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Resource implements BaseIdentity<String> {

    private String id;
    private String name;
    private String uri;
    private String type;
    private String iconUri;
    private Set<Scope> scopes;

    public Resource(String name, String uri, Scope... scopes) {
        this(name, uri, null, null, scopes);
    }

    public Resource(String name, String uri, Set<Scope> scopes) {
        this(name, uri, null, null, scopes);
    }

    public Resource(String name, String uri, String type, Scope... scopes) {
        this(name, uri, type, null, scopes);
    }

    public Resource(String name, String uri, String type, Set<Scope> scopes) {
        this(name, uri, type, null, scopes);
    }

    public Resource(String name, String uri, String type, String iconUri, Scope... scopes) {
        this.name = name;
        this.uri = uri;
        this.type = type;
        this.iconUri = iconUri;
        this.scopes = Arrays.stream(scopes).collect(Collectors.toSet());
    }

    public Resource(String name, String uri, String type, String iconUri, Set<Scope> scopes) {
        this.name = name;
        this.uri = uri;
        this.type = type;
        this.iconUri = iconUri;
        this.scopes = scopes;
    }

    public Resource(String id, String name, String uri, String type, String iconUri) {
        this.id = id;
        this.name = name;
        this.uri = uri;
        this.type = type;
        this.iconUri = iconUri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIconUri() {
        return iconUri;
    }

    public void setIconUri(String iconUri) {
        this.iconUri = iconUri;
    }

    public Set<Scope> getScopes() {
        if (scopes == null) {
            scopes = new HashSet<>();
        }
        return scopes;
    }

    public void setScopes(Set<Scope> scopes) {
        this.scopes = scopes;
    }

    public void addScope(Scope scope) {
        getScopes().add(scope);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Resource that = (Resource) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Resource{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", uri='" + uri + '\'' +
                ", type='" + type + '\'' +
                ", scopes=" + scopes +
                '}';
    }

}
