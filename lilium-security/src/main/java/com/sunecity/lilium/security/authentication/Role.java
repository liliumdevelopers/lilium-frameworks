package com.sunecity.lilium.security.authentication;

import com.sunecity.lilium.core.model.BaseIdentity;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Role implements BaseIdentity<String> {

    private String id;
    private String name;
    private String description;
    private boolean composite;
    private Composites composites;
    private Boolean clientRole;
    private String containerId;

    public Role(String name) {
        this.name = name;
    }

    public Role(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isComposite() {
        return composite;
    }

    public void setComposite(boolean composite) {
        this.composite = composite;
    }

    public Composites getComposites() {
        return composites;
    }

    public void setComposites(Composites composites) {
        this.composites = composites;
    }

    public Boolean getClientRole() {
        return clientRole;
    }

    public void setClientRole(Boolean clientRole) {
        this.clientRole = clientRole;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (id != null ? !id.equals(role.id) : role.id != null) return false;
        return name != null ? name.equals(role.name) : role.name == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    public static class Composites {

        private Set<String> realm;
        private Map<String, List<String>> client;

        public Set<String> getRealm() {
            return realm;
        }

        public void setRealm(Set<String> realm) {
            this.realm = realm;
        }

        public Map<String, List<String>> getClient() {
            return client;
        }

        public void setClient(Map<String, List<String>> client) {
            this.client = client;
        }

    }

}
