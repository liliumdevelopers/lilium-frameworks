package com.sunecity.lilium.security.authorization.policy;

import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.security.authorization.Logic;
import com.sunecity.lilium.security.authorization.permission.Permission;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

public abstract class Policy implements BaseIdentity<String> {

    private String id;
    private String name;
    private String description;
    private Logic logic = Logic.POSITIVE;
    private Set<Permission> permissions;

    public Policy(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Logic getLogic() {
        return logic;
    }

    public void setLogic(Logic logic) {
        this.logic = logic;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public abstract String getType();

    public abstract Map<String, String> getConfig() throws Exception;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Policy policy = (Policy) o;
        return Objects.equals(getId(), policy.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

}