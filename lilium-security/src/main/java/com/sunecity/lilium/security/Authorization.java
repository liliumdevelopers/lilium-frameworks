package com.sunecity.lilium.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sunecity.lilium.security.authentication.Group;
import com.sunecity.lilium.security.authentication.Role;
import com.sunecity.lilium.security.authentication.User;
import com.sunecity.lilium.security.authorization.DecisionStrategy;
import com.sunecity.lilium.security.authorization.Logic;
import com.sunecity.lilium.security.authorization.Resource;
import com.sunecity.lilium.security.authorization.permission.Permission;
import com.sunecity.lilium.security.authorization.permission.ResourcePermission;
import com.sunecity.lilium.security.authorization.permission.ScopePermission;
import com.sunecity.lilium.security.authorization.policy.AggregatedPolicy;
import com.sunecity.lilium.security.authorization.policy.DroolsPolicy;
import com.sunecity.lilium.security.authorization.policy.JsPolicy;
import com.sunecity.lilium.security.authorization.policy.Policy;
import com.sunecity.lilium.security.authorization.policy.RolePolicy;
import com.sunecity.lilium.security.authorization.policy.TimePolicy;
import com.sunecity.lilium.security.authorization.policy.UserPolicy;
import com.sunecity.lilium.security.authorization.scope.Scope;
import org.aeonbits.owner.ConfigFactory;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.AuthorizationResource;
import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.admin.client.resource.PoliciesResource;
import org.keycloak.admin.client.resource.PolicyResource;
import org.keycloak.admin.client.resource.ResourceResource;
import org.keycloak.admin.client.resource.ResourceScopeResource;
import org.keycloak.admin.client.resource.ResourceScopesResource;
import org.keycloak.admin.client.resource.ResourcesResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.authorization.PolicyRepresentation;
import org.keycloak.representations.idm.authorization.ResourceRepresentation;
import org.keycloak.representations.idm.authorization.ScopeRepresentation;

import javax.enterprise.context.Dependent;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by shahram on 10/9/16.
 */
@Dependent
public class Authorization implements Serializable {

    private ResourceScopesResource scopes;
    private ResourcesResource resources;
    private PoliciesResource policies;
    private Authentication authentication;
    private ObjectMapper mapper;

    public void init(String realm, String client) throws Exception {
        authentication = new Authentication();
        authentication.init(realm, client);
        mapper = new ObjectMapper();
        KeycloakConfig keycloakConfig = ConfigFactory.create(KeycloakConfig.class, System.getProperties(), System.getenv());
        String url = keycloakConfig.url();
        Keycloak kc = KeycloakBuilder.builder().serverUrl(url).realm(realm).username(keycloakConfig.username())
                .password(keycloakConfig.password()).clientId("admin-cli")
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).register(new CustomJacksonProvider()).build())
                .build();
        ClientsResource clientsResource = kc.realm(realm).clients();
        List<ClientRepresentation> clientRepresentations = clientsResource.findByClientId(client);
        if (clientRepresentations != null && !clientRepresentations.isEmpty()) {
            AuthorizationResource resource = clientsResource.get(clientRepresentations.get(0).getId()).authorization();
            scopes = resource.scopes();
            resources = resource.resources();
            policies = resource.policies();
        } else {
            throw new Exception("Client Not Found!");
        }
    }

    public Scope save(Scope scope) throws Exception {
        ScopeRepresentation scopeRepresentation = new ScopeRepresentation(scope.getName(), scope.getIconUri());
        Response response = scopes.create(scopeRepresentation);
        scope.setId(getParams(response).get("id"));
        return scope;
    }

    public void update(Scope scope) throws Exception {
        ResourceScopeResource scopeResource = scopes.scope(scope.getId());
        ScopeRepresentation scopeRepresentation = scopeResource.toRepresentation();
        scopeRepresentation.setName(scope.getName());
        scopeRepresentation.setIconUri(scope.getIconUri());
        scopeResource.update(scopeRepresentation);
    }

    public void deleteScope(String scopeId) throws Exception {
        scopes.scope(scopeId).remove();
    }

    public Scope getScope(String scopeId) throws Exception {
        ScopeRepresentation scopeRepresentation = scopes.scope(scopeId).toRepresentation();
        if (scopeRepresentation != null) {
            Scope scope = new Scope(scopeRepresentation.getName(), scopeRepresentation.getIconUri());
            scope.setId(scopeRepresentation.getId());
        }
        return null;
    }

    public Scope getScopeByName(String name) throws Exception {
        return scopes.scopes().stream().filter(s -> s.getName().equals(name)).findAny().map(s -> new Scope(s.getName(), s.getIconUri())).orElse(null);
    }

    public List<Scope> getAllScopes() throws Exception {
        List<Scope> scopes = new ArrayList<>();
        this.scopes.scopes().forEach(s -> {
            Scope scope = new Scope(s.getName(), s.getIconUri());
            scope.setId(s.getId());
        });
        return scopes;
    }

    public Resource save(Resource resource) throws Exception {
        ResourceRepresentation resourceRepresentation = new ResourceRepresentation();
        resourceRepresentation.setName(resource.getName());
        resourceRepresentation.setUri(resource.getUri());
        resourceRepresentation.setType(resource.getType());
        resourceRepresentation.setIconUri(resource.getIconUri());
        resourceRepresentation.setScopes(resource.getScopes().stream().map(s -> new ScopeRepresentation(s.getName(), s.getIconUri())).collect(Collectors.toSet()));
        Response response = resources.create(resourceRepresentation);
        resource.setId(getParams(response).get("_id"));
        return resource;
    }

    public void update(Resource resource) throws Exception {
        ResourceResource resourceResource = resources.resource(resource.getId());
        ResourceRepresentation resourceRepresentation = resourceResource.toRepresentation();
        resourceRepresentation.setName(resource.getName());
        resourceRepresentation.setUri(resource.getUri());
        resourceRepresentation.setType(resource.getType());
        resourceRepresentation.setIconUri(resource.getIconUri());
//        ResourceRepresentation resourceRepresentation = new ResourceRepresentation(resource.getName(), resource.getScopes().stream().map(s -> {
//            ScopeRepresentation scopeRepresentation = new ScopeRepresentation(s.getName(), s.getIconUri());
//            scopeRepresentation.setId(s.getId());
//            return scopeRepresentation;
//        }).collect(Collectors.toSet()), resource.getUri(), resource.getType(), resource.getIconUri());
        resourceResource.update(resourceRepresentation);
    }

    public void deleteResource(String resourceId) throws Exception {
        resources.resource(resourceId).remove();
    }

    public List<Resource> getResourcesByScope(Scope scope) throws Exception {
        ScopeRepresentation scopeRepresentation = scopes.scopes().stream().filter(s -> s.getName().equals(scope.getName())).findAny().orElse(null);
        if (scopeRepresentation != null) {
            return scopeRepresentation.getResources().stream().map(r -> new Resource(r.getId(), r.getName(), r.getUri(), r.getType(), r.getIconUri())).collect(Collectors.toList());
        }
        return null;
    }

    public Resource getResource(String resourceId) throws Exception {
        ResourceRepresentation resourceRepresentation = resources.resource(resourceId).toRepresentation();
        if (resourceRepresentation != null) {
            Resource resource = new Resource(resourceRepresentation.getName(), resourceRepresentation.getUri(), resourceRepresentation.getType(), resourceRepresentation.getIconUri());
            resource.setId(resourceRepresentation.getId());
        }
        return null;
    }

    public Resource getResourceByName(String name) throws Exception {
        ResourceRepresentation resourceRepresentation = resources.resources().stream().filter(r -> name.equals(r.getName())).findAny().orElse(null);
        if (resourceRepresentation != null) {
            return new Resource(resourceRepresentation.getName(), resourceRepresentation.getUri(), resourceRepresentation.getType(), resourceRepresentation.getIconUri());
        }
        return null;
    }

    public Resource getResourceByUri(String uri) throws Exception {
        ResourceRepresentation resourceRepresentation = resources.resources().stream().filter(r -> uri.equals(r.getUri())).findAny().orElse(null);
        if (resourceRepresentation != null) {
            return new Resource(resourceRepresentation.getName(), resourceRepresentation.getUri(), resourceRepresentation.getType(), resourceRepresentation.getIconUri());
        }
        return null;
    }

    public List<Resource> getAllResources() throws Exception {
        List<Resource> resources = new ArrayList<>();
        List<ResourceRepresentation> resourceRepresentations = this.resources.resources();
        for (ResourceRepresentation resourceRepresentation : resourceRepresentations) {
            Resource resource = new Resource(resourceRepresentation.getName(), resourceRepresentation.getUri(), resourceRepresentation.getType(), resourceRepresentation.getIconUri());
            resource.setId(resourceRepresentation.getId());
            resources.add(resource);
        }
        return resources;
    }

    public Policy save(Policy policy) throws Exception {
        PolicyRepresentation policyRepresentation = new PolicyRepresentation();
        policyRepresentation.setName(policy.getName());
        policyRepresentation.setDescription(policy.getDescription());
        policyRepresentation.setLogic(getLogic(policy.getLogic()));
        policyRepresentation.setType(policy.getType());
        policyRepresentation.setConfig(policy.getConfig());
        if (policy instanceof AggregatedPolicy) {
            policyRepresentation.setDecisionStrategy(getDecisionStrategy(((AggregatedPolicy) policy).getDecisionStrategy()));
        }
        Response response = policies.create(policyRepresentation);
        policy.setId(getParams(response).get("id"));
        return policy;
    }

    public void update(Policy policy) throws Exception {
        PolicyResource policyResource = policies.policy(policy.getId());
        PolicyRepresentation policyRepresentation = policyResource.toRepresentation();
        policyRepresentation.setName(policy.getName());
        policyRepresentation.setDescription(policy.getDescription());
        policyRepresentation.setLogic(getLogic(policy.getLogic()));
        policyRepresentation.setConfig(policy.getConfig());
        policyResource.update(policyRepresentation);
    }

    public void deletePolicy(String policyId) throws Exception {
        policies.policy(policyId).remove();
    }

    public Policy getPolicy(String policyId) throws Exception {
        return getPolicy(policies.policy(policyId).toRepresentation());
    }

    public List<Policy> getAllPolicies() throws Exception {
        List<Policy> policies = new ArrayList<>();
        for (PolicyRepresentation policyRepresentation : this.policies.policies()) {
            Policy policy = getPolicy(policyRepresentation);
            if (policy != null) {
                Set<Permission> permissions = new HashSet<>();
//                for (PolicyRepresentation representation : policyRepresentation.getDependentPolicies()) {
//                    Permission permission = getPermission(representation);
//                    if (permission != null) {
//                        permissions.add(permission);
//                    }
//                }
                policy.setPermissions(permissions);
                policies.add(policy);
            }
        }
        return policies;
    }

    public Permission save(Permission permission) throws Exception {
        PolicyRepresentation permissionRepresentation = new PolicyRepresentation();
        permissionRepresentation.setName(permission.getName());
        permissionRepresentation.setDescription(permission.getDescription());
        permissionRepresentation.setDecisionStrategy(getDecisionStrategy(permission.getDecisionStrategy()));
        permissionRepresentation.setType(permission.getType());
        permissionRepresentation.setConfig(permission.getConfig());
        Response response = policies.create(permissionRepresentation);
        permission.setId(getParams(response).get("id"));
        return permission;
    }

    public void update(Permission permission) throws Exception {
        PolicyResource permissionResource = policies.policy(permission.getId());
        PolicyRepresentation permissionRepresentation = permissionResource.toRepresentation();
        permissionRepresentation.setName(permission.getName());
        permissionRepresentation.setDescription(permission.getDescription());
        permissionRepresentation.setDecisionStrategy(getDecisionStrategy(permission.getDecisionStrategy()));
        permissionRepresentation.setConfig(permission.getConfig());
        permissionResource.update(permissionRepresentation);
    }

    public void deletePermission(String permissionId) throws Exception {
        policies.policy(permissionId).remove();
    }

//    public void deletePermissionAndPolicies(Permission permission) throws Exception {
//        policies.policy(permission.getId()).remove();
//        for (Policy policy : permission.getPolicies()) {
//            policies.policy(policy.getId()).remove();
//        }
//    }

    public Permission getPermission(String permissionId) throws Exception {
        return getPermission(policies.policy(permissionId).toRepresentation());
    }

    public List<Permission> getAllPermissions() throws Exception {
        List<Permission> permissions = new ArrayList<>();
        for (PolicyRepresentation policyRepresentation : this.policies.policies()) {
            Permission permission = getPermission(policyRepresentation);
            if (permission != null) {
                Set<Policy> policies = new HashSet<>();
//                for (PolicyRepresentation representation : policyRepresentation.getAssociatedPolicies()) {
//                    Policy policy = getPolicy(representation);
//                    if (policy != null) {
//                        policies.add(policy);
//                    }
//                }
                permission.setPolicies(policies);
                permissions.add(permission);
            }
        }
        return permissions;
    }

    private Policy getPolicy(PolicyRepresentation policyRepresentation) throws Exception {
        if (policyRepresentation.getType().equals("aggregated")) {
            AggregatedPolicy policy = new AggregatedPolicy(policyRepresentation.getName());
            policy.setId(policyRepresentation.getId());
            policy.setDescription(policyRepresentation.getDescription());
            policy.setLogic(getLogic(policyRepresentation.getLogic()));
            policy.setDecisionStrategy(getDecisionStrategy(policyRepresentation.getDecisionStrategy()));
            Map<String, String> config = policyRepresentation.getConfig();
            String applyPolicies = config.get("applyPolicies");
            //policyRepresentation.getAssociatedPolicies();
//            policies.policy("").
            return policy;
        } else if (policyRepresentation.getType().equals("drools")) {
            DroolsPolicy policy = new DroolsPolicy(policyRepresentation.getName(), "");
            policy.setId(policyRepresentation.getId());
            policy.setDescription(policyRepresentation.getDescription());
            policy.setLogic(getLogic(policyRepresentation.getLogic()));
            Map<String, String> config = policyRepresentation.getConfig();
            return policy;
        } else if (policyRepresentation.getType().equals("js")) {
            Map<String, String> config = policyRepresentation.getConfig();
            JsPolicy policy = new JsPolicy(policyRepresentation.getName(), config.get("code"));
            policy.setId(policyRepresentation.getId());
            policy.setDescription(policyRepresentation.getDescription());
            policy.setLogic(getLogic(policyRepresentation.getLogic()));
            return policy;
        } else if (policyRepresentation.getType().equals("role")) {
            RolePolicy policy = new RolePolicy(policyRepresentation.getName());
            policy.setId(policyRepresentation.getId());
            policy.setDescription(policyRepresentation.getDescription());
            policy.setLogic(getLogic(policyRepresentation.getLogic()));
            Map<String, String> config = policyRepresentation.getConfig();
            for (Map<String, String> role : mapper.readValue(config.get("roles"), Map[].class)) {
                policy.addRole(authentication.getRoleById(role.get("id")));
            }
            return policy;
        } else if (policyRepresentation.getType().equals("time")) {
            TimePolicy policy = new TimePolicy(policyRepresentation.getName());
            policy.setId(policyRepresentation.getId());
            policy.setDescription(policyRepresentation.getDescription());
            policy.setLogic(getLogic(policyRepresentation.getLogic()));
            Map<String, String> config = policyRepresentation.getConfig();
            return policy;
        } else if (policyRepresentation.getType().equals("user")) {
            UserPolicy policy = new UserPolicy(policyRepresentation.getName());
            policy.setId(policyRepresentation.getId());
            policy.setDescription(policyRepresentation.getDescription());
            policy.setLogic(getLogic(policyRepresentation.getLogic()));
            Map<String, String> config = policyRepresentation.getConfig();
            for (String user : mapper.readValue(config.get("users"), String[].class)) {
                policy.addUser(authentication.getUser(user));
            }
            return policy;
        }
        return null;
    }

    private Permission getPermission(PolicyRepresentation policyRepresentation) throws Exception {
        if (policyRepresentation.getType().equals("resource")) {
            ResourcePermission permission = new ResourcePermission(policyRepresentation.getName(), new HashSet<>());
            permission.setId(policyRepresentation.getId());
            permission.setDescription(policyRepresentation.getDescription());
            permission.setDecisionStrategy(getDecisionStrategy(policyRepresentation.getDecisionStrategy()));
            permission.setResourceType(policyRepresentation.getType());
            Map<String, String> config = policyRepresentation.getConfig();
            if (config.get("resources") != null) {
                for (String resource : mapper.readValue(config.get("resources"), String[].class)) {
//                    permission.getResources().add(getResource(resource));
                }
            }
            return permission;
        } else if (policyRepresentation.getType().equals("scope")) {
            ScopePermission permission = new ScopePermission(policyRepresentation.getName(), new HashSet<>());
            permission.setId(policyRepresentation.getId());
            permission.setDescription(policyRepresentation.getDescription());
            permission.setDecisionStrategy(getDecisionStrategy(policyRepresentation.getDecisionStrategy()));
            Map<String, String> config = policyRepresentation.getConfig();
            for (String scope : mapper.readValue(config.get("scopes"), String[].class)) {
//                permission.getScopes().add(getScope(scope));
            }
            return permission;
        }
        return null;
    }

    private org.keycloak.representations.idm.authorization.DecisionStrategy getDecisionStrategy(DecisionStrategy decisionStrategy) throws Exception {
        switch (decisionStrategy) {
            case AFFIRMATIVE:
                return org.keycloak.representations.idm.authorization.DecisionStrategy.AFFIRMATIVE;
            case CONSENSUS:
                return org.keycloak.representations.idm.authorization.DecisionStrategy.CONSENSUS;
            default:
                return org.keycloak.representations.idm.authorization.DecisionStrategy.UNANIMOUS;
        }
    }

    private DecisionStrategy getDecisionStrategy(org.keycloak.representations.idm.authorization.DecisionStrategy decisionStrategy) throws Exception {
        switch (decisionStrategy) {
            case AFFIRMATIVE:
                return DecisionStrategy.AFFIRMATIVE;
            case CONSENSUS:
                return DecisionStrategy.CONSENSUS;
            default:
                return DecisionStrategy.UNANIMOUS;
        }
    }

    private org.keycloak.representations.idm.authorization.Logic getLogic(Logic logic) throws Exception {
        return logic == Logic.POSITIVE ? org.keycloak.representations.idm.authorization.Logic.POSITIVE : org.keycloak.representations.idm.authorization.Logic.NEGATIVE;
    }

    private Logic getLogic(org.keycloak.representations.idm.authorization.Logic logic) throws Exception {
        return logic == org.keycloak.representations.idm.authorization.Logic.POSITIVE ? Logic.POSITIVE : Logic.NEGATIVE;
    }

    private Map<String, String> getParams(Response response) throws Exception {
        if (response.getStatus() != 201) {
            String error = response.readEntity(String.class);
            throw new Exception(error);
        }
        return response.readEntity(Map.class);
    }

    public List<Permission> getPermissions(User user) throws Exception {

        return null;
    }

    public List<Permission> getPermissions(Group group) throws Exception {

        return null;
    }

    public List<Permission> getPermissions(Role role) throws Exception {

        return null;
    }

}
