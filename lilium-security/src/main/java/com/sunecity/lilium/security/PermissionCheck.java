package com.sunecity.lilium.security;

import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.security.authorization.Resource;
import com.sunecity.lilium.security.authorization.scope.ClassScope;
import com.sunecity.lilium.security.authorization.scope.FieldScope;
import com.sunecity.lilium.security.authorization.scope.ObjectScope;
import com.sunecity.lilium.security.authorization.scope.PageScope;
import org.keycloak.AuthorizationContext;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Named
@SessionScoped
public class PermissionCheck implements Serializable {

    private AuthorizationContext authorizationContext;
    private Set<String> realmRoles;
    private Set<String> clientRoles;
    private Set<Resource> pages;

    public void initKeycloakPrincipal(KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal) throws Exception {
        realmRoles = new HashSet<>();
        clientRoles = new HashSet<>();
        pages = new HashSet<>();
        if (keycloakPrincipal != null) {
            KeycloakSecurityContext securityContext = keycloakPrincipal.getKeycloakSecurityContext();
            authorizationContext = securityContext.getAuthorizationContext();
            AccessToken token = securityContext.getToken();
            String client = token.getIssuedFor();
            AccessToken.Access realmAccess = token.getRealmAccess();
            AccessToken.Access resourceAccess = token.getResourceAccess(client);
            if (realmAccess != null) {
                realmRoles.addAll(realmAccess.getRoles());
            }
            if (resourceAccess != null) {
                clientRoles.addAll(resourceAccess.getRoles());
            }
        }
    }

    public boolean isInitialized() {
        return authorizationContext != null;
    }

    public boolean hasRole(String role) {
        return realmRoles.contains(role) || clientRoles.contains(role);
    }

    public boolean hasRealmRole(String role) {
        return realmRoles.contains(role);
    }

    public boolean hasClientRole(String role) {
        return clientRoles.contains(role);
    }

    public boolean hasPermission(String resource) {
        return authorizationContext.hasResourcePermission(resource);
    }

    public boolean hasPermission(String resource, String scope) {
        return authorizationContext.hasPermission(resource, scope);
    }

    public boolean hasPagePermission(String page) {
        return authorizationContext.hasPermission(page, PageScope.VIEW.scope());
    }

    public boolean hasPageUrlPermission(String url) {
        Resource page = pages.stream().filter(p -> p.getUri().endsWith(url)).findAny().orElse(null);
        return page != null && authorizationContext.hasPermission(page.getName(), PageScope.VIEW.scope());
    }

    public boolean isPageDisabled(String page) {
        return authorizationContext.hasPermission(page, PageScope.DISABLE.scope());
    }

    public boolean hasCreateFieldPermission(String field) {
        return authorizationContext.hasPermission(field, FieldScope.CREATE.scope()) && !authorizationContext.hasPermission(field, FieldScope.DENY_CREATE.scope());
    }

    public boolean hasReadFieldPermission(String field) {
        return authorizationContext.hasPermission(field, FieldScope.READ.scope()) && !authorizationContext.hasPermission(field, FieldScope.DENY_READ.scope());
    }

    public boolean hasUpdateFieldPermission(String field) {
        return authorizationContext.hasPermission(field, FieldScope.UPDATE.scope()) && !authorizationContext.hasPermission(field, FieldScope.DENY_UPDATE.scope());
    }

    public boolean hasDeleteFieldPermission(String field) {
        return authorizationContext.hasPermission(field, FieldScope.DELETE.scope()) && !authorizationContext.hasPermission(field, FieldScope.DENY_DELETE.scope());
    }

    public boolean isFieldDisabled(String field) {
        return authorizationContext.hasPermission(field, FieldScope.DISABLE.scope());
    }

    public boolean hasCreateClassPermission(String className) {
        try {
            Class<?> clas = Class.forName(className);
            return hasCreateClassPermission(clas);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public boolean hasCreateClassPermission(Class<?> clas) {
        return authorizationContext.hasPermission(clas.getName(), ClassScope.CREATE.scope()) && !authorizationContext.hasPermission(clas.getName(), ClassScope.DENY_CREATE.scope());
    }

    public boolean hasReadClassPermission(String className) {
        try {
            Class<?> clas = Class.forName(className);
            return hasReadClassPermission(clas);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public boolean hasReadClassPermission(Class<?> clas) {
        return authorizationContext.hasPermission(clas.getName(), ClassScope.READ.scope()) && !authorizationContext.hasPermission(clas.getName(), ClassScope.DENY_READ.scope());
    }

    public boolean hasUpdateClassPermission(String className) {
        try {
            Class<?> clas = Class.forName(className);
            return hasUpdateClassPermission(clas);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public boolean hasUpdateClassPermission(Class<?> clas) {
        return authorizationContext.hasPermission(clas.getName(), ClassScope.UPDATE.scope()) && !authorizationContext.hasPermission(clas.getName(), ClassScope.DENY_UPDATE.scope());
    }

    public boolean hasDeleteClassPermission(String className) {
        try {
            Class<?> clas = Class.forName(className);
            return hasDeleteClassPermission(clas);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public boolean hasDeleteClassPermission(Class<?> clas) {
        return authorizationContext.hasPermission(clas.getName(), ClassScope.DELETE.scope()) && !authorizationContext.hasPermission(clas.getName(), ClassScope.DENY_DELETE.scope());
    }

    public boolean hasReadObjectPermission(BaseIdentity<? extends Serializable> object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.READ.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.DENY_READ.scope());
    }

    public boolean hasUpdateObjectPermission(BaseIdentity<? extends Serializable> object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.UPDATE.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.DENY_UPDATE.scope());
    }

    public boolean hasDeleteObjectPermission(BaseIdentity<? extends Serializable> object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.DELETE.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object.getId(), ObjectScope.DENY_DELETE.scope());
    }

    public boolean hasReadObjectPermission(Object object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.READ.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.DENY_READ.scope());
    }

    public boolean hasUpdateObjectPermission(Object object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.UPDATE.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.DENY_UPDATE.scope());
    }

    public boolean hasDeleteObjectPermission(Object object) {
        return authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.DELETE.scope()) && !authorizationContext.hasPermission(object.getClass().getName() + object, ObjectScope.DENY_DELETE.scope());
    }

    public List<?> getReadPermittedObjects(List<?> objects) {
        return objects.stream().filter(o -> o instanceof BaseIdentity ? this.hasReadObjectPermission((BaseIdentity<?>) o) : this.hasReadObjectPermission(o)).collect(Collectors.toList());
    }

    public List<?> getUpdatePermittedObjects(List<?> objects) {
        return objects.stream().filter(o -> o instanceof BaseIdentity ? this.hasUpdateObjectPermission((BaseIdentity<?>) o) : this.hasUpdateObjectPermission(o)).collect(Collectors.toList());
    }

    public List<?> getDeletePermittedObjects(List<?> objects) {
        return objects.stream().filter(o -> o instanceof BaseIdentity ? this.hasDeleteObjectPermission((BaseIdentity<?>) o) : this.hasDeleteObjectPermission(o)).collect(Collectors.toList());
    }

}