package com.sunecity.lilium.security.authorization.policy;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.sunecity.lilium.security.authentication.Role;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by shahram on 10/11/16.
 */
public class RolePolicy extends Policy {

    private Set<Role> roles;

    public RolePolicy(String name, Role... roles) {
        super(name);
        this.roles = Arrays.stream(roles).collect(Collectors.toSet());
    }

    public RolePolicy(String name, Set<Role> roles) {
        super(name);
        this.roles = roles;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role) {
        if (roles == null) {
            roles = new HashSet<>();
        }
        roles.add(role);
    }

    @Override
    public String getType() {
        return "role";
    }

    @Override
    public Map<String, String> getConfig() throws Exception {
        Map<String, String> config = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.addMixIn(Object.class, PropertyFilterMixIn.class);
        SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept(("id"));
        FilterProvider filters = new SimpleFilterProvider().addFilter("filter", filter);
        config.put("roles", mapper.writer(filters).writeValueAsString(roles));
        return config;
    }

    @JsonFilter("filter")
    private class PropertyFilterMixIn {
    }

}
