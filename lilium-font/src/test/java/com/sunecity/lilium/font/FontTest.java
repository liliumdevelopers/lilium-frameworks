package com.sunecity.lilium.font;

import org.junit.Assert;
import org.junit.Test;

public class FontTest {

    @Test
    public void url() {
        Assert.assertTrue(FontType.ARIAL.getUrl().getPath().endsWith("Arial.ttf"));
    }

}
