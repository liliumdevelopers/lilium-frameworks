package com.sunecity.lilium.font;

/**
 * Created by shahram on 9/6/16.
 */
public interface Font {

    String getName();

    byte[] getBytes();

}
