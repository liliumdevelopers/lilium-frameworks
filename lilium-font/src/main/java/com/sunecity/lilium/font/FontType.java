package com.sunecity.lilium.font;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * @author Shahram Goodarzi
 */
public enum FontType implements Font {

    ARIAL("Arial/Arial.ttf"),
    COURIER_NEW("Courier_New/Courier_New.ttf"),
    DEJAVU_SANS("DejaVuSans/DejaVuSans.ttf"),
    DEJAVU_SERIF("DejaVuSerif/DejaVuSerif.ttf"),
    TIMES("Times/Times_New_Roman.ttf"),
    TAHOMA("Tahoma/tahoma.ttf"),
    LUTUS("Lotus/IRLotus.ttf"),
    NAZANIN("Nazanin/BNazanin.ttf"),
    TITR("Titr/IRTitr.ttf"),
    YEKAN("Yekan/IRYekan.ttf"),
    ZAR("Zar/IRZar.ttf"),
    NASTALIQ("Nastaliq/IranNastaliq.ttf");

    private final String file;

    FontType(String file) {
        this.file = file;
    }

    public String getName() {
        return file.substring(file.lastIndexOf('/'));
    }

    public InputStream getAsStream() {
        return FontType.class.getResourceAsStream("/fonts/" + file);
    }

    public byte[] getBytes() {
        try (InputStream inputStream = getAsStream(); ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            inputStream.transferTo(outputStream);
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public URL getUrl() {
        return FontType.class.getResource("/fonts/" + file);
    }

}