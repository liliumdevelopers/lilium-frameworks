import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by shahram on 4/29/17.
 */
public class JsonTest {

    public static void main(String[] args) {
        try {
//            System.out.println(new ObjectMapper().writer().writeValueAsString(new Model("shahram")));
//            System.out.println(new ObjectMapper().writer().writeValueAsString(new Model("shahram",
//                    new BigDecimal("122222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222312.12"))));
            System.out.println(new ObjectMapper().readerFor(Model.class).<Model>readValue("{\"name\":\"shahram\",\"age\":12.12}").getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
//        byte a=127;
//        System.out.println(++a);
    }

    static class Model {

        private String name;
        private BigDecimal age;

        public Model() {
        }

        public Model(String name, BigDecimal age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

//        public void setName(String name) {
//            this.name = name;
//        }

        public BigDecimal getAge() {
            return age;
        }

        public void setAge(BigDecimal age) {
            this.age = age;
        }
    }

}
