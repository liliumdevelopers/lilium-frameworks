package com.sunecity.lilium.rest.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sunecity.lilium.core.exception.ConcurrentException;
import com.sunecity.lilium.core.exception.ForeignKeyException;
import com.sunecity.lilium.core.exception.UniqueException;
import com.sunecity.lilium.core.exception.ValidationException;
import com.sunecity.lilium.core.validation.ValidateMessage;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

/**
 * Created by shahram on 7/23/16.
 */
public class ClientExceptionFilter implements ClientResponseFilter {

    @Override
    public void filter(ClientRequestContext clientRequestContext, ClientResponseContext clientResponseContext) throws IOException {
        if (clientResponseContext.getStatus() == Response.Status.BAD_REQUEST.getStatusCode()) {
            ObjectMapper mapper = new ObjectMapper();
            List<ValidateMessage> messages = mapper.readValue(clientResponseContext.getEntityStream(), new TypeReference<List<ValidateMessage>>() {
            });
            throw new ValidationException(Response.status(Response.Status.BAD_REQUEST).build(), messages);
        } else if (clientResponseContext.getStatus() == Response.Status.CONFLICT.getStatusCode()) {
            if (UniqueException.class.getSimpleName().equals(clientResponseContext.getHeaders().getFirst("Exception"))) {
                throw new UniqueException(Response.status(Response.Status.CONFLICT).build(), "", "");
            } else if (ForeignKeyException.class.getSimpleName().equals(clientResponseContext.getHeaders().getFirst("Exception"))) {
                throw new ForeignKeyException(Response.status(Response.Status.CONFLICT).build(), "", "");
            }
        } else if (clientResponseContext.getStatus() == Response.Status.PRECONDITION_FAILED.getStatusCode()) {
            throw new ConcurrentException(Response.status(Response.Status.PRECONDITION_FAILED).build(), "");
        }
    }

}
