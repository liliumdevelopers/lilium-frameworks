package com.sunecity.lilium.rest.client;

import com.sunecity.lilium.core.config.RestConfig;
import com.sunecity.lilium.core.inject.AbstractLocator;
import com.sunecity.lilium.rest.ext.JsonContextResolver;
import com.sunecity.lilium.rest.ext.JsonParamConverterProvider;
import org.aeonbits.owner.ConfigFactory;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.admin.client.resource.BearerAuthFilter;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by shahram on 8/4/16.
 */
public class RestLocator extends AbstractLocator {

    @Inject
    private HttpServletRequest request;

    private ResteasyWebTarget target;

    public RestLocator() {
    }

    @Override
    protected <T> T lookup(Class<T> clas) {
        try {
            if (target == null) {
                initRest();
            }
            return target.proxy(clas);
        } catch (Exception e) {
            logger.error("Can not Connect to Remote Rest Service.", e);
            return null;
        }
    }

    private void initRest() throws Exception {
        ResteasyClient client = new ResteasyClientBuilder().connectionPoolSize(10)
                .register(JsonParamConverterProvider.class).register(JsonContextResolver.class)
                .register(ServerExceptionFilter.class).register(ClientExceptionFilter.class).build();
        target = client.target(ConfigFactory.create(RestConfig.class).url());
        Object o = request.getAttribute(KeycloakSecurityContext.class.getName());
        if (o != null) {
            target = target.register(new BearerAuthFilter(((KeycloakSecurityContext) o).getTokenString()));
        }
    }

}
