package com.sunecity.lilium.rest.filter;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

//@Provider
public class CorsResponseFilter implements ContainerResponseFilter {

    private static final List<String> ALLOWED_HEADERS = List.of("origin", "accept", "content-type", "authorization");

    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        containerResponseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
        containerResponseContext.getHeaders().add("Access-Control-Allow-Headers", getAllowedHeaders(containerRequestContext));
        containerResponseContext.getHeaders().add("Access-Control-Allow-Credentials", "true");
        containerResponseContext.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        containerResponseContext.getHeaders().add("Access-Control-Max-Age", "1209600");
    }

    private String getAllowedHeaders(ContainerRequestContext responseContext) {
        List<String> headers = responseContext.getHeaders().get("Access-Control-Request-Headers");
        if (headers != null) {
            headers.addAll(ALLOWED_HEADERS);
            return headers.stream().collect(Collectors.joining(", "));
        }
        return ALLOWED_HEADERS.stream().collect(Collectors.joining(", "));
    }

}
