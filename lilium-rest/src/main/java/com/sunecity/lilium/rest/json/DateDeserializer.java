package com.sunecity.lilium.rest.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.sunecity.lilium.core.time.Date;
import com.sunecity.lilium.core.util.Numbers;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

/**
 * Created by shahram on 7/20/16.
 */
public class DateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        return Date.parse(Numbers.getTextEnglish(jsonParser.getValueAsString()), DateTimeFormatter.ISO_DATE_TIME);
    }

}
