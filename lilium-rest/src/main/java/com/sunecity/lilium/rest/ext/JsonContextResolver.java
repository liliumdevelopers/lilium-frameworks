package com.sunecity.lilium.rest.ext;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.sunecity.lilium.rest.json.LiliumModule;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * Created by shahram on 9/8/16.
 */
@Provider
public class JsonContextResolver implements ContextResolver<ObjectMapper> {

    private final ObjectMapper mapper;

    public JsonContextResolver() {
        mapper = new ObjectMapper();
        mapper.registerModules(new LiliumModule(), new Hibernate5Module());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public ObjectMapper getContext(Class<?> clas) {
        return mapper;
    }

}
