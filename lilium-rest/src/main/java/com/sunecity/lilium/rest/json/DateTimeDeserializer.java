package com.sunecity.lilium.rest.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.sunecity.lilium.core.time.DateTime;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

/**
 * Created by shahram on 7/20/16.
 */
public class DateTimeDeserializer extends JsonDeserializer<DateTime> {

    @Override
    public DateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        return DateTime.parse(jsonParser.getValueAsString(), formatter);
    }

}
