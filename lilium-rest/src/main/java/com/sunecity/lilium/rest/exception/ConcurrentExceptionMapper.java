package com.sunecity.lilium.rest.exception;

import com.sunecity.lilium.core.exception.ConcurrentException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by shahram on 7/23/16.
 */
@Provider
public class ConcurrentExceptionMapper implements ExceptionMapper<ConcurrentException> {

    @Override
    public Response toResponse(ConcurrentException e) {
        return Response.status(Response.Status.PRECONDITION_FAILED).entity(e.getMessage())
                .type(MediaType.APPLICATION_JSON_TYPE).header("Exception", e.getClass().getSimpleName()).build();
    }

}
