package com.sunecity.lilium.rest.filter;

import com.sunecity.lilium.rest.annotation.Created;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Created by shahram on 1/16/17.
 */
@Provider
@Created
public class CreatedFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        if (containerResponseContext.getStatus() == 200) {
            containerResponseContext.getHeaders().putSingle(HttpHeaders.LOCATION, containerRequestContext.getUriInfo().getAbsolutePathBuilder().path("/{id}").build(containerResponseContext.getEntity()));
            containerResponseContext.setStatusInfo(Response.Status.CREATED);
        }
    }

}
