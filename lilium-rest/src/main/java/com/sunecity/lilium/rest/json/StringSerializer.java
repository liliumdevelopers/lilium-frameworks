package com.sunecity.lilium.rest.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.sunecity.lilium.core.util.Numbers;

import java.io.IOException;

/**
 * Created by shahram on 7/20/16.
 */
public class StringSerializer extends JsonSerializer<String> {

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
//        if (serializers.getLocale().getLanguage().equals("fa")) {
//            gen.writeString(Numbers.getTextPersian(value));
//        } else {
            gen.writeString(value);
//        }
    }

}
