package com.sunecity.lilium.rest.filter;

import com.sunecity.lilium.rest.annotation.Paging;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;

/**
 * Created by shahram on 1/16/17.
 */
@Provider
@Paging
public class PagingFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        if (containerResponseContext.getStatus() == 200) {
            UriInfo uriInfo = containerRequestContext.getUriInfo();
            int firstParam = Integer.parseInt(uriInfo.getQueryParameters().getFirst("first"));
            int maxParam = Integer.parseInt(uriInfo.getQueryParameters().getFirst("max"));
            String metaData = uriInfo.getQueryParameters().getFirst("metaData");

            URI uri = uriInfo.getAbsolutePathBuilder().path("/count").build();
            Client client = ClientBuilder.newClient();
            Response response = client.target(uri).queryParam("metaData", URLEncoder.encode(metaData, "UTF-8")).request().get();
            long count = response.readEntity(long.class);
            response.close();
            client.close();

            if (firstParam > 1) {
                Link first = Link.fromUri(uriInfo.getAbsolutePathBuilder()
                        .queryParam("metaData", "{metaData}")
                        .queryParam("first", 1)
                        .queryParam("max", maxParam)
                        .build(metaData)).rel("first").build();
                containerResponseContext.getLinks().add(first);

                Link previous = Link.fromUri(uriInfo.getAbsolutePathBuilder()
                        .queryParam("metaData", "{metaData}")
                        .queryParam("first", firstParam - 1)
                        .queryParam("max", maxParam)
                        .build(metaData)).rel("previous").build();
                containerResponseContext.getLinks().add(previous);
            }

            for (int i = 0; i < 10; i++) {
                Link link = Link.fromUri(uriInfo.getAbsolutePathBuilder()
                        .queryParam("metaData", "{metaData}")
                        .queryParam("first", i)
                        .queryParam("max", maxParam)
                        .build(metaData)).rel("index" + i).build();
                containerResponseContext.getLinks().add(link);
            }

            Link next = Link.fromUri(uriInfo.getAbsolutePathBuilder()
                    .queryParam("metaData", "{metaData}")
                    .queryParam("first", firstParam + 1)
                    .queryParam("max", maxParam)
                    .build(metaData)).rel("next").build();
            containerResponseContext.getLinks().add(next);

            if (count > firstParam * maxParam) {
                Link last = Link.fromUri(uriInfo.getAbsolutePathBuilder()
                        .queryParam("metaData", "{metaData}")
                        .queryParam("first", count - (firstParam * maxParam))
                        .queryParam("max", maxParam)
                        .build(metaData)).rel("last").build();
                containerResponseContext.getLinks().add(last);
            }
        }
    }

}
