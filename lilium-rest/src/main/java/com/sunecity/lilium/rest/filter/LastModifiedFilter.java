package com.sunecity.lilium.rest.filter;

import com.sunecity.lilium.core.model.AuditLog;
import com.sunecity.lilium.core.time.DateTime;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * Created by shahram on 1/16/17.
 */
@Provider
public class LastModifiedFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        if (containerResponseContext.getStatus() == 200) {
            Object entity = containerResponseContext.getEntity();
            if (entity instanceof AuditLog) {
                AuditLog log = (AuditLog) entity;
                containerResponseContext.getHeaders().putSingle(HttpHeaders.LAST_MODIFIED, log.getUpdateTimestamp() != null ? log.getUpdateTimestamp() : log.getCreateTimestamp());
            } else if (entity instanceof Collection<?>) {
                Collection<?> entities = (Collection<?>) entity;
                if (entities.iterator().hasNext()) {
                    if (entities.iterator().next() instanceof AuditLog) {
                        AuditLog log = Collections.min(entities.stream().map(AuditLog.class::cast).collect(Collectors.toList()), (o11, o2) -> {
                            DateTime first = o11.getUpdateTimestamp() != null ? o11.getUpdateTimestamp() : o11.getCreateTimestamp();
                            DateTime second = o2.getUpdateTimestamp() != null ? o2.getUpdateTimestamp() : o2.getCreateTimestamp();
                            return first.compareTo(second);
                        });
                        containerResponseContext.getHeaders().putSingle(HttpHeaders.LAST_MODIFIED, log.getUpdateTimestamp() != null ? log.getUpdateTimestamp() : log.getCreateTimestamp());
                    }
                }
            }
        }
    }

}
