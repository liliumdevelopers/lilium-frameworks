package com.sunecity.lilium.rest.interceptor;

import com.sunecity.lilium.core.model.AuditLog;
import com.sunecity.lilium.core.model.BaseVersion;
import com.sunecity.lilium.rest.annotation.Update;
import org.keycloak.KeycloakPrincipal;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.IOException;

/**
 * Created by shahram on 1/16/17.
 */
@Provider
@Update
public class UpdateInterceptor implements ReaderInterceptor, WriterInterceptor {

    @Context
    private SecurityContext securityContext;

    @Override
    public Object aroundReadFrom(ReaderInterceptorContext readerInterceptorContext) throws IOException, WebApplicationException {
        Object o = readerInterceptorContext.proceed();
        if (o instanceof AuditLog) {
            ((AuditLog) o).setUpdateUser(((KeycloakPrincipal) securityContext.getUserPrincipal()).getKeycloakSecurityContext().getToken().getPreferredUsername());
        }
        return o;
    }

    @Override
    public void aroundWriteTo(WriterInterceptorContext writerInterceptorContext) throws IOException, WebApplicationException {
        Object o = writerInterceptorContext.getEntity();
        if (o instanceof BaseVersion<?>) {
            writerInterceptorContext.setEntity(((BaseVersion<?>) o).getVersion());
        }
    }

}
