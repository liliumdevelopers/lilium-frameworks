package com.sunecity.lilium.rest.exception;

import com.sunecity.lilium.core.validation.ValidateMessage;

import javax.validation.ConstraintViolationException;
import javax.validation.ElementKind;
import javax.validation.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by shahram on 1/28/17.
 */
@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    public Response toResponse(ConstraintViolationException constraintViolationException) {
        List<ValidateMessage> messages = constraintViolationException.getConstraintViolations().stream()
                .map(v -> new ValidateMessage(v.getLeafBean().getClass().getSimpleName(), getName(v.getPropertyPath()), v.getMessage())).collect(Collectors.toList());
        return Response.status(Response.Status.BAD_REQUEST).entity(messages).build();
    }

    private String getName(Path nodes) {
        for (Path.Node node : nodes) {
            if (node.getKind() == ElementKind.PROPERTY) {
                return node.getName();
            }
        }
        return null;
    }

}