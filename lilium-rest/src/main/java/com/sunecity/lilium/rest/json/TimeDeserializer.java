package com.sunecity.lilium.rest.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.sunecity.lilium.core.time.Time;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

/**
 * Created by shahram on 7/20/16.
 */
public class TimeDeserializer extends JsonDeserializer<Time> {

    @Override
    public Time deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        return Time.parse(jsonParser.getValueAsString(), formatter);
    }

}
