package com.sunecity.lilium.rest.filter;

import com.sunecity.lilium.core.model.file.ArrayFile;
import com.sunecity.lilium.rest.annotation.Download;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * Created by shahram on 1/16/17.
 */
@Provider
@Download
public class DownloadFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        if (containerResponseContext.getStatus() == 200) {
            Object o = containerResponseContext.getEntity();
            if (o instanceof ArrayFile) {
                ArrayFile file = (ArrayFile) containerResponseContext.getEntity();
                containerResponseContext.setEntity(file.getValue());
                containerResponseContext.getHeaders().add("Content-Type", file.getType());
                containerResponseContext.getHeaders().add("Content-Length", file.getSize());
                containerResponseContext.getHeaders().add("Content-Disposition", "attachment; filename*=UTF-8''" + URLEncoder.encode(file.getName(), "UTF-8"));
            } else if (o instanceof InputStream) {
                ((InputStream) o).transferTo(containerResponseContext.getEntityStream());
//                copy((InputStream) o, containerResponseContext.getEntityStream());
            }
        }
    }

    private void copy(InputStream input, OutputStream output) throws IOException {
        try {
            input.transferTo(output);
        } catch (IOException e) {
            if (!e.getMessage().equals("Broken pipe")) {
                throw e;
            }
        }
    }

}
