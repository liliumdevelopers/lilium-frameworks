package com.sunecity.lilium.rest.client;

import javax.servlet.http.HttpServletRequest;

/**
 * <b>Title:</b> ServerInfo
 * Jsf Utilities.
 *
 * @author Shahram Goodarzi
 * @version 1.5.0
 */
public final class ServerInfo {

    private final HttpServletRequest request;

    public ServerInfo(HttpServletRequest request) {
        this.request = request;
    }

    public String getCurrentUrl() {
        return request.getServletPath();
    }

    public String getLocalIp() {
        return request.getLocalAddr();
    }

    public int getLocalPort() {
        return request.getLocalPort();
    }

    public String getLocalAddress() {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
    }

}
