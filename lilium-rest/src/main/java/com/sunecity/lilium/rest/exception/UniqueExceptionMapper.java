package com.sunecity.lilium.rest.exception;

import com.sunecity.lilium.core.exception.UniqueException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by shahram on 7/23/16.
 */
@Provider
public class UniqueExceptionMapper implements ExceptionMapper<UniqueException> {

    @Override
    public Response toResponse(UniqueException e) {
        return Response.status(Response.Status.CONFLICT).entity(e.getMessage())
                .type(MediaType.APPLICATION_JSON_TYPE).header("Exception", e.getClass().getSimpleName()).build();
    }

    private class Unique {
        private String message;
        private String constraint;
    }

}
