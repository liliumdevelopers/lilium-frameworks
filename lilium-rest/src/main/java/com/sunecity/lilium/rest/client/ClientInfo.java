package com.sunecity.lilium.rest.client;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.DeviceType;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

public class ClientInfo implements Serializable {

    private static final long serialVersionUID = 577672835303993840L;

    private final HttpServletRequest request;

    public ClientInfo(HttpServletRequest request) {
        this.request = request;
    }

    public String getClientInfo() {
        return request.getHeader("user-agent");
    }

    public OperatingSystem getOperatingSystem() {
        UserAgent userAgent = new UserAgent(getClientInfo());
        return userAgent.getOperatingSystem();
    }

    public Browser getBrowser() {
        UserAgent userAgent = new UserAgent(getClientInfo());
        return userAgent.getBrowser();
    }

    public Version getBrowserVersion() {
        UserAgent userAgent = new UserAgent(getClientInfo());
        return userAgent.getBrowserVersion();
    }

    public DeviceType getDeviceType() {
        UserAgent userAgent = new UserAgent(getClientInfo());
        return userAgent.getOperatingSystem().getDeviceType();
    }

    public String getIp() {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.trim().length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.trim().length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.trim().length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.trim().length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.trim().length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

}
