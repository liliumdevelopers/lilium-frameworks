package com.sunecity.lilium.rest.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.sunecity.lilium.core.time.Time;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

/**
 * Created by shahram on 7/20/16.
 */
public class TimeSerializer extends JsonSerializer<Time> {

    @Override
    public void serialize(Time time, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        jsonGenerator.writeString(formatter.format(time));
    }

}
