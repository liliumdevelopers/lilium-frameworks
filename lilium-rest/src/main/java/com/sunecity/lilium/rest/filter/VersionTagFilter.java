package com.sunecity.lilium.rest.filter;

import com.sunecity.lilium.core.model.BaseVersion;
import com.sunecity.lilium.rest.annotation.VersionTag;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by shahram on 1/16/17.
 */
@Provider
@VersionTag
public class VersionTagFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        if (containerResponseContext.getStatus() == 200) {
            Object o = containerResponseContext.getEntity();

            if (o instanceof BaseVersion<?>) {
                Object version = ((BaseVersion<?>) o).getVersion();
                EntityTag tag = new EntityTag(String.valueOf(version));
                Response.ResponseBuilder builder = containerRequestContext.getRequest().evaluatePreconditions(tag);
                if (builder != null) {
                    containerResponseContext.setEntity(null);
                    containerResponseContext.setStatusInfo(Response.Status.NOT_MODIFIED);
                }
            } else if (o instanceof Collection<?>) {
                Collection<?> objects = (Collection<?>) o;
                List<BaseVersion> versions = objects.stream().filter(BaseVersion.class::isInstance).map(BaseVersion.class::cast)/*.map(BaseVersion::getVersion)*/.collect(Collectors.toList());
                EntityTag tag = new EntityTag(String.valueOf(versions));
                Response.ResponseBuilder builder = containerRequestContext.getRequest().evaluatePreconditions(tag);
                if (builder != null) {
                    containerResponseContext.setEntity(null);
                    containerResponseContext.setStatusInfo(Response.Status.NOT_MODIFIED);
                }
            }
        }
    }

}
