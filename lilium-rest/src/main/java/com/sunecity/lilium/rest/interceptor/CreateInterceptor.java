package com.sunecity.lilium.rest.interceptor;

import com.sunecity.lilium.core.model.AuditLog;
import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.rest.annotation.Create;
import org.keycloak.KeycloakPrincipal;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.IOException;

/**
 * Created by shahram on 1/16/17.
 */
@Provider
@Create
public class CreateInterceptor implements ReaderInterceptor, WriterInterceptor {

    @Context
    private SecurityContext securityContext;

    @Override
    public Object aroundReadFrom(ReaderInterceptorContext readerInterceptorContext) throws IOException, WebApplicationException {
        Object o = readerInterceptorContext.proceed();
        if (o instanceof AuditLog) {
            ((AuditLog) o).setCreateUser(((KeycloakPrincipal) securityContext.getUserPrincipal()).getKeycloakSecurityContext().getToken().getPreferredUsername());
        }
        return o;
    }

    @Override
    public void aroundWriteTo(WriterInterceptorContext writerInterceptorContext) throws IOException, WebApplicationException {
        Object o = writerInterceptorContext.getEntity();
        if (o instanceof BaseIdentity<?>) {
            writerInterceptorContext.setEntity(((BaseIdentity<?>) o).getId());
        }
    }

}
