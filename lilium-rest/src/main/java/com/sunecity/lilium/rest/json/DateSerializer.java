package com.sunecity.lilium.rest.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.sunecity.lilium.core.time.Date;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

/**
 * Created by shahram on 7/20/16.
 */
public class DateSerializer extends JsonSerializer<Date> {

    @Override
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
//        jsonGenerator.writeString(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm").format(date.atStartOfDay()));
        jsonGenerator.writeString(DateTimeFormatter.ISO_DATE_TIME.format(date.toLocalDate().atStartOfDay()));
    }

}
