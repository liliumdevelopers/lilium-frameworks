package com.sunecity.lilium.rest.client;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by shahram on 7/23/16.
 */
public class ServerExceptionFilter implements ClientResponseFilter {

    @Override
    public void filter(ClientRequestContext clientRequestContext, ClientResponseContext clientResponseContext) throws IOException {
        if (clientResponseContext.getStatus() >= 500) {
            try (Scanner scanner = new Scanner(clientResponseContext.getEntityStream()).useDelimiter("\\A")) {
                throw new ResponseProcessingException(Response.serverError().build(), scanner.hasNext() ? new String(scanner.next().getBytes("ISO-8859-1"), "UTF-8") : null);
            }
        }
    }

}
