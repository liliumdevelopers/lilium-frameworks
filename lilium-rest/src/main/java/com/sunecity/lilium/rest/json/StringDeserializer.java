package com.sunecity.lilium.rest.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.sunecity.lilium.core.util.Numbers;

import java.io.IOException;

/**
 * Created by shahram on 7/20/16.
 */
public class StringDeserializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return Numbers.getTextEnglish(jsonParser.getValueAsString());
    }

}
