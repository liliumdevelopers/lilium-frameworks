package com.sunecity.lilium.rest.json;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.sunecity.lilium.core.time.Date;
import com.sunecity.lilium.core.time.DateTime;
import com.sunecity.lilium.core.time.Time;
import org.javamoney.moneta.Money;

/**
 * Created by shahram on 7/20/16.
 */
public class LiliumModule extends SimpleModule {

    public LiliumModule() {
        super("lilium");

        addSerializer(Date.class, new DateSerializer());
        addSerializer(DateTime.class, new DateTimeSerializer());
        addSerializer(Time.class, new TimeSerializer());
        addSerializer(Money.class, new MoneySerializer());
        addDeserializer(Date.class, new DateDeserializer());
        addDeserializer(DateTime.class, new DateTimeDeserializer());
        addDeserializer(Time.class, new TimeDeserializer());
        addDeserializer(Money.class, new MoneyDeserializer());

        addSerializer(String.class, new StringSerializer());
        addDeserializer(String.class, new StringDeserializer());
    }

}
