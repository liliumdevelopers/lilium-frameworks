package com.sunecity.lilium.rest.ext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.sunecity.lilium.core.model.BaseIdentity;
import com.sunecity.lilium.core.time.Date;
import com.sunecity.lilium.rest.json.LiliumModule;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Created by shahram on 7/19/16.
 */
@Provider
public class DateParamConverterProvider implements ParamConverterProvider {

    @Override
    public <T> ParamConverter<T> getConverter(final Class<T> rawType, final Type genericType, final Annotation[] annotations) {
        if (!Date.class.isAssignableFrom(rawType)) {
            return null;
        }

        final ObjectMapper mapper = new ObjectMapper();
        mapper.registerModules(new LiliumModule(), new Hibernate5Module());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return new ParamConverter<T>() {

            @Override
            public T fromString(final String value) {
                try {
                    return mapper.readValue(new String(value.getBytes("ISO-8859-1"), "UTF-8"), rawType);
                } catch (IOException e) {
                    throw new ProcessingException(e);
                }
            }

            @Override
            public String toString(final T value) {
                try {
                    return mapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new ProcessingException(e);
                }
            }

        };
    }

}