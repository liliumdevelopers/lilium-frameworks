package com.sunecity.lilium.core.util;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.NotFoundException;
import org.apache.commons.beanutils.PropertyUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class PojoGenerator {

    private final CtClass cc;
    private Class<?> pojoClass;
    private Object pojo;
    private List<Object> pojos;

    public PojoGenerator(String name) throws NotFoundException {
        ClassPool pool = ClassPool.getDefault();
        cc = pool.makeClass(name);
        cc.addInterface(resolveCtClass(Serializable.class));
    }

    public static Class<?> generate(String className, Map<String, Class<?>> fields) throws NotFoundException, CannotCompileException {
        ClassPool pool = ClassPool.getDefault();
        CtClass cc = pool.makeClass(className);
        cc.addInterface(resolveCtClass(Serializable.class));
        for (Entry<String, Class<?>> entry : fields.entrySet()) {
            cc.addField(new CtField(resolveCtClass(entry.getValue()), entry.getKey(), cc));
            cc.addMethod(generateGetter(cc, entry.getKey(), entry.getValue()));
            cc.addMethod(generateSetter(cc, entry.getKey(), entry.getValue()));
        }
        return cc.toClass();
    }

    private static CtMethod generateGetter(CtClass declaringClass, String fieldName, Class<?> fieldClass) throws CannotCompileException {
        String getterName = fieldClass.getSimpleName().equalsIgnoreCase("boolean") ? "is" : "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
        return CtMethod.make("public " + fieldClass.getName() + " " + getterName + "(){" + "return this." + fieldName + ";" + "}", declaringClass);
    }

    private static CtMethod generateSetter(CtClass declaringClass, String fieldName, Class<?> fieldClass) throws CannotCompileException {
        String setterName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
        return CtMethod.make("public void " + setterName + "(" + fieldClass.getName() + " " + fieldName + ")" + "{" + "this." + fieldName + "=" + fieldName + ";" + "}", declaringClass);
    }

    private static CtClass resolveCtClass(Class<?> clazz) throws NotFoundException {
        ClassPool pool = ClassPool.getDefault();
        return pool.get(clazz.getName());
    }

    public void setSuperClass(Class<?> superClass) throws NotFoundException, CannotCompileException {
        cc.setSuperclass(resolveCtClass(superClass));
    }

    public void addInterface(Class<?> interfaceClass) throws NotFoundException {
        cc.addInterface(resolveCtClass(interfaceClass));
    }

    public void addField(String name, Class<?> type) throws NotFoundException, CannotCompileException {
        cc.addField(new CtField(resolveCtClass(type), name, cc));
        cc.addMethod(generateGetter(cc, name, type));
        cc.addMethod(generateSetter(cc, name, type));
    }

    public void generate() throws CannotCompileException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        pojoClass = cc.toClass();
        pojo = pojoClass.getConstructor().newInstance();
    }

    public Class<?> getPojoClass() {
        return pojoClass;
    }

    public Object getPojo() {
        return pojo;
    }

    public Object getPojos() {
        return pojos;
    }

    public void addPojo(String[] names, String[] values) throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        for (int i = 0; i < names.length; i++) {
            Object o = getPojoClass().getConstructor().newInstance();
            PropertyUtils.setProperty(o, names[i], values[i]);
        }
    }

    public Object getFieldValue(String name) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        return PropertyUtils.getProperty(pojo, name);
    }

    public void setFieldValue(String name, Object value) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        PropertyUtils.setProperty(pojo, name, value);
    }

}
