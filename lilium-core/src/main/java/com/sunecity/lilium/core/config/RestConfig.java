package com.sunecity.lilium.core.config;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:META-INF/rest.properties"})
public interface RestConfig extends Config {

    @DefaultValue("http://localhost:8080")
    default String url() {
        return "http://localhost:8080";
    }

}
