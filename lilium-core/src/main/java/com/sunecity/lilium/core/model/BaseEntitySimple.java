package com.sunecity.lilium.core.model;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class BaseEntitySimple<ID extends Serializable> implements BaseIdentity<ID> {

    private static final long serialVersionUID = -3164199209983061534L;

    public BaseEntitySimple() {
    }

    public BaseEntitySimple(ID id) {
        setId(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseEntitySimple<?> that = (BaseEntitySimple<?>) o;

        return getId() != null ? getId().equals(that.getId()) : that.getId() == null;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    @Override
    public String toString() {
        return super.toString() + getId();
    }

}
