package com.sunecity.lilium.core.localization;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Typed;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@Dependent
@Typed(MessageContext.class)
class DefaultMessageContext implements MessageContext {
    private static final long serialVersionUID = -110779217295211303L;


    @Inject
    private MessageInterpolator messageInterpolator = null;

    @Inject
    private MessageResolver messageResolver = null;

    private List<String> messageSources = new ArrayList<String>();

    public DefaultMessageContext() {
    }

    public DefaultMessageContext(MessageContext otherMessageContext) {
        messageInterpolator(otherMessageContext.getMessageInterpolator());
        messageResolver(otherMessageContext.getMessageResolver());

        messageSources.addAll(otherMessageContext.getMessageSources());
    }

    @Override
    public MessageContext clone() {
        return new DefaultMessageContext(this);
    }

    @Override
    public Message message() {
        return new DefaultMessage(this);
    }

    @Override
    public MessageContext messageSource(String... messageSource) {
        List<String> newMessageSources = new ArrayList<String>();

        for (String currentMessageSource : messageSource) {
            //don't insert message-sources twice
            if (!messageSources.contains(currentMessageSource)) {
                newMessageSources.add(currentMessageSource);
            }
        }
        // add on first position
        messageSources.addAll(0, newMessageSources);
        return this;
    }

    @Override
    public Locale getLocale() {
        return Locales.getLocale();
    }

    @Override
    public List<String> getMessageSources() {
        return Collections.unmodifiableList(messageSources);
    }

    @Override
    public MessageInterpolator getMessageInterpolator() {
        return messageInterpolator;
    }

    @Override
    public MessageContext messageInterpolator(MessageInterpolator messageInterpolator) {
        this.messageInterpolator = messageInterpolator;
        return this;
    }

    @Override
    public MessageResolver getMessageResolver() {
        return messageResolver;
    }

    @Override
    public MessageContext messageResolver(MessageResolver messageResolver) {
        this.messageResolver = messageResolver;
        return this;
    }
}
