package com.sunecity.lilium.core.inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.interceptor.InvocationContext;
import java.io.Serializable;

/**
 * Created by shahram on 10/17/16.
 */
public abstract class AbstractInterceptor implements Serializable {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public abstract Object aroundInvoke(InvocationContext invocationContext) throws Exception;

}
