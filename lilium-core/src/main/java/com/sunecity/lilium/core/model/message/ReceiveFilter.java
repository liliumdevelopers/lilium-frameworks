package com.sunecity.lilium.core.model.message;

import jodd.mail.EmailFilter;

/**
 * @author Shahram Goodarzi
 */
public class ReceiveFilter extends EmailFilter {

    private String folder;

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}