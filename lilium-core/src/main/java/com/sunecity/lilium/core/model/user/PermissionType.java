package com.sunecity.lilium.core.model.user;

public enum PermissionType {

	DENIED, PERMITTED, DISABLED, PERMITTED_CANT_SHARE;

	public String toString() {
		switch (this) {
		case DENIED:
			return "دسترسی نداشته باشد";
		case PERMITTED:
			return "دسترسی داشته باشد";
		case DISABLED:
			return "غیر فعال باشد";
		case PERMITTED_CANT_SHARE:
			return "دسترسی داشته ولی نتواند دسترسی دهد";
		default:
			return null;
		}
	}

}
