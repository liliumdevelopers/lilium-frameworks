package com.sunecity.lilium.core.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@MappedSuperclass
public abstract class BaseEntityTreeActivable<ID extends Serializable> extends BaseEntity<ID> implements Treeable<ID>, Activable<ID> {

    private static final long serialVersionUID = -3164199209983061534L;

    @NotBlank
    @Size(min = 1, max = 1000)
    @Column(name = "PATH")
    private String path;

    @Column(name = "IS_LEAF")
    private boolean leaf;

    @Column(name = "PARENT_ID", insertable = false, updatable = false)
    private ID parentId;

    @Column(name = "IS_ACTIVE")
    private boolean active;

    public BaseEntityTreeActivable() {
    }

    public BaseEntityTreeActivable(ID id) {
        super(id);
    }

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    @Override
    public ID getParentId() {
        return parentId;
    }

    public void setParentId(ID parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

}
