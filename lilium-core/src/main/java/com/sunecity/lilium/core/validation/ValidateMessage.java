package com.sunecity.lilium.core.validation;

import java.io.Serializable;

/**
 * @author Shahram Goodarzi
 */
public final class ValidateMessage implements Serializable {

    private String className;
    private String fieldName;
    private String message;

    public ValidateMessage() {
    }

    public ValidateMessage(String className, String fieldName, String message) {
        this.className = className;
        this.fieldName = fieldName;
        this.message = message;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
