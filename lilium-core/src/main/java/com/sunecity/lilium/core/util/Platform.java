package com.sunecity.lilium.core.util;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public final class Platform {

    private Platform() {
    }

    public static OsType getOsType() {
        String osName = System.getProperty("os.name");
        if (osName.startsWith("Solaris") || osName.startsWith("SunOS")) {
            return OsType.SOLARIS;
        } else if (osName.startsWith("Linux")) {
            if ("dalvik".equals(System.getProperty("java.vm.name").toLowerCase())) {
                return OsType.ANDROID;
            } else {
                return OsType.LINUX;
            }
        } else if (osName.startsWith("Mac") || osName.startsWith("Darwin")) {
            return OsType.MAC;
        } else if (osName.startsWith("Windows")) {
            return OsType.WINDOWS;
        } else if (osName.startsWith("AIX")) {
            return OsType.AIX;
        } else if (osName.startsWith("HP-UX")) {
            return OsType.UX;
        } else if (osName.startsWith("FreeBSD")) {
            return OsType.FREEBSD;
        } else if (osName.startsWith("OpenBSD")) {
            return OsType.OPENBSD;
        } else if (osName.equalsIgnoreCase("netbsd")) {
            return OsType.NETBSD;
        } else {
            return OsType.OTHER;
        }
    }

    public static boolean isSolaris() {
        return getOsType() == OsType.SOLARIS;
    }

    public static boolean isLinux() {
        return getOsType() == OsType.LINUX;
    }

    public static boolean isMac() {
        return getOsType() == OsType.MAC;
    }

    public static boolean isWindows() {
        return getOsType() == OsType.WINDOWS;
    }

    public static boolean isAix() {
        return getOsType() == OsType.AIX;
    }

    public static boolean isUx() {
        return getOsType() == OsType.UX;
    }

    public static boolean isFreeBsd() {
        return getOsType() == OsType.FREEBSD;
    }

    public static boolean isOpenBsd() {
        return getOsType() == OsType.OPENBSD;
    }

    public static boolean isNetBsd() {
        return getOsType() == OsType.NETBSD;
    }

    public static boolean isAndroid() {
        return getOsType() == OsType.ANDROID;
    }

    public static boolean isUnix() {
        switch (getOsType()) {
            case SOLARIS:
            case AIX:
            case UX:
            case FREEBSD:
            case OPENBSD:
            case NETBSD:
                return true;
            default:
                return false;
        }
    }

    public static boolean isUnixLike() {
        switch (getOsType()) {
            case SOLARIS:
            case AIX:
            case UX:
            case FREEBSD:
            case OPENBSD:
            case NETBSD:
            case LINUX:
                return true;
            default:
                return false;
        }
    }

    public static boolean is64Bit() {
        String model = System.getProperty("sun.arch.data.model", System.getProperty("com.ibm.vm.bitmode"));
        if (model != null) {
            return "64".equals(model);
        }
        String arch = System.getProperty("os.arch").toLowerCase();
        return "x86_64".equals(arch) || "ia64".equals(arch) || "ppc64".equals(arch) || "ppc64le".equals(arch) || "sparcv9".equals(arch) || "amd64".equals(arch);
    }

    public static boolean isIntel() {
        String arch = System.getProperty("os.arch").toLowerCase().trim();
        return arch.startsWith("i386") || arch.startsWith("x86") || arch.startsWith("amd64");
    }

    public static boolean isPpc() {
        String arch = System.getProperty("os.arch").toLowerCase().trim();
        return arch.startsWith("ppc") || arch.startsWith("powerpc");
    }

    public static boolean isArm() {
        String arch = System.getProperty("os.arch").toLowerCase().trim();
        return arch.startsWith("arm");
    }

    public static boolean isSparc() {
        String arch = System.getProperty("os.arch").toLowerCase().trim();
        return arch.startsWith("sparc");
    }

    public static String[] getMacAddress() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            List<String> addresses = new ArrayList<>();
            while (networkInterfaces.hasMoreElements()) {
                byte[] mac = networkInterfaces.nextElement().getHardwareAddress();
                if (mac != null) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < mac.length; i++) {
                        sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
                    }
                    addresses.add(sb.toString());
                }
            }
            return addresses.toArray(new String[addresses.size()]);
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }

    public enum OsType {
        SOLARIS, LINUX, MAC, WINDOWS, AIX, UX, FREEBSD, OPENBSD, NETBSD, ANDROID, OTHER
    }

}
