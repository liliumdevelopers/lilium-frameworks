package com.sunecity.lilium.core.log;

import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;

@Interceptor
@Log
@Priority(Interceptor.Priority.LIBRARY_BEFORE + 500)
public class LogInterceptor implements Serializable {

    @AroundInvoke
    public Object aroundInvoke(InvocationContext ctx) throws Exception {
        Log log = ctx.getMethod().getAnnotation(Log.class);
        try {
            if (!log.before().equals("")) {
                LoggerFactory.getLogger(ctx.getTarget().getClass()).info(log.before());
            }
            Object o = ctx.proceed();
            if (!log.after().equals("")) {
                LoggerFactory.getLogger(ctx.getTarget().getClass()).info(log.after());
            }
            return o;
        } catch (Exception e) {
            String message = log.onException().equals("") ? "Invocation of " + ctx.getTarget() + "::" + ctx.getMethod().getName() + " failed." : log.onException();
            LoggerFactory.getLogger(ctx.getTarget().getClass()).error(message, e);
            throw e;
        }
    }

}