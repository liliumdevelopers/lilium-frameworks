package com.sunecity.lilium.core.inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * Created by shahram on 8/4/16.
 */
public abstract class AbstractLocator implements Serializable {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected AbstractLocator() {
    }

    protected abstract <T> T lookup(Class<T> clas);

}
