package com.sunecity.lilium.core.util;

/**
 * Created by shahram on 1/5/17.
 */
public interface Builder<T> {

    T build();

}
