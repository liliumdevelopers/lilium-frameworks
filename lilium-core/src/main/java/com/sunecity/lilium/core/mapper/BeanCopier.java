package com.sunecity.lilium.core.mapper;

import ma.glasnost.orika.Converter;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BeanCopier<S, D> {

    private MapperFactory mapperFactory;
    private MapperFacade mapper;
    private ConverterFactory converterFactory;
    private Class<D> destination;
    private Converter<?, ?>[] converters;
    private String[] excludes;
    private Map<String, String> fields;

    public BeanCopier() {
        mapperFactory = new DefaultMapperFactory.Builder().unenhanceStrategy(new LazyProxyUnenhancer()).build();
        converterFactory = mapperFactory.getConverterFactory();
        converters = new Converter[0];
        excludes = new String[0];
        fields = new HashMap<>();
    }

    public void init(Class<S> source, Class<D> destination) {
        this.destination = destination;
        ClassMapBuilder<S, D> classMapBuilder = mapperFactory.classMap(source, destination);
        for (Converter<?, ?> converter : converters) {
            converterFactory.registerConverter(converter);
        }
        converterFactory.registerConverter(new ProxyConverter());
        converterFactory.registerConverter(new DateConverter());
        converterFactory.registerConverter(new DateTimeConverter());
        converterFactory.registerConverter(new MoneyConverter());
        converterFactory.registerConverter(new TimeConverter());
        for (String s : excludes) {
            classMapBuilder.exclude(s);
        }
        for (Map.Entry<String, String> e : fields.entrySet()) {
            classMapBuilder.field(e.getKey(), e.getValue());
        }
        classMapBuilder.byDefault().register();
        mapper = mapperFactory.getMapperFacade();
    }

    public void converter(Converter<?, ?>... converters) {
        this.converters = converters;
    }

    public void exclude(String... excludes) {
        this.excludes = excludes;
    }

    public void field(String source, String destination) {
        fields.put(source, destination);
    }

    public void registerClass(Class<?> source, Class<?> destination, String... excludes) {
        ClassMapBuilder<?, ?> classMapBuilder = mapperFactory.classMap(source, destination);
        for (String s : excludes) {
            classMapBuilder.exclude(s);
        }
        mapperFactory.registerClassMap(classMapBuilder.byDefault().toClassMap());
    }

    public D copy(S source) {
        return mapper.map(source, destination);
    }

    public List<D> copy(List<S> source) {
        return mapper.mapAsList(source, destination);
    }

}
