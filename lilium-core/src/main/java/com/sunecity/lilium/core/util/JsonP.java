package com.sunecity.lilium.core.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;

public class JsonP {

    public <T> void marshal(T t, OutputStream outputStream) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(outputStream, t);
    }

    public <T> String marshal(T t) throws IOException {
        try (Writer writer = new StringWriter()) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(writer, t);
            return writer.toString();
        }
    }

    public <T> T unmarshal(Class<T> clas, InputStream inputStream) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(inputStream, clas);
    }

    public <T> T unmarshal(Class<T> clas, String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, clas);
    }

}
