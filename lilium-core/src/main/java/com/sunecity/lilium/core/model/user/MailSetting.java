package com.sunecity.lilium.core.model.user;

import java.io.Serializable;
import java.util.Properties;

public class MailSetting implements Serializable {
	private static final long serialVersionUID = -8606627983212284732L;

	private String username;
	private String password;
	private String smtpHost;
	private String pop3Host;
	private String imapHost;
	private int smtpPort;
	private int pop3Port;
	private int imapPort;
	private Properties smtpProps;
	private Properties pop3Props;
	private Properties imapProps;
	private boolean smtpSsl;
	private boolean pop3Ssl;
	private boolean imapSsl;

	private MailSetting(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public static MailSetting gmail(String username, String password) {
		MailSetting mail = new MailSetting(username, password);
		mail.smtpHost = "smtp.gmail.com";
		mail.smtpPort = 465;
		mail.smtpSsl = true;
		mail.pop3Host = "pop.gmail.com";
		mail.pop3Port = 995;
		mail.pop3Ssl = true;
		mail.imapHost = "imap.gmail.com";
		mail.imapPort = 993;
		mail.imapSsl = true;
		return mail;
	}

	public static MailSetting yahoo(String username, String password) {
		MailSetting mail = new MailSetting(username, password);
		mail.smtpHost = "smtp.mail.yahoo.com";
		mail.smtpPort = 587;
		mail.imapHost = "imap.mail.yahoo.com";
		mail.imapPort = 993;
		mail.imapSsl = true;
		return mail;
	}

	public static MailSetting hotmail(String username, String password) {
		MailSetting mail = new MailSetting(username, password);
		mail.smtpHost = "smtp.live.com";
		mail.smtpPort = 25;
		mail.pop3Host = "pop3.live.com";
		mail.pop3Port = 995;
		return mail;
	}

	public static MailSetting aol(String username, String password) {
		MailSetting mail = new MailSetting(username, password);
		mail.smtpHost = "smtp.aol.com";
		mail.smtpPort = 587;
		mail.pop3Host = "pop.aol.com";
		mail.pop3Port = 995;
		mail.imapHost = "imap.aol.com ";
		mail.imapPort = 993;
		return mail;
	}

	public static MailSetting gmx(String username, String password) {
		MailSetting mail = new MailSetting(username, password);
		mail.smtpHost = "smtp.gmx.com";
		mail.smtpPort = 25;
		mail.pop3Host = "pop.gmx.com";
		mail.pop3Port = 995;
		mail.imapHost = "imap.gmx.com ";
		mail.imapPort = 993;
		return mail;
	}

	public static MailSetting zoho(String username, String password) {
		MailSetting mail = new MailSetting(username, password);
		mail.smtpHost = "smtp.zoho.com";
		mail.smtpPort = 465;
		mail.pop3Host = "pop.zoho.com";
		mail.pop3Port = 995;
		mail.imapHost = "imap.zoho.com ";
		mail.imapPort = 993;
		return mail;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getPop3Host() {
		return pop3Host;
	}

	public void setPop3Host(String pop3Host) {
		this.pop3Host = pop3Host;
	}

	public String getImapHost() {
		return imapHost;
	}

	public void setImapHost(String imapHost) {
		this.imapHost = imapHost;
	}

	public int getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}

	public int getPop3Port() {
		return pop3Port;
	}

	public void setPop3Port(int pop3Port) {
		this.pop3Port = pop3Port;
	}

	public int getImapPort() {
		return imapPort;
	}

	public void setImapPort(int imapPort) {
		this.imapPort = imapPort;
	}

	public Properties getSmtpProps() {
		return smtpProps;
	}

	public void setSmtpProps(Properties smtpProps) {
		this.smtpProps = smtpProps;
	}

	public Properties getPop3Props() {
		return pop3Props;
	}

	public void setPop3Props(Properties pop3Props) {
		this.pop3Props = pop3Props;
	}

	public Properties getImapProps() {
		return imapProps;
	}

	public void setImapProps(Properties imapProps) {
		this.imapProps = imapProps;
	}

	public boolean isSmtpSsl() {
		return smtpSsl;
	}

	public void setSmtpSsl(boolean smtpSsl) {
		this.smtpSsl = smtpSsl;
	}

	public boolean isPop3Ssl() {
		return pop3Ssl;
	}

	public void setPop3Ssl(boolean pop3Ssl) {
		this.pop3Ssl = pop3Ssl;
	}

	public boolean isImapSsl() {
		return imapSsl;
	}

	public void setImapSsl(boolean imapSsl) {
		this.imapSsl = imapSsl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
