package com.sunecity.lilium.core.mapper;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.javamoney.moneta.Money;

/**
 * Created by shahram on 12/14/16.
 */
public class MoneyConverter extends BidirectionalConverter<Money, Money> {

    @Override
    public Money convertTo(Money money, Type<Money> type, MappingContext mappingContext) {
        return null;
    }

    @Override
    public Money convertFrom(Money money, Type<Money> type, MappingContext mappingContext) {
        return null;
    }

}
