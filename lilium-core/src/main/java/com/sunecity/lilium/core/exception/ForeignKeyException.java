package com.sunecity.lilium.core.exception;

import javax.ejb.ApplicationException;
import javax.ws.rs.core.Response;

/**
 * @author Shahram Goodarzi
 */
@ApplicationException
public class ForeignKeyException extends ClientException {

    private static final long serialVersionUID = 8460803306428442691L;

    private final String constraint;

    public ForeignKeyException(String message, String constraint) {
        super(null, message);
        this.constraint = constraint;
    }

    public ForeignKeyException(Response response, String message, String constraint) {
        super(response, message);
        this.constraint = constraint;
    }

    public String getConstraint() {
        return constraint;
    }

}
