package com.sunecity.lilium.core.config;

import org.aeonbits.owner.Config;

/**
 * Created by shahram on 9/30/16.
 */
@Config.Sources({"classpath:META-INF/cache.properties"})
public interface CacheConfig extends Config {

    @DefaultValue("localhost")
    default String host() {
        return "localhost";
    }

    @DefaultValue("11222")
    default int port() {
        return 11222;
    }

}
