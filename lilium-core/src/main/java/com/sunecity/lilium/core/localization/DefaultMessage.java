package com.sunecity.lilium.core.localization;

import javax.enterprise.inject.Typed;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static com.sunecity.lilium.core.localization.MessageResolver.MISSING_RESOURCE_MARKER;

/**
 * {@inheritDoc}
 */
@Typed()
public class DefaultMessage implements Message
{
    private String messageTemplate;
    private List<Serializable> arguments = new ArrayList<Serializable>();

    private MessageContext messageContext;

    public DefaultMessage(MessageContext messageContext)
    {
        reset();

        this.messageContext = messageContext;
    }

    protected void reset()
    {
        messageTemplate = null;
        arguments = new ArrayList<Serializable>();
    }

    @Override
    public Message argument(Serializable... arguments)
    {
        if (arguments != null)
        {
            Collections.addAll(this.arguments, arguments);
        }
        return this;
    }

    @Override
    public Message template(String messageTemplate)
    {
        this.messageTemplate = messageTemplate;
        return this;
    }

    @Override
    public String getTemplate()
    {
        return messageTemplate;
    }

    @Override
    public Serializable[] getArguments()
    {
        return arguments.toArray(new Serializable[arguments.size()]);
    }



    @Override
    public String toString()
    {
        return toString((String) null);
    }

    @Override
    public String toString(String category)
    {

        // the string construction happens in 3 phases

        // first try to pickup the message via the MessageResolver
        String template = getTemplate();
        if (template == null)
        {
            return "";
        }

        String ret = template;
        MessageResolver messageResolver = messageContext.getMessageResolver();
        if (messageResolver != null)
        {
            String resolvedTemplate = messageResolver.getMessage(messageContext, template, category);

            if (resolvedTemplate == null)
            {
                // this means an error happened during message resolving
                resolvedTemplate = markAsUnresolved(template);
            }
            ret = resolvedTemplate;
            template = resolvedTemplate;
        }

        // last step is to interpolate the message
        MessageInterpolator messageInterpolator = messageContext.getMessageInterpolator();
        if (messageInterpolator != null)
        {
            Locale locale = messageContext.getLocale();

            ret = messageInterpolator.interpolate(template, getArguments(), locale);
        }

        return ret;
    }

    private String markAsUnresolved(String template)
    {
        if (messageTemplate.startsWith("{") && messageTemplate.endsWith("}"))
        {
            template = messageTemplate.substring(1, messageTemplate.length() - 1);
        }

        StringBuilder sb = new StringBuilder(MISSING_RESOURCE_MARKER + template + MISSING_RESOURCE_MARKER);
        if (getArguments() != null && getArguments().length > 0)
        {
            sb.append(" ").append(Arrays.toString(getArguments()));
        }

        return sb.toString();
    }

    @Override
    public String toString(MessageContext messageContext)
    {
        return toString(messageContext, null);
    }

    @Override
    public String toString(MessageContext messageContext, String category)
    {
        return messageContext.message()
                .template(getTemplate())
                .argument(getArguments())
                .toString(category);
    }


    /**
     * Attention, the {@link #messageContext} is deliberately not part of the equation!
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (!(o instanceof Message))
        {
            return false;
        }

        Message other = (Message) o;

        if (getTemplate() == null && other.getTemplate() != null)
        {
            return false;
        }

        if (getTemplate() != null && !getTemplate().equals(other.getTemplate()))
        {
            return false;
        }

        //noinspection RedundantIfStatement
        if (arguments != null
                ? !Arrays.equals(arguments.toArray(), other.getArguments())
                : other.getArguments() != null)
        {
            return false;
        }

        return true;
    }

    /**
     * Attention, the {@link #messageContext} is deliberately not part of the equation!
     */
    @Override
    public int hashCode()
    {
        int result = getTemplate().hashCode();
        result = 31 * result + (arguments != null ? arguments.hashCode() : 0);
        return result;
    }

    @Override
    public Message argumentArray(Serializable[] arguments)
    {
        if (arguments != null)
        {
            return argument(Arrays.asList(arguments)); 
        }
        return this;
    }

    @Override
    public Message argument(Collection<Serializable> arguments)
    {
        if (arguments != null)
        {
            this.arguments.addAll(arguments); 
        }
        return this;
    }
}
