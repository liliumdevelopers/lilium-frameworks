package com.sunecity.lilium.core.model.message;

/**
 * @author Shahram Goodarzi
 */
public class Attachment {

    private String name;
    private String contentType;
    private boolean inline;
    private byte[] attachment;

    public Attachment() {
    }

    public Attachment(String name, String contentType, byte[] attachment) {
        this.name = name;
        this.contentType = contentType;
        this.attachment = attachment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentType() {
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public boolean isInline() {
        return inline;
    }

    public void setInline(boolean inline) {
        this.inline = inline;
    }

    public byte[] getAttachment() {
        return attachment;
    }

    void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

}
