package com.sunecity.lilium.core.socket;

import java.io.Serializable;

/**
 * Created by shahram on 4/12/17.
 */
public class SocketMessage implements Serializable {

    private String title;
    private String detail;

    public SocketMessage() {
    }

    public SocketMessage(String title, String detail) {
        this.title = title;
        this.detail = detail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

}
