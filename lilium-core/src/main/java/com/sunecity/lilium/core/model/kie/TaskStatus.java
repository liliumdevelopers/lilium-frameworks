package com.sunecity.lilium.core.model.kie;

import com.sunecity.lilium.core.localization.Bundles;

/**
 * Created by shahram on 5/23/16.
 */
public enum TaskStatus {

    CREATED, READY, RESERVED, IN_PROGRESS, SUSPENDED, COMPLETED, FAILED, ERROR, EXITED, OBSOLETE;

    public String toString() {
        return Bundles.getValueLocale(this);
    }

}
