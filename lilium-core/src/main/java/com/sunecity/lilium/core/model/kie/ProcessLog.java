package com.sunecity.lilium.core.model.kie;

import com.sunecity.lilium.core.time.Date;

import java.io.Serializable;

/**
 * Created by shahram on 5/25/16.
 */
public class ProcessLog implements Serializable {

    private String username;
    private String processId;
    private String processName;
    private String processVersion;
    private Long processInstanceId;
    private Long parentProcessInstanceId;
    private String processInstanceDescription;
    private ProcessStatus status;
    private Date start;
    private Date end;
    private Long duration;
    private String outcome;
    private String externalId;

    public ProcessLog() {
    }

    public ProcessLog(String username, String processId, String processName, String processVersion, Long processInstanceId, Long parentProcessInstanceId, String processInstanceDescription, ProcessStatus status, Date start, Date end, Long duration, String outcome, String externalId) {
        this.username = username;
        this.processId = processId;
        this.processName = processName;
        this.processVersion = processVersion;
        this.processInstanceId = processInstanceId;
        this.parentProcessInstanceId = parentProcessInstanceId;
        this.processInstanceDescription = processInstanceDescription;
        this.status = status;
        this.start = start;
        this.end = end;
        this.duration = duration;
        this.outcome = outcome;
        this.externalId = externalId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessVersion() {
        return processVersion;
    }

    public void setProcessVersion(String processVersion) {
        this.processVersion = processVersion;
    }

    public Long getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(Long processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public Long getParentProcessInstanceId() {
        return parentProcessInstanceId;
    }

    public void setParentProcessInstanceId(Long parentProcessInstanceId) {
        this.parentProcessInstanceId = parentProcessInstanceId;
    }

    public String getProcessInstanceDescription() {
        return processInstanceDescription;
    }

    public void setProcessInstanceDescription(String processInstanceDescription) {
        this.processInstanceDescription = processInstanceDescription;
    }

    public ProcessStatus getStatus() {
        return status;
    }

    public void setStatus(ProcessStatus status) {
        this.status = status;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProcessLog that = (ProcessLog) o;

        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (processId != null ? !processId.equals(that.processId) : that.processId != null) return false;
        if (processName != null ? !processName.equals(that.processName) : that.processName != null) return false;
        if (processVersion != null ? !processVersion.equals(that.processVersion) : that.processVersion != null)
            return false;
        if (processInstanceId != null ? !processInstanceId.equals(that.processInstanceId) : that.processInstanceId != null)
            return false;
        if (parentProcessInstanceId != null ? !parentProcessInstanceId.equals(that.parentProcessInstanceId) : that.parentProcessInstanceId != null)
            return false;
        if (processInstanceDescription != null ? !processInstanceDescription.equals(that.processInstanceDescription) : that.processInstanceDescription != null)
            return false;
        if (status != that.status) return false;
        if (start != null ? !start.equals(that.start) : that.start != null) return false;
        if (end != null ? !end.equals(that.end) : that.end != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (outcome != null ? !outcome.equals(that.outcome) : that.outcome != null) return false;
        return externalId != null ? externalId.equals(that.externalId) : that.externalId == null;

    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (processId != null ? processId.hashCode() : 0);
        result = 31 * result + (processName != null ? processName.hashCode() : 0);
        result = 31 * result + (processVersion != null ? processVersion.hashCode() : 0);
        result = 31 * result + (processInstanceId != null ? processInstanceId.hashCode() : 0);
        result = 31 * result + (parentProcessInstanceId != null ? parentProcessInstanceId.hashCode() : 0);
        result = 31 * result + (processInstanceDescription != null ? processInstanceDescription.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (outcome != null ? outcome.hashCode() : 0);
        result = 31 * result + (externalId != null ? externalId.hashCode() : 0);
        return result;
    }

}
