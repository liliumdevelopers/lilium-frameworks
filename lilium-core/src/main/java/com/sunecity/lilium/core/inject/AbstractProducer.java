package com.sunecity.lilium.core.inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.spi.InjectionPoint;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by shahram on 8/4/16.
 */
public abstract class AbstractProducer implements Serializable {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected AbstractProducer() {
    }

    protected <T extends Annotation> T getAnnotation(Class<T> clas, InjectionPoint injectionPoint) {
        return (T) injectionPoint.getQualifiers().stream().filter(q -> q.annotationType() == clas).findAny().orElse(null);
    }

    protected <T> T lookup(Class<T> clas, LookupType type) {
        T t = null;
        try {
            Class<? extends AbstractLocator> locatorClass = type.getLocatorClass();
            AbstractLocator locator = locatorClass.getConstructor().newInstance();
            t = locator.lookup(clas);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            logger.error("Lookup failed.", e);
        }
        return t;
    }

}
