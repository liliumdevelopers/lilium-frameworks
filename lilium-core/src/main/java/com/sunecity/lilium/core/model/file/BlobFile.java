package com.sunecity.lilium.core.model.file;

import com.sunecity.lilium.core.model.BaseEntitySimple;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.InputStream;
import java.sql.Blob;

//import org.primefaces.model.DefaultStreamedContent;
//import org.primefaces.model.StreamedContent;
//import org.primefaces.model.UploadedFile;


@Entity
@Table(name = "SK_BLOB_FILES")
public class BlobFile extends BaseEntitySimple<Long> {

    private static final long serialVersionUID = 1658057822569645457L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "SIZE")
    private long size;

    @Lob
    @Column(name = "VALUE")
    private Blob value;

    public BlobFile() {
    }

    public BlobFile(String name, String type, long size, Blob value) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.value = value;
    }

//	public BlobFile(UploadedFile uploadedFile, Blob value) {
//		this.name = uploadedFile.getFileName();
//		this.type = uploadedFile.getContentType();
//		this.size = uploadedFile.getSize();
//		this.value = value;
//	}

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Blob getValue() {
        return value;
    }

    public void setValue(Blob value) {
        this.value = value;
    }

    @Transient
    public InputStream getInputStream() throws Exception {
        return value.getBinaryStream();
    }

//	@Transient
//	public StreamedContent getStreamedContent() throws Exception {
//		return new DefaultStreamedContent(getInputStream(), type, name);
//	}

}
