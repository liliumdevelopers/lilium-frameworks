package com.sunecity.lilium.core.exception;

import com.sunecity.lilium.core.model.kie.RuleMessage;

import javax.ejb.ApplicationException;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
@ApplicationException
public class RuleVerificationException extends Exception {

    private List<RuleMessage> messages;

    public RuleVerificationException() {
    }

    public RuleVerificationException(Throwable cause) {
        super(cause);
    }

    public RuleVerificationException(List<RuleMessage> messages, String message) {
        super(message);
        this.messages = messages;
    }

    public List<RuleMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<RuleMessage> messages) {
        this.messages = messages;
    }

}
