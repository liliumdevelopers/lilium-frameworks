package com.sunecity.lilium.core.util;

import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;

/**
 * @author Shahram Goodarzi
 */
@Dependent
public class MimeTypes implements Serializable {

    public static final String APPLICATION = "application";
    public static final String AUDIO = "audio";
    public static final String IMAGE = "image";
    public static final String TEXT = "text";
    public static final String VIDEO = "video";

    //////////////// mime-type ////////////////

    public String getMimeType(InputStream inputStream) throws IOException {
        TikaInputStream stream = TikaInputStream.get(inputStream);
        return getType(stream).toString();
    }

    public String getMimeType(byte[] bs) throws IOException {
        TikaInputStream stream = TikaInputStream.get(bs);
        return getType(stream).toString();
    }

    public String getMimeType(Path path) throws IOException {
        TikaInputStream stream = TikaInputStream.get(path);
        return getType(stream).toString();
    }

    public String getMimeType(URI uri) throws IOException {
        TikaInputStream stream = TikaInputStream.get(uri);
        return getType(stream).toString();
    }

    public String getMimeType(URL url) throws IOException {
        TikaInputStream stream = TikaInputStream.get(url);
        return getType(stream).toString();
    }

    //////////////// main-type ////////////////

    public String getType(InputStream inputStream) throws IOException {
        TikaInputStream stream = TikaInputStream.get(inputStream);
        return getType(stream).getType();
    }

    public String getType(byte[] bs) throws IOException {
        TikaInputStream stream = TikaInputStream.get(bs);
        return getType(stream).getType();
    }

    public String getType(Path path) throws IOException {
        TikaInputStream stream = TikaInputStream.get(path);
        return getType(stream).getType();
    }

    public String getType(URI uri) throws IOException {
        TikaInputStream stream = TikaInputStream.get(uri);
        return getType(stream).getType();
    }

    public String getType(URL url) throws IOException {
        TikaInputStream stream = TikaInputStream.get(url);
        return getType(stream).getType();
    }

    //////////////// sub-type ////////////////

    public String getSubtype(InputStream inputStream) throws IOException {
        TikaInputStream stream = TikaInputStream.get(inputStream);
        return getType(stream).getSubtype();
    }

    public String getSubtype(byte[] bs) throws IOException {
        TikaInputStream stream = TikaInputStream.get(bs);
        return getType(stream).getSubtype();
    }

    public String getSubtype(Path path) throws IOException {
        TikaInputStream stream = TikaInputStream.get(path);
        return getType(stream).getSubtype();
    }

    public String getSubtype(URI uri) throws IOException {
        TikaInputStream stream = TikaInputStream.get(uri);
        return getType(stream).getSubtype();
    }

    public String getSubtype(URL url) throws IOException {
        TikaInputStream stream = TikaInputStream.get(url);
        return getType(stream).getSubtype();
    }

    //////////////// MediaType ////////////////

    public MediaType getMediaType(InputStream inputStream) throws IOException {
        TikaInputStream stream = TikaInputStream.get(inputStream);
        return getType(stream);
    }

    public MediaType getMediaType(byte[] bs) throws IOException {
        TikaInputStream stream = TikaInputStream.get(bs);
        return getType(stream);
    }

    public MediaType getMediaType(Path path) throws IOException {
        TikaInputStream stream = TikaInputStream.get(path);
        return getType(stream);
    }

    public MediaType getMediaType(URI uri) throws IOException {
        TikaInputStream stream = TikaInputStream.get(uri);
        return getType(stream);
    }

    public MediaType getMediaType(URL url) throws IOException {
        TikaInputStream stream = TikaInputStream.get(url);
        return getType(stream);
    }

    private MediaType getType(TikaInputStream stream) throws IOException {
        TikaConfig config = TikaConfig.getDefaultConfig();
        Detector detector = config.getDetector();
        Metadata metadata = new Metadata();
        return detector.detect(stream, metadata);
    }

}
