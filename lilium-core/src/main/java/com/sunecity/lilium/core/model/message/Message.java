package com.sunecity.lilium.core.model.message;

/**
 * @author Shahram Goodarzi
 */
public class Message {

    public static final String TEXT_PLAIN = "text/plain";
    public static final String TEXT_HTML = "text/html";

    public static final String UTF_8 = "UTF-8";


    private String content;
    private String mimeType;
    private String encoding;

    public Message() {
    }

    public Message(String content, String mimeType, String encoding) {
        this.content = content;
        this.mimeType = mimeType;
        this.encoding = encoding;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMimeType() {
        if (mimeType == null) {
            if (getContent() != null)
                if (getContent().startsWith("<html>")) {
                    mimeType = TEXT_HTML;
                } else {
                    mimeType = TEXT_PLAIN;
                }
        }
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }
}
