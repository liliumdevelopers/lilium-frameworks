package com.sunecity.lilium.core.validation;

import com.sunecity.lilium.core.util.Numbers;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
public class JalaliCalendarValidator {

    private JalaliCalendarValidator() {
    }

    public static boolean isValid(String date) {
        return validation(Numbers.getTextEnglish(date));
    }

    private static boolean validation(String date) {
        List<String> numbers = new ArrayList<>();
        int l = date.length();
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < l; i++) {
            if (Character.isDigit(date.charAt(i))) {
                str.append(date.charAt(i));
            } else {
                numbers.add(str.toString());
                str = new StringBuilder();
            }
        }
        if (!str.toString().equals("")) {
            numbers.add(str.toString());
        }
        if (numbers.size() == 3) {
            if ((numbers.get(0).length() != 4) && (numbers.get(0).length() != 2)) {
                return false;
            }
            if ((numbers.get(1).length() != 1) && (numbers.get(1).length() != 2)) {
                return false;
            }
            if ((numbers.get(2).length() != 1) && (numbers.get(2).length() != 2)) {
                return false;
            }
            int y = Integer.parseInt(numbers.get(0));
            int m = Integer.parseInt(numbers.get(1));
            int d = Integer.parseInt(numbers.get(2));
            if (m > 12 || d > 31) {
                return false;
            }
            int dom = getJalaliMonthLength(y, m);
            if (d > dom) {
                return false;
            }
        } else {
            return numbers.size() == 1 && withoutSeparatorValidate(date);
        }
        return true;
    }

    private static boolean withoutSeparatorValidate(String date) {
        int l = date.length();
        if (l == 6 || l == 8) {
            try {
                Double.parseDouble(date);
            } catch (NumberFormatException ex) {
                return false;
            }
        } else
            return false;
        return true;
    }

    private static boolean isJalaliLeapYear(int year) {
        return ((year % 33 == 1) || (year % 33 == 5) || (year % 33 == 9) || (year % 33 == 13)
                || (year % 33 == 17) || (year % 33 == 22) || (year % 33 == 26) || (year % 33 == 30));
    }

    private static int getJalaliMonthLength(int year, int month) {
        return month <= 6 ? 31 : month <= 11 ? 30 : isJalaliLeapYear(year) ? 30 : 29;
    }

}
