package com.sunecity.lilium.core.validation;

import com.sunecity.lilium.core.util.Numbers;

/**
 * @author Shahram Goodarzi
 */
public class NationalId {

    private NationalId() {
    }

    public static boolean isValid(String id) {
        if (id == null || id.trim().length() == 0) {
            return false;
        }
        id = Numbers.getTextEnglish(id);
        id = id.replaceAll("-", "");
        id = id.replaceAll("_", "");
        id = id.replaceAll(" ", "");
        if (!preValidate(id)) {
            return false;
        }
        int sum = 0;
        for (int i = 0; i < id.length() - 1; i++) {
            sum += (Byte.valueOf(String.valueOf(id.charAt(i))) * (10 - i));
        }
        return (id.charAt(9) == String.valueOf(((sum % 11) >= 2) ? (11 - (sum % 11)) : (sum % 11)).charAt(0));
    }

    private static boolean preValidate(String id) {
        if (id.length() != 10) {
            return false;
        }
        for (int i = 0; i < id.length(); i++) {
            if (!Character.isDigit(id.charAt(i))) {
                return false;
            }
        }
        return !(id.equals("0000000000") || id.equals("1111111111")
                || id.equals("2222222222") || id.equals("3333333333")
                || id.equals("4444444444") || id.equals("5555555555")
                || id.equals("6666666666") || id.equals("7777777777")
                || id.equals("8888888888") || id.equals("9999999999"));
    }

}
