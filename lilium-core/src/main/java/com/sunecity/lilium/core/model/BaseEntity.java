package com.sunecity.lilium.core.model;

import com.sunecity.lilium.core.time.DateTime;
import org.javers.core.metamodel.annotation.DiffIgnore;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;
import javax.validation.constraints.Size;
import java.io.Serializable;

@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> extends BaseEntitySimple<ID> implements BaseVersion<Integer>, AuditLog {

    private static final long serialVersionUID = -3164199209983061534L;

    @Version
    @Column(name = "VERSION")
    @DiffIgnore
    private int version;

    @Column(name = "SAVE_USER")
    @Size(max = 100)
    @DiffIgnore
    private String createUser;

    @Column(name = "UPDATE_USER")
    @Size(max = 100)
    @DiffIgnore
    private String updateUser;

    @Column(name = "SAVE_TIMESTAMP")
    @DiffIgnore
    private DateTime createTimestamp;

    @Column(name = "UPDATE_TIMESTAMP")
    @DiffIgnore
    private DateTime updateTimestamp;

    public BaseEntity() {
    }

    public BaseEntity(ID id) {
        setId(id);
    }

//    public abstract void setId(ID id);

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String getCreateUser() {
        return createUser;
    }

    @Override
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Override
    public String getUpdateUser() {
        return updateUser;
    }

    @Override
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Override
    public DateTime getCreateTimestamp() {
        return createTimestamp;
    }

    @Override
    public void setCreateTimestamp(DateTime createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    @Override
    public DateTime getUpdateTimestamp() {
        return updateTimestamp;
    }

    @Override
    public void setUpdateTimestamp(DateTime updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    @PrePersist
    private void createTimestamp() {
        createTimestamp = DateTime.now();
    }

    @PreUpdate
    private void updateTimestamp() {
        updateTimestamp = DateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseEntity<?> that = (BaseEntity<?>) o;

        if (version != that.version) return false;
        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (createUser != null ? !createUser.equals(that.createUser) : that.createUser != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;
        if (createTimestamp != null ? !createTimestamp.equals(that.createTimestamp) : that.createTimestamp != null)
            return false;
        return updateTimestamp != null ? updateTimestamp.equals(that.updateTimestamp) : that.updateTimestamp == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + version;
        result = 31 * result + (createUser != null ? createUser.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        result = 31 * result + (createTimestamp != null ? createTimestamp.hashCode() : 0);
        result = 31 * result + (updateTimestamp != null ? updateTimestamp.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + getId();
    }

}
