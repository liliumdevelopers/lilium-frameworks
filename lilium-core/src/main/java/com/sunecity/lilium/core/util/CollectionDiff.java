package com.sunecity.lilium.core.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by shahram on 5/22/17.
 */
public class CollectionDiff<T extends Collection<E>, E> {

    private T oldCollection;
    private T newCollection;

    public CollectionDiff(T oldCollection, T newCollection) {
        this.oldCollection = oldCollection;
        this.newCollection = newCollection;
    }

    public T getOldCollection() {
        return oldCollection;
    }

    public void setOldCollection(T oldCollection) {
        this.oldCollection = oldCollection;
    }

    public T getNewCollection() {
        return newCollection;
    }

    public void setNewCollection(T newCollection) {
        this.newCollection = newCollection;
    }

    public Collection<E> getUnion() {
        Set<E> collection = new HashSet<>(oldCollection);
        collection.addAll(newCollection);
        return collection;
    }

    public Collection<E> getIntersect() {
        Set<E> collection1 = new HashSet<>(oldCollection);
        Set<E> collection2 = new HashSet<>(newCollection);
        collection1.retainAll(newCollection);
        collection2.retainAll(oldCollection);
        collection1.addAll(collection2);
        return collection1;
    }

    public Collection<E> getSymmetricDiff() {
        Set<E> collection1 = new HashSet<>(oldCollection);
        Set<E> collection2 = new HashSet<>(newCollection);
        collection1.removeAll(newCollection);
        collection2.removeAll(oldCollection);
        collection1.addAll(collection2);
        return collection1;
    }

    public Collection<E> getNew() {
        Set<E> collection = new HashSet<>(newCollection);
        collection.removeAll(oldCollection);
        return collection;
    }

    public Collection<E> getOld() {
        Set<E> collection = new HashSet<>(oldCollection);
        collection.removeAll(newCollection);
        return collection;
    }

}
