package com.sunecity.lilium.core.localization;

import java.io.Serializable;
import java.util.Collection;

/**
 * Basic interface for all messages.
 * <p>
 * <p>
 * A <code>Message</code> is not a simple String but all the information needed to create those Strings for multiple
 * situations.</p>
 */
public interface Message extends Serializable {
    /**
     * @param messageTemplate message key (or plain text) for the current message
     * @return the current instance of the message builder to allow a fluent API
     */
    Message template(String messageTemplate);

    /**
     * @param arguments numbered and/or named argument(s) for the current message
     * @return the current instance of the message builder to allow a fluent API
     */
    Message argument(Serializable... arguments);

    /**
     * Argument array. Similar to argument except it is meant to handle an array being passed in via a chain.
     *
     * @param arguments the arguments
     * @return the message
     */
    Message argumentArray(Serializable[] arguments);

    /**
     * Argument. Similar to the other argument methods, this one handles collections.
     *
     * @param arguments the arguments
     * @return the message
     */
    Message argument(Collection<Serializable> arguments);

    /**
     * @return the message key (or plain text) of the current message
     */
    String getTemplate();

    /**
     * @return all named and numbered arguments
     */
    Object[] getArguments();

    String toString();

    String toString(MessageContext messageContext);

    /**
     * Renders the Message to a String. While resolving the
     * message we will first search for a messageTemplate with the given category by just adding an underscore '_' and
     * the category String to the {@link #getTemplate()}. If no such template exists we will fall back to the version
     * without the category String.
     * <p>
     * DeltaSpike JSF messages e.g. distinguish between categories
     * {@code &quot;summary&quot;} and {@code &quot;detail&quot;}
     * to allow a short and a more detailed explanation in Error, Warn and Info popups at the same time.
     * </p>
     */
    String toString(String category);

    String toString(MessageContext messageContext, String category);

}
