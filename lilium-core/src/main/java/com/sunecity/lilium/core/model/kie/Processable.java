package com.sunecity.lilium.core.model.kie;

import java.io.Serializable;

/**
 * Created by shahram on 5/26/16.
 */
public interface Processable extends Serializable {

    Long getProcessId();

    void setProcessId(Long processId);

    String getProcessState();

    void setProcessState(String processState);

}
