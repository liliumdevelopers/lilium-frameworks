package com.sunecity.lilium.core.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class NestedBuilder<T, V> {

    protected T parent;

    /**
     * To get the parent builder
     *
     * @return T the instance of the parent builder
     */
    public T done() {
        Class<?> parentClass = parent.getClass();
        try {
            V build = this.build();
            String methodName = "with" + build.getClass().getSimpleName();
            Method method = parentClass.getDeclaredMethod(methodName, build.getClass());
            method.invoke(parent, build);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        return parent;
    }

    /**
     * @param parent
     */
    public <P extends NestedBuilder<T, V>> P withParentBuilder(T parent) {
        this.parent = parent;
        return (P) this;
    }

    public abstract V build();

}