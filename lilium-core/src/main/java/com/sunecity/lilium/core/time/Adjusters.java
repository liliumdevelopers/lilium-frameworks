package com.sunecity.lilium.core.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.Chronology;
import java.time.chrono.JapaneseDate;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Locale;

public class Adjusters {

    public static void sh() {
        TemporalAdjusters.firstDayOfNextYear();
    }

    public static TemporalAdjuster nextWorkingDay() {
        Chronology japaneseChronology = Chronology.ofLocale(Locale.JAPAN);
        ChronoLocalDate now = japaneseChronology.dateNow();
        JapaneseDate japaneseDate = JapaneseDate.from(LocalDate.now());


        return temporal -> {
            DayOfWeek dayOfWeek = DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
            int dayToAdd = 1;
            if (dayOfWeek == DayOfWeek.THURSDAY) {
                dayToAdd = 3;
            } else if (dayOfWeek == DayOfWeek.FRIDAY) {
                dayToAdd = 2;
            }
            return temporal.plus(dayToAdd, ChronoUnit.DAYS);
        };
    }

}
