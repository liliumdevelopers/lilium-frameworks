package com.sunecity.lilium.core.model;

public interface BaseVersion<T> {

    T getVersion();

    void setVersion(T version);

}
