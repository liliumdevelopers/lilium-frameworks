package com.sunecity.lilium.core.exception;

import javax.ejb.ApplicationException;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.core.Response;

/**
 * Created by shahram on 10/25/16.
 */
@ApplicationException
public class ClientException extends ResponseProcessingException {

    public ClientException(Response response, Throwable cause) {
        super(response, cause);
    }

    public ClientException(Response response, String message, Throwable cause) {
        super(response, message, cause);
    }

    public ClientException(Response response, String message) {
        super(response, message);
    }

    public ClientException(Throwable cause) {
        super(null, cause);
    }

    public ClientException(String message, Throwable cause) {
        super(null, message, cause);
    }

    public ClientException(String message) {
        super(null, message);
    }

}
