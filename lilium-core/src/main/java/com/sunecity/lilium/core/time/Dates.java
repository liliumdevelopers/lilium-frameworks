package com.sunecity.lilium.core.time;

import java.time.LocalDate;

public class Dates {

    public static LocalDate from() {
        return LocalDate.now();
    }

    public static String format(LocalDate date) {
        return date.toString();
    }

}
