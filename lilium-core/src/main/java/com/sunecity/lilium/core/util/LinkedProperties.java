package com.sunecity.lilium.core.util;

import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

public class LinkedProperties extends Properties {

    private static final long serialVersionUID = -5917477305259169845L;

    private Map<Object, Object> linkMap = new LinkedHashMap<>();

    @Override
    public synchronized Object put(Object key, Object value) {
        return linkMap.put(key, value);
    }

    @Override
    public synchronized boolean contains(Object value) {
        return linkMap.containsValue(value);
    }

    @Override
    public boolean containsValue(Object value) {
        return linkMap.containsValue(value);
    }

    @Override
    public Set<Entry<Object, Object>> entrySet() {
        return linkMap.entrySet();
    }

    @Override
    public synchronized Object remove(Object key) {
        return linkMap.remove(key);
    }

    @Override
    public synchronized void clear() {
        linkMap.clear();
    }

    @Override
    public synchronized boolean containsKey(Object key) {
        return linkMap.containsKey(key);
    }

    @Override
    public Collection<Object> values() {
        return linkMap.values();
    }

    @Override
    public Set<Object> keySet() {
        return linkMap.keySet();
    }

    @Override
    public synchronized boolean isEmpty() {
        return linkMap.isEmpty();
    }

    @Override
    public synchronized int size() {
        return linkMap.size();
    }

    @Override
    public synchronized Object get(Object key) {
        return linkMap.get(key);
    }

    @Override
    public void putAll(Map<?, ?> m) {
        linkMap.putAll(m);
    }

    @Override
    public synchronized Enumeration<Object> elements() {
        throw new UnsupportedOperationException("Use keySet() or entrySet() instead");
    }

    @Override
    public synchronized Enumeration<Object> keys() {
        throw new UnsupportedOperationException("Use keySet() or entrySet() instead");
    }

    @Override
    public Enumeration<?> propertyNames() {
        throw new UnsupportedOperationException("Use keySet() or entrySet() instead");
    }

}
