package com.sunecity.lilium.core.time;

import java.time.LocalDateTime;

public class DateTimes {

    public static LocalDateTime from() {
        return LocalDateTime.now();
    }

    public static String format(LocalDateTime dateTime) {
        return dateTime.toString();
    }

}
