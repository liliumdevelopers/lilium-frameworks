package com.sunecity.lilium.core.comm;

import purejavacomm.CommPortIdentifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
public class CommPort {

    private final String portName;

    private CommPort(String portName) {
        this.portName = portName;
    }

    public static CommPort getSerialCommPort(String portName) throws IOException {
        return getSerialCommPorts().stream().filter(c -> c.getPortName().equals(portName)).findAny().orElseThrow(() -> new IOException("CommPort " + portName + " not found."));
    }

    public static List<CommPort> getSerialCommPorts() {
        List<CommPort> commPorts = new ArrayList<>();
        Enumeration<CommPortIdentifier> commPortIdentifiers = CommPortIdentifier.getPortIdentifiers();
        while (commPortIdentifiers.hasMoreElements()) {
            CommPortIdentifier commPortIdentifier = commPortIdentifiers.nextElement();
            if (commPortIdentifier.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                commPorts.add(new CommPort(commPortIdentifier.getName()));
            }
        }
        return commPorts;
    }

    public String getPortName() {
        return portName;
    }

    @Override
    public String toString() {
        return "CommPort: " + portName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommPort commPort = (CommPort) o;

        return portName.equals(commPort.portName);

    }

    @Override
    public int hashCode() {
        return portName.hashCode();
    }

}
