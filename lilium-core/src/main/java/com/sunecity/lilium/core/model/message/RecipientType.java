package com.sunecity.lilium.core.model.message;

/**
 * @author Shahram Goodarzi
 */
public enum RecipientType {

    TO, CC, BCC

}
