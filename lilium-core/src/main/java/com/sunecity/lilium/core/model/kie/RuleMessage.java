package com.sunecity.lilium.core.model.kie;

import java.io.Serializable;

/**
 * Created by shahram on 7/21/15.
 */
public class RuleMessage implements Serializable {

    private String message;

    public RuleMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
