package com.sunecity.lilium.core.validation;

/**
 * @author Shahram Goodarzi
 */
public class CompanyId {

    public static boolean isValid(String id) {
        char[] chars = id.toCharArray();
        int control = Character.getNumericValue(chars[10]);
        int sum = 0;
        for (int i = 0, j[] = {29, 27, 23, 19, 17}, factor = Character.getNumericValue(chars[9]) + 2; i < 10; i++) {
            sum += factor + Character.getNumericValue(chars[i]) * j[i % 5];
        }
        return sum % 11 == control;
    }

}
