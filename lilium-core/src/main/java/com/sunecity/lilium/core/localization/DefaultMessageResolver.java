package com.sunecity.lilium.core.localization;

import javax.enterprise.context.Dependent;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

@Dependent
@SuppressWarnings("UnusedDeclaration")
public class DefaultMessageResolver implements MessageResolver {
    private static final long serialVersionUID = 5834411208472341006L;

    @Override
    public String getMessage(MessageContext messageContext, String messageTemplate, String category) {
        // we can use {{ as escaping for now
        if (messageTemplate.startsWith("{{")) {
            // in which case we just cut of the first '{'
            return messageTemplate.substring(1);
        }

        if (messageTemplate.startsWith("{") && messageTemplate.endsWith("}")) {
            String resourceKey = messageTemplate.substring(1, messageTemplate.length() - 1);

            List<String> messageSources = getMessageSources(messageContext);

            if (messageSources == null || messageSources.isEmpty()) {
                // using {} without a bundle is always an error
                return null;
            }

            Iterator<String> messageSourceIterator = messageSources.iterator();

            Locale locale = messageContext.getLocale();

            String currentMessageSource;
            while (messageSourceIterator.hasNext()) {
                currentMessageSource = messageSourceIterator.next();

                try {
                    ResourceBundle messageBundle = getResourceBundle(currentMessageSource, locale);

                    if (category != null && category.length() > 0) {
                        try {
                            return messageBundle.getString(resourceKey + "_" + category);
                        } catch (MissingResourceException e) {
                            // we fallback on the version without the category
                            return messageBundle.getString(resourceKey);
                        }
                    }

                    return messageBundle.getString(resourceKey);
                } catch (MissingResourceException e) {
                    if (!messageSourceIterator.hasNext()) {
                        return null;
                    }
                }
            }
        }

        return messageTemplate;
    }

    protected List<String> getMessageSources(MessageContext messageContext) {
        return messageContext.getMessageSources();
    }

    public static ResourceBundle getResourceBundle(String bundleName, Locale locale) {
        return ResourceBundle.getBundle(bundleName, locale, getClassLoader(null));
    }

    public static ClassLoader getClassLoader(Object o) {
        return System.getSecurityManager() != null? AccessController.doPrivileged(new GetClassLoaderAction(o)) :getClassLoaderInternal(o);
    }

    static class GetClassLoaderAction implements PrivilegedAction<ClassLoader> {
        private Object object;

        GetClassLoaderAction(Object object) {
            this.object = object;
        }

        public ClassLoader run() {
            try {
                return getClassLoaderInternal(this.object);
            } catch (Exception var2) {
                return null;
            }
        }
    }

    private static ClassLoader getClassLoaderInternal(Object o) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        if(loader == null && o != null) {
            loader = o.getClass().getClassLoader();
        }

        if(loader == null) {
            loader = GetClassLoaderAction.class.getClassLoader();
        }

        return loader;
    }

}
