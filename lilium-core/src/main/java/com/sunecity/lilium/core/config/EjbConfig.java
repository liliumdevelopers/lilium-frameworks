package com.sunecity.lilium.core.config;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:META-INF/ejb.properties"})
public interface EjbConfig extends Config {

    @DefaultValue("")
    default String appName() {
        return "";
    }

    String moduleName();

}
