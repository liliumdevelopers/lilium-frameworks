package com.sunecity.lilium.core.model.kie;

import com.sunecity.lilium.core.time.Date;

import java.io.Serializable;

/**
 * Created by shahram on 5/23/16.
 */
public class Task implements Serializable {

    private Long id;
    private String name;
    private String statusId;
    private Integer priority;
    private String actualOwnerId;
    private String actualOwner;
    private String createdById;
    private String createdBy;
    private Date createdOn;
    private Date activationTime;
    private Date expirationTime;
    private String processId;
    private Long processInstanceId;
    private Long processSessionId;
    private String deploymentId;
    private Long parentId;
    private String subject;
    private String description;
    private TaskStatus status;
    private Boolean skippable;
    private Boolean quickTaskSummary;
    private String entity;
    private String entityId;

    public Task() {
    }

    public Task(Long id, String name, String statusId, Integer priority, String actualOwnerId, String actualOwner, String createdById, String createdBy, Date createdOn, Date activationTime, Date expirationTime, String processId, Long processInstanceId, Long processSessionId, String deploymentId, Long parentId, String subject, String description, TaskStatus status, Boolean skippable, Boolean quickTaskSummary) {
        this.id = id;
        this.name = name;
        this.statusId = statusId;
        this.priority = priority;
        this.actualOwnerId = actualOwnerId;
        this.actualOwner = actualOwner;
        this.createdById = createdById;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.activationTime = activationTime;
        this.expirationTime = expirationTime;
        this.processId = processId;
        this.processInstanceId = processInstanceId;
        this.processSessionId = processSessionId;
        this.deploymentId = deploymentId;
        this.parentId = parentId;
        this.subject = subject;
        this.description = description;
        this.status = status;
        this.skippable = skippable;
        this.quickTaskSummary = quickTaskSummary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getActualOwnerId() {
        return actualOwnerId;
    }

    public void setActualOwnerId(String actualOwnerId) {
        this.actualOwnerId = actualOwnerId;
    }

    public String getActualOwner() {
        return actualOwner;
    }

    public void setActualOwner(String actualOwner) {
        this.actualOwner = actualOwner;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getActivationTime() {
        return activationTime;
    }

    public void setActivationTime(Date activationTime) {
        this.activationTime = activationTime;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public Long getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(Long processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public Long getProcessSessionId() {
        return processSessionId;
    }

    public void setProcessSessionId(Long processSessionId) {
        this.processSessionId = processSessionId;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public Boolean getSkippable() {
        return skippable;
    }

    public void setSkippable(Boolean skippable) {
        this.skippable = skippable;
    }

    public Boolean getQuickTaskSummary() {
        return quickTaskSummary;
    }

    public void setQuickTaskSummary(Boolean quickTaskSummary) {
        this.quickTaskSummary = quickTaskSummary;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (id != null ? !id.equals(task.id) : task.id != null) return false;
        if (name != null ? !name.equals(task.name) : task.name != null) return false;
        if (statusId != null ? !statusId.equals(task.statusId) : task.statusId != null) return false;
        if (priority != null ? !priority.equals(task.priority) : task.priority != null) return false;
        if (actualOwnerId != null ? !actualOwnerId.equals(task.actualOwnerId) : task.actualOwnerId != null)
            return false;
        if (actualOwner != null ? !actualOwner.equals(task.actualOwner) : task.actualOwner != null) return false;
        if (createdById != null ? !createdById.equals(task.createdById) : task.createdById != null) return false;
        if (createdBy != null ? !createdBy.equals(task.createdBy) : task.createdBy != null) return false;
        if (createdOn != null ? !createdOn.equals(task.createdOn) : task.createdOn != null) return false;
        if (activationTime != null ? !activationTime.equals(task.activationTime) : task.activationTime != null)
            return false;
        if (expirationTime != null ? !expirationTime.equals(task.expirationTime) : task.expirationTime != null)
            return false;
        if (processId != null ? !processId.equals(task.processId) : task.processId != null) return false;
        if (processInstanceId != null ? !processInstanceId.equals(task.processInstanceId) : task.processInstanceId != null)
            return false;
        if (processSessionId != null ? !processSessionId.equals(task.processSessionId) : task.processSessionId != null)
            return false;
        if (deploymentId != null ? !deploymentId.equals(task.deploymentId) : task.deploymentId != null) return false;
        if (parentId != null ? !parentId.equals(task.parentId) : task.parentId != null) return false;
        if (subject != null ? !subject.equals(task.subject) : task.subject != null) return false;
        if (description != null ? !description.equals(task.description) : task.description != null) return false;
        if (status != task.status) return false;
        if (skippable != null ? !skippable.equals(task.skippable) : task.skippable != null) return false;
        if (quickTaskSummary != null ? !quickTaskSummary.equals(task.quickTaskSummary) : task.quickTaskSummary != null)
            return false;
        if (entity != null ? !entity.equals(task.entity) : task.entity != null) return false;
        return entityId != null ? entityId.equals(task.entityId) : task.entityId == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (statusId != null ? statusId.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        result = 31 * result + (actualOwnerId != null ? actualOwnerId.hashCode() : 0);
        result = 31 * result + (actualOwner != null ? actualOwner.hashCode() : 0);
        result = 31 * result + (createdById != null ? createdById.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdOn != null ? createdOn.hashCode() : 0);
        result = 31 * result + (activationTime != null ? activationTime.hashCode() : 0);
        result = 31 * result + (expirationTime != null ? expirationTime.hashCode() : 0);
        result = 31 * result + (processId != null ? processId.hashCode() : 0);
        result = 31 * result + (processInstanceId != null ? processInstanceId.hashCode() : 0);
        result = 31 * result + (processSessionId != null ? processSessionId.hashCode() : 0);
        result = 31 * result + (deploymentId != null ? deploymentId.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (skippable != null ? skippable.hashCode() : 0);
        result = 31 * result + (quickTaskSummary != null ? quickTaskSummary.hashCode() : 0);
        result = 31 * result + (entity != null ? entity.hashCode() : 0);
        result = 31 * result + (entityId != null ? entityId.hashCode() : 0);
        return result;
    }

}
