package com.sunecity.lilium.core.localization;

import java.io.Serializable;
import java.util.Locale;

/**
 * Implementations are responsible to replace placeholders in a message with the final value.
 * <p>
 * <p>
 * An application can provide a custom implementation as &#064;Alternative.</p>
 * <p>
 * <p>
 * A simple implementation which uses the {@link String#format(Locale, String, Object...)} will be used by
 * default.</p>
 */
public interface MessageInterpolator extends Serializable {
    /**
     * Replaces the arguments of the given message with the given arguments.
     *
     * @param messageText the message text which has to be interpolated
     * @param arguments   a list of numbered and/or named arguments for the current message
     * @param locale      to use for the formatting
     * @return the final (interpolated) message text if it was possible to replace the parameters with the given
     * arguments, or the unmodified messageText otherwise
     */
    String interpolate(String messageText, Serializable[] arguments, Locale locale);
}
