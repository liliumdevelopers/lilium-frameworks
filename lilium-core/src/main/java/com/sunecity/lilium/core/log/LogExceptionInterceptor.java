package com.sunecity.lilium.core.log;

import com.sunecity.lilium.core.exception.ClientException;
import com.sunecity.lilium.core.exception.LiliumException;
import org.apache.commons.beanutils.ConstructorUtils;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;
import java.lang.reflect.Constructor;

@Interceptor
@LogException
@Priority(Interceptor.Priority.LIBRARY_BEFORE + 501)
public class LogExceptionInterceptor implements Serializable {

    @AroundInvoke
    public Object aroundInvoke(InvocationContext ctx) throws Exception {
        LogException logException = ctx.getMethod().getAnnotation(LogException.class);
        try {
            return ctx.proceed();
        } catch (Exception e) {
            if (!logException.exception().isAssignableFrom(e.getClass())) {
                throw e;
            } else {
                if (logException.throwException() == LiliumException.class) {
                    if (e instanceof ClientException) {
                        throw e;
                    } else {
                        log(logException.value(), e, ctx);
                        throw new LiliumException(e);
                    }
                } else {
                    log(logException.value(), e, ctx);
                    String message = logException.value().equals("") ? "Invocation of " + ctx.getTarget() + "::" + ctx.getMethod().getName() + " failed." : logException.value();
                    Constructor<? extends Exception> constructor = ConstructorUtils.getAccessibleConstructor(logException.throwException(), new Class<?>[]{String.class, Throwable.class});
                    if (constructor == null) {
                        constructor = ConstructorUtils.getAccessibleConstructor(logException.throwException(), new Class<?>[]{Throwable.class, String.class});
                        if (constructor == null) {
                            constructor = ConstructorUtils.getAccessibleConstructor(logException.throwException(), String.class);
                            if (constructor == null) {
                                constructor = ConstructorUtils.getAccessibleConstructor(logException.throwException(), Throwable.class);
                                if (constructor == null) {
                                    throw logException.throwException().getConstructor().newInstance();
                                } else {
                                    throw constructor.newInstance(e);
                                }
                            } else {
                                throw constructor.newInstance(message);
                            }
                        } else {
                            throw constructor.newInstance(e, message);
                        }
                    } else {
                        throw constructor.newInstance(message, e);
                    }
                }
            }
        }
    }

    private void log(String value, Exception e, InvocationContext ctx) throws Exception {
        String message = value.equals("") ? "Invocation of " + ctx.getTarget() + "::" + ctx.getMethod().getName() + " failed." : value;
        LoggerFactory.getLogger(ctx.getTarget().getClass()).error(message, e);
        removeStackTrace(e);
    }

    private void removeStackTrace(Throwable throwable) throws Exception {
        throwable.setStackTrace(new StackTraceElement[0]);
        if (throwable.getCause() != null) {
            removeStackTrace(throwable.getCause());
        }
    }

}