package com.sunecity.lilium.core.model;

import com.sunecity.lilium.core.time.DateTime;

/**
 * Created by shahram on 2/2/17.
 */
public interface AuditLog {

    String getCreateUser();

    void setCreateUser(String createUser);

    String getUpdateUser();

    void setUpdateUser(String updateUser);

    DateTime getCreateTimestamp();

    void setCreateTimestamp(DateTime createTimestamp);

    DateTime getUpdateTimestamp();

    void setUpdateTimestamp(DateTime updateTimestamp);

}
