package com.sunecity.lilium.core.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Shahram Goodarzi
 */
public final class CdTray {

    private CdTray() {
    }

    public static void open(String drive) throws IOException, InterruptedException {
        if (Platform.isUnixLike()) {
            Runtime.getRuntime().exec("eject " + drive).waitFor();
        } else if (Platform.isWindows()) {
            File file = File.createTempFile("realhowto", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new FileWriter(file);
            String vbs = "Set wmp = CreateObject(\"WMPlayer.OCX\") \n"
                    + "Set cd = wmp.cdromCollection.getByDriveSpecifier(\""
                    + drive + "\") \n" + "cd.Eject";
            fw.write(vbs);
            fw.close();
            Runtime.getRuntime().exec("wscript " + file.getPath()).waitFor();
        } else if (Platform.isMac()) {
            if (drive != null) {
                Runtime.getRuntime().exec("drutil tray close -open" + drive).waitFor();
            } else {
                Runtime.getRuntime().exec("drutil tray open").waitFor();
            }
        } else {
            throw new UnsupportedOperationException("Os not Supported.");
        }
    }

    public static void close(String drive) throws IOException, InterruptedException {
        if (Platform.isUnixLike()) {
            Runtime.getRuntime().exec("eject -t " + drive).waitFor();
        } else if (Platform.isWindows()) {
            File file = File.createTempFile("realhowto", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new FileWriter(file);
            String vbs = "Set wmp = CreateObject(\"WMPlayer.OCX\") \n"
                    + "Set cd = wmp.cdromCollection.getByDriveSpecifier(\""
                    + drive + "\") \n" + "cd.Eject \n " + "cd.Eject ";
            fw.write(vbs);
            fw.close();
            Runtime.getRuntime().exec("wscript " + file.getPath()).waitFor();
        } else if (Platform.isMac()) {
            if (drive != null) {
                Runtime.getRuntime().exec("drutil tray close -drive" + drive).waitFor();
            } else {
                Runtime.getRuntime().exec("drutil tray close").waitFor();
            }
        } else {
            throw new UnsupportedOperationException("Os not Supported.");
        }
    }

}
