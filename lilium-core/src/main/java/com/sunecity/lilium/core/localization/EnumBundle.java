package com.sunecity.lilium.core.localization;

public interface EnumBundle {

	String getBundle();

}
