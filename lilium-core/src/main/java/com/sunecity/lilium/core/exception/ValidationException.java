package com.sunecity.lilium.core.exception;

import com.sunecity.lilium.core.validation.ValidateMessage;

import javax.ejb.ApplicationException;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by shahram on 1/28/17.
 */
@ApplicationException
public class ValidationException extends ClientException {

    private List<ValidateMessage> validateMessages;

    public ValidationException(List<ValidateMessage> validateMessages) {
        super(null, "");
        this.validateMessages = validateMessages;
    }

    public ValidationException(Response response, List<ValidateMessage> validateMessages) {
        super(response, "");
        this.validateMessages = validateMessages;
    }

    public List<ValidateMessage> getValidateMessages() {
        return validateMessages;
    }

    public void setValidateMessages(List<ValidateMessage> validateMessages) {
        this.validateMessages = validateMessages;
    }

}
