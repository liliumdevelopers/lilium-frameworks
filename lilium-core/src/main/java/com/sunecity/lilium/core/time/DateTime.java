package com.sunecity.lilium.core.time;

import java.io.Serializable;
import java.time.Clock;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.time.chrono.ChronoZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalField;
import java.time.temporal.TemporalQueries;
import java.time.temporal.TemporalQuery;
import java.time.temporal.TemporalUnit;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.time.temporal.ValueRange;
import java.time.zone.ZoneRules;
import java.util.Objects;

import static com.sunecity.lilium.core.time.Time.HOURS_PER_DAY;
import static com.sunecity.lilium.core.time.Time.MICROS_PER_DAY;
import static com.sunecity.lilium.core.time.Time.MILLIS_PER_DAY;
import static com.sunecity.lilium.core.time.Time.MINUTES_PER_DAY;
import static com.sunecity.lilium.core.time.Time.NANOS_PER_DAY;
import static com.sunecity.lilium.core.time.Time.NANOS_PER_HOUR;
import static com.sunecity.lilium.core.time.Time.NANOS_PER_MINUTE;
import static com.sunecity.lilium.core.time.Time.NANOS_PER_SECOND;
import static com.sunecity.lilium.core.time.Time.SECONDS_PER_DAY;
import static java.time.temporal.ChronoField.NANO_OF_SECOND;

/**
 * A date-time without a time-zone.
 */
public final class DateTime implements Temporal, TemporalAdjuster, ChronoLocalDateTime<Date>, Serializable {

    /**
     * The minimum supported {@code DateTime}, '-999999999-01-01T00:00:00'.
     * This is the local date-time of midnight at the start of the minimum date.
     * This combines {@link Date#MIN} and {@link Time#MIN}.
     * This could be used by an application as a "far past" date-time.
     */
    public static final DateTime MIN = DateTime.of(Date.MIN, Time.MIN);
    /**
     * The maximum supported {@code DateTime}, '+999999999-12-31T23:59:59.999999999'.
     * This is the local date-time just before midnight at the end of the maximum date.
     * This combines {@link Date#MAX} and {@link Time#MAX}.
     * This could be used by an application as a "far future" date-time.
     */
    public static final DateTime MAX = DateTime.of(Date.MAX, Time.MAX);

    /**
     * Serialization version.
     */
    private static final long serialVersionUID = 6207766400415563566L;

    /**
     * The date part.
     */
    private final Date date;
    /**
     * The time part.
     */
    private final Time time;

    /**
     * Constructor.
     *
     * @param date the date part of the date-time, validated not null
     * @param time the time part of the date-time, validated not null
     */
    private DateTime(Date date, Time time) {
        this.date = date;
        this.time = time;
    }

    /**
     * Obtains the current date-time from the system clock in the default time-zone.
     *
     * @return the current date-time using the system clock and default time-zone, not null
     */
    public static DateTime now() {
        return now(Clock.systemDefaultZone());
    }

    /**
     * Obtains the current date-time from the system clock in the specified time-zone.
     *
     * @param zone the zone ID to use, not null
     * @return the current date-time using the system clock, not null
     */
    public static DateTime now(ZoneId zone) {
        return now(Clock.system(zone));
    }

    /**
     * Obtains the current date-time from the specified clock.
     *
     * @param clock the clock to use, not null
     * @return the current date-time, not null
     */
    public static DateTime now(Clock clock) {
        //TODO
        Objects.requireNonNull(clock, "clock");
        final Instant now = clock.instant();
        ZoneOffset offset = clock.getZone().getRules().getOffset(now);
        return ofEpochSecond(now.getEpochSecond(), now.getNano(), offset);
    }

    /**
     * Obtains an instance of {@code DateTime} from year, month,
     * day, hour and minute, setting the second and nanosecond to zero.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, not null
     * @param dayOfMonth the day-of-month to represent, from 1 to 31
     * @param hour       the hour-of-day to represent, from 0 to 23
     * @param minute     the minute-of-hour to represent, from 0 to 59
     * @return the local date-time, not null
     * @throws DateTimeException if the value of any field is out of range,
     *                           or if the day-of-month is invalid for the month-year
     */
    public static DateTime of(int year, Month month, int dayOfMonth, int hour, int minute) {
        Date date = Date.of(year, month, dayOfMonth);
        Time time = Time.of(hour, minute);
        return new DateTime(date, time);
    }

    /**
     * Obtains an instance of {@code DateTime} from year, month,
     * day, hour, minute and second, setting the nanosecond to zero.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, not null
     * @param dayOfMonth the day-of-month to represent, from 1 to 31
     * @param hour       the hour-of-day to represent, from 0 to 23
     * @param minute     the minute-of-hour to represent, from 0 to 59
     * @param second     the second-of-minute to represent, from 0 to 59
     * @return the local date-time, not null
     * @throws DateTimeException if the value of any field is out of range,
     *                           or if the day-of-month is invalid for the month-year
     */
    public static DateTime of(int year, Month month, int dayOfMonth, int hour, int minute, int second) {
        Date date = Date.of(year, month, dayOfMonth);
        Time time = Time.of(hour, minute, second);
        return new DateTime(date, time);
    }

    /**
     * Obtains an instance of {@code DateTime} from year, month,
     * day, hour, minute, second and nanosecond.
     *
     * @param year         the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month        the month-of-year to represent, not null
     * @param dayOfMonth   the day-of-month to represent, from 1 to 31
     * @param hour         the hour-of-day to represent, from 0 to 23
     * @param minute       the minute-of-hour to represent, from 0 to 59
     * @param second       the second-of-minute to represent, from 0 to 59
     * @param nanoOfSecond the nano-of-second to represent, from 0 to 999,999,999
     * @return the local date-time, not null
     * @throws DateTimeException if the value of any field is out of range,
     *                           or if the day-of-month is invalid for the month-year
     */
    public static DateTime of(int year, Month month, int dayOfMonth, int hour, int minute, int second, int nanoOfSecond) {
        Date date = Date.of(year, month, dayOfMonth);
        Time time = Time.of(hour, minute, second, nanoOfSecond);
        return new DateTime(date, time);
    }

    /**
     * Obtains an instance of {@code DateTime} from year, month,
     * day, hour and minute, setting the second and nanosecond to zero.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, not null
     * @param dayOfMonth the day-of-month to represent, from 1 to 31
     * @param hour       the hour-of-day to represent, from 0 to 23
     * @param minute     the minute-of-hour to represent, from 0 to 59
     * @return the local date-time, not null
     * @throws DateTimeException if the value of any field is out of range,
     *                           or if the day-of-month is invalid for the month-year
     */
    public static DateTime of(int year, PersianMonth month, int dayOfMonth, int hour, int minute) {
        Date date = Date.of(year, month, dayOfMonth);
        Time time = Time.of(hour, minute);
        return new DateTime(date, time);
    }

    /**
     * Obtains an instance of {@code DateTime} from year, month,
     * day, hour, minute and second, setting the nanosecond to zero.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, not null
     * @param dayOfMonth the day-of-month to represent, from 1 to 31
     * @param hour       the hour-of-day to represent, from 0 to 23
     * @param minute     the minute-of-hour to represent, from 0 to 59
     * @param second     the second-of-minute to represent, from 0 to 59
     * @return the local date-time, not null
     * @throws DateTimeException if the value of any field is out of range,
     *                           or if the day-of-month is invalid for the month-year
     */
    public static DateTime of(int year, PersianMonth month, int dayOfMonth, int hour, int minute, int second) {
        Date date = Date.of(year, month, dayOfMonth);
        Time time = Time.of(hour, minute, second);
        return new DateTime(date, time);
    }

    /**
     * Obtains an instance of {@code DateTime} from year, month,
     * day, hour, minute, second and nanosecond.
     *
     * @param year         the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month        the month-of-year to represent, not null
     * @param dayOfMonth   the day-of-month to represent, from 1 to 31
     * @param hour         the hour-of-day to represent, from 0 to 23
     * @param minute       the minute-of-hour to represent, from 0 to 59
     * @param second       the second-of-minute to represent, from 0 to 59
     * @param nanoOfSecond the nano-of-second to represent, from 0 to 999,999,999
     * @return the local date-time, not null
     * @throws DateTimeException if the value of any field is out of range,
     *                           or if the day-of-month is invalid for the month-year
     */
    public static DateTime of(int year, PersianMonth month, int dayOfMonth, int hour, int minute, int second, int nanoOfSecond) {
        Date date = Date.of(year, month, dayOfMonth);
        Time time = Time.of(hour, minute, second, nanoOfSecond);
        return new DateTime(date, time);
    }

    /**
     * Obtains an instance of {@code DateTime} from a date and time.
     *
     * @param date the local date, not null
     * @param time the local time, not null
     * @return the local date-time, not null
     */
    public static DateTime of(Date date, Time time) {
        Objects.requireNonNull(date, "date");
        Objects.requireNonNull(time, "time");
        return new DateTime(date, time);
    }

    /**
     * Obtains an instance of {@code DateTime} from an {@code Instant} and zone ID.
     *
     * @param instant the instant to create the date-time from, not null
     * @param zone    the time-zone, which may be an offset, not null
     * @return the local date-time, not null
     * @throws DateTimeException if the result exceeds the supported range
     */
    public static DateTime ofInstant(Instant instant, ZoneId zone) {
        Objects.requireNonNull(instant, "instant");
        Objects.requireNonNull(zone, "zone");
        ZoneRules rules = zone.getRules();
        ZoneOffset offset = rules.getOffset(instant);
        return ofEpochSecond(instant.getEpochSecond(), instant.getNano(), offset);
    }

    /**
     * Obtains an instance of {@code DateTime} using seconds from the
     * epoch of 1970-01-01T00:00:00Z.
     *
     * @param epochSecond  the number of seconds from the epoch of 1970-01-01T00:00:00Z
     * @param nanoOfSecond the nanosecond within the second, from 0 to 999,999,999
     * @param offset       the zone offset, not null
     * @return the local date-time, not null
     * @throws DateTimeException if the result exceeds the supported range,
     *                           or if the nano-of-second is invalid
     */
    public static DateTime ofEpochSecond(long epochSecond, int nanoOfSecond, ZoneOffset offset) {
        Objects.requireNonNull(offset, "offset");
        NANO_OF_SECOND.checkValidValue(nanoOfSecond);
        long localSecond = epochSecond + offset.getTotalSeconds();
        long localEpochDay = Math.floorDiv(localSecond, SECONDS_PER_DAY);
        int secsOfDay = Math.floorMod(localSecond, SECONDS_PER_DAY);
        Date date = Date.ofEpochDay(localEpochDay);
        Time time = Time.ofNanoOfDay(secsOfDay * NANOS_PER_SECOND + nanoOfSecond);
        return new DateTime(date, time);
    }

    public static DateTime ofEpochSecondPersian(long epochSecond, int nanoOfSecond, ZoneOffset offset) {
        //TODO
        Objects.requireNonNull(offset, "offset");
        NANO_OF_SECOND.checkValidValue(nanoOfSecond);
        long localSecond = epochSecond + offset.getTotalSeconds();
        long localEpochDay = Math.floorDiv(localSecond, SECONDS_PER_DAY);
        int secsOfDay = Math.floorMod(localSecond, SECONDS_PER_DAY);
        Date date = Date.ofEpochDay(localEpochDay);
        Time time = Time.ofNanoOfDay(secsOfDay * NANOS_PER_SECOND + nanoOfSecond);
        return new DateTime(date, time);
    }

    /**
     * Obtains an instance of {@code DateTime} from a temporal object.
     *
     * @param temporal the temporal object to convert, not null
     * @return the local date-time, not null
     * @throws DateTimeException if unable to convert to a {@code DateTime}
     */
    public static DateTime from(TemporalAccessor temporal) {
        if (temporal instanceof DateTime) {
            return (DateTime) temporal;
        }
        try {
            Date date = Date.from(temporal);
            Time time = Time.from(temporal);
            return new DateTime(date, time);
        } catch (DateTimeException ex) {
            throw new DateTimeException("Unable to obtain DateTime from TemporalAccessor: " + temporal + " of type " + temporal.getClass().getName(), ex);
        }
    }

    /**
     * Obtains an instance of {@code DateTime} from a text string such as {@code 2007-12-03T10:15:30}.
     *
     * @param text the text to parse such as "2007-12-03T10:15:30", not null
     * @return the parsed local date-time, not null
     * @throws DateTimeParseException if the text cannot be parsed
     */
    public static DateTime parse(CharSequence text) {
        return parse(text, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    public static DateTime parsePersian(String text) {
        Date date;
        Time time;
        if (text.contains(" ")) {
            date = Date.parsePersian(text.substring(0, text.lastIndexOf(' ')));
            time = Time.parse(text.substring(text.lastIndexOf(' ') + 1, text.length()));
        } else {
            date = Date.parsePersian(text);
            time = Time.of(0, 0, 0, 0);
        }
        return new DateTime(date, time);
    }

    /**
     * Obtains an instance of {@code DateTime} from a text string using a specific formatter.
     *
     * @param text      the text to parse, not null
     * @param formatter the formatter to use, not null
     * @return the parsed local date-time, not null
     * @throws DateTimeParseException if the text cannot be parsed
     */
    public static DateTime parse(CharSequence text, DateTimeFormatter formatter) {
        Objects.requireNonNull(formatter, "formatter");
        return formatter.parse(text, DateTime::from);
    }

    /**
     * Returns a copy of this date-time with the new date and time, checking
     * to see if a new object is in fact required.
     *
     * @param newDate the date of the new date-time, not null
     * @param newTime the time of the new date-time, not null
     * @return the date-time, not null
     */
    private DateTime with(Date newDate, Time newTime) {
        if (date == newDate && time == newTime) {
            return this;
        }
        return new DateTime(newDate, newTime);
    }

    /**
     * Checks if the specified field is supported.
     *
     * @param field the field to check, null returns false
     * @return true if the field is supported on this date-time, false if not
     */
    @Override
    public boolean isSupported(TemporalField field) {
        if (field instanceof PersianField) {
            return field.isDateBased() || field.isTimeBased();
        }
        if (field instanceof ChronoField) {
            return field.isDateBased() || field.isTimeBased();
        }
        return field != null && field.isSupportedBy(this);
    }

    /**
     * Checks if the specified unit is supported.
     *
     * @param unit the unit to check, null returns false
     * @return true if the unit can be added/subtracted, false if not
     */
    @Override
    public boolean isSupported(TemporalUnit unit) {
        return ChronoLocalDateTime.super.isSupported(unit);
    }

    /**
     * Gets the range of valid values for the specified field.
     *
     * @param field the field to query the range for, not null
     * @return the range of valid values for the field, not null
     * @throws DateTimeException                if the range for the field cannot be obtained
     * @throws UnsupportedTemporalTypeException if the field is not supported
     */
    @Override
    public ValueRange range(TemporalField field) {
        if (field instanceof ChronoField || field instanceof PersianField) {
            return (field.isTimeBased() ? time.range(field) : date.range(field));
        }
        return field.rangeRefinedBy(this);
    }

    /**
     * Gets the value of the specified field from this date-time as an {@code int}.
     *
     * @param field the field to get, not null
     * @return the value for the field
     * @throws DateTimeException                if a value for the field cannot be obtained or
     *                                          the value is outside the range of valid values for the field
     * @throws UnsupportedTemporalTypeException if the field is not supported or
     *                                          the range of values exceeds an {@code int}
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public int get(TemporalField field) {
        if (field instanceof ChronoField || field instanceof PersianField) {
            return (field.isTimeBased() ? time.get(field) : date.get(field));
        }
        return ChronoLocalDateTime.super.get(field);
    }

    /**
     * Gets the value of the specified field from this date-time as a {@code long}.
     *
     * @param field the field to get, not null
     * @return the value for the field
     * @throws DateTimeException                if a value for the field cannot be obtained
     * @throws UnsupportedTemporalTypeException if the field is not supported
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public long getLong(TemporalField field) {
        if (field instanceof ChronoField || field instanceof PersianField) {
            return (field.isTimeBased() ? time.getLong(field) : date.getLong(field));
        }
        return field.getFrom(this);
    }

    /**
     * Gets the {@code Date} part of this date-time.
     *
     * @return the date part of this date-time, not null
     */
    @Override
    public Date toLocalDate() {
        return date;
    }

    /**
     * Gets the year field.
     *
     * @return the year, from MIN_YEAR to MAX_YEAR
     */
    public int getYear() {
        return date.getYear();
    }

    /**
     * Gets the month-of-year field from 1 to 12.
     *
     * @return the month-of-year, from 1 to 12
     * @see #getMonth()
     */
    public int getMonthValue() {
        return date.getMonthValue();
    }

    /**
     * Gets the month-of-year field using the {@code Month} enum.
     *
     * @return the month-of-year, not null
     * @see #getMonthValue()
     */
    public Month getMonth() {
        return date.getMonth();
    }

    /**
     * Gets the day-of-month field.
     *
     * @return the day-of-month, from 1 to 31
     */
    public int getDayOfMonth() {
        return date.getDayOfMonth();
    }

    /**
     * Gets the day-of-year field.
     *
     * @return the day-of-year, from 1 to 365, or 366 in a leap year
     */
    public int getDayOfYear() {
        return date.getDayOfYear();
    }

    /**
     * Gets the day-of-week field, which is an enum {@code DayOfWeek}.
     *
     * @return the day-of-week, not null
     */
    public DayOfWeek getDayOfWeek() {
        return date.getDayOfWeek();
    }

    /**
     * Gets the {@code LocalTime} part of this date-time.
     *
     * @return the time part of this date-time, not null
     */
    @Override
    public LocalTime toLocalTime() {
        return time.toLocalTime();
    }

    public Time toTime() {
        return time;
    }

    /**
     * Gets the hour-of-day field.
     *
     * @return the hour-of-day, from 0 to 23
     */
    public int getHour() {
        return time.getHour();
    }

    /**
     * Gets the minute-of-hour field.
     *
     * @return the minute-of-hour, from 0 to 59
     */
    public int getMinute() {
        return time.getMinute();
    }

    /**
     * Gets the second-of-minute field.
     *
     * @return the second-of-minute, from 0 to 59
     */
    public int getSecond() {
        return time.getSecond();
    }

    /**
     * Gets the nano-of-second field.
     *
     * @return the nano-of-second, from 0 to 999,999,999
     */
    public int getNano() {
        return time.getNano();
    }

    /**
     * Returns an adjusted copy of this date-time.
     *
     * @param adjuster the adjuster to use, not null
     * @return a {@code DateTime} based on {@code this} with the adjustment made, not null
     * @throws DateTimeException   if the adjustment cannot be made
     * @throws ArithmeticException if numeric overflow occurs
     */
    @Override
    public DateTime with(TemporalAdjuster adjuster) {
        if (adjuster instanceof Date) {
            return with((Date) adjuster, time);
        } else if (adjuster instanceof Time) {
            return with(date, (Time) adjuster);
        } else if (adjuster instanceof DateTime) {
            return (DateTime) adjuster;
        }
        return (DateTime) adjuster.adjustInto(this);
    }

    /**
     * Returns a copy of this date-time with the specified field set to a new value.
     *
     * @param field    the field to set in the result, not null
     * @param newValue the new value of the field in the result
     * @return a {@code DateTime} based on {@code this} with the specified field set, not null
     * @throws DateTimeException                if the field cannot be set
     * @throws UnsupportedTemporalTypeException if the field is not supported
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public DateTime with(TemporalField field, long newValue) {
        if (field instanceof ChronoField || field instanceof PersianField) {
            if (field.isTimeBased()) {
                return with(date, time.with(field, newValue));
            } else {
                return with(date.with(field, newValue), time);
            }
        }
        return field.adjustInto(this, newValue);
    }

    /**
     * Returns a copy of this {@code DateTime} with the year altered.
     * The time does not affect the calculation and will be the same in the result.
     * If the day-of-month is invalid for the year, it will be changed to the last valid day of the month.
     *
     * @param year the year to set in the result, from MIN_YEAR to MAX_YEAR
     * @return a {@code DateTime} based on this date-time with the requested year, not null
     * @throws DateTimeException if the year value is invalid
     */
    public DateTime withYear(int year) {
        return with(date.withYear(year), time);
    }

    /**
     * Returns a copy of this {@code DateTime} with the month-of-year altered.
     * The time does not affect the calculation and will be the same in the result.
     * If the day-of-month is invalid for the year, it will be changed to the last valid day of the month.
     *
     * @param month the month-of-year to set in the result, from 1 (January) to 12 (December)
     * @return a {@code DateTime} based on this date-time with the requested month, not null
     * @throws DateTimeException if the month-of-year value is invalid
     */
    public DateTime withMonth(int month) {
        return with(date.withMonth(month), time);
    }

    /**
     * Returns a copy of this {@code DateTime} with the day-of-month altered.
     * If the resulting {@code DateTime} is invalid, an exception is thrown.
     * The time does not affect the calculation and will be the same in the result.
     *
     * @param dayOfMonth the day-of-month to set in the result, from 1 to 28-31
     * @return a {@code DateTime} based on this date-time with the requested day, not null
     * @throws DateTimeException if the day-of-month value is invalid,
     *                           or if the day-of-month is invalid for the month-year
     */
    public DateTime withDayOfMonth(int dayOfMonth) {
        return with(date.withDayOfMonth(dayOfMonth), time);
    }

    /**
     * Returns a copy of this {@code DateTime} with the day-of-year altered.
     * If the resulting {@code DateTime} is invalid, an exception is thrown.
     *
     * @param dayOfYear the day-of-year to set in the result, from 1 to 365-366
     * @return a {@code DateTime} based on this date with the requested day, not null
     * @throws DateTimeException if the day-of-year value is invalid,
     *                           or if the day-of-year is invalid for the year
     */
    public DateTime withDayOfYear(int dayOfYear) {
        return with(date.withDayOfYear(dayOfYear), time);
    }

    /**
     * Returns a copy of this {@code DateTime} with the hour-of-day value altered.
     *
     * @param hour the hour-of-day to set in the result, from 0 to 23
     * @return a {@code DateTime} based on this date-time with the requested hour, not null
     * @throws DateTimeException if the hour value is invalid
     */
    public DateTime withHour(int hour) {
        Time newTime = time.withHour(hour);
        return with(date, newTime);
    }

    /**
     * Returns a copy of this {@code DateTime} with the minute-of-hour value altered.
     *
     * @param minute the minute-of-hour to set in the result, from 0 to 59
     * @return a {@code DateTime} based on this date-time with the requested minute, not null
     * @throws DateTimeException if the minute value is invalid
     */
    public DateTime withMinute(int minute) {
        Time newTime = time.withMinute(minute);
        return with(date, newTime);
    }

    /**
     * Returns a copy of this {@code DateTime} with the second-of-minute value altered.
     *
     * @param second the second-of-minute to set in the result, from 0 to 59
     * @return a {@code DateTime} based on this date-time with the requested second, not null
     * @throws DateTimeException if the second value is invalid
     */
    public DateTime withSecond(int second) {
        Time newTime = time.withSecond(second);
        return with(date, newTime);
    }

    /**
     * Returns a copy of this {@code DateTime} with the nano-of-second value altered.
     *
     * @param nanoOfSecond the nano-of-second to set in the result, from 0 to 999,999,999
     * @return a {@code DateTime} based on this date-time with the requested nanosecond, not null
     * @throws DateTimeException if the nano value is invalid
     */
    public DateTime withNano(int nanoOfSecond) {
        Time newTime = time.withNano(nanoOfSecond);
        return with(date, newTime);
    }

    /**
     * Returns a copy of this {@code DateTime} with the time truncated.
     *
     * @param unit the unit to truncate to, not null
     * @return a {@code DateTime} based on this date-time with the time truncated, not null
     * @throws DateTimeException                if unable to truncate
     * @throws UnsupportedTemporalTypeException if the field is not supported
     */
    public DateTime truncatedTo(TemporalUnit unit) {
        return with(date, time.truncatedTo(unit));
    }

    /**
     * Returns a copy of this date-time with the specified amount added.
     *
     * @param amountToAdd the amount to add, not null
     * @return a {@code DateTime} based on this date-time with the addition made, not null
     * @throws DateTimeException   if the addition cannot be made
     * @throws ArithmeticException if numeric overflow occurs
     */
    @Override
    public DateTime plus(TemporalAmount amountToAdd) {
        if (amountToAdd instanceof Period) {
            Period periodToAdd = (Period) amountToAdd;
            return with(date.plus(periodToAdd), time);
        }
        Objects.requireNonNull(amountToAdd, "amountToAdd");
        return (DateTime) amountToAdd.addTo(this);
    }

    /**
     * Returns a copy of this date-time with the specified amount added.
     *
     * @param amountToAdd the amount of the unit to add to the result, may be negative
     * @param unit        the unit of the amount to add, not null
     * @return a {@code DateTime} based on this date-time with the specified amount added, not null
     * @throws DateTimeException                if the addition cannot be made
     * @throws UnsupportedTemporalTypeException if the unit is not supported
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public DateTime plus(long amountToAdd, TemporalUnit unit) {
        if (unit instanceof ChronoUnit) {
            ChronoUnit f = (ChronoUnit) unit;
            switch (f) {
                case NANOS:
                    return plusNanos(amountToAdd);
                case MICROS:
                    return plusDays(amountToAdd / MICROS_PER_DAY).plusNanos((amountToAdd % MICROS_PER_DAY) * 1000);
                case MILLIS:
                    return plusDays(amountToAdd / MILLIS_PER_DAY).plusNanos((amountToAdd % MILLIS_PER_DAY) * 1000_000);
                case SECONDS:
                    return plusSeconds(amountToAdd);
                case MINUTES:
                    return plusMinutes(amountToAdd);
                case HOURS:
                    return plusHours(amountToAdd);
                case HALF_DAYS:
                    return plusDays(amountToAdd / 256).plusHours((amountToAdd % 256) * 12);
            }
            return with(date.plus(amountToAdd, unit), time);
        }
        return unit.addTo(this, amountToAdd);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in years added.
     *
     * @param years the years to add, may be negative
     * @return a {@code DateTime} based on this date-time with the years added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime plusYears(long years) {
        Date newDate = date.plusYears(years);
        return with(newDate, time);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in months added.
     *
     * @param months the months to add, may be negative
     * @return a {@code DateTime} based on this date-time with the months added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime plusMonths(long months) {
        Date newDate = date.plusMonths(months);
        return with(newDate, time);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in weeks added.
     *
     * @param weeks the weeks to add, may be negative
     * @return a {@code DateTime} based on this date-time with the weeks added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime plusWeeks(long weeks) {
        Date newDate = date.plusWeeks(weeks);
        return with(newDate, time);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in days added.
     *
     * @param days the days to add, may be negative
     * @return a {@code DateTime} based on this date-time with the days added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime plusDays(long days) {
        Date newDate = date.plusDays(days);
        return with(newDate, time);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in hours added.
     *
     * @param hours the hours to add, may be negative
     * @return a {@code DateTime} based on this date-time with the hours added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime plusHours(long hours) {
        return plusWithOverflow(date, hours, 0, 0, 0, 1);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in minutes added.
     *
     * @param minutes the minutes to add, may be negative
     * @return a {@code DateTime} based on this date-time with the minutes added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime plusMinutes(long minutes) {
        return plusWithOverflow(date, 0, minutes, 0, 0, 1);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in seconds added.
     *
     * @param seconds the seconds to add, may be negative
     * @return a {@code DateTime} based on this date-time with the seconds added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime plusSeconds(long seconds) {
        return plusWithOverflow(date, 0, 0, seconds, 0, 1);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in nanoseconds added.
     *
     * @param nanos the nanos to add, may be negative
     * @return a {@code DateTime} based on this date-time with the nanoseconds added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime plusNanos(long nanos) {
        return plusWithOverflow(date, 0, 0, 0, nanos, 1);
    }

    /**
     * Returns a copy of this date-time with the specified amount subtracted.
     *
     * @param amountToSubtract the amount to subtract, not null
     * @return a {@code DateTime} based on this date-time with the subtraction made, not null
     * @throws DateTimeException   if the subtraction cannot be made
     * @throws ArithmeticException if numeric overflow occurs
     */
    @Override
    public DateTime minus(TemporalAmount amountToSubtract) {
        if (amountToSubtract instanceof Period) {
            Period periodToSubtract = (Period) amountToSubtract;
            return with(date.minus(periodToSubtract), time);
        }
        Objects.requireNonNull(amountToSubtract, "amountToSubtract");
        return (DateTime) amountToSubtract.subtractFrom(this);
    }

    /**
     * Returns a copy of this date-time with the specified amount subtracted.
     *
     * @param amountToSubtract the amount of the unit to subtract from the result, may be negative
     * @param unit             the unit of the amount to subtract, not null
     * @return a {@code DateTime} based on this date-time with the specified amount subtracted, not null
     * @throws DateTimeException                if the subtraction cannot be made
     * @throws UnsupportedTemporalTypeException if the unit is not supported
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public DateTime minus(long amountToSubtract, TemporalUnit unit) {
        return (amountToSubtract == Long.MIN_VALUE ? plus(Long.MAX_VALUE, unit).plus(1, unit) : plus(-amountToSubtract, unit));
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in years subtracted.
     *
     * @param years the years to subtract, may be negative
     * @return a {@code DateTime} based on this date-time with the years subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime minusYears(long years) {
        return (years == Long.MIN_VALUE ? plusYears(Long.MAX_VALUE).plusYears(1) : plusYears(-years));
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in months subtracted.
     *
     * @param months the months to subtract, may be negative
     * @return a {@code DateTime} based on this date-time with the months subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime minusMonths(long months) {
        return (months == Long.MIN_VALUE ? plusMonths(Long.MAX_VALUE).plusMonths(1) : plusMonths(-months));
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in weeks subtracted.
     *
     * @param weeks the weeks to subtract, may be negative
     * @return a {@code DateTime} based on this date-time with the weeks subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime minusWeeks(long weeks) {
        return (weeks == Long.MIN_VALUE ? plusWeeks(Long.MAX_VALUE).plusWeeks(1) : plusWeeks(-weeks));
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in days subtracted.
     *
     * @param days the days to subtract, may be negative
     * @return a {@code DateTime} based on this date-time with the days subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime minusDays(long days) {
        return (days == Long.MIN_VALUE ? plusDays(Long.MAX_VALUE).plusDays(1) : plusDays(-days));
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in hours subtracted.
     *
     * @param hours the hours to subtract, may be negative
     * @return a {@code DateTime} based on this date-time with the hours subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime minusHours(long hours) {
        return plusWithOverflow(date, hours, 0, 0, 0, -1);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in minutes subtracted.
     *
     * @param minutes the minutes to subtract, may be negative
     * @return a {@code DateTime} based on this date-time with the minutes subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime minusMinutes(long minutes) {
        return plusWithOverflow(date, 0, minutes, 0, 0, -1);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in seconds subtracted.
     *
     * @param seconds the seconds to subtract, may be negative
     * @return a {@code DateTime} based on this date-time with the seconds subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime minusSeconds(long seconds) {
        return plusWithOverflow(date, 0, 0, seconds, 0, -1);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period in nanoseconds subtracted.
     *
     * @param nanos the nanos to subtract, may be negative
     * @return a {@code DateTime} based on this date-time with the nanoseconds subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public DateTime minusNanos(long nanos) {
        return plusWithOverflow(date, 0, 0, 0, nanos, -1);
    }

    /**
     * Returns a copy of this {@code DateTime} with the specified period added.
     *
     * @param newDate the new date to base the calculation on, not null
     * @param hours   the hours to add, may be negative
     * @param minutes the minutes to add, may be negative
     * @param seconds the seconds to add, may be negative
     * @param nanos   the nanos to add, may be negative
     * @param sign    the sign to determine add or subtract
     * @return the combined result, not null
     */
    private DateTime plusWithOverflow(Date newDate, long hours, long minutes, long seconds, long nanos, int sign) {
        if ((hours | minutes | seconds | nanos) == 0) {
            return with(newDate, time);
        }
        long totDays = nanos / NANOS_PER_DAY + seconds / SECONDS_PER_DAY + minutes / MINUTES_PER_DAY + hours / HOURS_PER_DAY;
        totDays *= sign;
        long totNanos = nanos % NANOS_PER_DAY + (seconds % SECONDS_PER_DAY) * NANOS_PER_SECOND + (minutes % MINUTES_PER_DAY) * NANOS_PER_MINUTE + (hours % HOURS_PER_DAY) * NANOS_PER_HOUR;
        long curNoD = time.toNanoOfDay();
        totNanos = totNanos * sign + curNoD;
        totDays += Math.floorDiv(totNanos, NANOS_PER_DAY);
        long newNoD = Math.floorMod(totNanos, NANOS_PER_DAY);
        Time newTime = (newNoD == curNoD ? time : Time.ofNanoOfDay(newNoD));
        return with(newDate.plusDays(totDays), newTime);
    }

    /**
     * Queries this date-time using the specified query.
     *
     * @param <R>   the type of the result
     * @param query the query to invoke, not null
     * @return the query result, null may be returned (defined by the query)
     * @throws DateTimeException   if unable to query (defined by the query)
     * @throws ArithmeticException if numeric overflow occurs (defined by the query)
     */
    @SuppressWarnings("unchecked")
    @Override
    public <R> R query(TemporalQuery<R> query) {
        if (query == TemporalQueries.localDate()) {
            return (R) date;
        }
        return ChronoLocalDateTime.super.query(query);
    }

    /**
     * Adjusts the specified temporal object to have the same date and time as this object.
     *
     * @param temporal the target object to be adjusted, not null
     * @return the adjusted object, not null
     * @throws DateTimeException   if unable to make the adjustment
     * @throws ArithmeticException if numeric overflow occurs
     */
    @Override
    public Temporal adjustInto(Temporal temporal) {
        return ChronoLocalDateTime.super.adjustInto(temporal);
    }

    /**
     * Calculates the amount of time until another date-time in terms of the specified unit.
     *
     * @param endExclusive the end date, exclusive, which is converted to a {@code DateTime}, not null
     * @param unit         the unit to measure the amount in, not null
     * @return the amount of time between this date-time and the end date-time
     * @throws DateTimeException                if the amount cannot be calculated, or the end
     *                                          temporal cannot be converted to a {@code DateTime}
     * @throws UnsupportedTemporalTypeException if the unit is not supported
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public long until(Temporal endExclusive, TemporalUnit unit) {
        DateTime end = DateTime.from(endExclusive);
        if (unit instanceof ChronoUnit) {
            if (unit.isTimeBased()) {
                long amount = date.daysUntil(end.date);
                if (amount == 0) {
                    return time.until(end.time, unit);
                }
                long timePart = end.time.toNanoOfDay() - time.toNanoOfDay();
                if (amount > 0) {
                    amount--;
                    timePart += NANOS_PER_DAY;
                } else {
                    amount++;
                    timePart -= NANOS_PER_DAY;
                }
                switch ((ChronoUnit) unit) {
                    case NANOS:
                        amount = Math.multiplyExact(amount, NANOS_PER_DAY);
                        break;
                    case MICROS:
                        amount = Math.multiplyExact(amount, MICROS_PER_DAY);
                        timePart = timePart / 1000;
                        break;
                    case MILLIS:
                        amount = Math.multiplyExact(amount, MILLIS_PER_DAY);
                        timePart = timePart / 1_000_000;
                        break;
                    case SECONDS:
                        amount = Math.multiplyExact(amount, SECONDS_PER_DAY);
                        timePart = timePart / NANOS_PER_SECOND;
                        break;
                    case MINUTES:
                        amount = Math.multiplyExact(amount, MINUTES_PER_DAY);
                        timePart = timePart / NANOS_PER_MINUTE;
                        break;
                    case HOURS:
                        amount = Math.multiplyExact(amount, HOURS_PER_DAY);
                        timePart = timePart / NANOS_PER_HOUR;
                        break;
                    case HALF_DAYS:
                        amount = Math.multiplyExact(amount, 2);
                        timePart = timePart / (NANOS_PER_HOUR * 12);
                        break;
                }
                return Math.addExact(amount, timePart);
            }
            Date endDate = end.date;
            if (endDate.isAfter(date) && end.time.isBefore(time)) {
                endDate = endDate.minusDays(1);
            } else if (endDate.isBefore(date) && end.time.isAfter(time)) {
                endDate = endDate.plusDays(1);
            }
            return date.until(endDate, unit);
        }
        return unit.between(this, end);
    }

    /**
     * Formats this date-time using the specified formatter.
     *
     * @param formatter the formatter to use, not null
     * @return the formatted date-time string, not null
     * @throws DateTimeException if an error occurs during printing
     */
    @Override
    public String format(DateTimeFormatter formatter) {
        Objects.requireNonNull(formatter, "formatter");
        return formatter.format(this);
    }

    /**
     * Combines this date-time with an offset to create an {@code OffsetDateTime}.
     *
     * @param offset the offset to combine with, not null
     * @return the offset date-time formed from this date-time and the specified offset, not null
     */
    public OffsetDateTime atOffset(ZoneOffset offset) {
        return OffsetDateTime.of(this.toLocalDateTime(), offset);
    }

    /**
     * Combines this date-time with a time-zone to create a {@code ZonedDateTime}.
     *
     * @param zone the time-zone to use, not null
     * @return the zoned date-time formed from this date-time, not null
     */
    @Override
    public ChronoZonedDateTime atZone(ZoneId zone) {
        return ZonedDateTime.of(this.toLocalDateTime(), zone);
    }

    public ZonedDateTime atZoned(ZoneId zone) {
        return ZonedDateTime.of(this.toLocalDateTime(), zone);
    }

    /**
     * Compares this date-time to another date-time.
     *
     * @param other the other date-time to compare to, not null
     * @return the comparator value, negative if less, positive if greater
     */
    @Override
    public int compareTo(ChronoLocalDateTime<?> other) {
        if (other instanceof DateTime) {
            return compareTo0((DateTime) other);
        }
        return ChronoLocalDateTime.super.compareTo(other);
    }

    private int compareTo0(DateTime other) {
        int cmp = date.compareTo0(other.toLocalDate());
        if (cmp == 0) {
            cmp = time.compareTo(other.toTime());
        }
        return cmp;
    }

    /**
     * Checks if this date-time is after the specified date-time.
     *
     * @param other the other date-time to compare to, not null
     * @return true if this date-time is after the specified date-time
     */
    @Override
    public boolean isAfter(ChronoLocalDateTime<?> other) {
        if (other instanceof DateTime) {
            return compareTo0((DateTime) other) > 0;
        }
        return ChronoLocalDateTime.super.isAfter(other);
    }

    /**
     * Checks if this date-time is before the specified date-time.
     *
     * @param other the other date-time to compare to, not null
     * @return true if this date-time is before the specified date-time
     */
    @Override
    public boolean isBefore(ChronoLocalDateTime<?> other) {
        if (other instanceof DateTime) {
            return compareTo0((DateTime) other) < 0;
        }
        return ChronoLocalDateTime.super.isBefore(other);
    }

    /**
     * Checks if this date-time is equal to the specified date-time.
     *
     * @param other the other date-time to compare to, not null
     * @return true if this date-time is equal to the specified date-time
     */
    @Override
    public boolean isEqual(ChronoLocalDateTime<?> other) {
        if (other instanceof DateTime) {
            return compareTo0((DateTime) other) == 0;
        }
        return ChronoLocalDateTime.super.isEqual(other);
    }

    /**
     * Checks if this date-time is equal to another date-time.
     *
     * @param obj the object to check, null returns false
     * @return true if this is equal to the other date-time
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof DateTime) {
            DateTime other = (DateTime) obj;
            return date.equals(other.date) && time.equals(other.time);
        }
        return false;
    }

    /**
     * A hash code for this date-time.
     *
     * @return a suitable hash code
     */
    @Override
    public int hashCode() {
        return date.hashCode() ^ time.hashCode();
    }

    /**
     * Outputs this date-time as a {@code String}, such as {@code 2007-12-03T10:15:30}.
     *
     * @return a string representation of this date-time, not null
     */
    @Override
    public String toString() {
        return date.toString() + ' ' + time.toString();
    }

    public <T extends ChronoLocalDateTime<?>> boolean isBetween(T start, T end) {
        final boolean isAtOrAfterStart = this.compareTo(start) >= 0;
        final boolean isAtOrBeforeEnd = this.compareTo(end) <= 0;
        return isAtOrAfterStart && isAtOrBeforeEnd;
    }

    public LocalDateTime toLocalDateTime() {
        return LocalDateTime.of(getYear(), getMonth(), getDayOfMonth(), getHour(), getMinute(), getSecond(), getNano());
    }

}
