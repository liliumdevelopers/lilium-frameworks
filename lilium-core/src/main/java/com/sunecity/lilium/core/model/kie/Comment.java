package com.sunecity.lilium.core.model.kie;

import com.sunecity.lilium.core.time.Date;

import java.io.Serializable;

/**
 * Created by shahram on 5/23/16.
 */
public class Comment implements Serializable {

    private Long id;
    private String text;
    private Date date;
    private String username;

    public Comment() {
    }

    public Comment(Long id, String text, Date date, String username) {
        this.id = id;
        this.text = text;
        this.date = date;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
