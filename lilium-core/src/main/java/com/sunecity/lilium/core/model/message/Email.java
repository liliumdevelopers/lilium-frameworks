package com.sunecity.lilium.core.model.message;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Shahram Goodarzi
 */
public class Email {

    private List<Message> messages;
    private String subject;
    private Date date;
    private List<Attachment> attachments;
    private String from;
    private List<MessageRecipient> recipients;

    public Email() {
    }

    public Email(List<Message> messages, String subject, String from) {
        this.messages = messages;
        this.subject = subject;
        this.from = from;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void addMessage(Message message) {
        if (getMessages() == null) {
            messages = new ArrayList<>();
        }
        messages.add(message);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getDate() {
        if (date == null) {
            date = new Date();
        }
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public void addAttachment(Attachment attachment) {
        if (getAttachments() == null) {
            attachments = new ArrayList<>();
        }
        attachments.add(attachment);
    }

    public void addAttachment(String name, String contentType, byte[] attachment) {
        if (getAttachments() == null) {
            attachments = new ArrayList<>();
        }
        attachments.add(new Attachment(name, contentType, attachment));
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<MessageRecipient> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<MessageRecipient> recipients) {
        this.recipients = recipients;
    }

    public void addRecipient(MessageRecipient recipient) {
        if (getRecipients() == null) {
            recipients = new ArrayList<>();
        }
        recipients.add(recipient);
    }

    public void addTo(String recipient) {
        if (getRecipients() == null) {
            recipients = new ArrayList<>();
        }
        recipients.add(new MessageRecipient(recipient, RecipientType.TO));
    }

    public void addCc(String recipient) {
        if (getRecipients() == null) {
            recipients = new ArrayList<>();
        }
        recipients.add(new MessageRecipient(recipient, RecipientType.CC));
    }

    public void addBcc(String recipient) {
        if (getRecipients() == null) {
            recipients = new ArrayList<>();
        }
        recipients.add(new MessageRecipient(recipient, RecipientType.BCC));
    }

}
