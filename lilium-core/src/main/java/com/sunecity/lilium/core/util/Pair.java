package com.sunecity.lilium.core.util;

import java.io.Serializable;

public class Pair<S, H> implements Serializable {

    private S key;
    private H value;

    public Pair() {
    }

    public Pair(S key) {
        this.key = key;
    }

    public Pair(S key, H value) {
        this.key = key;
        this.value = value;
    }

    public S getKey() {
        return key;
    }

    public void setKey(S key) {
        this.key = key;
    }

    public H getValue() {
        return value;
    }

    public void setValue(H value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return !(key != null ? !key.equals(pair.key) : pair.key != null) && !(value != null ? !value.equals(pair.value) : pair.value != null);
    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "[key: " + String.valueOf(key) + ", value: " + String.valueOf(value) + "]";
    }

}
