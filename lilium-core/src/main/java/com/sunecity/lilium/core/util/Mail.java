package com.sunecity.lilium.core.util;

import com.sunecity.lilium.core.model.message.Attachment;
import com.sunecity.lilium.core.model.message.Message;
import com.sunecity.lilium.core.model.message.MessageRecipient;
import com.sunecity.lilium.core.model.message.ReceiveFilter;
import com.sunecity.lilium.core.model.message.RecipientType;
import com.sunecity.lilium.core.model.user.MailSetting;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailAttachmentBuilder;
import jodd.mail.ImapServer;
import jodd.mail.ImapSslServer;
import jodd.mail.MailServer;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import jodd.mail.SmtpSslServer;

import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Mail implements Serializable {

    private static final long serialVersionUID = -8428751528618945671L;

    private SendMailSession sendMailSession;
    private ReceiveMailSession receiveMailSession;

    public Mail(MailSetting mailSetting) {
        sendMailSession = createSmtpServer(mailSetting).createSession();
        receiveMailSession = createImapServer(mailSetting).createSession();
    }

    public Mail(Session session) throws NoSuchProviderException {
        sendMailSession = new SendMailSession(session, session.getTransport());
    }

    public void send(List<com.sunecity.lilium.core.model.message.Email> emails) throws AddressException {
        for (com.sunecity.lilium.core.model.message.Email email : emails) {
            send(email);
        }
    }

    public void send(com.sunecity.lilium.core.model.message.Email email) throws AddressException {
        sendMailSession.open();
        Email e = createDefaultEmail();
        e.from(EmailAddress.of(email.getFrom()));
        e.sentDate(new Date());
        e.subject(email.getSubject());
        e.to(getMailAddress(email, RecipientType.TO));
        e.cc(getMailAddress(email, RecipientType.CC));
        e.bcc(getMailAddress(email, RecipientType.BCC));
        for (Message message : email.getMessages()) {
            if (message.getMimeType().equals(Message.TEXT_HTML)) {
                e.htmlMessage(message.getContent(), "UTF-8");
            } else {
                e.textMessage(message.getContent(), "UTF-8");
            }
        }
        setAttachment(email, e);
        sendMailSession.sendMail(e);
        sendMailSession.close();
    }

    public List<com.sunecity.lilium.core.model.message.Email> receive(ReceiveFilter receiveFilter) {
        receiveMailSession.open();
        if (receiveFilter.getFolder() != null && receiveFilter.getFolder().trim().length() != 0) {
            receiveMailSession.useFolder(receiveFilter.getFolder());
        } else {
            receiveMailSession.useDefaultFolder();
        }
        ReceivedEmail[] receivedEmails;
        if (receiveFilter.getSearchTerm() != null) {
            receivedEmails = receiveMailSession.receiveEmail(receiveFilter);
        } else {
            receivedEmails = receiveMailSession.receiveEmail();
        }
        List<com.sunecity.lilium.core.model.message.Email> emails = parseEmail(receivedEmails);
        receiveMailSession.close();
        return emails;
    }

    public String[] getFolders() {
        if (receiveMailSession != null) {
            return receiveMailSession.getAllFolders();
        }
        return null;
    }

    private List<com.sunecity.lilium.core.model.message.Email> parseEmail(ReceivedEmail[] receivedEmails) {
        List<com.sunecity.lilium.core.model.message.Email> emails = new ArrayList<>();
        if (receivedEmails.length != 0) {
            for (ReceivedEmail receivedEmail : receivedEmails) {
                com.sunecity.lilium.core.model.message.Email email = new com.sunecity.lilium.core.model.message.Email();
                email.setSubject(receivedEmail.subject());
                email.setFrom(receivedEmail.from().getEmail());
                email.setDate(receivedEmail.receivedDate());

                if (receivedEmail.to().length != 0) {
                    for (EmailAddress to : receivedEmail.to()) {
                        email.addTo(to.getEmail());
                    }
                }
                if (receivedEmail.cc().length != 0) {
                    for (EmailAddress cc : receivedEmail.cc()) {
                        email.addCc(cc.getEmail());
                    }
                }

                if (receivedEmail.attachments() != null && !receivedEmail.attachments().isEmpty()) {
                    for (EmailAttachment emailAttachment : receivedEmail.attachments()) {
                        email.addAttachment(emailAttachment.getName(),
                                emailAttachment.getContentId(), emailAttachment.toByteArray());
                    }
                }

                receivedEmail.messages().stream().filter(message -> message.getContent() != null).forEach(message -> {
                    Message m = new Message();
                    m.setContent(message.getContent());
                    m.setMimeType(message.getMimeType());
                    m.setEncoding(message.getEncoding());
                    email.addMessage(m);
                });
                emails.add(email);
            }
        }
        return emails;
    }

    private EmailAddress[] getMailAddress(com.sunecity.lilium.core.model.message.Email email, RecipientType type)
            throws AddressException {
        List<EmailAddress> addresses = new ArrayList<>();
        if (email.getRecipients() != null && !email.getRecipients().isEmpty()) {
            for (MessageRecipient recipient : email.getRecipients()) {
                if (recipient.getRecipientType() == type) {
                    addresses.add(EmailAddress.of(new InternetAddress(recipient.getRecipient())));
                }
            }
        }
        return listToArray(addresses);
    }

    private EmailAddress[] listToArray(List<EmailAddress> addresses) {
        EmailAddress[] mailAddresses = new EmailAddress[0];
        if (!addresses.isEmpty()) {
            mailAddresses = new EmailAddress[addresses.size()];
            for (int i = 0; i < addresses.size(); i++) {
                mailAddresses[i] = addresses.get(i);
            }
        }
        return mailAddresses;
    }

    private void setAttachment(com.sunecity.lilium.core.model.message.Email email, Email e) {
        if (email.getAttachments() != null && !email.getAttachments().isEmpty()) {
            for (Attachment attachment : email.getAttachments()) {
                EmailAttachmentBuilder emailAttachment = EmailAttachment.with().name(attachment.getName()).content(attachment.getAttachment(), attachment.getContentType()).inline(attachment.isInline());
                e.attachment(emailAttachment);
            }
        }
    }

    private Email createDefaultEmail() {
        return Email.create();
    }

    private SmtpServer createSmtpServer(MailSetting mailSetting) {
        if (mailSetting.isSmtpSsl()) {
            return SmtpSslServer.create().host(mailSetting.getSmtpHost()).port(mailSetting.getSmtpPort()).auth(mailSetting.getUsername(), mailSetting.getPassword()).property("mail.smtp.starttls.enable", "true").ssl(true).buildSmtpMailServer();
        } else {
            return SmtpServer.create().host(mailSetting.getSmtpHost()).port(mailSetting.getSmtpPort()).auth(mailSetting.getUsername(), mailSetting.getPassword()).property("mail.smtp.starttls.enable", "true").buildSmtpMailServer();
        }
    }

    private ImapServer createImapServer(MailSetting mailSetting) {
        ImapServer imapServer;
        if (mailSetting.isImapSsl()) {
            imapServer = new ImapSslServer(new MailServer.Builder().host(mailSetting.getImapHost()).port(mailSetting.getImapPort()).auth(mailSetting.getUsername(), mailSetting.getPassword()));

        } else {
            imapServer = new ImapServer(new MailServer.Builder().host(mailSetting.getImapHost()).port(mailSetting.getImapPort()).auth(mailSetting.getUsername(), mailSetting.getPassword()));
        }
        return imapServer;
    }
}
