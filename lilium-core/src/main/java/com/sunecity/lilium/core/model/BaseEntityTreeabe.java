package com.sunecity.lilium.core.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@MappedSuperclass
public abstract class BaseEntityTreeabe<ID extends Serializable> extends BaseEntity<ID> implements Treeable<ID> {

    private static final long serialVersionUID = -3164199209983061534L;

    @NotBlank
    @Size(min = 1, max = 1000)
    @Column(name = "PATH")
    private String path;

    @Column(name = "IS_LEAF")
    private boolean leaf;

    @Column(name = "PARENT_ID", insertable = false, updatable = false)
    private ID parentId;

    public BaseEntityTreeabe() {
    }

    public BaseEntityTreeabe(ID id) {
        super(id);
    }

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    @Override
    public ID getParentId() {
        return parentId;
    }

    public void setParentId(ID parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BaseEntityTreeabe<?> that = (BaseEntityTreeabe<?>) o;

        if (getVersion() != null ? !getVersion().equals(that.getVersion()) : that.getVersion() != null) return false;
        if (leaf != that.leaf) return false;
        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getCreateUser() != null ? !getCreateUser().equals(that.getCreateUser()) : that.getCreateUser() != null)
            return false;
        if (getUpdateUser() != null ? !getUpdateUser().equals(that.getUpdateUser()) : that.getUpdateUser() != null)
            return false;
        if (getCreateTimestamp() != null ? !getCreateTimestamp().equals(that.getCreateTimestamp()) : that.getCreateTimestamp() != null)
            return false;
        if (getUpdateTimestamp() != null ? !getUpdateTimestamp().equals(that.getUpdateTimestamp()) : that.getUpdateTimestamp() != null)
            return false;
        if (path != null ? !path.equals(that.path) : that.path != null) return false;
        return parentId != null ? parentId.equals(that.parentId) : that.parentId == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getId() != null ? getId().hashCode() : 0);
        result = 31 * result + getVersion();
        result = 31 * result + (getCreateUser() != null ? getCreateUser().hashCode() : 0);
        result = 31 * result + (getUpdateUser() != null ? getUpdateUser().hashCode() : 0);
        result = 31 * result + (getCreateTimestamp() != null ? getCreateTimestamp().hashCode() : 0);
        result = 31 * result + (getUpdateTimestamp() != null ? getUpdateTimestamp().hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (leaf ? 1 : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        return result;
    }

}
