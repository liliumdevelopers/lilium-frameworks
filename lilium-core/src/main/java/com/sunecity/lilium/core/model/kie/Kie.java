package com.sunecity.lilium.core.model.kie;

import com.sunecity.lilium.core.model.BaseEntitySimple;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by shahram on 5/29/16.
 */
@Entity
@Table(name = "SK_KIES")
public class Kie extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KieSeq")
    @SequenceGenerator(name = "KieSeq", sequenceName = "SK_KIES_SEQ")
    @Column(name = "KIE_ID")
    private Long id;

    @Column(name = "URL")
    private String url;

    @Column(name = "DEPLOYMENT_ID")
    private String deploymentId;

    public Kie() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Kie kie = (Kie) o;

        if (id != null ? !id.equals(kie.id) : kie.id != null) return false;
        if (url != null ? !url.equals(kie.url) : kie.url != null) return false;
        return deploymentId != null ? deploymentId.equals(kie.deploymentId) : kie.deploymentId == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (deploymentId != null ? deploymentId.hashCode() : 0);
        return result;
    }

}
