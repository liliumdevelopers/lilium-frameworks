package com.sunecity.lilium.core.inject;

import com.sunecity.lilium.core.config.EjbConfig;
import org.aeonbits.owner.ConfigFactory;

import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * Created by shahram on 8/4/16.
 */
public class EjbLocator extends AbstractLocator {

    private Context context;
    private String appName;
    private String moduleName;

    public EjbLocator() {
    }

    @Override
    protected <T> T lookup(Class<T> clas) {
        try {
            if (context == null) {
                initEjb();
            }
            return (T) context.lookup("java:global/" + (appName != null ? appName + "/" : "") + moduleName + "/" + clas.getSimpleName() + "Impl" + "!" + clas.getName());
        } catch (Exception e) {
            logger.error("Can not Connect to Remote EJB.", e);
            return null;
        }
    }

    private void initEjb() throws Exception {
        context = new InitialContext();
        EjbConfig ejbConfig = ConfigFactory.create(EjbConfig.class);
        appName = ejbConfig.appName().equals("") ? null : ejbConfig.appName();
        moduleName = ejbConfig.moduleName();
    }

}
