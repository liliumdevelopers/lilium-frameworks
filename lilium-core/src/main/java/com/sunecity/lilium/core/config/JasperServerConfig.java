package com.sunecity.lilium.core.config;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:META-INF/jasperServer.properties"})
public interface JasperServerConfig extends Config {

    String username();

    String password();

    String url();

    String reportPath();

}
