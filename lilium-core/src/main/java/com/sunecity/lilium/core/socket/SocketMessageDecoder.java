package com.sunecity.lilium.core.socket;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.io.IOException;

/**
 * Created by shahram on 4/12/17.
 */
public class SocketMessageDecoder implements Decoder.Text<SocketMessage> {

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void init(EndpointConfig endpointConfig) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public SocketMessage decode(String s) throws DecodeException {
        try {
            return mapper.readValue(s, SocketMessage.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean willDecode(String s) {
        return true;
    }

}
