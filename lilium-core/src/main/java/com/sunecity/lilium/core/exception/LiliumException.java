package com.sunecity.lilium.core.exception;

import javax.ejb.ApplicationException;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.core.Response;

/**
 * Created by shahram on 10/25/16.
 */
@ApplicationException
public class LiliumException extends ResponseProcessingException {

    public LiliumException(Response response, Throwable cause) {
        super(response, cause);
    }

    public LiliumException(Response response, String message, Throwable cause) {
        super(response, message, cause);
    }

    public LiliumException(Response response, String message) {
        super(response, message);
    }

    public LiliumException(Throwable cause) {
        super(null, cause);
    }

    public LiliumException(String message, Throwable cause) {
        super(null, message, cause);
    }

    public LiliumException(String message) {
        super(null, message);
    }

}
