package com.sunecity.lilium.core.model;

import com.sunecity.lilium.core.time.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by shahram on 7/8/15.
 */
@Entity
@Table(name = "SK_RULES")
@NamedQueries({
        @NamedQuery(name = "selectRulesByTypeAndDate", query = "SELECT r FROM Rule r WHERE r.type = :type AND r.fromDate <= :date OR r.toDate >= :date")
})
public class Rule extends BaseEntitySimple<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RuleSeq")
    @SequenceGenerator(name = "RuleSeq", sequenceName = "SK_RULE_SEQ")
    @Column(name = "RULE_ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "FROM_DATE")
    private Date fromDate;

    @Column(name = "TO_DATE")
    private Date toDate;

    @Column(name = "RULE")
    @Lob
    private byte[] ruleFile;

    public Rule() {
    }

    public Rule(String name, byte[] ruleFile) {
        this.name = name;
        this.ruleFile = ruleFile;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public byte[] getRuleFile() {
        return ruleFile;
    }

    public void setRuleFile(byte[] ruleFile) {
        this.ruleFile = ruleFile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Rule rule = (Rule) o;

        if (id != null ? !id.equals(rule.id) : rule.id != null) return false;
        if (name != null ? !name.equals(rule.name) : rule.name != null) return false;
        if (type != null ? !type.equals(rule.type) : rule.type != null) return false;
        if (fromDate != null ? !fromDate.equals(rule.fromDate) : rule.fromDate != null) return false;
        return !(toDate != null ? !toDate.equals(rule.toDate) : rule.toDate != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (fromDate != null ? fromDate.hashCode() : 0);
        result = 31 * result + (toDate != null ? toDate.hashCode() : 0);
        return result;
    }

}
