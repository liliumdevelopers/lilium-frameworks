package com.sunecity.lilium.core.comm;

/**
 * @author Shahram Goodarzi
 */
public enum BaudRate {

    B4800(4800),
    B9600(9600),
    B14400(14400),
    B19200(19200),
    B28800(28800),
    B33600(33600),
    B38400(38400),
    B56000(56000),
    B57600(57600),
    B115200(115200),
    B230400(230400),
    B460800(460800),
    B921600(921600);

    private final int baudRate;

    BaudRate(int baudRate) {
        this.baudRate = baudRate;
    }

    public static BaudRate getBaudRate(String baudRate) throws Exception {
        switch (baudRate) {
            case "4800":
                return B4800;
            case "9600":
                return B9600;
            case "14400":
                return B14400;
            case "19200":
                return B19200;
            case "28800":
                return B28800;
            case "33600":
                return B33600;
            case "38400":
                return B38400;
            case "56000":
                return B56000;
            case "57600":
                return B57600;
            case "115200":
                return B115200;
            case "230400":
                return B230400;
            case "460800":
                return B460800;
            case "921600":
                return B921600;
            default:
                throw new Exception("BaudRate " + baudRate + " doesn't exist.");
        }
    }

    public static BaudRate getBaudRate(int baudRate) throws Exception {
        switch (baudRate) {
            case 4800:
                return B4800;
            case 9600:
                return B9600;
            case 14400:
                return B14400;
            case 19200:
                return B19200;
            case 28800:
                return B28800;
            case 33600:
                return B33600;
            case 38400:
                return B38400;
            case 56000:
                return B56000;
            case 57600:
                return B57600;
            case 115200:
                return B115200;
            case 230400:
                return B230400;
            case 460800:
                return B460800;
            case 921600:
                return B921600;
            default:
                throw new Exception("BaudRate " + baudRate + " doesn't exist.");
        }
    }

    public int getBaudRate() {
        return baudRate;
    }

}
