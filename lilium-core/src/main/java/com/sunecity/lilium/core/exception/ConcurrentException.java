package com.sunecity.lilium.core.exception;

import javax.ejb.ApplicationException;
import javax.ws.rs.core.Response;

/**
 * @author Shahram Goodarzi
 */
@ApplicationException
public class ConcurrentException extends ClientException {

    private static final long serialVersionUID = 9109133422680478768L;

    public ConcurrentException(String message) {
        super(null, message);
    }

    public ConcurrentException(Response response, String message) {
        super(response, message);
    }

}
