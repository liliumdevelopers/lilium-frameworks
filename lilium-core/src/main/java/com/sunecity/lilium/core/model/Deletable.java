package com.sunecity.lilium.core.model;

public interface Deletable {

    boolean isDeleted();

    void setDeleted(boolean deleted);

}
