package com.sunecity.lilium.core.config;

import org.aeonbits.owner.Config;

/**
 * Created by shahram on 9/30/16.
 */
@Config.Sources({"classpath:META-INF/bpm.properties"})
public interface BpmConfig extends Config {

    String username();

    String password();

    String url();

    String driver();

    String jndi();

    String persistenceUnit();

}
