package com.sunecity.lilium.core.model.file;

import com.sunecity.lilium.core.model.BaseEntitySimple;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Base64;

@MappedSuperclass
public abstract class ArrayFile extends BaseEntitySimple<Long> {

    private static final long serialVersionUID = 1658057822569645457L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ArrayFileSeq")
    @SequenceGenerator(name = "ArrayFileSeq", sequenceName = "ARRAY_FILE_SEQ")
    @Column(name = "FILE_ID")
    private Long id;

    @Column(name = "FILE_NAME")
    @Size(max = 150)
    @NotBlank
    private String name;

    @Column(name = "FILE_TYPE")
    @Size(max = 50)
    @NotBlank
    private String type;

    @Column(name = "FILE_SIZE")
    private long size;

    @Lob
    @Column(name = "VALUE")
    private byte[] value;

    @Column(name = "DESCRIPTION")
    @Size(max = 1000)
    private String description;

    public ArrayFile() {
    }

    public ArrayFile(Long id) {
        super(id);
    }

    public ArrayFile(ArrayFile file) {
        this.name = file.getName();
        this.type = file.getType();
        this.size = file.getSize();
        this.value = file.getValue();
    }

    public ArrayFile(String name, String type, long size, byte[] value) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.value = value;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    public String getValueBase64() {
        return Base64.getEncoder().encodeToString(value);
    }

    @Transient
    public boolean isImage() {
        return type.startsWith("image/");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ArrayFile arrayFile = (ArrayFile) o;

        if (size != arrayFile.size) return false;
        if (id != null ? !id.equals(arrayFile.id) : arrayFile.id != null) return false;
        if (name != null ? !name.equals(arrayFile.name) : arrayFile.name != null) return false;
        if (type != null ? !type.equals(arrayFile.type) : arrayFile.type != null) return false;
        return description != null ? description.equals(arrayFile.description) : arrayFile.description == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (int) (size ^ (size >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

}