package com.sunecity.lilium.core.model.user;

import java.io.Serializable;

public class GlobalSetting implements Serializable {

	private static final long serialVersionUID = 1496200859406393105L;

	private int sessionTimeout;
	private boolean multipleLogin;
	private boolean audit;
	private MailSetting mailSetting;

	public GlobalSetting() {
	}

	public int getSessionTimeout() {
		return sessionTimeout;
	}

	public long getSessionTimeoutMillis() {
		return sessionTimeout * 1000;
	}

	public void setSessionTimeout(int sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}

	public boolean isMultipleLogin() {
		return multipleLogin;
	}

	public void setMultipleLogin(boolean multipleLogin) {
		this.multipleLogin = multipleLogin;
	}

	public boolean isAudit() {
		return audit;
	}

	public void setAudit(boolean audit) {
		this.audit = audit;
	}

	public MailSetting getMailSetting() {
		return mailSetting;
	}

	public void setMailSetting(MailSetting mailSetting) {
		this.mailSetting = mailSetting;
	}

}
