package com.sunecity.lilium.core.model;

import java.io.Serializable;

/**
 * Created by shahram on 6/7/17.
 */
public class Move<T extends Serializable> implements Serializable {

    private T entity;
    private T oldParent;
    private T newParent;

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    public T getOldParent() {
        return oldParent;
    }

    public void setOldParent(T oldParent) {
        this.oldParent = oldParent;
    }

    public T getNewParent() {
        return newParent;
    }

    public void setNewParent(T newParent) {
        this.newParent = newParent;
    }

}
