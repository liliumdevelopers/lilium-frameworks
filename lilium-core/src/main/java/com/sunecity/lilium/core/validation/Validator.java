package com.sunecity.lilium.core.validation;

import com.sunecity.lilium.core.util.Numbers;
import org.quartz.CronExpression;

import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Shahram Goodarzi
 */
public final class Validator {

    private Validator() {
    }

    public static boolean isValid(String pattern, String matcher) {
        Pattern pat = Pattern.compile(pattern);
        Matcher match = pat.matcher(matcher);
        return match.matches();
    }

    public static <T> boolean isValid(T t) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        javax.validation.Validator validator = factory.getValidator();
        return validator.validate(t).size() == 0;
    }

    public static boolean isNumber(String value) {
        return value.matches("[-|\\+]?(\\p{N})+");
    }

    public static boolean isCharacter(String value) {
        return value.matches("(\\p{L})+");
    }

    public static boolean isCharacterOrNumber(String value) {
        return value.matches("(\\p{L}|\\p{N})+");
    }

    public static boolean isEmail(String email) {
        email = Numbers.getTextEnglish(email);
        return isValid("^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$", email);
    }

    public static boolean isDomain(String domain) {
        domain = Numbers.getTextEnglish(domain);
        return isValid("^([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}$", domain);
    }

    public static boolean isUrl(String url) {
        url = Numbers.getTextEnglish(url);
        return isValid("^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$", url);
    }

    public static boolean isTel(String tel) {
        tel = Numbers.getTextEnglish(tel);
        return isValid("(0|\\+98|0098)+[1-8]{1}+[0-9]{9}", tel);
    }

    public static boolean isCell(String cell) {
        cell = Numbers.getTextEnglish(cell);
        return isValid("((09)|(\\+989)|(00989))+[0-9]{9}", cell);
    }

    public static boolean isTelOrCell(String number) {
        number = Numbers.getTextEnglish(number);
        return isValid("((0)|(\\+98)|(0098))+[1-9]{1}+[0-9]{9}", number);
    }

    public static boolean isPostalCode(String postalCode) {
        return PostalCode.isValid(postalCode);
    }

    public static boolean isNationalId(String nationalId) {
        return NationalId.isValid(nationalId);
    }

    public static boolean isCompanyId(String companyId) {
        return CompanyId.isValid(companyId);
    }

    public static boolean isUsername(String username) {
        username = Numbers.getTextEnglish(username);
        return isValid("^[a-zA-Z0-9_-]{4,64}$", username);
    }

    public static boolean isPassword(String password) {
        password = Numbers.getTextEnglish(password);
        return isValid("^[a-zA-Z0-9@#$%_-]{4,64}$", password);
    }

    public static boolean isIp(String ip) {
        ip = Numbers.getTextEnglish(ip);
        return isValid("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$", ip);
    }

    public static boolean isMacAddress(String macAddress) {
        return isValid("^([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$", macAddress);
    }

    public static boolean isHexadecimal(String value) {
        value = Numbers.getTextEnglish(value);
        return isValid("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", value);
    }

    public static boolean isISBN(String isbn) {
        isbn = Numbers.getTextEnglish(isbn);
        return isValid("ISBN(-1(?:(0)|3))?:?\\x20+(?(1)(?(2)(?:(?=.{13}$)\\d{1,5}([ -])\\d{1,7}\\3\\d{1,6}\\3(?:\\d|x)$)|(?:(?=.{17}$)97(?:8|9)([ -])\\d{1,5}\\4\\d{1,7}\\4\\d{1,6}\\4\\d$))|(?(.{13}$)(?:\\d{1,5}([ -])\\d{1,7}\\5\\d{1,6}\\5(?:\\d|x)$)|(?:(?=.{17}$)97(?:8|9)([ -])\\d{1,5}\\6\\d{1,7}\\6\\d{1,6}\\6\\d$)))", isbn);
    }

    public static boolean isTime(String time) {
        time = Numbers.getTextEnglish(time);
        return isValid("(((([0-1]{1}[0-9]{1})|(2[0-3]{1})|([0-9]{1}))|((([0-1]{1}[0-9]{1})|(2[0-3]{1})|([0-9]{1}))((:(([0-5]{1}[0-9]{1})|([0-9]{1})):(([0-5]{1}[0-9]{1})|([0-9]{1})))|(:(([0-5]{1}[0-9]{1})|([0-9]{1}))))))|(((([0-1]{1}[0-9]{1})|(2[0-3]{1}))|((([0-1]{1}[0-9]{1})|(2[0-3]{1}))(([0-5]{1}[0-9]{1}[0-5]{1}[0-9]{1})|([0-5]{1}[0-9]{1}))))))", time);
    }

    public static boolean isCronExpression(String cronExpression) {
        return CronExpression.isValidExpression(cronExpression);
    }

    public static boolean isPersianDate(String date) {
        return JalaliCalendarValidator.isValid(date);
    }

    public static boolean isBlank(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isDate(String date) {
        return JalaliCalendarValidator.isValid(date);
    }

}
