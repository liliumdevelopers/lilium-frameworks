package com.sunecity.lilium.core.socket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 * Created by shahram on 4/12/17.
 */
public class SocketMessageEncoder implements Encoder.Text<SocketMessage> {

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void init(EndpointConfig endpointConfig) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public String encode(SocketMessage socketMessage) throws EncodeException {
        try {
            return mapper.writeValueAsString(socketMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

}
