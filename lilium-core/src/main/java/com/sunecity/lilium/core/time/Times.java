package com.sunecity.lilium.core.time;

import com.sunecity.lilium.core.util.Numbers;
import com.sunecity.lilium.core.validation.Validator;

import java.util.Calendar;

/**
 * @author Shahram Goodarzi
 */
public class Times {

    private Times() {
    }

    public static String getDiff(Calendar firstTime, Calendar secondTime) {
        return getTime(getSeconds(secondTime) - getSeconds(firstTime));
    }

    public static String getDiff(String firstTime, String secondTime) {
        if (Validator.isBlank(firstTime) || !Validator.isTime(firstTime) || Validator.isBlank(secondTime) || !Validator.isTime(secondTime)) {
            return null;
        }
        return getTime(getSeconds(secondTime) - getSeconds(firstTime));
    }

    public static int getDiffSeconds(Calendar firstTime, Calendar secondTime) {
        return getSeconds(secondTime) - getSeconds(firstTime);
    }

    public static int getDiffSeconds(String firstTime, String secondTime) {
        if (Validator.isBlank(firstTime) || !Validator.isTime(firstTime) || Validator.isBlank(secondTime) || !Validator.isTime(secondTime)) {
            return 0;
        }
        return getSeconds(secondTime) - getSeconds(firstTime);
    }

    public static int getDiffMinutes(Calendar firstTime, Calendar secondTime) {
        return ((firstTime.get(Calendar.HOUR_OF_DAY) * 60) + firstTime.get(Calendar.MINUTE)) - ((secondTime.get(Calendar.HOUR_OF_DAY) * 60) + secondTime.get(Calendar.MINUTE));
    }

    public static int getDiffMinutes(String firstTime, String secondTime) {
        if (Validator.isBlank(firstTime) || !Validator.isTime(firstTime) || Validator.isBlank(secondTime) || !Validator.isTime(secondTime)) {
            return 0;
        }
        firstTime = Numbers.getTextEnglish(firstTime);
        secondTime = Numbers.getTextEnglish(secondTime);
        int firstMinute = 0;
        int secondMinute = 0;
        String[] stringsFirst = firstTime.split(":");
        if (stringsFirst.length > 1) {
            firstMinute += Integer.parseInt(stringsFirst[0]) * 60;
            firstMinute += Integer.parseInt(stringsFirst[1]);
        } else {
            if (stringsFirst[0].length() < 3) {
                firstMinute += Integer.parseInt(stringsFirst[0]) * 60;
            } else {
                firstMinute += Integer.parseInt(stringsFirst[0].substring(0, 2)) * 60 + Integer.parseInt(stringsFirst[0].substring(2, 4));
            }
        }
        String[] stringsSecond = secondTime.split(":");
        if (stringsSecond.length > 1) {
            secondMinute += Integer.parseInt(stringsSecond[0]) * 60;
            secondMinute += Integer.parseInt(stringsSecond[1]);
        } else {
            if (stringsSecond[0].length() < 3) {
                secondMinute += Integer.parseInt(stringsSecond[0]) * 60;
            } else {
                secondMinute += Integer.parseInt(stringsSecond[0].substring(0, 2)) * 60 + Integer.parseInt(stringsSecond[0].substring(2, 4));
            }
        }
        return secondMinute - firstMinute;
    }

    public static int getDiffHours(Calendar firstTime, Calendar secondTime) {
        return secondTime.get(Calendar.HOUR_OF_DAY) - firstTime.get(Calendar.HOUR_OF_DAY);
    }

    public static int getDiffHours(String firstTime, String secondTime) {
        if (Validator.isBlank(firstTime) || !Validator.isTime(firstTime) || Validator.isBlank(secondTime) || !Validator.isTime(secondTime)) {
            return 0;
        }
        firstTime = firstTime.length() > 1 ? firstTime.substring(0, 2) : firstTime;
        secondTime = secondTime.length() > 1 ? secondTime.substring(0, 2) : secondTime;
        return (getSeconds(secondTime) - getSeconds(firstTime)) / 3600;
    }

    public static int getSeconds(String time) {
        if (Validator.isBlank(time) || !Validator.isTime(time)) {
            throw new IllegalArgumentException("Time is not Correct.");
        }
        time = Numbers.getTextEnglish(time);
        int seconds = 0;
        String[] strings = time.split(":");
        if (strings.length > 1) {
            seconds += Integer.parseInt(strings[0]) * 3600;
            seconds += Integer.parseInt(strings[1]) * 60;
            if (strings.length > 2)
                seconds += Integer.parseInt(strings[2]);
        } else {
            if (strings[0].length() < 3) {
                seconds += Integer.parseInt(strings[0]) * 3600;
            } else {
                if (strings[0].length() == 6) {
                    seconds += Integer.parseInt(strings[0].substring(0, 2)) * 3600 + Integer.parseInt(strings[0].substring(2, 4)) * 60 + Integer.parseInt(strings[0].substring(4, 6));
                } else {
                    seconds += Integer.parseInt(strings[0].substring(0, 2)) * 3600 + Integer.parseInt(strings[0].substring(2, 4)) * 60;
                }
            }
        }
        return seconds;
    }

    public static int getSeconds(Calendar calendar) {
        return (calendar.get(Calendar.HOUR_OF_DAY) * 3600) + (calendar.get(Calendar.MINUTE) * 60) + calendar.get(Calendar.SECOND);
    }

    public static String getTime(int seconds) {
        int hour = seconds / 3600;
        int minute = (seconds - (hour * 3600)) / 60;
        int second = seconds - ((hour * 3600) + (minute * 60));
        String time = (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute) + ":" + (second < 10 ? "0" + second : second);
        // return Numbers.getTextLocale(time);
        return time;
    }

    public static String getTime(Calendar calendar) {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        String time = (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute) + ":" + (second < 10 ? "0" + second : second);
        return Numbers.getTextLocale(time);
//		return time;
    }

}
