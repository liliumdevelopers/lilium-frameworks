package com.sunecity.lilium.core.localization;

import java.io.Serializable;

/**
 * Implementations have to resolve the text stored for a given key in the message source they are aware of.
 * Implementations should always be &#064;Dependent scoped!
 */
public interface MessageResolver extends Serializable {
    String MISSING_RESOURCE_MARKER = "???";

    /**
     * @param messageContext  messageContext which should be used
     * @param messageTemplate the message key (or inline text) of the current message
     * @param category        the category of the message, e.g. 'longText'. Can be <code>null</code>
     * @return the final but not interpolated message text or <code>null</code> if an error happened or the resource
     * could not be resolved.
     */
    String getMessage(MessageContext messageContext, String messageTemplate, String category);
}
