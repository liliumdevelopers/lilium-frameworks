package com.sunecity.lilium.core.util;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.io.OutputStream;

public class JaxB {

    public <T> void marshal(T t, OutputStream outputStream) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(t.getClass());
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(t, outputStream);
    }

    @SuppressWarnings("unchecked")
    public <T> T unmarshal(Class<T> clas, InputStream inputStream) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(clas);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (T) unmarshaller.unmarshal(inputStream);
    }

}
