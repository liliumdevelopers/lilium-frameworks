package com.sunecity.lilium.core.inject;

/**
 * Created by shahram on 8/7/16.
 */
public enum LookupType {

    REST("com.sunecity.lilium.rest.client.RestLocator"), EJB("com.sunecity.lilium.core.inject.EjbLocator");

    private String className;

    LookupType(String className) {
        this.className = className;
    }

    public Class<? extends AbstractLocator> getLocatorClass() throws ClassNotFoundException {
        return (Class<? extends AbstractLocator>) Class.forName(className);
    }

}
