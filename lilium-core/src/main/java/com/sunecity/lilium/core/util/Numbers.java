package com.sunecity.lilium.core.util;

import com.sunecity.lilium.core.localization.Locales;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * @author Shahram Goodarzi
 */
public final class Numbers {

    public static String getMoney(MonetaryAmount money) {
        DecimalFormat formatter = new DecimalFormat("###,###.######");
        return getTextLocale(formatter.format(Money.from(money).getNumberStripped()));
    }

    public static String round(double number, int decimalLen) {
        return new BigDecimal(number).setScale(decimalLen, RoundingMode.CEILING).toString();
    }

    public static String getTextLocale(String text) {
        return Locales.getLocale().getLanguage().equalsIgnoreCase("fa") ? getTextPersian(text) : getTextEnglish(text);
    }

    public static String getTextPersian(String text) {
        if (text == null || text.trim().length() == 0) {
            return text;
        }
        StringBuilder s = new StringBuilder();
        char[] cs = text.toCharArray();
        for (char c : cs) {
            if (Character.isDigit(c)) {
                s.append((char) (Character.getNumericValue(c) + 1776));
            } else if (c == '٫') {
                s.append(".");
            } else {
                s.append(c);
            }
        }
        return s.toString();
    }

    public static String getTextEnglish(String text) {
        if (text == null || text.trim().length() == 0) {
            return text;
        }
        StringBuilder s = new StringBuilder();
        char[] cs = text.toCharArray();
        for (char c : cs) {
            if (Character.isDigit(c)) {
                s.append(Character.getNumericValue(c));
            } else {
                s.append(c);
            }
        }
        return s.toString();
    }

    public static String normalizeUUID(String uuid) {
        return uuid.replaceAll("-", "");
    }

    public static String split(String s) {
        boolean b = isDecimal(s);
        String decimal = "";
        StringBuilder result = new StringBuilder();
        if (b && s.indexOf('.') != -1) {
            decimal = s.substring(s.lastIndexOf('.'), s.length());
            s = s.substring(0, s.lastIndexOf('.'));
        }
        String[] strings = new StringBuilder(s).reverse().toString().split("(?<=\\G...)");
        result.append(new StringBuilder(strings[strings.length - 1]).reverse().toString());
        for (int i = strings.length - 2; i >= 0; i--) {
            result.append(",").append(new StringBuilder(strings[i]).reverse().toString());
        }
        if (b && !decimal.matches("\\.[0]*")) {
            result.append(decimal);
        }
        return result.toString();
    }

    private static boolean isDecimal(String string) {
        try {
            @SuppressWarnings("unused")
            Double d = Double.valueOf(getTextEnglish(string));
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

}
