package com.sunecity.lilium.core.model.kie;

import com.sunecity.lilium.core.localization.Bundles;

/**
 * Created by shahram on 5/24/16.
 */
public enum ProcessStatus {

    PENDING, ACTIVE, COMPLETED, ABORTED, SUSPENDED;

    public String toString() {
        return Bundles.getValueLocale(this);
    }

}
