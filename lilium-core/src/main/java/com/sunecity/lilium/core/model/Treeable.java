package com.sunecity.lilium.core.model;

import java.io.Serializable;

public interface Treeable<ID extends Serializable> extends BaseIdentity<ID> {

    ID getParentId();

    String getPath();

    boolean isLeaf();

}