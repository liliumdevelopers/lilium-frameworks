package com.sunecity.lilium.core.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class BaseEntityActivable<ID extends Serializable> extends BaseEntity<ID> implements Activable<ID> {

    private static final long serialVersionUID = -3164199209983061534L;

    @Column(name = "IS_ACTIVE")
    private boolean active;

    public BaseEntityActivable() {
    }

    public BaseEntityActivable(ID id) {
        super(id);
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BaseEntityActivable<?> that = (BaseEntityActivable<?>) o;

        if (active != that.active) return false;
        if (getVersion() != null ? !getVersion().equals(that.getVersion()) : that.getVersion() != null) return false;
        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getCreateUser() != null ? !getCreateUser().equals(that.getCreateUser()) : that.getCreateUser() != null)
            return false;
        if (getUpdateUser() != null ? !getUpdateUser().equals(that.getUpdateUser()) : that.getUpdateUser() != null)
            return false;
        if (getCreateTimestamp() != null ? !getCreateTimestamp().equals(that.getCreateTimestamp()) : that.getCreateTimestamp() != null)
            return false;
        return getUpdateTimestamp() != null ? getUpdateTimestamp().equals(that.getUpdateTimestamp()) : that.getUpdateTimestamp() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getId() != null ? getId().hashCode() : 0);
        result = 31 * result + getVersion();
        result = 31 * result + (getCreateUser() != null ? getCreateUser().hashCode() : 0);
        result = 31 * result + (getUpdateUser() != null ? getUpdateUser().hashCode() : 0);
        result = 31 * result + (getCreateTimestamp() != null ? getCreateTimestamp().hashCode() : 0);
        result = 31 * result + (getUpdateTimestamp() != null ? getUpdateTimestamp().hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

}
