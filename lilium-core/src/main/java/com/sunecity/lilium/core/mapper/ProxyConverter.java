package com.sunecity.lilium.core.mapper;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import ma.glasnost.orika.metadata.TypeFactory;

import java.util.Collection;

/**
 * Created by shahram on 12/14/16.
 */
public class ProxyConverter extends BidirectionalConverter<Object, Object> {

    @Override
    public Object convertTo(Object source, Type<Object> destinationType, MappingContext mappingContext) {
        mappingContext.reset();
        return null;
    }

    @Override
    public Object convertFrom(Object source, Type<Object> destinationType, MappingContext mappingContext) {
        return null;
    }

    @Override
    public boolean canConvert(Type<?> sourceType, Type<?> destinationType) {
        return TypeFactory.valueOf(Collection.class).isAssignableFrom(sourceType);
    }

}
