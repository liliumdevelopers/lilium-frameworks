package com.sunecity.lilium.core.localization;

import javax.enterprise.context.ApplicationScoped;
import java.io.Serializable;
import java.util.Locale;

/**
 * {@inheritDoc}
 */
@ApplicationScoped
public class DefaultMessageInterpolator implements MessageInterpolator, Serializable {
    private static final long serialVersionUID = -8854087197813424812L;

    @Override
    public String interpolate(String messageTemplate, Serializable[] arguments, Locale locale) {
        if (arguments == null || arguments.length == 0) {
            return messageTemplate;
        }

        return String.format(locale, messageTemplate, arguments);
    }
}
