package com.sunecity.lilium.core.config;

import org.aeonbits.owner.Config;

/**
 * Created by shahram on 9/6/16.
 */
@Config.Sources({"classpath:META-INF/server.properties"})
public interface ServerConfig extends Config {

    @DefaultValue("http")
    default String scheme() {
        return "http";
    }

    @DefaultValue("localhost")
    default String address() {
        return "localhost";
    }

    @DefaultValue("8080")
    default int port() {
        return 8080;
    }

    @DefaultValue("/")
    default String contextPath() {
        return "/";
    }

}
