package com.sunecity.lilium.core.util;

import com.sunecity.lilium.core.model.Treeable;

import javax.enterprise.context.Dependent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by shahram on 4/26/16.
 */
@Dependent
public class Recursion implements Serializable {

    public List<String> getParents(List<? extends Treeable> children) throws Exception {
        List<String> parents = new ArrayList<>();
        for (Treeable child : children) {
            List<String> split = Arrays.stream(child.getPath().split("#")).skip(1).map(s -> "#" + s).collect(Collectors.toList());
            for (int i = 1; i < split.size(); i++) {
                parents.add(split.stream().limit(split.size() - i).reduce((a, b) -> a + b).orElseThrow(IllegalArgumentException::new));
            }
        }
        return parents;
    }

    public List<String> getParents(Treeable child) throws Exception {
        List<String> parents = new ArrayList<>();
        List<String> split = Arrays.stream(child.getPath().split("#")).skip(1).map(s -> "#" + s).collect(Collectors.toList());
        for (int i = 1; i < split.size(); i++) {
            parents.add(split.stream().limit(split.size() - i).reduce((a, b) -> a + b).orElseThrow(IllegalArgumentException::new));
        }
        return parents;
    }

    public String getAncestor(String child) throws Exception {
        return "#" + child.split("#")[1];
    }

}
