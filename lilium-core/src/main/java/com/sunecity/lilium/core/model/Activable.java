package com.sunecity.lilium.core.model;

import java.io.Serializable;

public interface Activable<ID extends Serializable> extends BaseIdentity<ID> {

    boolean isActive();

    void setActive(boolean active);

}
