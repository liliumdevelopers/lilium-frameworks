package com.sunecity.lilium.core.util;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by shahram on 11/19/16.
 */
public class Images {

    public static byte[] resize(byte[] image, int width, int height) throws IOException {
        String type = new MimeTypes().getSubtype(image);
        BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(image));
        bufferedImage = Scalr.resize(bufferedImage, Scalr.Mode.FIT_EXACT, width, height);
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            ImageIO.write(bufferedImage, type, outputStream);
            return outputStream.toByteArray();
        }
    }

    public static byte[] resizeToWidth(byte[] image, int width) throws IOException {
        String type = new MimeTypes().getSubtype(image);
        BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(image));
        bufferedImage = Scalr.resize(bufferedImage, Scalr.Mode.FIT_TO_WIDTH, width);
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            ImageIO.write(bufferedImage, type, outputStream);
            return outputStream.toByteArray();
        }
    }

    public static byte[] resizeToHeight(byte[] image, int height) throws IOException {
        String type = new MimeTypes().getSubtype(image);
        BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(image));
        bufferedImage = Scalr.resize(bufferedImage, Scalr.Mode.FIT_TO_HEIGHT, height);
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            ImageIO.write(bufferedImage, type, outputStream);
            return outputStream.toByteArray();
        }
    }

}
