package com.sunecity.lilium.core.mapper;

import com.sunecity.lilium.core.time.DateTime;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

/**
 * Created by shahram on 12/14/16.
 */
public class DateTimeConverter extends BidirectionalConverter<DateTime, DateTime> {

    @Override
    public DateTime convertTo(DateTime dateTime, Type<DateTime> type, MappingContext mappingContext) {
        return DateTime.from(dateTime);
    }

    @Override
    public DateTime convertFrom(DateTime dateTime, Type<DateTime> type, MappingContext mappingContext) {
        return DateTime.from(dateTime);
    }

}
