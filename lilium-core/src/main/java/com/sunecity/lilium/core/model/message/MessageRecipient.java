package com.sunecity.lilium.core.model.message;

/**
 * @author Shahram Goodarzi
 */
public class MessageRecipient {

    private String recipient;
    private RecipientType recipientType;

    public MessageRecipient() {
    }

    public MessageRecipient(String recipient, RecipientType recipientType) {
        this.recipient = recipient;
        this.recipientType = recipientType;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public RecipientType getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(RecipientType recipientType) {
        this.recipientType = recipientType;
    }

}
