package com.sunecity.lilium.core.mapper;

import com.sunecity.lilium.core.time.Time;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

/**
 * Created by shahram on 12/14/16.
 */
public class TimeConverter extends BidirectionalConverter<Time, Time> {

    @Override
    public Time convertTo(Time time, Type<Time> type, MappingContext mappingContext) {
        return Time.from(time);
    }

    @Override
    public Time convertFrom(Time time, Type<Time> type, MappingContext mappingContext) {
        return Time.from(time);
    }

}
