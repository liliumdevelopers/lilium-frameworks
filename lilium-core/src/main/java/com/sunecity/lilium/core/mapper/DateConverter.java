package com.sunecity.lilium.core.mapper;

import com.sunecity.lilium.core.time.Date;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

/**
 * Created by shahram on 12/14/16.
 */
public class DateConverter extends BidirectionalConverter<Date, Date> {

    @Override
    public Date convertTo(Date date, Type<Date> type, MappingContext mappingContext) {
        return Date.from(date);
    }

    @Override
    public Date convertFrom(Date date, Type<Date> type, MappingContext mappingContext) {
        return Date.from(date);
    }

}
