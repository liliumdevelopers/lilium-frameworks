package com.sunecity.lilium.core.localization;

import com.sunecity.lilium.core.util.EncrypterDecrypter;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Locale;

/**
 * @author Shahram Goodarzi
 */
public class Locales {

    public static final Locale PERSIAN = new Locale("fa");
    public static final Locale PERSIAN_IRAN = new Locale.Builder().setLanguage("fa").setRegion("IR")
            .setExtension(Locale.UNICODE_LOCALE_EXTENSION, "nu-arabext").build();
    public static final Locale KURDISH = new Locale("ku");
    public static final Locale MALAY_MALAYSIA = new Locale("ms", "MY");
    public static final Locale ARABIC_QATAR = new Locale("ar", "QA");
    public static final Locale ICELANDIC_ICELAND = new Locale("is", "IS");
    public static final Locale FINNISH_FINLAND = new Locale("fi", "FI");
    public static final Locale POLISH = new Locale("pl");
    public static final Locale ENGLISH_MALTA = new Locale("en", "MT");
    public static final Locale ITALIAN_SWITZERLAND = new Locale("it", "CH");
    public static final Locale DUTCH_BELGIUM = new Locale("nl", "BE");
    public static final Locale ARABIC_SAUDIARABIA = new Locale("ar", "SA");
    public static final Locale ARABIC_IRAQ = new Locale("ar", "IQ");
    public static final Locale SPANISH_PUERTORICO = new Locale("es", "PR");
    public static final Locale SPANISH_CHILE = new Locale("es", "CL");
    public static final Locale FINNISH = new Locale("fi");
    public static final Locale GERMAN_AUSTRIA = new Locale("de", "AT");
    public static final Locale DANISH = new Locale("da");
    public static final Locale ENGLISH_UNITEDKINGDOM = new Locale("en", "GB");
    public static final Locale SPANISH_PANAMA = new Locale("es", "PA");
    public static final Locale SERBIAN = new Locale("sr");
    public static final Locale ARABIC_YEMEN = new Locale("ar", "YE");
    public static final Locale MACEDONIAN_MACEDONIA = new Locale("mk", "MK");
    public static final Locale MACEDONIAN = new Locale("mk");
    public static final Locale ENGLISH_CANADA = new Locale("en", "CA");
    public static final Locale VIETNAMESE_VIETNAM = new Locale("vi", "VN");
    public static final Locale DUTCH_NETHERLANDS = new Locale("nl", "NL");
    public static final Locale SPANISH_UNITEDSTATES = new Locale("es", "US");
    public static final Locale CHINESE_CHINA = new Locale("zh", "CN");
    public static final Locale SPANISH_HONDURAS = new Locale("es", "HN");
    public static final Locale ENGLISH_UNITEDSTATES = new Locale("en", "US");
    public static final Locale FRENCH = new Locale("fr");
    public static final Locale THAI = new Locale("th");
    public static final Locale ARABIC = new Locale("ar");
    public static final Locale ARABIC_MOROCCO = new Locale("ar", "MA");
    public static final Locale LATVIAN = new Locale("lv");
    public static final Locale GERMAN = new Locale("de");
    public static final Locale INDONESIAN_INDONESIA = new Locale("in", "ID");
    public static final Locale CROATIAN = new Locale("hr");
    public static final Locale ENGLISH_SOUTHAFRICA = new Locale("en", "ZA");
    public static final Locale KOREAN_SOUTHKOREA = new Locale("ko", "KR");
    public static final Locale ARABIC_TUNISIA = new Locale("ar", "TN");
    public static final Locale INDONESIAN = new Locale("in");
    public static final Locale JAPANESE = new Locale("ja");
    public static final Locale SERBIAN_SERBIA = new Locale("sr", "RS");
    public static final Locale BELARUSIAN_BELARUS = new Locale("be", "BY");
    public static final Locale CHINESE_TAIWAN = new Locale("zh", "TW");
    public static final Locale ARABIC_SUDAN = new Locale("ar", "SD");
    public static final Locale PORTUGUESE = new Locale("pt");
    public static final Locale ICELANDIC = new Locale("is");
    public static final Locale JAPANESE_JAPAN_JP = new Locale("ja", "JP", "JP");
    public static final Locale SPANISH_BOLIVIA = new Locale("es", "BO");
    public static final Locale ARABIC_ALGERIA = new Locale("ar", "DZ");
    public static final Locale MALAY = new Locale("ms");
    public static final Locale SPANISH_ARGENTINA = new Locale("es", "AR");
    public static final Locale ARABIC_UNITEDARABEMIRATES = new Locale("ar", "AE");
    public static final Locale FRENCH_CANADA = new Locale("fr", "CA");
    public static final Locale SLOVENIAN = new Locale("sl");
    public static final Locale SPANISH = new Locale("es");
    public static final Locale LITHUANIAN_LITHUANIA = new Locale("lt", "LT");
    public static final Locale SERBIAN_LATIN_MONTENEGRO = new Locale("sr", "ME");
    public static final Locale ARABIC_SYRIA = new Locale("ar", "SY");
    public static final Locale RUSSIAN_RUSSIA = new Locale("ru", "RU");
    public static final Locale FRENCH_BELGIUM = new Locale("fr", "BE");
    public static final Locale SPANISH_SPAIN = new Locale("es", "ES");
    public static final Locale BULGARIAN = new Locale("bg");
    public static final Locale SWEDISH = new Locale("sv");
    public static final Locale ENGLISH = new Locale("en");
    public static final Locale HEBREW = new Locale("iw");
    public static final Locale DANISH_DENMARK = new Locale("da", "DK");
    public static final Locale SPANISH_COSTARICA = new Locale("es", "CR");
    public static final Locale CHINESE_HONGKONG = new Locale("zh", "HK");
    public static final Locale CHINESE = new Locale("zh");
    public static final Locale CATALAN_SPAIN = new Locale("ca", "ES");
    public static final Locale THAI_THAILAND = new Locale("th", "TH");
    public static final Locale UKRAINIAN_UKRAINE = new Locale("uk", "UA");
    public static final Locale SPANISH_DOMINICANREPUBLIC = new Locale("es", "DO");
    public static final Locale SPANISH_VENEZUELA = new Locale("es", "VE");
    public static final Locale POLISH_POLAND = new Locale("pl", "PL");
    public static final Locale ARABIC_LIBYA = new Locale("ar", "LY");
    public static final Locale ARABIC_JORDAN = new Locale("ar", "JO");
    public static final Locale ITALIAN = new Locale("it");
    public static final Locale UKRAINIAN = new Locale("uk");
    public static final Locale HUNGARIAN_HUNGARY = new Locale("hu", "HU");
    public static final Locale IRISH = new Locale("ga");
    public static final Locale SPANISH_GUATEMALA = new Locale("es", "GT");
    public static final Locale SPANISH_PARAGUAY = new Locale("es", "PY");
    public static final Locale BULGARIAN_BULGARIA = new Locale("bg", "BG");
    public static final Locale CROATIAN_CROATIA = new Locale("hr", "HR");
    public static final Locale SERBIAN_LATIN_BOSNIAANDHERZEGOVINA = new Locale("sr", "BA");
    public static final Locale ROMANIAN_ROMANIA = new Locale("ro", "RO");
    public static final Locale FRENCH_LUXEMBOURG = new Locale("fr", "LU");
    public static final Locale NORWEGIAN = new Locale("no");
    public static final Locale LITHUANIAN = new Locale("lt");
    public static final Locale ENGLISH_SINGAPORE = new Locale("en", "SG");
    public static final Locale SPANISH_ECUADOR = new Locale("es", "EC");
    public static final Locale SERBIAN_BOSNIAANDHERZEGOVINA = new Locale("sr", "BA");
    public static final Locale SPANISH_NICARAGUA = new Locale("es", "NI");
    public static final Locale SLOVAK = new Locale("sk");
    public static final Locale RUSSIAN = new Locale("ru");
    public static final Locale MALTESE = new Locale("mt");
    public static final Locale SPANISH_ELSALVADOR = new Locale("es", "SV");
    public static final Locale DUTCH = new Locale("nl");
    public static final Locale HINDI_INDIA = new Locale("hi", "IN");
    public static final Locale ESTONIAN = new Locale("et");
    public static final Locale GREEK_GREECE = new Locale("el", "GR");
    public static final Locale SLOVENIAN_SLOVENIA = new Locale("sl", "SI");
    public static final Locale ITALIAN_ITALY = new Locale("it", "IT");
    public static final Locale JAPANESE_JAPAN = new Locale("ja", "JP");
    public static final Locale GERMAN_LUXEMBOURG = new Locale("de", "LU");
    public static final Locale FRENCH_SWITZERLAND = new Locale("fr", "CH");
    public static final Locale MALTESE_MALTA = new Locale("mt", "MT");
    public static final Locale ARABIC_BAHRAIN = new Locale("ar", "BH");
    public static final Locale ALBANIAN = new Locale("sq");
    public static final Locale VIETNAMESE = new Locale("vi");
    public static final Locale SERBIAN_MONTENEGRO = new Locale("sr", "ME");
    public static final Locale PORTUGUESE_BRAZIL = new Locale("pt", "BR");
    public static final Locale NORWEGIAN_NORWAY = new Locale("no", "NO");
    public static final Locale GREEK = new Locale("el");
    public static final Locale GERMAN_SWITZERLAND = new Locale("de", "CH");
    public static final Locale CHINESE_SINGAPORE = new Locale("zh", "SG");
    public static final Locale ARABIC_KUWAIT = new Locale("ar", "KW");
    public static final Locale ARABIC_EGYPT = new Locale("ar", "EG");
    public static final Locale IRISH_IRELAND = new Locale("ga", "IE");
    public static final Locale SPANISH_PERU = new Locale("es", "PE");
    public static final Locale CZECH_CZECHREPUBLIC = new Locale("cs", "CZ");
    public static final Locale TURKISH_TURKEY = new Locale("tr", "TR");
    public static final Locale CZECH = new Locale("cs");
    public static final Locale SPANISH_URUGUAY = new Locale("es", "UY");
    public static final Locale ENGLISH_IRELAND = new Locale("en", "IE");
    public static final Locale ENGLISH_INDIA = new Locale("en", "IN");
    public static final Locale ARABIC_OMAN = new Locale("ar", "OM");
    public static final Locale SERBIAN_SERBIAANDMONTENEGRO = new Locale("sr", "CS");
    public static final Locale CATALAN = new Locale("ca");
    public static final Locale BELARUSIAN = new Locale("be");
    public static final Locale SERBIAN_LATIN = new Locale("sr");
    public static final Locale KOREAN = new Locale("ko");
    public static final Locale ALBANIAN_ALBANIA = new Locale("sq", "AL");
    public static final Locale PORTUGUESE_PORTUGAL = new Locale("pt", "PT");
    public static final Locale LATVIAN_LATVIA = new Locale("lv", "LV");
    public static final Locale SERBIAN_LATIN_SERBIA = new Locale("sr", "RS");
    public static final Locale SLOVAK_SLOVAKIA = new Locale("sk", "SK");
    public static final Locale SPANISH_MEXICO = new Locale("es", "MX");
    public static final Locale ENGLISH_AUSTRALIA = new Locale("en", "AU");
    public static final Locale NORWEGIAN_NORWAY_NYNORSK = new Locale("no", "NO", "NY");
    public static final Locale ENGLISH_NEWZEALAND = new Locale("en", "NZ");
    public static final Locale SWEDISH_SWEDEN = new Locale("sv", "SE");
    public static final Locale ROMANIAN = new Locale("ro");
    public static final Locale ARABIC_LEBANON = new Locale("ar", "LB");
    public static final Locale GERMAN_GERMANY = new Locale("de", "DE");
    public static final Locale THAI_THAILAND_TH = new Locale("th", "TH", "TH");
    public static final Locale TURKISH = new Locale("tr");
    public static final Locale SPANISH_COLOMBIA = new Locale("es", "CO");
    public static final Locale ENGLISH_PHILIPPINES = new Locale("en", "PH");
    public static final Locale ESTONIAN_ESTONIA = new Locale("et", "EE");
    public static final Locale GREEK_CYPRUS = new Locale("el", "CY");
    public static final Locale HUNGARIAN = new Locale("hu");
    public static final Locale FRENCH_FRANCE = new Locale("fr", "FR");

    private Locales() {
    }

    private static boolean isWebApp() {
        try {
            Class.forName("javax.faces.context.FacesContext");
            return FacesContext.getCurrentInstance() != null;
        } catch (ClassNotFoundException | NoClassDefFoundError e) {
            return false;
        }
    }

    public static boolean isRtl() {
        switch (getLocale().getLanguage()) {
            case "fa":
            case "ku":
            case "ar":
                return true;
            default:
                return false;
        }
    }

    public static Locale getLocale() {
        Locale locale;
        if (isWebApp()) {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Object o = ((HttpSession) externalContext.getSession(true)).getAttribute("lang." + ((HttpServletRequest) externalContext.getRequest()).getServerName() + ((HttpServletRequest) externalContext.getRequest()).getContextPath());
            if (o != null) {
                locale = new Locale(o.toString());
            } else {
                String lang = null;
                String langEncrypted;
                try {
                    Cookie[] cookies = ((HttpServletRequest) externalContext.getRequest()).getCookies();
                    if (cookies != null) {
                        langEncrypted = Arrays.stream(cookies).filter(c -> {
                            try {
                                return c.getName() != null && c.getName().equals(EncrypterDecrypter.encrypt("lang." + ((HttpServletRequest) externalContext.getRequest()).getServerName() + ((HttpServletRequest) externalContext.getRequest()).getContextPath(), "sunecity")) && c.getValue() != null && c.getValue().trim().length() != 0;
                            } catch (GeneralSecurityException e) {
                                return false;
                            }
                        }).findAny().map(Cookie::getValue).map(c -> {
                            try {
                                return URLDecoder.decode(c, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                return null;
                            }
                        }).orElse(null);
                        if (langEncrypted != null && langEncrypted.trim().length() != 0) {
                            try {
                                lang = EncrypterDecrypter.decrypt(langEncrypted, "sunecity");
                            } catch (Exception e) {
                                lang = null;
                                String encoded = URLEncoder.encode(EncrypterDecrypter.encrypt("lang." + ((HttpServletRequest) externalContext.getRequest()).getServerName() + ((HttpServletRequest) externalContext.getRequest()).getContextPath(), "sunecity"), "UTF-8");
                                Arrays.stream(cookies).filter(c -> c.getName() != null && c.getName().equals(encoded)).findAny().ifPresent(c -> {
                                    c.setMaxAge(0);
                                    c.setValue(null);
                                    ((HttpServletResponse) externalContext.getResponse()).addCookie(c);
                                });
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (lang != null && lang.trim().length() != 0) {
                    locale = new Locale(lang);
                } else {
                    if (FacesContext.getCurrentInstance().getViewRoot() != null) {
                        locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
                    } else {
                        locale = FacesContext.getCurrentInstance().getApplication().getDefaultLocale();
                    }
                }
            }
        } else {
            locale = Locale.getDefault();
        }
        return locale;
    }

    public static void setLocale(Locale locale) {
        if (isWebApp()) {
            FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            ((HttpSession) externalContext.getSession(true)).setAttribute("lang." + ((HttpServletRequest) externalContext.getRequest()).getServerName() + ((HttpServletRequest) externalContext.getRequest()).getContextPath(), locale.getLanguage());
            try {
                Cookie cookie = new Cookie(EncrypterDecrypter.encrypt("lang." + ((HttpServletRequest) externalContext.getRequest()).getServerName() + ((HttpServletRequest) externalContext.getRequest()).getContextPath(), "sunecity"), EncrypterDecrypter.encrypt(locale.getLanguage(), "sunecity"));
                cookie.setMaxAge(Integer.MAX_VALUE);
                cookie.setPath("/");
                ((HttpServletResponse) externalContext.getResponse()).addCookie(cookie);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Locale.setDefault(locale);
        }
    }

}
