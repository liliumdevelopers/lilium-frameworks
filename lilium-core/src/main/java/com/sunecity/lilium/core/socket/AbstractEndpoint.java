package com.sunecity.lilium.core.socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.CloseReason;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by shahram on 4/13/17.
 */
public abstract class AbstractEndpoint<T extends SocketMessage> {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

//    protected final List<Session> sessions = new CopyOnWriteArrayList<>();
    protected final Set<Session> sessions = Collections.synchronizedSet(new HashSet<>());

    @OnOpen
    public void onOpen(Session session, EndpointConfig endpointConfig) {
        sessions.add(session);
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        sessions.remove(session);
    }

    @OnError
    public void onError(Session session, Throwable error) {
        logger.warn("Problem in websocket");
    }

    @OnMessage
    public void onMessage(T message, Session session) {
    }

    public int push(T message, String... userIds) throws IOException, EncodeException {
        int count = 0;
        for (Session session : sessions) {
            for (String userId : userIds) {
                if (userId.equals(session.getUserPrincipal().getName())) {
                    session.getBasicRemote().sendObject(message);
                    count++;
                }
            }
        }
        return count;
    }

    public int pushAll(T message) throws IOException, EncodeException {
        int count = 0;
        for (Session session : sessions) {
            session.getBasicRemote().sendObject(message);
            count++;
        }
        return count;
    }

}
