package com.sunecity.lilium.core.util;

import javax.crypto.Cipher;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Base64;

/**
 * @author Shahram Goodarzi
 */
public class EncrypterDecrypter {

    private EncrypterDecrypter() {
    }

    public static Cipher getECipher(String password) throws GeneralSecurityException {
        byte[] desKeyData = password.getBytes();
        DESKeySpec desKeySpec = new DESKeySpec(desKeyData);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
        String algorithm = secretKey.getAlgorithm();
        return getECipher(secretKey, algorithm);
    }

    public static Cipher getDCipher(String password) throws GeneralSecurityException {
        byte[] desKeyData = password.getBytes();
        DESKeySpec desKeySpec = new DESKeySpec(desKeyData);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
        String algorithm = secretKey.getAlgorithm();
        return getDCipher(secretKey, algorithm);
    }

    public static Cipher getECipher(SecretKey secretKey, String algorithm) throws GeneralSecurityException {
        Cipher ecipher = Cipher.getInstance(algorithm);
        ecipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return ecipher;
    }

    public static Cipher getDCipher(SecretKey secretKey, String algorithm) throws GeneralSecurityException {
        Cipher dcipher = Cipher.getInstance(algorithm);
        dcipher.init(Cipher.DECRYPT_MODE, secretKey);
        return dcipher;
    }

    public static SecretKey getSecretKey(String password) throws GeneralSecurityException {
        byte[] desKeyData = password.getBytes();
        DESKeySpec desKeySpec = new DESKeySpec(desKeyData);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        return keyFactory.generateSecret(desKeySpec);
    }

    public static String encrypt(String str, String password) throws GeneralSecurityException {
        return Base64.getEncoder().encodeToString(getECipher(password).doFinal(str.getBytes()));
    }

    public static String encrypt(String str, SecretKey secretKey) throws GeneralSecurityException {
        return encrypt(str, secretKey, secretKey.getAlgorithm());
    }

    public static String encrypt(String str, SecretKey secretKey, String algorithm) throws GeneralSecurityException {
        return Base64.getEncoder().encodeToString(getECipher(secretKey, algorithm).doFinal(str.getBytes()));
    }

    public static String decrypt(String str, String password) throws GeneralSecurityException {
        try {
            return new String(getDCipher(password).doFinal(Base64.getDecoder().decode(str)), "UTF8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String decrypt(String str, SecretKey secretKey) throws GeneralSecurityException {
        return decrypt(str, secretKey, secretKey.getAlgorithm());
    }

    public static String decrypt(String str, SecretKey secretKey, String algorithm) throws GeneralSecurityException {
        try {
            return new String(getDCipher(secretKey, algorithm).doFinal(Base64.getDecoder().decode(str)), "UTF8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static Object encrypt(Serializable object, String password) throws GeneralSecurityException, IOException {
        return new SealedObject(object, getECipher(password));
    }

    public static Object encrypt(Serializable object, SecretKey secretKey) throws GeneralSecurityException, IOException {
        return encrypt(object, secretKey, secretKey.getAlgorithm());
    }

    public static Object encrypt(Serializable object, SecretKey secretKey, String algorithm) throws GeneralSecurityException, IOException {
        return new SealedObject(object, getECipher(secretKey, algorithm));
    }

    public static Object decrypt(Object object, String password) throws GeneralSecurityException, IOException, ClassNotFoundException {
        return ((SealedObject) object).getObject(getSecretKey(password));
    }

    public static Object decrypt(Object object, SecretKey secretKey) throws GeneralSecurityException, IOException, ClassNotFoundException {
        return decrypt(object, secretKey, secretKey.getAlgorithm());
    }

    public static Object decrypt(Object object, SecretKey secretKey, String algorithm) throws GeneralSecurityException, IOException, ClassNotFoundException {
        return ((SealedObject) object).getObject(secretKey, algorithm);
    }

    public static void encryptFile(String sourceFile, String password) throws GeneralSecurityException, IOException {
        File source = new File(sourceFile);
        FileInputStream fin = new FileInputStream(source);
        ByteArrayOutputStream fout = new ByteArrayOutputStream();

        Cipher ecipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        ecipher.init(Cipher.ENCRYPT_MODE, getSecretKey(password));

        byte[] iv = ecipher.getIV();
        DataOutputStream dout = new DataOutputStream(fout);
        dout.writeInt(iv.length);
        dout.write(iv);

        byte[] input = new byte[(int) source.length()];
        while (true) {
            int bytesRead = fin.read(input);
            if (bytesRead == -1) {
                break;
            }
            byte[] output = ecipher.update(input, 0, bytesRead);
            if (output != null) {
                dout.write(output);
            }
        }

        byte[] output = ecipher.doFinal();
        if (output != null) {
            dout.write(output);
        }
        fin.close();
        dout.flush();
        dout.close();
        byte[] bs = fout.toByteArray();
        fout.flush();
        fout.close();
        FileOutputStream fileOutputStream = new FileOutputStream(source);
        fileOutputStream.write(bs);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    public static void decryptFile(String sourceFile, String password) throws GeneralSecurityException, IOException {
        File source = new File(sourceFile);
        FileInputStream fin = new FileInputStream(source);
        ByteArrayOutputStream fout = new ByteArrayOutputStream();

        DataInputStream din = new DataInputStream(fin);
        int ivSize = din.readInt();
        byte[] iv = new byte[ivSize];
        din.readFully(iv);
        IvParameterSpec ivps = new IvParameterSpec(iv);

        Cipher dcipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        dcipher.init(Cipher.DECRYPT_MODE, getSecretKey(password), ivps);

        byte[] input = new byte[(int) source.length()];
        while (true) {
            int bytesRead = fin.read(input);
            if (bytesRead == -1) {
                break;
            }
            byte[] output = dcipher.update(input, 0, bytesRead);
            if (output != null) {
                fout.write(output);
            }
        }

        byte[] output = dcipher.doFinal();
        if (output != null) {
            fout.write(output);
        }
        fin.close();
        din.close();
        byte[] bs = fout.toByteArray();
        fout.flush();
        fout.close();
        FileOutputStream fileOutputStream = new FileOutputStream(source);
        fileOutputStream.write(bs);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    public static void encryptFile(String sourceFile, String destfile, String password) throws GeneralSecurityException, IOException {
        File source = new File(sourceFile);
        File dest;
        if (destfile == null) {
            dest = new File(sourceFile + "-enc");
        } else {
            dest = new File(destfile);
        }
        FileInputStream fin = new FileInputStream(source);
        FileOutputStream fout = new FileOutputStream(dest);

        Cipher ecipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        ecipher.init(Cipher.ENCRYPT_MODE, getSecretKey(password));

        byte[] iv = ecipher.getIV();
        DataOutputStream dout = new DataOutputStream(fout);
        dout.writeInt(iv.length);
        dout.write(iv);

        byte[] input = new byte[(int) source.length()];
        while (true) {
            int bytesRead = fin.read(input);
            if (bytesRead == -1) {
                break;
            }
            byte[] output = ecipher.update(input, 0, bytesRead);
            if (output != null) {
                dout.write(output);
            }
        }

        byte[] output = ecipher.doFinal();
        if (output != null) {
            dout.write(output);
        }
        fin.close();
        fout.flush();
        fout.close();
        dout.flush();
        dout.close();
        if (destfile == null) {
            source.deleteOnExit();
        }
    }

    public static void decryptFile(String sourceFile, String destfile, String password) throws GeneralSecurityException, IOException {
        File source = new File(sourceFile);
        File dest;
        if (destfile == null) {
            dest = new File(sourceFile.replaceFirst("-enc", ""));
        } else {
            dest = new File(destfile);
        }
        FileInputStream fin = new FileInputStream(source);
        FileOutputStream fout = new FileOutputStream(dest);

        DataInputStream din = new DataInputStream(fin);
        int ivSize = din.readInt();
        byte[] iv = new byte[ivSize];
        din.readFully(iv);
        IvParameterSpec ivps = new IvParameterSpec(iv);

        Cipher dcipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        dcipher.init(Cipher.DECRYPT_MODE, getSecretKey(password), ivps);

        byte[] input = new byte[(int) source.length()];
        while (true) {
            int bytesRead = fin.read(input);
            if (bytesRead == -1) {
                break;
            }
            byte[] output = dcipher.update(input, 0, bytesRead);
            if (output != null) {
                fout.write(output);
            }
        }

        byte[] output = dcipher.doFinal();
        if (output != null) {
            fout.write(output);
        }
        fin.close();
        fout.flush();
        fout.close();
        din.close();
        if (destfile == null) {
            source.deleteOnExit();
        }
    }

}
