package com.sunecity.lilium.core.util;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shahram on 11/13/16.
 */
public class Encoding {

    public static void convert(InputStream inputStream, OutputStream outputStream) throws IOException {
        CharsetDetector detector = new CharsetDetector();
        detector.setText(inputStream);
        CharsetMatch match = detector.detect();
        try (BufferedReader reader = new BufferedReader(match.getReader());
             BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"))) {
            char[] buffer = new char[8192];
            int len;
            while ((len = reader.read(buffer)) >= 0) {
                writer.write(buffer, 0, len);
            }
        }
    }

    public static void convert(InputStream inputStream, OutputStream outputStream, String encoding) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, encoding));
             BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"))) {
            char[] buffer = new char[8192];
            int len;
            while ((len = reader.read(buffer)) >= 0) {
                writer.write(buffer, 0, len);
            }
        }
    }

    public static List<String> getEncodings() {
        return new ArrayList<>(Charset.availableCharsets().keySet());
    }

}