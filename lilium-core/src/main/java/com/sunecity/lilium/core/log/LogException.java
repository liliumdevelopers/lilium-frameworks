package com.sunecity.lilium.core.log;

import com.sunecity.lilium.core.exception.LiliumException;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Inherited
public @interface LogException {

    @Nonbinding
    String value() default "";

    @Nonbinding
    Class<? extends Exception> exception() default Exception.class;

    @Nonbinding
    Class<? extends Exception> throwException() default Exception.class;

}