package com.sunecity.lilium.core;

import org.junit.Test;
import tec.uom.se.quantity.Quantities;
import tec.uom.se.unit.Units;

import javax.measure.Quantity;
import javax.measure.quantity.Length;
import javax.measure.quantity.Speed;
import javax.measure.quantity.Time;

/**
 * Created by shahram on 9/14/16.
 */
public class UnitTest {

    @Test
    public void timeTest() {
        Quantity<Length> distance = Quantities.getQuantity(52, Units.METRE);
        Quantity<Speed> airplaneSpeed = Quantities.getQuantity(26, Units.KILOMETRE_PER_HOUR);
        Quantity<Time> eta = (Quantity<Time>) distance.divide(airplaneSpeed);
        System.out.println("ETA: " + eta.to(Units.SECOND));
    }

}
