package com.sunecity.lilium.core.util;

import org.junit.Test;
import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.OperatingSystem;

public class HardWareTest {

    @Test
    public void test() {
        SystemInfo systemInfo = new SystemInfo();
        HardwareAbstractionLayer hardware = systemInfo.getHardware();
        OperatingSystem operatingSystem = systemInfo.getOperatingSystem();

        System.out.println(hardware.getProcessor().getName());
        System.out.println(hardware.getMemory().getAvailable());
        System.out.println(hardware.getDiskStores()[0].getName());
        System.out.println(hardware.getDiskStores()[0].getSerial());
        System.out.println(hardware.getPowerSources()[0].getName());

        System.out.println(operatingSystem.getManufacturer());
        System.out.println(operatingSystem.getFamily());
    }

}
