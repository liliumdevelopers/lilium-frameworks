package com.sunecity.lilium.core.util;

import org.junit.Test;

import java.io.FileOutputStream;

/**
 * Created by shahram on 11/22/16.
 */
public class EncodingTest {

    @Test
    public void test() throws Exception {
        Encoding.convert(getClass().getResourceAsStream("/encoded.srt"), new FileOutputStream("target/decoded.srt"),"Windows-1256");
    }

}
