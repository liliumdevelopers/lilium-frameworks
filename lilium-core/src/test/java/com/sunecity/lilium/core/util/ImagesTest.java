package com.sunecity.lilium.core.util;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by shahram on 11/19/16.
 */
public class ImagesTest {

    @Test
    public void test() throws IOException {
        byte[] bs = Files.readAllBytes(Paths.get("/home/shahram/Pictures/1.png"));
        bs = Images.resize(bs, 200,300);
        Files.write(Paths.get("/home/shahram/Pictures/11111.png"), bs);
    }

}
