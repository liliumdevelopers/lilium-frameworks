package com.sunecity.lilium.core.time;

import java.time.LocalTime;

/**
 * Created by shahram on 9/4/15.
 */
public class TimeTest {

    public static void main(String[] args) {
        LocalTime time = LocalTime.now();
        System.out.println(time);
    }

}
