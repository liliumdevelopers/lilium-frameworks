package com.sunecity.lilium.core.time;

import com.ibm.icu.util.Calendar;
import com.sunecity.lilium.core.localization.Locales;
import org.junit.Test;

/**
 * Created by shahram on 11/22/16.
 */
public class CalendarTest {

    @Test
    public void persian() {
        Calendar calendar = Calendar.getInstance(Locales.PERSIAN);
        calendar.set(Calendar.DAY_OF_MONTH,5);
        System.out.println(calendar.isWeekend());
    }

}
