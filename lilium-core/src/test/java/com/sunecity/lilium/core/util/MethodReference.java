package com.sunecity.lilium.core.util;

import com.sunecity.lilium.core.model.BaseEntity;
import jodd.methref.Methref;
import org.junit.Assert;

public class MethodReference {

    public static void main(String[] args) {
        Methref<BaseEntity> mr = Methref.on(BaseEntity.class);
        Assert.assertEquals(mr.ref(mr.to().getVersion()), "getVersion");
    }

}