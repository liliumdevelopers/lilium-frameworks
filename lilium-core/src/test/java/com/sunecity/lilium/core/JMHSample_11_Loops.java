package com.sunecity.lilium.core;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.RunnerException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class JMHSample_11_Loops {

    public static void main(String[] args) throws RunnerException {
//        Options opt = new OptionsBuilder()
//                .include(JMHSample_11_Loops.class.getSimpleName())
//                .warmupIterations(5)
//                .measurementIterations(5)
//                .forks(4)
//                .build();
//
//        new Runner(opt).run();
        var locales = new ArrayList<Locale>();
        for (Locale locale : Locale.getAvailableLocales()) {
            try {
                final int directionality = Character.getDirectionality(locale.getDisplayLanguage(locale).charAt(0));
                if (directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT ||
                        directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC) {
                    locales.add(locale);
                }
            } catch (Exception e) {
//                System.out.println(e.getMessage());
            }

        }
        locales.sort(Comparator.comparing(Locale::toString));
        System.out.print("[");
        for (Locale locale : locales) {
            System.out.print("'" + locale + "', ");
        }
        System.out.print("]");
    }

    @Benchmark
    @OperationsPerInvocation(1)
    public void test() {
        int array[][] = new int[1000][1000];

        for (int k = 0; k < 1000; k++) {
            for (int i = 0; i < 1000; i++) {
                for (int j = 0; j < 1000; j++) {
                    array[i][j] = 20 * 145;
                }
            }
        }
    }

}