package com.sunecity.lilium.core.util;

import org.junit.Test;

import java.util.Arrays;

public class PlatformTest {

    @Test
    public void main() {
        System.out.println(Platform.getOsType());
        System.out.println(Platform.is64Bit());
        System.out.println(Platform.isIntel());

        Arrays.stream(Platform.getMacAddress()).forEach(System.out::println);
    }

}
