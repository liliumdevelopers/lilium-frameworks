package com.sunecity.lilium.core.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by shahram on 11/9/15.
 */
public class StreamTest {

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("adf", "cgi", "dwr");
        Stream<String> stream = strings.stream().filter(s -> {
            System.out.println(s);
            return s.contains("g");
        });
        stream.count();
        List<Integer> one = Arrays.asList(1, 2, 3);
        List<Integer> two = Arrays.asList(3, 4);

    }

}
