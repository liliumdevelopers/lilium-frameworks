package com.sunecity.lilium.core.util;

import javassist.CannotCompileException;
import javassist.NotFoundException;
import org.apache.commons.beanutils.PropertyUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class PojoGeneratorTest {

    public static void main(String[] args) {

        try {
            Map<String, Class<?>> fields = new HashMap<>();
            fields.put("name", String.class);
            fields.put("family", String.class);
            Class<?> clas = PojoGenerator.generate("com.sunecity.Student", fields);
            Object o = clas.getConstructor().newInstance();
            PropertyUtils.setProperty(o, "name", "shahram");
            System.out.println(PropertyUtils.getProperty(o, "name"));
        } catch (NotFoundException | CannotCompileException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        try {
            PojoGenerator pojoGenerator = new PojoGenerator("com.sunecity.lilium.Student");
            pojoGenerator.addField("name", String.class);
            pojoGenerator.addField("family", String.class);
            pojoGenerator.generate();
            pojoGenerator.setFieldValue("name", "shahram");
            System.out.println(pojoGenerator.getFieldValue("name"));
            System.out.println(pojoGenerator.getPojo());
        } catch (NotFoundException | CannotCompileException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }

}
