package com.sunecity.lilium.core.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by shahram on 5/22/17.
 */
public class CollectionDiffTest {

    @Test
    public void diffTest() {
        List<String> list1 = Arrays.asList("1", "2", "3", "4");
        List<String> list2 = Arrays.asList("9", "5", "3", "7", "1");
        CollectionDiff<List<String>, String> collection = new CollectionDiff<>(list1, list2);

        System.out.println("Union");
        System.out.println(collection.getUnion());

        System.out.println("Intersect");
        System.out.println(collection.getIntersect());

        System.out.println("SymmetricDiff");
        System.out.println(collection.getSymmetricDiff());

        System.out.println("NewCollection");
        System.out.println(collection.getNew());

        System.out.println("OldCollection");
        System.out.println(collection.getOld());


    }

}
