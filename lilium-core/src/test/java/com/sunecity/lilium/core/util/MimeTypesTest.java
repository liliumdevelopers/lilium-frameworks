package com.sunecity.lilium.core.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class MimeTypesTest {

    @Test
    public void testJavaMime() {
        try {
            Assert.assertEquals(new MimeTypes().getMimeType(MimeTypesTest.class.getResourceAsStream("MimeTypesTest.class")), "application/java-vm");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
