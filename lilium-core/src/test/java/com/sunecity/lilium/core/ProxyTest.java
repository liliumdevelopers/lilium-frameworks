package com.sunecity.lilium.core;

import org.junit.Test;

import java.lang.reflect.Proxy;

/**
 * Created by shahram on 10/30/16.
 */
public class ProxyTest {

    @Test
    public void test() {
        Object o = new ProxyTest().proxy();
        Tree tree = (Tree) o;
        tree.sayHello();
        tree.sayBye("shahram");
        System.out.println(tree.getClass());
    }

    Object proxy() {
        Tree tree = new TreeImpl();
        return Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Tree.class}, (proxy, method, args) -> {
            System.out.println("Before Method...");
            method.invoke(tree, args);
            System.out.println("Method Name: " + method);
            return proxy;
        });
    }

    interface Tree {
        void sayHello();

        void sayBye(String name);
    }

    private class TreeImpl implements Tree {

        public void sayHello() {
            System.out.println("Hello");
        }

        public void sayBye(String name) {
            System.out.println("Bye " + name);
        }

    }

}
