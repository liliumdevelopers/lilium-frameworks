package com.sunecity.lilium.core.validation;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by shahram on 11/26/15.
 */
public class ValidatorTest {

    @Test
    public void main() {
        Assert.assertTrue(Validator.isMacAddress("78:24:af:dc:ea:ff"));
    }

    @Test
    public void number() {
        Assert.assertTrue(Validator.isNumber("-۱۵۰"));
    }

    @Test
    public void character() {
        Assert.assertTrue(Validator.isCharacter("شهرام"));
    }

    @Test
    public void characterOrNumber() {
        Assert.assertTrue(Validator.isCharacterOrNumber("شماره۲۵"));
    }

}
