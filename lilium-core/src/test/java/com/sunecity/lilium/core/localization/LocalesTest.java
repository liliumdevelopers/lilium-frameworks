package com.sunecity.lilium.core.localization;

import java.util.Locale;

public class LocalesTest {

    public static void main(String[] args) {
        Locale locale = Locales.ARABIC_IRAQ;
        System.out.println(locale.getLanguage());
        Locales.setLocale(Locales.ARABIC_IRAQ);
        System.out.println(Locales.isRtl());
    }

}
