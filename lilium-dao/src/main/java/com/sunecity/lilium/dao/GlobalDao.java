package com.sunecity.lilium.dao;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sunecity.lilium.core.exception.ConcurrentException;
import com.sunecity.lilium.core.exception.Exceptions;
import com.sunecity.lilium.core.exception.ForeignKeyException;
import com.sunecity.lilium.core.exception.UniqueException;
import com.sunecity.lilium.data.IReport;
import com.sunecity.lilium.data.criteria.CriteriaMetaData;
import com.sunecity.lilium.data.criteria.DeleteMetaData;
import com.sunecity.lilium.data.criteria.QueryMetaData;
import com.sunecity.lilium.data.criteria.StoredProcedureMetaData;
import com.sunecity.lilium.data.criteria.UpdateMetaData;
import com.sunecity.lilium.data.report.DynamicReport;
import com.sunecity.lilium.data.report.Report;
import com.sunecity.lilium.data.report.ReportType;
import com.sunecity.lilium.report.jasper.JasperReport;

import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Flushable;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generic General Dao for handling all kinds of database operations
 * including save, update, delete and select
 *
 * @author Shahram Goodarzi
 */
public abstract class GlobalDao implements IReport, Flushable {

    private static final String ENTITY_GRAPH = "javax.persistence.fetchgraph";

    @Inject
    private Exceptions exceptions;

    protected abstract EntityManager getEntityManager();

    public <T extends Serializable> T create(T entity) throws Exception {
        getEntityManager().persist(entity);
        return entity;
    }

    public <T extends Collection<? extends Serializable>> T create(T entities) throws Exception {
        entities.forEach(getEntityManager()::persist);
        return entities;
    }

    public <T extends Serializable> T update(T entity) throws Exception {
        entity = getEntityManager().merge(entity);
        return entity;
    }

    public <T extends Collection<? extends Serializable>> T update(T entities) throws Exception {
        entities.forEach(getEntityManager()::merge);
        return entities;
    }

    public <T extends Serializable> void delete(Class<T> clas, Serializable... ids) throws Exception {
        for (Serializable id : ids) {
            final T t = getEntityManager().getReference(clas, id);
            getEntityManager().remove(t);
        }
    }

    public <T extends Serializable> void delete(Class<T> clas, List<? extends Serializable> ids) throws Exception {
        for (Serializable id : ids) {
            final T t = getEntityManager().getReference(clas, id);
            getEntityManager().remove(t);
        }
    }

    public <T extends Serializable> T selectById(Class<T> clas, Serializable id) throws Exception {
        return getEntityManager().find(clas, id);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectById(Class<T> clas, Serializable id, String graph) throws Exception {
        final JPAQuery<T> jpaQuery = new JPAQuery<>(getEntityManager());
        final PathBuilder<T> entityPath = new PathBuilder<>(clas, "entity");
        jpaQuery.from(entityPath).where(entityPath.get("id").eq(id));
        final Query query = jpaQuery.createQuery();
        final EntityGraph<?> entityGraph = getEntityManager().createEntityGraph(graph);
        query.setHint(ENTITY_GRAPH, entityGraph);
        List<T> result = query.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectSingle(QueryMetaData metaData) throws Exception {
        final Query query = getEntityManager().createNamedQuery(metaData.getQuery());
        setParameters(query, metaData);
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = getEntityManager().createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        final List<T> result = query.getResultList();
        if (result.size() > 1) {
            throw new PersistenceException("More than one result found.");
        }
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectFirst(QueryMetaData metaData) throws Exception {
        final Query query = getEntityManager().createNamedQuery(metaData.getQuery());
        setParameters(query, metaData);
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        query.setMaxResults(1);
        final List<T> result = query.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> List<T> select(QueryMetaData metaData) throws Exception {
        final Query query = getEntityManager().createNamedQuery(metaData.getQuery());
        setParameters(query, metaData);
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        if (metaData.getMax() != null) {
            query.setMaxResults(metaData.getMax());
        }
        return query.getResultList();
    }


    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectQuerySingle(QueryMetaData metaData) throws Exception {
        final Query query = getEntityManager().createQuery(metaData.getQuery());
        setParameters(query, metaData);
        final List<T> result = query.getResultList();
        if (result.size() > 1) {
            throw new PersistenceException("More than one result found.");
        }
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectQueryFirst(QueryMetaData metaData) throws Exception {
        final Query query = getEntityManager().createQuery(metaData.getQuery());
        setParameters(query, metaData);
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        query.setMaxResults(1);
        final List<T> result = query.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> List<T> selectQuery(QueryMetaData metaData) throws Exception {
        final Query query = getEntityManager().createQuery(metaData.getQuery());
        setParameters(query, metaData);
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        if (metaData.getMax() != null) {
            query.setMaxResults(metaData.getMax());
        }
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectSingle(CriteriaMetaData metaData) throws Exception {
        final JPAQuery<T> jpaQuery = new JPAQuery<>(getEntityManager(), metaData.getMetadata());
        if (metaData.getResult() != null) {
            jpaQuery.select(metaData.getResult());
        }
        final Query query = jpaQuery.createQuery();
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = getEntityManager().createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        final List<T> result = query.getResultList();
        if (result.size() > 1) {
            throw new PersistenceException("More than one result found.");
        }
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectFirst(CriteriaMetaData metaData) throws Exception {
        final JPAQuery<T> jpaQuery = new JPAQuery<>(getEntityManager(), metaData.getMetadata());
        if (metaData.getResult() != null) {
            jpaQuery.select(metaData.getResult());
        }
        final Query query = jpaQuery.createQuery();
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = getEntityManager().createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        query.setMaxResults(1);
        final List<T> result = query.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> List<T> select(CriteriaMetaData metaData) throws Exception {
        final JPAQuery<T> jpaQuery = new JPAQuery<>(getEntityManager(), metaData.getMetadata());
        if (metaData.getResult() != null) {
            jpaQuery.select(metaData.getResult());
        }
        final Query query = jpaQuery.createQuery();
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = getEntityManager().createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        if (metaData.getMax() != null) {
            query.setMaxResults(metaData.getMax());
        }
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectSingle(StoredProcedureMetaData metaData) throws Exception {
        StoredProcedureQuery storedProcedureQuery = getEntityManager().createStoredProcedureQuery(metaData.getQuery());
        if (metaData.getInParams() != null) {
            metaData.getInParams().forEach(storedProcedureQuery::setParameter);
        }
        final List<T> result = storedProcedureQuery.getResultList();
        if (result.size() > 1) {
            throw new PersistenceException("More than one result found.");
        }
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectFirst(StoredProcedureMetaData metaData) throws Exception {
        StoredProcedureQuery storedProcedureQuery = getEntityManager().createStoredProcedureQuery(metaData.getQuery());
        if (metaData.getInParams() != null) {
            metaData.getInParams().forEach(storedProcedureQuery::setParameter);
        }
        storedProcedureQuery.setMaxResults(1);
        final List<T> result = storedProcedureQuery.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> List<T> select(StoredProcedureMetaData metaData) throws Exception {
        StoredProcedureQuery storedProcedureQuery = getEntityManager().createStoredProcedureQuery(metaData.getQuery());
        if (metaData.getInParams() != null) {
            metaData.getInParams().forEach(storedProcedureQuery::setParameter);
        }
        return storedProcedureQuery.getResultList();
    }

    public int executeNamedStoredProcedureQuery(StoredProcedureMetaData metaData) throws Exception {
        StoredProcedureQuery storedProcedureQuery = getEntityManager().createStoredProcedureQuery(metaData.getQuery());
        if (metaData.getInParams() != null) {
            metaData.getInParams().forEach(storedProcedureQuery::setParameter);
        }
//        storedProcedureQuery.execute();
        if (metaData.getOutParams() != null) {
            Map<String, Object> outParams = new HashMap<>();
            for (Map.Entry<String, Object> entry : metaData.getOutParams().entrySet()) {
                outParams.put(entry.getKey(), storedProcedureQuery.getOutputParameterValue(entry.getKey()));
            }
            metaData.setOutParams(outParams);
        }
        return storedProcedureQuery.executeUpdate();
    }

    public int executeNamedQuery(QueryMetaData metaData) throws Exception {
        Query query = getEntityManager().createNamedQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        return query.executeUpdate();
    }

    public int executeQuery(QueryMetaData metaData) throws Exception {
        Query query = getEntityManager().createQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        return query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public int update(UpdateMetaData updateMetaData) throws Exception {
        try {
            JPAUpdateClause updateClause = new JPAUpdateClause(getEntityManager(), updateMetaData.getEntityPath()).where(updateMetaData.getPredicates());
            updateMetaData.getUpdates().forEach((k, v) -> updateClause.set(k, (Expression) v));
            int result = ((Number) updateClause.execute()).intValue();
            getEntityManager().flush();
            getEntityManager().clear();
            return result;
        } catch (Exception e) {
            String uniqueException = exceptions.isUniqueException(e);
            if (uniqueException != null) {
                throw new UniqueException(e.getMessage(), uniqueException);
            } else if (exceptions.isConcurrentException(e)) {
                throw new ConcurrentException(e.getMessage());
            } else {
                throw e;
            }
        }
    }

    public int delete(DeleteMetaData deleteMetaData) throws Exception {
        try {
            int result = ((Number) new JPADeleteClause(getEntityManager(), deleteMetaData.getEntityPath()).where(deleteMetaData.getPredicates()).execute()).intValue();
            getEntityManager().flush();
            getEntityManager().clear();
            return result;
        } catch (Exception e) {
            String foreignKeyException = exceptions.isForeignKeyException(e);
            if (foreignKeyException != null) {
                throw new ForeignKeyException(e.getMessage(), foreignKeyException);
            } else {
                throw e;
            }
        }
    }

    public int selectCount(CriteriaMetaData metaData) throws Exception {
        final JPAQuery<?> jpaQuery = new JPAQuery<>(getEntityManager(), metaData.getMetadata());
        return ((Number) jpaQuery.fetchCount()).intValue();
    }

    @Override
    public String createReport(Report report, ReportType reportType, CriteriaMetaData metaData) throws Exception {
        metaData.setFirst(null);
        metaData.setMax(null);
        ((DynamicReport) report).setValues(select(metaData));
        return JasperReport.createReport(report, reportType, select(metaData), null);
    }

    @Override
    public void flush() {
        try {
            getEntityManager().flush();
        } catch (Exception e) {
            String uniqueException = exceptions.isUniqueException(e);
            if (uniqueException != null) {
                throw new UniqueException(e.getMessage(), uniqueException);
            } else if (exceptions.isConcurrentException(e)) {
                throw new ConcurrentException(e.getMessage());
            } else {
                String foreignKeyException = exceptions.isForeignKeyException(e);
                if (foreignKeyException != null) {
                    throw new ForeignKeyException(e.getMessage(), foreignKeyException);
                } else {
                    throw e;
                }
            }
        }
    }

    public <T extends Serializable> T detach(T t) throws Exception {
        getEntityManager().detach(t);
        return t;
    }

    public void clear() throws Exception {
        getEntityManager().clear();
    }

    public CriteriaBuilder getCriteriaBuilder() {
        return getEntityManager().getCriteriaBuilder();
    }

    public <T extends Serializable> List<T> select(CriteriaQuery<T> criteriaQuery) throws Exception {
        TypedQuery<T> query = getEntityManager().createQuery(criteriaQuery);
        return query.getResultList();
    }

    private void setParameters(Query query, QueryMetaData metaData) {
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        if (metaData.getPositionParams() != null) {
            metaData.getPositionParams().forEach(query::setParameter);
        }
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = getEntityManager().createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
    }

}
