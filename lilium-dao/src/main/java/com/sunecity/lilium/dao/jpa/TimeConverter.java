package com.sunecity.lilium.dao.jpa;

import com.sunecity.lilium.core.time.Time;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Jpa converter for Time.
 *
 * @author Shahram Goodarzi
 */
@Converter(autoApply = true)
public class TimeConverter implements AttributeConverter<Time, java.sql.Time> {

    /**
     * Converts Time to database time.
     *
     * @param time Time
     * @return database time
     */
    @Override
    public java.sql.Time convertToDatabaseColumn(Time time) {
        if (time == null) {
            return null;
        }
        return java.sql.Time.valueOf(time.toLocalTime());
    }

    /**
     * Converts database time to Time.
     *
     * @param time database time
     * @return Time
     */
    @Override
    public Time convertToEntityAttribute(java.sql.Time time) {
        if (time == null) {
            return null;
        }
        return Time.from(time.toLocalTime());
    }

}
