package com.sunecity.lilium.dao.jpa;

import org.javamoney.moneta.Money;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.math.BigDecimal;

/**
 * Jpa converter for Money.
 *
 * @author Shahram Goodarzi
 */
@Converter(autoApply = true)
public class MoneyConverter implements AttributeConverter<Money, BigDecimal> {

    /**
     * Converts Money to BigDecimal.
     *
     * @param money Money
     * @return BigDecimal value of money
     */
    @Override
    public BigDecimal convertToDatabaseColumn(Money money) {
        if (money == null) {
            return null;
        }
        return money.getNumberStripped();
    }

    /**
     * Converts BigDecimal to Money.
     *
     * @param money BigDecimal value of money
     * @return Money
     */
    @Override
    public Money convertToEntityAttribute(BigDecimal money) {
        if (money == null) {
            return null;
        }
        return Money.of(money, "IRR");
    }

}
