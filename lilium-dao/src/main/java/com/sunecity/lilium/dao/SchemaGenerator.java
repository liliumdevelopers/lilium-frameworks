package com.sunecity.lilium.dao;

import javax.enterprise.context.Dependent;
import javax.persistence.Persistence;
import java.util.Properties;

/**
 * Generate ddl file from entities.
 *
 * @author Shahram Goodarzi
 */
@Dependent
public class SchemaGenerator {

    public void generate(String persistenceUnitName, String destination) {
        final Properties persistenceProperties = new Properties();

        persistenceProperties.setProperty("hibernate.hbm2ddl.auto", "");
        persistenceProperties.setProperty("javax.persistence.schema-generation.database.action", "none");
        persistenceProperties.setProperty("javax.persistence.create-database-schemas", "true");
        persistenceProperties.setProperty("javax.persistence.schema-generation.scripts.action", "create");
        persistenceProperties.setProperty("javax.persistence.schema-generation.create-source", "metadata");
        persistenceProperties.setProperty("javax.persistence.schema-generation.scripts.create-target", destination);

        Persistence.generateSchema(persistenceUnitName, persistenceProperties);
    }

}
