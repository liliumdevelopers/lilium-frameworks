package com.sunecity.lilium.dao;

import com.sunecity.lilium.data.repository.BaseNode;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;

/**
 * @author Shahram Goodarzi
 */
public abstract class GlobalRepoFile {

    protected abstract String url();

    public void createFolder(String name) {
    }

    public void createFile() {

    }

    public void rename(Path oldName, String newName) throws IOException {
        Files.move(oldName, oldName.resolveSibling(newName));
    }

    public void copy(Path sourcePath, final Path targetPath) throws IOException {
        Files.walkFileTree(sourcePath, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
                Files.createDirectories(targetPath.resolve(sourcePath.relativize(dir)));
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
                Files.copy(file, targetPath.resolve(sourcePath.relativize(file)), StandardCopyOption.REPLACE_EXISTING);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public void move(Path sourcePath, final Path targetPath) throws IOException {
        Files.walkFileTree(sourcePath, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
                Files.createDirectories(targetPath.resolve(sourcePath.relativize(dir)));
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
                Files.move(file, targetPath.resolve(sourcePath.relativize(file)), StandardCopyOption.REPLACE_EXISTING);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public void delete(Path path) throws IOException {
        Files.walkFileTree(path, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public void update() {

    }

    public boolean exists(BaseNode node) {
        return Files.exists(getPath(node));
    }

    public void get() {

    }

    public void setAttribute(BaseNode node, String attribute, Object value) throws IOException {
        Files.setAttribute(getPath(node), attribute, value);
    }

    public Object getAttribute(BaseNode node, String attribute) throws IOException {
        return Files.getAttribute(getPath(node), attribute);
    }

    public void deleteAttribute(Path path, String attribute) throws IOException {
        Files.setAttribute(path, attribute, null);
    }

    void rename(String name, String newName) {
//        node.getSession().move(node.getPath(), node.getParent().getPath() + "/" + newName);
        // Don't forget - not necessarily here at this place:
        // node.getSession().save();
    }

    private Path getPath(BaseNode node){
        return Paths.get(node.getName());
    }

}
