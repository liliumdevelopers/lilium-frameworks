package com.sunecity.lilium.dao.config;

import org.aeonbits.owner.Config;

/**
 * Created by shahram on 8/25/16.
 */
@Config.Sources({"classpath:META-INF/database.properties"})
public interface DatabaseConfig extends Config {

    String username();

    String password();

    @DefaultValue("")
    default String url() {
        return "";
    }

    @DefaultValue("")
    default String dbName() {
        return "";
    }

    @DefaultValue("localhost")
    default String host() {
        return "";
    }

    @DefaultValue("5432")
    default int port() {
        return 5432;
    }

    @DefaultValue("${dumpExecutable}")
    default String dumpExecutable() {
        return System.getProperty("dumpExecutable");
    }

    @DefaultValue("${dumpPath}")
    default String dumpPath() {
        return System.getProperty("dumpPath");
    }

}
