package com.sunecity.lilium.dao.flush;

/**
 * Created by shahram on 2/24/17.
 */
public interface Flushable {

    void flush() throws Exception;

}
