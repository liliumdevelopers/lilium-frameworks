package com.sunecity.lilium.dao.javers;

import org.javamoney.moneta.Money;
import org.javers.core.json.BasicStringTypeAdapter;

import java.math.BigDecimal;

/**
 * Created by shahram on 7/9/16.
 */
public class MoneyAdapter extends BasicStringTypeAdapter {

    @Override
    public String serialize(Object o) {
        Money money = (Money) o;
        return String.valueOf(money.getNumberStripped());
    }

    @Override
    public Object deserialize(String s) {
        return Money.of(new BigDecimal(s), "IRR");
    }

    @Override
    public Class getValueType() {
        return Money.class;
    }

}
