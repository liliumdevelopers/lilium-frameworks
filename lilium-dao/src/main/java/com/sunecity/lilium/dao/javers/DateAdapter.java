package com.sunecity.lilium.dao.javers;

import com.sunecity.lilium.core.time.Date;
import org.javers.core.json.BasicStringTypeAdapter;

import java.time.format.DateTimeFormatter;

/**
 * Created by shahram on 7/9/16.
 */
public class DateAdapter extends BasicStringTypeAdapter {

    @Override
    public String serialize(Object o) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        Date date = (Date) o;
        return date.format(formatter);
    }

    @Override
    public Object deserialize(String s) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        return Date.parse(s, formatter);
    }

    @Override
    public Class getValueType() {
        return Date.class;
    }

}
