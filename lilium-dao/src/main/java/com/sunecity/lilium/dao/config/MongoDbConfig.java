package com.sunecity.lilium.dao.config;

import org.aeonbits.owner.Config;

/**
 * Created by shahram on 7/11/16.
 */
@Config.Sources({"classpath:META-INF/mongodb.properties"})
public interface MongoDbConfig extends Config {

    String username();

    String password();

    String database();

    @DefaultValue("localhost")
    default String host() {
        return "localhost";
    }

    @DefaultValue("27017")
    default int port() {
        return 27017;
    }

}
