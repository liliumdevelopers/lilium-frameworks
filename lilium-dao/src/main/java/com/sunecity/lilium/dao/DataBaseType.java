package com.sunecity.lilium.dao;

import org.h2.tools.Script;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Enumeration to define different types of DataBases.
 *
 * @author Shahram Goodarzi
 */
public enum DataBaseType {

    H2("org.h2.Driver",
            db -> "jdbc:h2:" + db.getHost() + "/" + db.getDbName(),
            (db, b) -> {
                String[] args = {
                        "-url",
                        db.getUrl(),
                        "-user",
                        db.getUsername(),
                        "-password",
                        db.getPassword(),
                        "-script",
                        b};
                try {
                    Script.main(args);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }),
    MY_SQL("com.mysql.jdbc.Driver",
            db -> "jdbc:mysql://" + db.getHost() + ":" + db.getPort() + "/" + db.getDbName(),
            (db, b) -> {
                String executeCmd = db.getDumpExecutable() + " -h " + db.getHost() + " -P " + db.getPort()
                        + " -u " + db.getUsername() + " -p" + db.getPassword() + " " + db.getDbName() + " -r " + b;
                try {
                    Process process = Runtime.getRuntime().exec(executeCmd);
                    BufferedReader r = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                    r.lines().forEach(System.err::println);
                    r.close();
                    process.waitFor();
                } catch (IOException | InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }),
    POSTGRE_SQL("org.postgresql.Driver",
            db -> "jdbc:postgresql://" + db.getHost() + ":" + db.getPort() + "/" + db.getDbName(),
            (db, b) -> {
                List<String> cmds = new ArrayList<>();
                cmds.add(db.getDumpExecutable());
                cmds.add("-h");
                cmds.add(db.getHost());
                cmds.add("-p");
                cmds.add(String.valueOf(db.getPort()));
                cmds.add("-Ft");
                cmds.add("-U");
                cmds.add(db.getUsername());
                cmds.add("-d");
                cmds.add(db.getDbName());
                cmds.add("-f");
                cmds.add(b);
                ProcessBuilder pb = new ProcessBuilder(cmds);
                Map<String, String> env = pb.environment();
                env.put("PGPASSWORD", db.getPassword());
                try {
                    Process process = pb.start();
                    BufferedReader r = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                    r.lines().forEach(l -> {
                    });
                    r.close();
                    process.waitFor();
                } catch (IOException | InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }),
    SQL_SERVER("com.microsoft.sqlserver.jdbc.SQLServerDriver",
            db -> "jdbc:sqlserver://" + db.getHost() + ":" + db.getPort() + ";databaseName=" + db.getDbName(),
            (db, b) -> {
                throw new UnsupportedOperationException();
            }),
    ORACLE("oracle.jdbc.OracleDriver",
            db -> "jdbc:oracle:thin:@" + db.getHost() + ":" + db.getPort() + ":" + db.getDbName(),
            (db, b) -> {
                throw new UnsupportedOperationException();
            });

    private final String driver;
    private final Function<DataBase, String> url;
    private final BiConsumer<DataBase, String> backup;

    DataBaseType(String driver, Function<DataBase, String> url, BiConsumer<DataBase, String> backup) {
        this.driver = driver;
        this.url = url;
        this.backup = backup;
    }

    public String getDriver() {
        return driver;
    }

    String getUrl(DataBase dataBase) {
        return url.apply(dataBase);
    }

    void backup(DataBase dataBase, String backup) {
        this.backup.accept(dataBase, backup);
    }

}
