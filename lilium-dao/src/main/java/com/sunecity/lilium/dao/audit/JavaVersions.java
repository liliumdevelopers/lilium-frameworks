package com.sunecity.lilium.dao.audit;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.sunecity.lilium.dao.DataBase;
import com.sunecity.lilium.dao.DataBaseType;
import com.sunecity.lilium.dao.config.MongoDbConfig;
import com.sunecity.lilium.dao.javers.DateAdapter;
import com.sunecity.lilium.dao.javers.DateTimeAdapter;
import com.sunecity.lilium.dao.javers.MoneyAdapter;
import com.sunecity.lilium.dao.javers.TimeAdapter;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.repository.mongo.MongoRepository;
import org.javers.repository.sql.ConnectionProvider;
import org.javers.repository.sql.DialectName;
import org.javers.repository.sql.JaversSqlRepository;
import org.javers.repository.sql.SqlRepositoryBuilder;

import javax.enterprise.context.Dependent;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by shahram on 7/11/16.
 */
@Dependent
public class JavaVersions implements Serializable {

    public Javers getSqlJavers(DataBase dataBase) throws SQLException {
        Connection connection = DriverManager.getConnection(dataBase.getUrl(), dataBase.getUsername(), dataBase.getPassword());
        ConnectionProvider connectionProvider = () -> connection;
        JaversSqlRepository sqlRepository = SqlRepositoryBuilder.sqlRepository()
                .withConnectionProvider(connectionProvider).withDialect(getDialect(dataBase.getDataBaseType())).build();
        return JaversBuilder.javers().registerValueTypeAdapter(new DateAdapter()).registerValueTypeAdapter(new DateTimeAdapter()).
                registerValueTypeAdapter(new MoneyAdapter()).registerValueTypeAdapter(new TimeAdapter()).
                registerJaversRepository(sqlRepository).build();
    }

    public Javers getSqlJavers(Connection connection, DataBaseType dataBaseType) {
        ConnectionProvider connectionProvider = () -> connection;
        JaversSqlRepository sqlRepository = SqlRepositoryBuilder.sqlRepository()
                .withConnectionProvider(connectionProvider).withDialect(getDialect(dataBaseType)).build();
        return JaversBuilder.javers().registerValueTypeAdapter(new DateAdapter()).registerValueTypeAdapter(new DateTimeAdapter()).
                registerValueTypeAdapter(new MoneyAdapter()).registerValueTypeAdapter(new TimeAdapter()).
                registerJaversRepository(sqlRepository).build();
    }

    public Javers getSqlJavers(ConnectionProvider connectionProvider, DataBaseType dataBaseType) {
        JaversSqlRepository sqlRepository = SqlRepositoryBuilder.sqlRepository()
                .withConnectionProvider(connectionProvider).withDialect(getDialect(dataBaseType)).build();
        return JaversBuilder.javers().registerValueTypeAdapter(new DateAdapter()).registerValueTypeAdapter(new DateTimeAdapter()).
                registerValueTypeAdapter(new MoneyAdapter()).registerValueTypeAdapter(new TimeAdapter()).
                registerJaversRepository(sqlRepository).build();
    }

    public Javers getNoSqlJavers(MongoDbConfig config) {
        MongoCredential credential = MongoCredential.createCredential(config.username(), config.database(), config.password().toCharArray());
        MongoClient mongoClient = new MongoClient(new ServerAddress(config.host(), config.port()), credential, MongoClientOptions.builder().build());
        MongoDatabase mongoDb = mongoClient.getDatabase(config.database());
        MongoRepository mongoRepo = new MongoRepository(mongoDb);
        return JaversBuilder.javers().registerValueTypeAdapter(new DateAdapter()).registerValueTypeAdapter(new DateTimeAdapter()).
                registerValueTypeAdapter(new MoneyAdapter()).registerValueTypeAdapter(new TimeAdapter()).
                registerJaversRepository(mongoRepo).build();
    }

    private DialectName getDialect(DataBaseType dataBaseType) {
        switch (dataBaseType) {
            case H2:
                return DialectName.H2;
            case MY_SQL:
                return DialectName.MYSQL;
            case POSTGRE_SQL:
                return DialectName.POSTGRES;
            case SQL_SERVER:
                return DialectName.MSSQL;
            case ORACLE:
                return DialectName.ORACLE;
            default:
                return null;
        }
    }

}
