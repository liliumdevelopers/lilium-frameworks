package com.sunecity.lilium.dao.jpa;

import com.sunecity.lilium.core.time.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Jpa converter for Date.
 *
 * @author Shahram Goodarzi
 */
@Converter(autoApply = true)
public class DateConverter implements AttributeConverter<Date, java.sql.Date> {

    /**
     * Converts Date to database date.
     *
     * @param date Date
     * @return database date
     */
    @Override
    public java.sql.Date convertToDatabaseColumn(Date date) {
        if (date == null) {
            return null;
        }
        return java.sql.Date.valueOf(date.toLocalDate());
    }

    /**
     * Converts date to Date.
     *
     * @param date database date
     * @return Date
     */
    @Override
    public Date convertToEntityAttribute(java.sql.Date date) {
        if (date == null) {
            return null;
        }
        return Date.from(date.toLocalDate());
    }

}
