package com.sunecity.lilium.dao.javers;

import com.sunecity.lilium.core.time.DateTime;
import org.javers.core.json.BasicStringTypeAdapter;

import java.time.format.DateTimeFormatter;

/**
 * Created by shahram on 7/9/16.
 */
public class DateTimeAdapter extends BasicStringTypeAdapter {

    @Override
    public String serialize(Object o) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        DateTime dateTime = (DateTime) o;
        return dateTime.format(formatter);
    }

    @Override
    public Object deserialize(String s) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        return DateTime.parse(s, formatter);
    }

    @Override
    public Class getValueType() {
        return DateTime.class;
    }

}
