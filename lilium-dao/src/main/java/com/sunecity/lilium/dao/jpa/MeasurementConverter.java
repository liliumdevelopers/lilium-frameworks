package com.sunecity.lilium.dao.jpa;

import tec.uom.se.spi.Measurement;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.math.BigDecimal;

/**
 * Created by shahram on 4/11/17.
 */
@Converter(autoApply = true)
public class MeasurementConverter implements AttributeConverter<Measurement<?>, BigDecimal> {

    @Override
    public BigDecimal convertToDatabaseColumn(Measurement<?> measurement) {
//        measurement.
        return null;
    }

    @Override
    public Measurement<?> convertToEntityAttribute(BigDecimal bigDecimal) {
        return null;
    }

}
