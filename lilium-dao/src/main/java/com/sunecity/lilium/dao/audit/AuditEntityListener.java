package com.sunecity.lilium.dao.audit;

import com.sunecity.lilium.core.model.AuditLog;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;

import javax.ejb.EJBContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.security.Principal;

/**
 * Jpa EntityListener for Audit.
 *
 * @author Shahram Goodarzi
 */
public class AuditEntityListener {

    /**
     * PrePersist method for adding Principal and Timestamp to Entity
     *
     * @param entity Object to be modified before persist
     */
    @PrePersist
    void prePersist(Object entity) {
        if (entity instanceof AuditLog) {
            AuditLog auditLog = (AuditLog) entity;
            auditLog.setCreateUser(getUsername());
        }
    }

    /**
     * PreUpdate method for adding Principal and Timestamp to Entity
     *
     * @param entity Object to be modified before update
     */
    @PreUpdate
    void preUpdate(Object entity) {
        if (entity instanceof AuditLog) {
            AuditLog auditLog = (AuditLog) entity;
            auditLog.setUpdateUser(getUsername());
        }
    }

    private String getUsername() {
        try {
            Context context = new InitialContext();
            EJBContext ejbContext = (EJBContext) context.lookup("java:comp/EJBContext");
            Principal principal = ejbContext.getCallerPrincipal();
            if (principal instanceof KeycloakPrincipal<?>) {
                KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
                return keycloakPrincipal.getKeycloakSecurityContext().getToken().getPreferredUsername();
            } else {
                return principal.getName();
            }
        } catch (Exception e) {
            return null;
        }
    }

}
