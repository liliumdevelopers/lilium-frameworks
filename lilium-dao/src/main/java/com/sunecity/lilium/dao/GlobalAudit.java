package com.sunecity.lilium.dao;

import com.sunecity.lilium.core.model.BaseEntitySimple;
import com.sunecity.lilium.data.criteria.AuditMetaData;
import org.javers.core.Javers;
import org.javers.core.diff.Change;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.repository.jql.JqlQuery;
import org.javers.repository.jql.QueryBuilder;
import org.javers.shadow.Shadow;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.security.Principal;
import java.util.List;
import java.util.Map;

/**
 * Created by shahram on 6/7/16.
 */
public abstract class GlobalAudit {

    public abstract Javers getJavers();

    public void commit(BaseEntitySimple<?> entity) throws Exception {
        getJavers().commit(getUsername(), entity);
    }

    public void commitDelete(Class<BaseEntitySimple<?>> entityClass, Serializable id) throws Exception {
        BaseEntitySimple entity = entityClass.getConstructor().newInstance();
        entity.setId(id);
        getJavers().commitShallowDelete(getUsername(), entity);
    }

    public List<CdoSnapshot> getSnapshots(AuditMetaData metaData) throws Exception {
        return getJavers().findSnapshots(getQuery(metaData));
    }

    public <T> List<Shadow<T>> getShadows(AuditMetaData metaData) throws Exception {
        return getJavers().findShadows(getQuery(metaData));
    }

    public List<Change> getChanges(AuditMetaData metaData) throws Exception {
        return getJavers().findChanges(getQuery(metaData));
    }

    private JqlQuery getQuery(AuditMetaData metaData) throws Exception {
        QueryBuilder queryBuilder;
        if (metaData.getId() != null) {
            queryBuilder = QueryBuilder.byInstanceId(metaData.getId(), metaData.getClas());
        } else {
            queryBuilder = QueryBuilder.byClass(metaData.getClas());
        }
        if (metaData.getAuthor() != null) {
            queryBuilder = queryBuilder.byAuthor(metaData.getAuthor());
        }
        if (metaData.getFrom() != null) {
            queryBuilder = queryBuilder.from(metaData.getFrom().toLocalDateTime());
        }
        if (metaData.getTo() != null) {
            queryBuilder = queryBuilder.from(metaData.getTo().toLocalDateTime());
        }
        if (metaData.getFirst() != null) {
            queryBuilder = queryBuilder.skip(metaData.getFirst());
        }
        if (metaData.getMax() != null) {
            queryBuilder = queryBuilder.limit(metaData.getMax());
        }
        if (metaData.getCommitProperties() != null) {
            for (Map.Entry<String, String> property : metaData.getCommitProperties().entrySet()) {
                queryBuilder = queryBuilder.withCommitProperty(property.getKey(), property.getValue());
            }
        }
        if (metaData.getChangeProperties() != null) {
            for (String property : metaData.getChangeProperties()) {
                queryBuilder = queryBuilder.withChangedProperty(property);
            }
        }
        return queryBuilder.build();
    }

    private String getUsername() {
        String username = null;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
            Principal principal = facesContext.getExternalContext().getUserPrincipal();
            if (principal instanceof KeycloakPrincipal) {
                KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
                username = keycloakPrincipal.getKeycloakSecurityContext().getIdToken().getPreferredUsername();
            }
        }
        return username;
    }

}
