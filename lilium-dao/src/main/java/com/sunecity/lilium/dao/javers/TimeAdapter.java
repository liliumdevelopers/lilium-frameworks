package com.sunecity.lilium.dao.javers;

import com.sunecity.lilium.core.time.Time;
import org.javers.core.json.BasicStringTypeAdapter;

import java.time.format.DateTimeFormatter;

/**
 * Created by shahram on 7/9/16.
 */
public class TimeAdapter extends BasicStringTypeAdapter {

    @Override
    public String serialize(Object o) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        Time time = (Time) o;
        return time.format(formatter);
    }

    @Override
    public Object deserialize(String s) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        return Time.parse(s, formatter);
    }

    @Override
    public Class getValueType() {
        return Time.class;
    }

}
