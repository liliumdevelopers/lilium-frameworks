package com.sunecity.lilium.dao.flush;

import com.sunecity.lilium.data.Flush;

import javax.annotation.Priority;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * Created by shahram on 2/6/17.
 */
@Interceptor
@Flush
@Priority(Interceptor.Priority.APPLICATION + 400)
public class FlushInterceptor implements Serializable {

    @AroundInvoke
    public Object around(InvocationContext invocationContext) throws Exception {
        Object o = invocationContext.proceed();
        Object target = invocationContext.getTarget();
        Field field = target.getClass().getDeclaredField("generalDao");
        field.setAccessible(true);
        Flushable flushable = (Flushable) field.get(target);
        flushable.flush();
        return o;
    }

}
