package com.sunecity.lilium.dao;

import com.sunecity.lilium.dao.config.DatabaseConfig;
import org.aeonbits.owner.ConfigFactory;
import org.flywaydb.core.Flyway;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * Jdbc helper for sql statements.
 *
 * @author Shahram Goodarzi
 */
public class DataBase {

    private DataBaseType dataBaseType;
    private String url;
    private String username;
    private String password;
    private String dbName;
    private String host;
    private int port;
    private String dumpExecutable;
    private String dumpPath;

    public DataBase(DataBaseType dataBaseType) {
        this.dataBaseType = dataBaseType;
        DatabaseConfig databaseConfig = ConfigFactory.create(DatabaseConfig.class, System.getProperties(), System.getenv());
        this.url = databaseConfig.url();
        this.username = databaseConfig.username();
        this.password = databaseConfig.password();
        this.dbName = databaseConfig.dbName();
        this.host = databaseConfig.host();
        this.port = databaseConfig.port();
        this.dumpExecutable = databaseConfig.dumpExecutable();
        this.dumpPath = databaseConfig.dumpPath();
    }

    public DataBase(DataBaseType dataBaseType, String name) throws IOException {
        this.dataBaseType = dataBaseType;
        Properties properties = new Properties();
        properties.load(getClass().getResourceAsStream("/META-INF/" + name + ".properties"));
        DatabaseConfig databaseConfig = ConfigFactory.create(DatabaseConfig.class, properties, System.getProperties(), System.getenv());
        this.url = databaseConfig.url();
        this.username = databaseConfig.username();
        this.password = databaseConfig.password();
        this.dbName = databaseConfig.dbName();
        this.host = databaseConfig.host();
        this.port = databaseConfig.port();
        this.dumpExecutable = databaseConfig.dumpExecutable();
        this.dumpPath = databaseConfig.dumpPath();
    }

    public DataBase(DataBaseType dataBaseType, String url, String username, String password) {
        this.dataBaseType = dataBaseType;
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public DataBase(DataBaseType dataBaseType, String host, int port, String dbName, String username, String password) {
        this.dataBaseType = dataBaseType;
        this.host = host;
        this.port = port;
        this.dbName = dbName;
        this.username = username;
        this.password = password;
    }

    public DataBaseType getDataBaseType() {
        return dataBaseType;
    }

    public void setDataBaseType(DataBaseType dataBaseType) {
        this.dataBaseType = dataBaseType;
    }

    public String getUrl() {
        if (url == null && host != null && dbName != null) {
            url = dataBaseType.getUrl(this);
        }
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDumpExecutable() {
        return dumpExecutable;
    }

    public void setDumpExecutable(String dumpExecutable) {
        this.dumpExecutable = dumpExecutable;
    }

    public String getDumpPath() {
        return dumpPath;
    }

    public void setDumpPath(String dumpPath) {
        this.dumpPath = dumpPath;
    }

    public String getDriver() {
        return dataBaseType.getDriver();
    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName(getDriver());
        return DriverManager.getConnection(getUrl(), username, password);
    }

    public Statement getStatement() throws SQLException, ClassNotFoundException {
        return getConnection().createStatement();
    }

    public PreparedStatement getPreparedStatement(String sql) throws SQLException, ClassNotFoundException {
        return getConnection().prepareStatement(sql);
    }

    public void runScript(InputStream inputStream) throws SQLException, ClassNotFoundException, IOException {
        try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8")) {
            new SqlScriptRunner(getConnection()).runScript(inputStreamReader);
        }
    }

    public void backup(String backup) {
        dataBaseType.backup(this, backup);
    }

    public void migrate() {
        Flyway flyway = Flyway.configure().dataSource(getUrl(), getUsername(), getPassword()).load();
        flyway.migrate();
    }

}
