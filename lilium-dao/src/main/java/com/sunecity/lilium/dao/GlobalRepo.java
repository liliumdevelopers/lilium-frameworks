package com.sunecity.lilium.dao;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.sunecity.lilium.dao.config.MongoDbConfig;
import com.sunecity.lilium.data.repository.BaseNode;
import org.apache.jackrabbit.oak.Oak;
import org.apache.jackrabbit.oak.jcr.Jcr;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.Workspace;
import java.io.IOException;
import java.util.Collections;

/**
 * @author Shahram Goodarzi
 */
public abstract class GlobalRepo {

    private Repository repo;

    public GlobalRepo(MongoDbConfig config) throws IOException {
        MongoCredential credential = MongoCredential.createCredential(config.username(), config.database(), config.password().toCharArray());
        MongoClient mongoClient = new MongoClient(new ServerAddress(config.host(), config.port()), credential, MongoClientOptions.builder().build());
        MongoDatabase db = mongoClient.getDatabase("test2");
//        DocumentNodeStore ns = new DocumentMK.Builder().setMongoDB(db).getNodeStore();
//        repo = new Jcr(new Oak()).createRepository();
    }

    public void createFolder(String name) throws RepositoryException {
        Session session = repo.login();
        Node root = session.getRootNode();
        Node hello = root.getNode(name);
//        session.
    }

    public void createFile() {

    }

//    public void rename(Path oldName, String newName) throws IOException {
//    }
//
//    public void copy(Path sourcePath, final Path targetPath) throws IOException {
//
//    }
//
//    public void move(Path sourcePath, final Path targetPath) throws IOException {
//
//    }
//
//    public void delete(Path path) throws IOException {
//
//    }
//
//    public void update() {
//
//    }
//
//    public boolean exists(Path path) {
//        return Files.exists(path);
//    }
//
//    public void get() {
//
//    }

    public void setAttribute(BaseNode node, String attribute, String value) throws IOException, RepositoryException {
        getNode(node).setProperty(attribute,value);
    }

    public Object getAttribute(BaseNode node, String attribute) throws IOException, RepositoryException {
        return getNode(node).getProperty(attribute);
    }

    public void deleteAttribute(BaseNode node, String attribute) throws IOException {
//        getNode(node).set
    }

    void node() throws RepositoryException {
        Session session = repo.login(new SimpleCredentials("admin", "admin".toCharArray()));

        Workspace workspace = session.getWorkspace();
//        workspace.
        Node root = session.getRootNode();
        if (root.hasNode("hello")) {
            Node hello = root.getNode("hello");
            long count = hello.getProperty("count").getLong();
            hello.setProperty("count", count + 1);
            System.out.println("found the hello node, count = " + count);
        } else {
            System.out.println("creating the hello node");
            root.addNode("hello").setProperty("count", 1);
        }
        session.save();
//        JcrUtils.readFile()
//        Binary binary = session.getValueFactory().createBinary(file.getInputStream());
//        Node node =root.addNode("ll");
//        node.setProperty("jcr:data",binary);

//session.move();

        session.logout();
//        fs.close();
//        ns.dispose();
    }

    void rename(Node node, String newName) throws RepositoryException {
        node.getSession().move(node.getPath(), node.getParent().getPath() + "/" + newName);
        // Don't forget - not necessarily here at this place:
        // node.getSession().save();
    }

    private Node getNode(BaseNode node){
        return null;
    }

}
