package com.sunecity.lilium.validation.test;

import com.sunecity.lilium.core.time.Date;
import com.sunecity.lilium.core.time.PersianMonth;
import org.junit.Assert;
import org.junit.Test;

/**
 * <b>Title:</b> PastTest
 * TestCase for PastCheck
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public class PastTest {

    @Test
    public void pastDate() {
        Date date = Date.of(1394, PersianMonth.MORDAD, 30);
        final Date now = Date.of(1394, PersianMonth.MORDAD, 30);
        date = date.minusDays(1);
        Assert.assertTrue(date.isBefore(now));
    }

}
