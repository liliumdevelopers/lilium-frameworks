package com.sunecity.lilium.validation.test;

import org.javamoney.moneta.Money;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.Locale;
import java.util.Set;

/**
 * Created by shahram on 1/1/17.
 */
public class ValidationTest {

    static {
        Locale.setDefault(new Locale("fa"));
    }

    @Test
    public void validationTest() {
        Model model = new Model();
        model.setUrl("http//www.google.com");
        model.setAge(25.222);
        model.setPattern("55554");
        model.setBarcode("ggg<");
        model.setMoney(Money.of(100, "USS"));
        model.setName("Shahram");
        Set<ConstraintViolation<Model>> constraintViolations = Validation.buildDefaultValidatorFactory().getValidator().validate(model);
        for (ConstraintViolation<Model> violation : constraintViolations) {
            System.out.println(violation.getMessage());
        }
    }

}
