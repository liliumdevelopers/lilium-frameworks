package com.sunecity.lilium.validation.test;

import com.sunecity.lilium.core.time.Date;
import com.sunecity.lilium.core.time.PersianMonth;
import org.junit.Assert;
import org.junit.Test;

/**
 * <b>Title:</b> FutureTest
 * TestCase for FutureCheck
 *
 * @author Shahram Goodarzi
 * @version 1.0.0
 */
public class FutureTest {

    @Test
    public void futureDate() {
        Date date = Date.of(1394, PersianMonth.MORDAD, 30);
        final Date now = Date.of(1394, PersianMonth.MORDAD, 30);
        date = date.plusDays(1);
        Assert.assertTrue(date.isAfter(now));
    }

}
