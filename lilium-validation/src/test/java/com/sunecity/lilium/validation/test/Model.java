package com.sunecity.lilium.validation.test;

import com.sunecity.lilium.validation.ExactLength;
import org.hibernate.validator.constraints.Currency;
import org.hibernate.validator.constraints.EAN;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Mod10Check;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.money.MonetaryAmount;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;

/**
 * Created by shahram on 1/1/17.
 */
public class Model {

    @URL
    private String url;

    @DecimalMax(value = "20.55", inclusive = true)
    @Digits(integer = 10, fraction = 1)
    private double age;

    @Pattern(regexp = "$(\\d\\w)^")
    private String pattern;

    @EAN(type = EAN.Type.EAN8)
    @Mod10Check
    private String barcode;

    @Currency({"IRR", "USD"})
    private MonetaryAmount money;

    @Length(max = 2)
    @ExactLength(2)
    @SafeHtml
    private String name;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public MonetaryAmount getMoney() {
        return money;
    }

    public void setMoney(MonetaryAmount money) {
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
