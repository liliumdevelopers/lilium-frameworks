package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.NationalId;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for NationalId Annotation
 * Checks if National Id is valid.
 *
 * @author Shahram Goodarzi
 */
public class NationalIdCheck implements ConstraintValidator<NationalId, String> {

    @Override
    public void initialize(NationalId id) {
    }

    /**
     * @param value   National Id
     * @param context ConstraintValidatorContext
     * @return true if National Id is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.trim().length() == 0 || Validator.isNationalId(value);
    }

}
