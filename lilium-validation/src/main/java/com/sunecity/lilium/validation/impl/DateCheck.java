package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for Date Annotation
 * Checks if Date is valid.
 *
 * @author Shahram Goodarzi
 */
public class DateCheck implements ConstraintValidator<Date, String> {

    @Override
    public void initialize(Date calendar) {
    }

    /**
     * @param value   Date
     * @param context ConstraintValidatorContext
     * @return true if Date is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.trim().length() == 0 || Validator.isDate(value);
    }

}
