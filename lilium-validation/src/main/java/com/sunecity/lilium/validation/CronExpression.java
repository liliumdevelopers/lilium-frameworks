package com.sunecity.lilium.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Validation Annotation for Cron Expression
 *
 * @author Shahram Goodarzi
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
public @interface CronExpression {

    String message() default "{com.sunecity.lilium.validation.CronExpression.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
