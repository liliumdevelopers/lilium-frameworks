package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.time.Date;
import com.sunecity.lilium.core.time.DateTime;
import com.sunecity.lilium.validation.Future;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for Future Annotation
 * Checks if date is in future.
 *
 * @author Shahram Goodarzi
 */
public class FutureCheck implements ConstraintValidator<Future, Object> {

    private boolean inclusive;

    @Override
    public void initialize(Future futureDate) {
        inclusive = futureDate.inclusive();
    }

    /**
     * @param value   Date to be tested
     * @param context ConstraintValidatorContext
     * @return true if date is in the future
     */
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        if (value instanceof Date) {
            final Date date = (Date) value;
            if (inclusive) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{com.sunecity.lilium.validation.Future.message}").addConstraintViolation();
                date.plusDays(1);
            }
            return date.isAfter(Date.now());
        } else if (value instanceof DateTime) {
            final DateTime dateTime = (DateTime) value;
            return dateTime.isAfter(DateTime.now());
        }
        return false;
    }

}
