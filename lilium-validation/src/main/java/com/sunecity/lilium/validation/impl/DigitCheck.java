package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.util.Numbers;
import com.sunecity.lilium.validation.Digit;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * <b>Title:</b> DigitCheck
 * Implementation for Digit Annotation
 * Checks if Digit is valid.
 *
 * @author Shahram Goodarzi
 * @version 1.1.2
 */
public class DigitCheck implements ConstraintValidator<Digit, Object> {

    private int minIntegerLen;
    private int minDecimalLen;
    private int maxIntegerLen;
    private int maxDecimalLen;
    private double min;
    private double max;

    @Override
    public void initialize(Digit digit) {
        maxIntegerLen = digit.maxIntegerLen();
        maxDecimalLen = digit.maxDecimalLen();
        minIntegerLen = digit.minIntegerLen();
        minDecimalLen = digit.minDecimalLen();
        min = digit.min();
        max = digit.max();
    }

    /**
     * @param value   Digit
     * @param context ConstraintValidatorContext
     * @return true if Digit is valid
     */
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value != null && value instanceof String && value.toString().trim().length() == 0) {
            return true;
        }
        if (value == null) {
            return true;
        }
        String str = Numbers.getTextEnglish(value.toString());
        if (str.endsWith("\\.0")) {
            str = str.substring(0, str.length() - 2);
        }
        if (str.matches("([-]?[0-9]{" + minIntegerLen + "," + maxIntegerLen
                + "})|([-]?[0-9]{" + minIntegerLen + "," + maxIntegerLen
                + "}\\.[0-9]{" + minDecimalLen + "," + maxDecimalLen + "})")) {
            if (value instanceof Number) {
                final double number = ((Number) value).doubleValue();
                if (number >= min && number <= max) {
                    return true;
                }
            } else if (value instanceof String && Double.parseDouble(str) >= min && Double.parseDouble(str) <= max) {
                return true;
            }
        }
        return false;
    }

}
