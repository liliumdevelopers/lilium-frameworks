package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.Color;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for Color Annotation
 * Checks if Color is valid.
 *
 * @author Shahram Goodarzi
 */
public class ColorCheck implements ConstraintValidator<Color, String> {

    @Override
    public void initialize(Color color) {
    }

    /**
     * @param value   color
     * @param context ConstraintValidatorContext
     * @return true if Color is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.trim().length() == 0 || Validator.isHexadecimal(value);
    }

}
