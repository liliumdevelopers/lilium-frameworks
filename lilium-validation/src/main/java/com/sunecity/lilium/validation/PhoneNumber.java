package com.sunecity.lilium.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Size;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Validation Annotation for Telephone or Cellphone
 *
 * @author Shahram Goodarzi
 */
@Size(max = 15)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
public @interface PhoneNumber {

    PhoneType value() default PhoneType.BOTH;

    String message() default "{com.sunecity.lilium.validation.PhoneNumber.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    enum PhoneType {
        CEll, PHONE, BOTH
    }

}
