package com.sunecity.lilium.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Size;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Validation Annotation for Postal Code
 *
 * @author Shahram Goodarzi
 */
@Size(min = 10, max = 10)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
public @interface PostalCode {

    String message() default "{com.sunecity.lilium.validation.PostalCode.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
