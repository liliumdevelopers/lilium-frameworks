package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.Ip;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for Ip Annotation
 * Checks if Ip is valid.
 *
 * @author Shahram Goodarzi
 */
public class IpCheck implements ConstraintValidator<Ip, String> {

    @Override
    public void initialize(Ip ip) {
    }

    /**
     * @param value   Ip
     * @param context ConstraintValidatorContext
     * @return true if Ip is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.trim().length() == 0 || Validator.isIp(value);
    }

}
