package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.Username;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for Username Annotation
 * Checks if Username is valid.
 *
 * @author Shahram Goodarzi
 */
public class UsernameCheck implements ConstraintValidator<Username, String> {

    @Override
    public void initialize(Username username) {
    }

    /**
     * @param value   Username
     * @param context ConstraintValidatorContext
     * @return true if Username is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !(value == null || value.trim().length() == 0) && Validator.isUsername(value);
    }

}
