package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.CompanyId;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for CompanyId Annotation
 * Checks if Company Id is valid.
 *
 * @author Shahram Goodarzi
 */
public class CompanyIdCheck implements ConstraintValidator<CompanyId, String> {

    @Override
    public void initialize(CompanyId id) {
    }

    /**
     * @param value   Company Id
     * @param context ConstraintValidatorContext
     * @return true if Company Id is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.trim().length() == 0 || Validator.isCompanyId(value);
    }

}
