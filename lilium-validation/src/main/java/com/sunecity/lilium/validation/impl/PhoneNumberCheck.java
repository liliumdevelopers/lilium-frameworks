package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.PhoneNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for PhoneNumber Annotation
 * Checks if Telephone or Cellphone number is valid.
 *
 * @author Shahram Goodarzi
 */
public class PhoneNumberCheck implements ConstraintValidator<PhoneNumber, String> {

    private PhoneNumber.PhoneType type;

    @Override
    public void initialize(PhoneNumber phoneNumber) {
        this.type = phoneNumber.value();
    }

    /**
     * @param value   Telephone or Cellphone number
     * @param context ConstraintValidatorContext
     * @return true if Telephone or Cellphone number is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.trim().length() == 0) {
            return true;
        }
        if (type == PhoneNumber.PhoneType.CEll) {
            return Validator.isCell(value);
        } else if (type == PhoneNumber.PhoneType.PHONE) {
            return Validator.isTel(value);
        } else {
            return Validator.isTelOrCell(value);
        }
    }

}
