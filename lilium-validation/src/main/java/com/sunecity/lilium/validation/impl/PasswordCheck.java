package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.Password;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for Password Annotation
 * Checks if Password is valid.
 *
 * @author Shahram Goodarzi
 */
public class PasswordCheck implements ConstraintValidator<Password, String> {

    @Override
    public void initialize(Password password) {
    }

    /**
     * @param value   Password
     * @param context ConstraintValidatorContext
     * @return true if Password is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !(value == null || value.trim().length() == 0) && Validator.isPassword(value);
    }

}
