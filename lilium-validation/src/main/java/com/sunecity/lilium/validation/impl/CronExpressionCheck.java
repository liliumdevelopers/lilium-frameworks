package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.CronExpression;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for CronExpression Annotation
 * Checks if Cron Expression is valid.
 *
 * @author Shahram Goodarzi
 */
public class CronExpressionCheck implements ConstraintValidator<CronExpression, String> {

    @Override
    public void initialize(CronExpression cronExpression) {
    }

    /**
     * @param value   Cron Expression
     * @param context ConstraintValidatorContext
     * @return true if Cron Expression is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.trim().length() == 0 || Validator.isCronExpression(value);
    }

}
