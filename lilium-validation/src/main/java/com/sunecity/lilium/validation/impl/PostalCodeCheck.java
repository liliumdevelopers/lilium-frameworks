package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.PostalCode;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for PostalCode Annotation
 * Checks if Postal Code is valid.
 *
 * @author Shahram Goodarzi
 */
public class PostalCodeCheck implements ConstraintValidator<PostalCode, String> {

    @Override
    public void initialize(PostalCode postalCode) {
    }

    /**
     * @param value   Postal Code
     * @param context ConstraintValidatorContext
     * @return true if Postal Code is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.trim().length() == 0 || Validator.isPostalCode(value);
    }

}
