/**
 * Set of validation annotations based on validation-api
 *
 * @author Shahram Goodarzi
 */
package com.sunecity.lilium.validation;