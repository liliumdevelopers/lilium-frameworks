package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.validation.Validator;
import com.sunecity.lilium.validation.Time;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for Time Annotation
 * Checks if Time is valid.
 *
 * @author Shahram Goodarzi
 */
public class TimeCheck implements ConstraintValidator<Time, String> {

    @Override
    public void initialize(Time time) {
    }

    /**
     * @param value   Time
     * @param context ConstraintValidatorContext
     * @return true if Time is valid
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.trim().length() == 0 || Validator.isTime(value);
    }

}
