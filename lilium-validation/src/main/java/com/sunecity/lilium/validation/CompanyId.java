package com.sunecity.lilium.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Size;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Validation Annotation for Company Id
 *
 * @author Shahram Goodarzi
 */
@ExactLength(11)
@Size(min = 11, max = 11)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
public @interface CompanyId {

    String message() default "{com.sunecity.lilium.validation.CompanyId.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
