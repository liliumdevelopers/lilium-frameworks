package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.validation.ExactLength;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for ExactLength Annotation
 * Checks if Exact Length is valid.
 *
 * @author Shahram Goodarzi
 */
public class ExactLengthCheck implements ConstraintValidator<ExactLength, Object> {

    private boolean isNumber;
    private int length;

    @Override
    public void initialize(ExactLength exactLength) {
        isNumber = exactLength.isNumber();
        length = exactLength.value();
    }

    /**
     * @param value   Object to be tested
     * @param context ConstraintValidatorContext
     * @return true if Exact Length is valid
     */
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null || value.toString().trim().length() == 0) {
            return true;
        }
        if (isNumber) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate("{com.sunecity.lilium.validation.ExactNumberLength.message}").addConstraintViolation();
        }
        return value.toString().length() == length;
    }

}
