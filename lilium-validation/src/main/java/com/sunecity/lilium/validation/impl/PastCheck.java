package com.sunecity.lilium.validation.impl;

import com.sunecity.lilium.core.time.Date;
import com.sunecity.lilium.core.time.DateTime;
import com.sunecity.lilium.validation.Past;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementation for Past Annotation
 * Checks if date is in past.
 *
 * @author Shahram Goodarzi
 */
public class PastCheck implements ConstraintValidator<Past, Object> {

    private boolean inclusive;

    @Override
    public void initialize(Past pastDate) {
        inclusive = pastDate.inclusive();
    }

    /**
     * @param value   Date to be tested
     * @param context ConstraintValidatorContext
     * @return true if date is in the past
     */
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        if (value instanceof Date) {
            final Date date = (Date) value;
            if (inclusive) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{com.sunecity.lilium.validation.Past.message}").addConstraintViolation();
                date.minusDays(1);
            }
            return date.isBefore(Date.now());
        } else if (value instanceof DateTime) {
            final DateTime dateTime = (DateTime) value;
            return dateTime.isBefore(DateTime.now());
        }
        return false;
    }

}
